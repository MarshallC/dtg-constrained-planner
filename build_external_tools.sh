cd external_source_code
cd druplig
./configure.sh
make
cd ..
cd lingeling
./configure.sh
make
cd ..
cd drat-trim
gcc -O2 -o drat-trim drat-trim.c
cd ..
cd ..
cp external_source_code/lingeling/lingeling src/solvers/lingeling
cp external_source_code/drat-trim/drat-trim src/solvers/drat-trim
mv src/solvers/drat-trim src/solvers/drup-trim
