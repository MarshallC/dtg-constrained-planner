%Start state
:- init(F), not holds(F, 0).
:- holds(F, 0), not init(F).

%Goal
:- goal(F), not holds(F, h).

%Preconditions
:- exec(A, T), pre(A, F), not holds(F, T).

%Effects
:- exec(A, T), add(A, F), not holds(F, T+1).
:- exec(A, T), del(A, F),     holds(F, T+1).

%Successor state axioms
:- holds(F, T), T > 0, not holds(F, T-1), not 1 { exec(A, T-1) : add(A, F) }.
:- holds(F, T), T < h, not holds(F, T+1), not 1 { exec(A, T)   : del(A, F) }.

%Mutex
:- pre(A1, F), del(A2, F), A1 != A2, T = 0..h-1, not { exec(A1, T), exec(A2, T) } 1.

%Generate solutions
%------------------------------------------------------------------------------

{ exec(A, T)  : action(A) : T = 0..h-1 }.
{ holds(F, T) : fluent(F) : T = 0..h }.

#hide holds/2.


 
%Domain
%------------------------------------------------------------------------------
mutex(A1, A2) :- action(A1), action(A2), A1 != A2, add(A1, F), del(A2, F).
mutex(A1, A2) :- action(A1), action(A2), A1 != A2, pre(A1, F), del(A2, F).
mutex(A1, A2) :- mutex_a(A2, A1).
 
