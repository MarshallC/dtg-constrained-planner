%Name: Nathan Robinson
%Date: 2013-04-
%File: blocks_world.asp
 
%This should create the type graph


reachable(F, 0) :- init(F).
applicable(A, 0) :- action(A), pre-cond(A, C), reachable(C, 0).

reachable(C, 0) :- cond-pred(C, F), reachable(F, 0), cond-var(C, X), binding(







action(move(A, B, C, D, E, F)) :- airplane(A), airplanetype(B), direction(C), segment(D), segment(E), direction(F).
pre-cond(move(A, B, C, D, E, F), c0) :- action(move(A, B, C, D, E, F)).
binding(a1_a, A) :- airplane(A).
binding(a1_t, B) :- airplanetype(B).
binding(a1_d1, C) :- direction(C).
binding(a1_s1, D) :- segment(D).
binding(a1_s2, E) :- segment(E).
binding(a1_d2, F) :- direction(F).
cond(c0, and).
cond-child(c0, c1).
cond(c1, pred).
cond-pred(c1, has-type(a1_a, a1_t)).

cond-var(c1, a1_a).


cond-child(c0, c2).
cond(c2, pred).
cond-pred(c2, is-moving(a1_a)).
cond-child(c0, c3).
cond(c3, pred).
cond-pred(c3, not-equal(a1_s1, a1_s2)).
cond-child(c0, c4).
cond(c4, pred).
cond-pred(c4, facing(a1_a, a1_d1)).
cond-child(c0, c5).
cond(c5, pred).
cond-pred(c5, can-move(a1_s1, a1_s2, a1_d1)).
cond-child(c0, c6).
cond(c6, pred).
cond-pred(c6, move-dir(a1_s1, a1_s2, a1_d2)).
cond-child(c0, c7).
cond(c7, pred).
cond-pred(c7, at-segment(a1_a, a1_s1)).
cond-child(c0, c8).
cond(c8, forall).
binding(c8_a1, A) :- airplane(A).
cond-child(c8, c9).
cond(c9, or).
cond-child(c9, c10).
cond(c10, pred).
cond-pred(c10, equal(c8_a1, a1_a)).
cond-child(c9, c11).
cond(c11, pred).
cond-pred(c11, nprec-blocked(a1_s2, c8_a1)).
cond-child(c0, c12).
cond(c12, forall).
binding(c12_s, A) :- segment(A).
cond-child(c12, c13).
cond(c13, or).
cond-child(c13, c14).
cond(c14, pred).
cond-pred(c14, nprec-occupied(c12_s)).
cond-child(c13, c15).
cond(c15, pred).
cond-pred(c15, nprec-is-blocked(c12_s, a1_t, a1_s2, a1_d2)).
cond-child(c13, c16).
cond(c16, pred).
cond-pred(c16, equal(c12_s, a1_s1)).
action(park(A, B, C, D)) :- airplane(A), airplanetype(B), segment(C), direction(D).
pre(park(A, B, C, D), at-segment(A, C)) :- action(park(A, B, C, D)).
pre(park(A, B, C, D), facing(A, D)) :- action(park(A, B, C, D)).
pre(park(A, B, C, D), is-moving(A)) :- action(park(A, B, C, D)).




applicable(A, T) :- action(A), T = 0..h-1, { not reachable(F, T) : fluent(F) : pre(A, F) } 0,
    { not mutex(F1, F2, T) : pre(A, F1) : pre(A, F2) } 0.
 
 
 
 
#hide action/1. #hide pre/2. #hide add/2. #hide del/2. #hide fluent/1.
 



#hide block/1. #hide init/1. #hide goal/1.
