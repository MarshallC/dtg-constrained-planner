%Instance
%------------------------------------------------------------------------------
 
 
 
%Horizon bound
#const h = 3.
 
%Objects
block(1..5).

%Start state
init(on(1, table)). init(on(2, 1)). init(on(3, 2)). init(clear(3)).
init(on(4, table)). init(on(5, 4)). init(clear(5)).

%Goal
goal(on(1, 2)).
goal(on(4, 5)).


goal(forall(passenger(X), served(X))) 


{


(?p - passenger) (served ?p)





%Actions
action(move_to_table(X, Y)) :- block(X), block(Y), X != Y.
pre(move_to_table(X, Y), on(X, Y))     :- action(move_to_table(X, Y)).
pre(move_to_table(X, Y), clear(X))     :- action(move_to_table(X, Y)).
add(move_to_table(X, Y), on(X, table)) :- action(move_to_table(X, Y)).
add(move_to_table(X, Y), clear(Y))     :- action(move_to_table(X, Y)).
del(move_to_table(X, Y), on(X, Y))     :- action(move_to_table(X, Y)).

 
action(move_from_table(X, Y)) :- block(X), block(Y), X != Y.
pre(move_from_table(X, Y), on(X, table)) :- action(move_from_table(X, Y)).
pre(move_from_table(X, Y), clear(Y))     :- action(move_from_table(X, Y)).
pre(move_from_table(X, Y), clear(X))     :- action(move_from_table(X, Y)).
add(move_from_table(X, Y), on(X, Y))     :- action(move_from_table(X, Y)).
del(move_from_table(X, Y), on(X, table)) :- action(move_from_table(X, Y)).
del(move_from_table(X, Y), clear(Y))     :- action(move_from_table(X, Y)).

action(move(X, Y, Z)) :- block(X), block(Y), block(Z), X != Y, X != Z, Y != Z.
pre(move(X, Y, Z), on(X, Y)) :- action(move(X, Y, Z)).
pre(move(X, Y, Z), clear(X)) :- action(move(X, Y, Z)).
pre(move(X, Y, Z), clear(Z)) :- action(move(X, Y, Z)).
add(move(X, Y, Z), on(X, Z)) :- action(move(X, Y, Z)).
add(move(X, Y, Z), clear(Y)) :- action(move(X, Y, Z)).
del(move(X, Y, Z), on(X, Y)) :- action(move(X, Y, Z)).
del(move(X, Y, Z), clear(Z)) :- action(move(X, Y, Z)).
+change(T) :- reachable(F, T), T > 0, not reachable(F, T-1).
 
%Fluents
fluent(on(X, Y))     :- block(X), block(Y), X != Y.
fluent(on(X, table)) :- block(X).
fluent(clear(X))     :- block(X).


#hide block/1. #hide init/1. #hide goal/1.

