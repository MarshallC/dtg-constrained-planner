#!/bin/bash

DOMAIN_STRING=$1
PROBLEM_STRING=$2


DOMAIN_NAME=$(basename "$DOMAIN_STRING")
PROBLEM_NAME=$(basename "$PROBLEM_STRING")

for i in 20 ; do 
    ERRORS__FILE_NAME=`echo "tmp_files/errors.${i}.${DOMAIN_NAME}.${PROBLEM_NAME}.errors"`
    OUTPUT__FILE_NAME=`echo "tmp_files/output.${i}.${DOMAIN_NAME}.${PROBLEM_NAME}.output"`

    python2.7 ./src/planner.py -domain $DOMAIN_STRING   -problem $PROBLEM_STRING -exp_name l  -plangraph_invariants True -encoding precise -horizon $i -solver minisat 2> $ERRORS__FILE_NAME  > $OUTPUT__FILE_NAME


    if grep SAT $OUTPUT__FILE_NAME | grep -q -v "UNSAT"  ; then
	echo "SOLVED THE PROBLEM..."
	break
    fi
done
