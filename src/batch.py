import shlex
import time
import sys
from RunBash import runBash
from subprocess import Popen, PIPE, call

max_processes = 4
filename = sys.argv[1]

def bash(command):
    return "bash -c \"" + command.rstrip() + "\""





runBash("touch DONE_" +filename)

with open("DONE_" + filename, "r") as DONE_batch_file:
    completed_commands = set(DONE_batch_file.readlines())

with open("DONE_" + filename, "a", 1) as DONE_batch_file:
    with open(filename, "r") as batch_file:
        commands = [x for x in batch_file.readlines() if (x not in completed_commands) and (x[0] != '#')]
        if len(commands) == 0:
            print("nothing left to run")
        commands.reverse()
        running_processes = []

        while True:
            time.sleep(0.1)
            for (process, command) in running_processes:
                process.poll()
                if process.returncode != None: # completed
                    print("completed: " + command)
                    DONE_batch_file.write(command)
                    running_processes.remove((process, command))

            while len(commands) != 0 and len(running_processes) < max_processes:
                command = commands.pop()
                print("          starting: " + command)
                args = shlex.split(bash(command))
                process = Popen(args, stdout=PIPE, stderr=PIPE)
                running_processes.append((process, command))

            if len(running_processes) == 0:
                break
print("Complete")
