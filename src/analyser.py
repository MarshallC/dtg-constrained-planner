import sys
import pickle
import networkx as nx
import matplotlib.pyplot as plt
from wrapper_node import Node
import random

def draw_graph(nodes):
    root_node = nodes[0]
    solution_nodes = [node for node in nodes if node.solution_found]
    
    completed_nodes_graph = nx.DiGraph()
    completed_nodes = [node for node in nodes if node.SAT_time != None]
    for node in completed_nodes:
        for child in [node for node in node.children if node.SAT_time != None]:
            completed_nodes_graph.add_edge(node, child)
            
    pos = nx.spectral_layout(completed_nodes_graph)
    # Override pos
    for node_i in range(len(completed_nodes)):
        node = completed_nodes[node_i]
        pos[node][0] = node_i
        pos[node][1] = node.SAT_time_to_node + node.SAT_time
        

    nx.draw(completed_nodes_graph, pos, font_size=16, with_labels=False)
    #nx.draw_networkx_nodes(completed_nodes_graph, pos, font_size=16, with_labels=False)
    nx.draw_networkx_nodes(completed_nodes_graph, pos, nodelist=[root_node], node_color='g', node_size=500, alpha=0.8)
    nx.draw_networkx_nodes(completed_nodes_graph, pos, nodelist=solution_nodes, node_color='y', node_size=500, alpha=0.8)
    nx.draw_networkx_labels(completed_nodes_graph, pos)
    plt.show() # Actually show it

def get_random_path_time(nodes):
    node = nodes[0]
    while len(node.children) != 0:
        node = node.children[random.randint(0,len(node.children)-1)]
    if node.SAT_time == None:
        return None
    return node.SAT_time_to_node + node.SAT_time

def show_graph_from_filename(nodes_filename):
    with open(nodes_filename, "rb") as nodes_file:
        all_nodes = pickle.load(nodes_file)
        print("here")

    #for node in all_nodes:
        #print("==NODE==")
        #print(node.run_name)
        #print(node.SAT_time_to_node)

    draw_graph(all_nodes)



filenames = sys.argv[1:]

if len(filenames) == 0:
    all_lines = sys.stdin.readlines()
    nodes_filename = [line for line in all_lines if "nodes filename: " in line][0].split("nodes filename: ")[-1].rstrip()
    show_graph_from_filename(nodes_filename)
else:
    print("filenames: " + str(filenames))
    for filename in filenames:
        with open(filename, "r") as in_file:
            all_lines = in_file.readlines()
            nodes_filename = [line for line in all_lines if "nodes filename: " in line][0].split("nodes filename: ")[-1].rstrip()
            show_graph_from_filename(nodes_filename)
