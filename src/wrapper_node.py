class Node():
    def __init__(self):
        self.DTG_use_existing = None
        self.DTG_file = None
        self.DTG_ag_to_edge = None # TODO fix this name
        self.horizon = None
        self.run_name = None
        self.run_command = None
        self.SAT_time_to_node = None
        self.children = []
        self.SAT_time = None
        self.total_time = None
        self.solution_found = None
        self.DTG_edges_text = None

    def __str__(self):
        return self.__dump__()

    def __dump__(self):
        return self.run_name.split("@")[-1]
    
    def full_print(self):
        retstr = "=== Node ===\n"
        retstr += "self.DTG_use_existing: " + str(self.DTG_use_existing) + "\n"
        retstr += "self.DTG_file: " + str(self.DTG_file) + "\n"
        retstr += "self.DTG_ag_to_edge: " + str(self.DTG_ag_to_edge) + "\n"
        retstr += "self.horizon: " + str(self.horizon) + "\n"
        retstr += "self.run_name: " + str(self.run_name) + "\n"
        retstr += "self.run_command: " + str(self.run_command) + "\n"
        retstr += "self.children: " + str(self.children) + "\n"
        retstr += "self.SAT_time: " + str(self.SAT_time) + "\n"
        retstr += "self.total_time: " + str(self.total_time) + "\n"
        retstr += "self.solution_found: " + str(self.solution_found) + "\n"
        retstr += "self.DTG_edges_text: " + str(self.DTG_edges_text) + "\n"
        return retstr

    def copy(self):
        new_node = Node()
        
        new_node.DTG_use_existing = self.DTG_use_existing
        new_node.DTG_file = self.DTG_file
        new_node.DTG_ag_to_edge = self.DTG_ag_to_edge
        new_node.horizon = self.horizon
        new_node.run_name = self.run_name
        new_node.run_command = self.run_command
        new_node.SAT_time_to_node = self.SAT_time_to_node
        new_node.children = [child.copy() for child in self.children]
        new_node.SAT_time = self.SAT_time
        new_node.total_time = self.total_time
        new_node.solution_found = self.solution_found
        new_node.DTG_edges_text = self.DTG_edges_text
        return new_node
