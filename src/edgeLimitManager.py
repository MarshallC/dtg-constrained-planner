import os
from customDebugPrint import dprint

# Wrapper for a dictionary
class edgeLimitManager():
    def __init__(self, filename, initial = {}):
        self.filename = filename
        self.edges = initial
        
    def __str__(self):
        ret = self.filename + "\n"
        for edge in self.edges.keys():
            ret += edge + " : " + str(self.edges[edge]) + "\n"
        return ret

    def save(self):
        filename = self.filename
        try:
            with file(filename, 'w') as problematic_edges_file:
                for edge in sorted(self.edges.keys()):
                    problematic_edges_file.write(edge + " " + str(self.edges[edge]) + "\n")
        except IOError:
            dprint("Error writing problmaic edges file: " + filename)
            raise IOError, "Error writing problmaic edges file: " + filename

    def get_edges(self):
        filename = self.filename
        try:
            with file(filename, 'r') as problematic_edges_file:
                lines = [line.rstrip() for line in problematic_edges_file.readlines()]
                edges = {}
                for line in lines:
                    str_crossings = line.split(" ")[-1]
                    if str_crossings == "inf":
                        crossings = float("inf")
                    else:
                        crossings = int(str_crossings)
                        
                    edge_name = ""
                    for edge_name_segment in line.split(" ")[0:-2]:
                        edge_name += edge_name_segment + " "
                    edge_name += line.split(" ")[-2]
                    
                    edges[edge_name] = crossings
                print("Opening edges file to get edges: ")
                for edge in edges.keys():
                    dprint(edge + " : " + str(edges[edge]))
                self.edges = edges
            
        except IOError:
            dprint("Error trying to read problematic edges file: " + filename)
            raise IOError

    def get_1_inc_children(self, problematic_edges):
        dprint("creating children dealing with edges: " + str(len(problematic_edges)))
        children = []
        for edge in problematic_edges:
            dprint("incrementing edge: " + str(edge))
            child = edgeLimitManager(self.filename, self.edges.copy())
            child.edges[edge] += 1
            children.append(child)
        return children

    def get_all_inc_children(self, problematic_edges):
        dprint("creating children dealing with edges: " + str(len(problematic_edges)))
        child = edgeLimitManager(self.filename, self.edges.copy())
        for edge in problematic_edges:
            dprint("incrementing edge: " + str(edge))
            child.edges[edge] += 1
        return [child]

    def get_no_change_children(self, problematic_edges):
        dprint("creating children dealing with edges: " + str(len(problematic_edges)))
        child = edgeLimitManager(self.filename, self.edges.copy())
        return [child]
