import sys
import datetime
from time import gmtime, strftime, time
import socket
from RunBash import runBash
import os
import getopt
import pickle
import networkx as nx
import shutil
from wrapper_node import Node
import copy
import random
import heapq


def get_bool(string):
    if string == "True":
        return True
    elif string == "False":
        return False
    else:
        assert False


# Define constants
flat = "flat"
limited = "limited"

# parsing input parameters
all_options = ['domain', 'problem', 'encoding', 'solver', 'DTG_increment_parallel', 'DTG_increment_horizon_0', 'DTG_simulate_only_fastest_path', "fixed_horizon", "DTG_nonzero_as_infinite", "DTG_sole_random_child"]
required_options = ['domain', 'problem', 'encoding', 'solver']
args_parsed = getopt.getopt(sys.argv[1:],'', [x+'=' for x in all_options])
text_args = {key.split("-")[-1]:val for key,val in args_parsed[0]}

# Checking on input parameters
for required_option in required_options:
    assert required_option in text_args.keys(), "Does not have option: " + required_option
assert text_args["encoding"] in [limited, flat], "Invalid encoding: " + text_args["encoding"]
if "fixed_horizon" in text_args.keys():
    try:
        x = int(text_args["fixed_horizon"])
    except:
        assert False, "fixed horizon needs to be an integer"
        
if "fixed_horizon" in text_args.keys():
    try:
        x = get_bool(text_args["DTG_nonzero_as_infinite"])
    except:
        assert False, "DTG_nonzero_as_infinite needs to be True or False. it is:" + text_args["DTG_nonzero_as_infinite"]

if text_args["encoding"] == limited:
    assert "DTG_increment_horizon_0" in text_args.keys(), "encoding is limted, need to specify DTG_increment_horizon_0"
    assert "DTG_increment_parallel" in text_args.keys(), "encoding is limted, need to specify DTG_increment_parallel"
    assert text_args["DTG_increment_horizon_0"] in ["True", "False"], "DTG_increment_horizon_0 needs to be True or False"
    if text_args["DTG_increment_parallel"] == "True":
        assert 'DTG_simulate_only_fastest_path' in text_args.keys(), "encoding is limted, DTG_increment_parallel is true, need to specify DTG_simulate_only_fastest_path"
        assert text_args["DTG_simulate_only_fastest_path"] in ["True", "False"], "DTG_simulate_only_fastest_path needs to be True or False"

        
class ArgsClass:
    def __init__(self, text_args):
        if "domain" in text_args.keys():
            self.domain = text_args["domain"]
        if "problem" in text_args.keys():
            self.problem = text_args["problem"]
        if "encoding" in text_args.keys():
            self.encoding = text_args["encoding"]
        if "solver" in text_args.keys():
            self.solver = text_args["solver"]
        if "DTG_increment_parallel" in text_args.keys():
            self.DTG_increment_parallel = get_bool(text_args["DTG_increment_parallel"])
        if "DTG_increment_horizon_0" in text_args.keys():
            self.DTG_increment_horizon_0 = get_bool(text_args["DTG_increment_horizon_0"])
        if "DTG_simulate_only_fastest_path" in text_args.keys():
            self.DTG_simulate_only_fastest_path = get_bool(text_args["DTG_simulate_only_fastest_path"])
        if "fixed_horizon" in text_args.keys():
            self.fixed_horizon = int(text_args["fixed_horizon"])
        else:
            self.fixed_horizon = None
        if "DTG_nonzero_as_infinite" in text_args.keys():
            self.DTG_nonzero_as_infinite = get_bool(text_args["DTG_nonzero_as_infinite"])
        else:
            self.DTG_nonzero_as_infinite = False
        if "DTG_sole_random_child" in text_args.keys():
            self.DTG_sole_random_child = get_bool(text_args["DTG_sole_random_child"])
        else:
            self.DTG_sole_random_child = False
        
    def __str__(self):
        retstr = ""
        retstr += "self.domain: " + str(self.domain) + "\n"
        retstr += "self.problem: " + str(self.problem) + "\n"
        retstr += "self.encoding: " + str(self.encoding) + "\n"
        retstr += "self.solver: " + str(self.solver) + "\n"
        retstr += "self.DTG_increment_parallel: " + str(self.DTG_increment_parallel) + "\n"
        retstr += "self.DTG_increment_horizon_0: " + str(self.DTG_increment_horizon_0) + "\n"
        retstr += "self.DTG_simulate_only_fastest_path: " + str(self.DTG_simulate_only_fastest_path) + "\n"
        return retstr
        
args = ArgsClass(text_args)
print(args)
fixed_horizon_set = not (args.fixed_horizon == None)

# Helper functions
def tprint(x):
    [before_point, after_point] = str(time()).split(".")
    str_time = ".".join([before_point,after_point[:5]])
    print("[" + str_time + "] " + str(x))

def get_edges_text(node):
    if node.DTG_edges_text != None:
        return node.DTG_edges_text
    try:
        with open(dir_edges + "/" + node.DTG_file) as edges_file:
            text = edges_file.read()
            node.DTG_edges_text = text
            return text
    except:
        assert False
        
def get_all_nodes_from_root(root_node):
    all_nodes = []
    frontier = [root_node]
    while(len(frontier) != 0):
        node = frontier.pop()
        if node in all_nodes:
            continue
        all_nodes.append(node)
        frontier.extend(node.children)
    return all_nodes

class SATTimeQueue():
        # Keep a sorted queue
    def __init__(self, elements):
        self.queue = [] #heapq.heapify([])
        self.extend(elements)

    def __len__(self):
        return len(self.queue)

    def extend(self, elements):
        for element in elements:
            heapq.heappush(self.queue, (element.SAT_time_to_node, element))

    def pop(self):
        (SAT_time_to_node, element) = heapq.heappop(self.queue)
        return element

    def test(self):
        x = float("inf")
        for i in range(len(self.queue)):
            y = self.queue[i].SAT_time_to_node
            assert x>=y, "Out of order!!" + str([x.SAT_time_to_node for x in self.queue])
            x = y
        
problem_name = args.problem.split("/")[-1] + "-" + args.domain.split("/")[-1] + "-at-" + str(repr(time())) # unique
print("unique problem name: " + problem_name)

# Set up directory for external files
local_dir_cnfs = "tmp_files" + "/" + problem_name + "_cnfs"
local_dir_edges = "tmp_files" + "/" + problem_name + "_edges"
local_dir_logs = "tmp_files" + "/" + problem_name + "_logs"

dir_edges = os.getcwd() + "/" + local_dir_edges
dir_logs = os.getcwd() + "/" + local_dir_logs
dir_cnfs = os.getcwd() + "/" + local_dir_cnfs

# create directories
access_rights = 0o755
os.mkdir(dir_edges, access_rights)
os.mkdir(dir_logs, access_rights)
os.mkdir(dir_cnfs, access_rights)

# filename to save whole node object in
nodes_filename = dir_edges + "/wrapper_nodes_" + problem_name

if args.encoding == limited:
    if fixed_horizon_set:
        horizon_increments = [0]
    else:
        if args.DTG_increment_horizon_0:
            horizon_increments = [0,1]
        else:
            horizon_increments = [1]
                    
this_layer = []
DTG_global_next_number = 1
initial_node = Node()
if fixed_horizon_set:
    initial_node.horizon = args.fixed_horizon
else:
    initial_node.horizon = 1
initial_node.SAT_time_to_node = 0
if args.encoding == limited:
    initial_node.DTG_use_existing = False
    initial_node.DTG_file = "DTG_edges@0" #    initial_node.DTG_file = "DTG_edges-" + problem_name + "@0"
    initial_node.DTG_ag_to_edge = "DTG_ag_to_edge_mapping"    #    initial_node.DTG_ag_to_edge = "DTG_ag_to_edge_mapping-" + problem_name
    initial_node.run_name = initial_node.DTG_file + "h" + str(initial_node.horizon)
else:
    initial_node.run_name = "Flat@" + str(initial_node.horizon) #    initial_node.run_name = "Flat-" + problem_name + "@" + str(initial_node.horizon)
this_layer.append(initial_node)

# Set up dictionary of run instances
h_edges_to_node = {}

if not args.DTG_simulate_only_fastest_path: # Doing full breadth search
    all_paths_fastest_node = None
    
# Start Running
first_run = True
while True:
    #print("starting a new this_layer: " + str([x.run_name for x in this_layer]))
    if args.DTG_simulate_only_fastest_path:
        this_layer_active = list(this_layer)
    else:
        this_layer_active = SATTimeQueue(this_layer)
        
    while len(this_layer_active) != 0:
        working_node = this_layer_active.pop()
        exp_name = str(id(working_node))

        # Case for processing all paths, if found a solution, and the working node SAT_time_to_node is greater than the path to solution, then found solution is actual best
        if not args.DTG_simulate_only_fastest_path: # all paths, full bredth search
            if all_paths_fastest_node != None: # So one exists, check if time to get solution from it is less than time to get to base of working node
                if all_paths_fastest_node.SAT_time + all_paths_fastest_node.SAT_time_to_node < working_node.SAT_time_to_node:
                    # Have found the actual shortest
                    break

        # Print what is being done
        tprint("Starting: " + str(id(working_node)))
        tprint("run_name: " + working_node.run_name)
        
        # Determine if this run should ACTUALLY be ran
        if args.encoding == limited:
            if not first_run:
                h_edges = (working_node.horizon, get_edges_text(working_node))
                if h_edges in h_edges_to_node.keys():
                    # Already run, just get everything from that
                    completed_node = h_edges_to_node[h_edges]
                
                    working_node.run_name = completed_node.run_name
                    working_node.DTG_file = completed_node.DTG_file

                    working_node.children = [child.copy() for child in completed_node.children]
                    '''
                    if len(completed_node.children)==0:
                        working_node.children = []
                    else:
                        working_node.children = [[child.copy() for child in completed_node.children][random.randint(0,len(completed_node.children)-1)]]
                    '''
                    
                    if None in working_node.children:
                        assert False, "children: " + str(working_node.children)
                    working_node.run_command = completed_node.run_command
                    working_node.SAT_time = completed_node.SAT_time
                    working_node.total_time = completed_node.total_time
                    working_node.solution_found = completed_node.solution_found

                    working_node.logFilePath = dir_logs + "/LOG_OF_" + working_node.run_name
                    # Print Result
                    tprint("total_time: " + str(working_node.total_time))
                    tprint("SAT_time: " + str(working_node.SAT_time))
                    tprint("solution_found: " + str(working_node.solution_found))
                    tprint("children_count: " + str(children_count))
                    tprint("children: " + str([id(child) for child in working_node.children]))
                    tprint("horizon: " + str(working_node.horizon))
                    tprint("log file (inhereted): " + working_node.logFilePath)
                    tprint("==========================")

                    # For the simulate all paths case
                    if not args.DTG_simulate_only_fastest_path:
                        if solution_found:
                            if all_paths_fastest_node == None:
                                all_paths_fastest_node = working_node
                            else:
                                if all_paths_fastest_node.SAT_time_to_node + all_paths_fastest_node.SAT_time > working_node.SAT_time_to_node + working_node.SAT_time:
                                    all_paths_fastest_node = working_node

                    # For the simulate all paths case
                    if not args.DTG_simulate_only_fastest_path:
                        this_layer_active.extend(working_node.children)

                    continue
        first_run = False
        
        
        # Setup planner run
        run_command = "python2.7 ./src/planner.py -domain " + args.domain + " -problem " + args.problem + " -exp_name " + exp_name  + " -plangraph_invariants True -encoding " + args.encoding + " -horizon " + str(working_node.horizon) + " -solver " + args.solver + " -tmp_path " + local_dir_cnfs + " -DTG_nonzero_as_infinite " + str(args.DTG_nonzero_as_infinite)
        if args.encoding == limited:
            tprint(working_node.DTG_ag_to_edge)
            run_command += " -DTG_use_existing " + str(working_node.DTG_use_existing) + " -DTG_file " + working_node.DTG_file + " -DTG_ag_to_edge " + working_node.DTG_ag_to_edge + " -DTG_increment_parallel " + str(args.DTG_increment_parallel) + " -DTG_edge_location " + dir_edges + " -DTG_file_next_suffix " + str(DTG_global_next_number) + " -DTG_sole_random_child " + str(args.DTG_sole_random_child)
        else:
            tprint("No DTG_ag_to_edge file relevant")
        tprint("running command: " + run_command)
        (exitcode, out, err, total_time) = runBash(run_command)
        
        if exitcode != 0:
            tprint("error in execution, output and error:")
            tprint(out)
            tprint(err)
            assert False, "Error reading in planner"
            
        tprint("Finished " + working_node.run_name)

        # Save as done:
        if args.encoding == limited:
            edges_text = get_edges_text(working_node)
            assert (working_node.horizon, edges_text) not in h_edges_to_node.keys(), "Ran an instance that has already been run before (inefficient)"
            h_edges_to_node[(working_node.horizon, edges_text)] = working_node

        # Extract values from instance run
        error_extracting_values = False
        try:
            SAT_time =     float([line for line in out.split("\n") if "Solution time" in line][0].split(" ")[2])
            solution_found = len([line for line in out.split("\n") if "Plan, length:" in line]) == 1 # TODO Have a better check
        except:
            error_extracting_values = True
        if error_extracting_values:
            tprint(out)
            tprint(err)
            assert False, "Error reading SAT_time or solution_found"
    
        # For the simulate all paths case
        if not args.DTG_simulate_only_fastest_path:
            if solution_found:
                if all_paths_fastest_node == None:
                    all_paths_fastest_node = working_node
                else:
                    if all_paths_fastest_node.SAT_time_to_node + all_paths_fastest_node.SAT_time > working_node.SAT_time_to_node + SAT_time:
                        all_paths_fastest_node = working_node

        # Extract children count
        if args.encoding == limited:
            if not solution_found:
                children_DTG_files = [x.split("creating_child_with_name:")[-1] for x in out.split("\n") if "creating_child_with_name:" in x]
                children_count = len(children_DTG_files)
                assert children_count, "Error: No children, found but the solution is not found?"
            else:
                children_count = 0
        else:
            if not solution_found:
                children_count = 1
            else:
                children_count = 0
        
        # Create nodes for children
        for child_number in range(children_count):
            if args.encoding == limited:
                for horizon_increment in horizon_increments:
                    
                    # Apply a specific exemption to not have exactly the same child, could get stuck in inifinite loop when only going down shortest SAT path
                    if horizon_increment == 0:
                        dummy_child_node = Node()
                        dummy_child_node.DTG_file = children_DTG_files[child_number]
                        if get_edges_text(dummy_child_node) == get_edges_text(working_node):
                            continue # As same edges and same horizon
                    
                    child = Node()
                    working_node.children.append(child)
                    child.SAT_time_to_node = working_node.SAT_time_to_node + SAT_time
                    child.horizon = working_node.horizon + horizon_increment
                    child.DTG_use_existing = True
                    child.DTG_file = children_DTG_files[child_number]
                    child.DTG_ag_to_edge = working_node.DTG_ag_to_edge
                    child.run_name = child.DTG_file + "h" + str(child.horizon)
                        
            else: # Flat, just one child
                child = Node()
                working_node.children.append(child)
                #child.parents.append(working_node)
                if fixed_horizon_set:
                    child.horizon = working_node.horizon
                else:
                    child.horizon = working_node.horizon + 1
                child.SAT_time_to_node = working_node.SAT_time_to_node + SAT_time
                child.run_name = "@".join(working_node.run_name.split("@")[:-1]) + "@" + str(child.horizon)

        '''
        # Change children
        if len(working_node.children)==0:
            working_node.children = []
        else:
            working_node.children = [working_node.children[random.randint(0,len(working_node.children)-1)]]
        '''
        
        # Save information back to node
        working_node.run_command = run_command
        working_node.SAT_time = SAT_time
        working_node.total_time = total_time
        working_node.solution_found = solution_found

        # Save log
        logFilePath = dir_logs + "/LOG_OF_" + working_node.run_name
        with open(logFilePath, "w+") as log_file:
            log_file.write("log_File_of: " + working_node.run_name + "\n")
            log_file.write("total_planner_time: " + str(working_node.total_time) + "\n")
            log_file.write("SAT_time: " + str(working_node.SAT_time) + "\n")
            log_file.write("children_count: " + str(children_count) + "\n")
            log_file.write("horizon: " + str(working_node.horizon) + "\n")
            log_file.write("output:\n\n")
            log_file.write(out)
            
        # Print Result
        tprint("total_time: " + str(working_node.total_time))
        tprint("SAT_time: " + str(working_node.SAT_time))
        tprint("solution_found: " + str(working_node.solution_found))
        tprint("children_count: " + str(children_count))
        tprint("children: " + str([id(child) for child in working_node.children]))
        tprint("horizon: " + str(working_node.horizon))
        tprint("log file: " + logFilePath)
        tprint("==========================")

        # Increase file number for next time
        if args.encoding == limited:
            DTG_global_next_number += children_count

        # If all paths add to frontier
        if not args.DTG_simulate_only_fastest_path:
            this_layer_active.extend(working_node.children)

    # Finished everything in this_layer, try next_layer IF a solution has not yet been found
    # Find node on this_layer that finished fastest, use this nodes children as next layer
    if args.DTG_simulate_only_fastest_path:
        # find fastest node in layer
        this_layers_fastest_node = Node()
        this_layers_fastest_node.SAT_time = float("inf")
        for this_layer_node in this_layer:
            if this_layer_node.SAT_time < this_layers_fastest_node.SAT_time:
                this_layers_fastest_node = this_layer_node

        # If fastest found a solution, break
        if this_layers_fastest_node.solution_found:
            solution_node = this_layers_fastest_node
            break
        else:
            # use this node to set next layer
            if args.encoding == limited:
                if set([(node.horizon, get_edges_text(node)) for node in this_layer]) == set([(node.horizon, get_edges_text(node)) for node in this_layers_fastest_node.children]):
                    draw_graph(initial_node, this_layer)
                    assert False, "Next layer is same as this, stuck in an infinite loop, layer:" + str([x.run_name for x in this_layer])

            this_layer = this_layers_fastest_node.children
        
    elif not args.DTG_simulate_only_fastest_path:
        solution_node = all_paths_fastest_node
        break
    

tprint("DONE")
tprint("optimal_path_SAT_time_sum: " + str(solution_node.SAT_time + solution_node.SAT_time_to_node))
tprint("optimal path last step run_command: " + solution_node.run_command)
#draw_graph(initial_node, this_layer)
with open(nodes_filename, "wb") as nodes_file:
    pickle.dump(get_all_nodes_from_root(initial_node), nodes_file)
tprint("nodes filename: " + nodes_filename)

# Compress output
command_compress_dir_edges = "tar -cvzf " + local_dir_edges + ".tar.gz " + local_dir_edges
command_compress_dir_logs = "tar -cvzf " + local_dir_logs + ".tar.gz " + local_dir_logs
command_compress_dir_cnfs = "tar -cvzf " + local_dir_cnfs + ".tar.gz " + local_dir_cnfs

'''
(exitcode, out, err, total_time) = runBash(command_compress_dir_edges)
(exitcode, out, err, total_time) = runBash(command_compress_dir_logs)
(exitcode, out, err, total_time) = runBash(command_compress_dir_cnfs)

shutil.rmtree(local_dir_edges)
shutil.rmtree(local_dir_logs)
shutil.rmtree(local_dir_cnfs)
'''
