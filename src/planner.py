""" File:        planner.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2013-
    Desctiption: 

    Liscence:   [Refine this!]
"""
from RunBash import runBash

import sys, os, time, subprocess, errno

from utilities import CodeException, remove, PRE_SUFFIX, GROUND_SUFFIX,\
    solving_error_code, extracting_error_code
from cmd_line import process_args
from problem import ProblemException
from parser import ParsingException, Parser, Grounder
from preprocessors import PreprocessingException, SASPlusPreprocessor,\
    CachetPreprocessor, PlangraphPreprocessor

from edgeLimitManager import edgeLimitManager

from cnf_encodings import EncodingException, EncodingWrapper
from solvers import SolvingException, SolverWrapper

import networkx as nx
#import matplotlib.pyplot as plt
from problem import Predicate
from problem import PredicateCondition
from customDebugPrint import dprint

class PlanningException(CodeException):
    def __init__(self, message, code):
        self.message = message
        self.code = code

def write_error_sln(output_file_name, exp_name, code):
    """ Write the given experiment name and error code to the error file
        
        (str, str, int) -> None
    """
    try:
        with file(output_file_name, 'w') as sln_file:
            print >> sln_file, exp_name
            print >> sln_file, 'NSAT'
            print >> sln_file, code
    except IOError:
        print "Error opening sln file:", out_name


def to_node(v):
    return "(" + str(v[0]) + ", " + str(v[1]) + ", " + str(v[2]) + ")"

def main():
    start_time = time.time()
    
    #Process the possible encodings, and solvers so we can provide useful
    #cmd line advice
    try:
        encoding_wrapper = EncodingWrapper()
        encoding_wrapper.read_encoding_list()
        
        solver_wrapper = SolverWrapper()
        solver_wrapper.read_solver_list()
    
    except (EncodingException, SolvingException) as e:
        print e.message
        sys.exit(1)
    
    #Process the cmd line args
    args = process_args(\
        encoding_wrapper.valid_encodings, encoding_wrapper.default_encoding,
        solver_wrapper.valid_solvers, solver_wrapper.default_solver)
    
    arg_processing_time = time.time()
    
    #Report on system start up
    if not args.quiet:
        print "Encodings registered:    ", len(encoding_wrapper.valid_encodings)
        print "Solvers registered:      ", len(solver_wrapper.valid_solvers)
        print "Cmd line arg processing time:", (arg_processing_time - start_time)
    
    #Ensure that the tmp_dir exists
    try:
        os.makedirs(args.tmp_path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            print "Error: could not create temporary directory:", args.tmp_path
            sys.exit(1)
    
    #Parse the input PDDL
    try:
        parser = Parser(args.domain_file_name, args.problem_file_name, args.quiet)
        if not args.quiet: print "Parsing the PDDL domain"
        parser.parse_domain()
        if not args.quiet: print "Parsing the PDDL problem"
        parser.parse_problem()
        
        if not args.quiet: print "Simplifying the problem representation"
        problem = parser.problem

        # Where to introduce the artificial preconditions and effects
        #print("planner: AA printing a representation of the problem (actions):")
        #for action in problem.actions.iteritems():
        #    print("Action: " + action[1].__dump__())
        #for predicate in problem.predicates.iteritems():
        #    print("Predicate: " + predicate[1].__dump__())
            
        problem.simplify()
        problem.assign_cond_codes()

        end_parsing_time = time.time()
        if not args.quiet: print "Parsing time:", (end_parsing_time - arg_processing_time)

        if not args.quiet: print "Grounding the problem"
        grounder = Grounder(problem, os.path.join(args.tmp_path, args.exp_name + PRE_SUFFIX),
            os.path.join(args.tmp_path, args.exp_name + GROUND_SUFFIX), args.quiet)
        grounder.ground()
        
        end_grounding_time = time.time()
        if not args.quiet: print "Grounding time:", (end_grounding_time - end_parsing_time)
        
        if not args.quiet: print "Simplifying the ground encoding..."
        problem.compute_static_preds()
        problem.link_groundings()
        problem.make_flat_preconditions()
        problem.make_flat_effects()
        problem.get_encode_conds()
        problem.make_cond_and_cond_eff_lists()
        problem.link_conditions_to_actions()
        problem.make_strips_conditions()
        
        problem.prune_useless_strips_actions()

        problem.compute_conflict_mutex()
        
        end_linking_time = time.time()
        if not args.quiet: print "Simplify time:", (end_linking_time - end_grounding_time)
        
        object_invariants = []
        if args.plangraph_invariants:
            if not args.quiet: print "Generating Plangraph invariants..."
            plangraph_preprocessor = PlangraphPreprocessor(problem, args.quiet, args.plangraph_timeless_only)
            object_invariants = plangraph_preprocessor.run()
            if object_invariants == False:
                raise PreprocessingError()

        # Printing
        #print("planner: TEST example strips[0]          : " + str(pre[0]))
        #print("planner: TEST flat_ground_preconditons[0]: " + str(action.flat_ground_preconditions[grounding][0][0]))
        #print("planner: TEST flat_ground to be added    : " + str(condition_generated_precondition[0]))
        end_plangraph_time = time.time()
        
        if args.plangraph_invariants and not args.quiet:
            print "Plangraph invariants time:", (end_plangraph_time - end_linking_time)
        
        if args.sas_invariants:
            if not args.quiet: print "Generating SAS+ invariants..."
            sas_invariants = SASPlusPreprocessor(problem, args.quiet)
            sas_invariants.run()
        
        end_sas_time = time.time()    
        if args.sas_invariants and not args.quiet:
            print "SAS+ invariants time:", (end_sas_time - end_plangraph_time)
        
        if not args.quiet:
            print "Generating base encoding:", args.encoding, "..."

        #Make doing this conditional on actually requiring it for preprocessing
        #Or an encoding
        problem.precisely_split()

        # Instantiate encoding and pass problem
        encoding_wrapper.instantiate_encoding(args.encoding, args.encoding_args,
            problem, args.quiet)
        encoding = encoding_wrapper.encoding
        encoding.horizon = args.horizon
        encoding.metalayer_copy_layer = args.copy_layer
        encoding.cg_cut_above = args.cut_above

        if "limited" in str(encoding): # TODO ammend
            #if args.DTG_use_existing and ()
            # Create and give the edgeLimitManager

            encoding.edgeLimitManager = edgeLimitManager(args.DTG_edge_location + "/" + args.DTG_file) # TODO review if necessary
            
            dprint("set up edgeLimitManager")
            if args.DTG_use_existing: # use exisiting
                encoding.edgeLimitManager.get_edges()
                dprint("Using edge limits previously calculated")
            else:
                dprint("computing relaxed plan for later use")
                plangraph_preprocessor.calculate_relaxed_plan()
                encoding.set_relaxed_plan(plangraph_preprocessor.get_relaxed_plan()) # pass relaxed plan from plangrpah preprocessor to limited

            if not args.DTG_use_existing: # TODO swap
                dprint("computing DTG_edges for later use")
                encoding.calculate_DTG_edges(plangraph_preprocessor.get_state_mutexes(), args.DTG_use_existing, args.DTG_edge_location + "/" + args.DTG_ag_to_edge) # TODO shouldn't pass use existing
            else:
                dprint("loading DTG_edges for later use")
                encoding.load_DTG_edges(args.DTG_use_existing, args.DTG_edge_location + "/" + args.DTG_ag_to_edge)
                
            # if args.DTG_use_existing then file already contais initial ones
            if not args.DTG_use_existing:
                encoding.set_initial_edge_crossing_limits()
                encoding.edgeLimitManager.save() # Save it now, before going foward

            #new_horizon = encoding.get_new_horizon() + args.horizon_offset
            #dprint("Setting new horizon + offset: " + str(new_horizon))
            #encoding.horizon = new_horizon
            #args.horizon = new_horizon
            
        else:
            if args.DTG_use_existing:
                dprint("ERROR: trying to use DTG edge limiting with incompatable encoding")
                raise NotImplementedError
            
        encoding.encode_base()
        
        #Consider moving this stuff after the cachet invariants code
        encoding.add_object_invariants(object_invariants)
        
        end_encoding_base_time = time.time()
        if not args.quiet: print "Encoding generation time:",\
            (end_encoding_base_time - end_sas_time)

        if args.cachet_invariants:
            cachet_invariants = CachetPreprocessor(problem, encoding.clauses,
                encoding.variables, encoding.vars_per_layer, args.tmp_path,
                args.exp_name, args.cachet_unary_only, args.quiet)
            cachet_invariants.run()
            encoding.add_invariants(cachet_invariants.invariants)
        
        end_cachet_time = time.time()
        
        if args.cachet_invariants and not args.quiet:
            print "Cachet invariants time:", (end_cachet_time - end_encoding_base_time)

        
    except (ParsingException, PreprocessingException, ProblemException) as e:
        print e
        if args.soln_file != None:
            write_error_sln(args.soln_file, args.exp_name, e.code)
        sys.exit(1)
    
    if not args.quiet: print "Planning..."
    try:
        #We have something called query strategy in cmd line args
        
        cnf_file_name = os.path.join(args.tmp_path,
            args.exp_name + "_" + str(args.horizon) + ".cnf")

        variable_order_file_name = os.path.join(args.tmp_path,
            args.exp_name + "_" + str(args.horizon) + ".ordering")

        if not args.quiet: print "Writing CNF file..."
        encoding.compute_cnf_codes(args.horizon)

        # Add clauses manually to encode counters
        if "limited" in str(encoding):
            encoding.add_edge_counters(args.DTG_use_existing, args.tmp_path, args)
        else:
            if args.DTG_use_existing:
                dprint("ERROR: trying to use DTG edge limiting with incompatable encoding")
                raise NotImplementedError
        
        encoding.write_cnf(cnf_file_name)

        # Write these additional clauses to encode counters
        if "limited" in str(encoding):
            encoding.write_counters_to_cnf(cnf_file_name)
        else:
            if args.DTG_use_existing:
                dprint("ERROR: trying to use DTG edge limiting with incompatable encoding")
                raise NotImplementedError

        encoding.write_ordering(variable_order_file_name)
        end_writing_cnf_time = time.time()
        if not args.quiet:
            print "Writing time:", (end_writing_cnf_time - end_cachet_time)
            
            print "Clause statistics:"
            for ctype, count in encoding.get_clause_types().iteritems():
                print ctype, ":", count

        if args.debug_cnf:
            if not args.quiet: print "Writing debug CNF..."
            encoding.write_debug_cnf(cnf_file_name + "_dbg", args.horizon)
        end_writing_dbg_cnf_time = time.time()
        if not args.quiet and args.debug_cnf:
            print "Writing time:", (end_writing_dbg_cnf_time - end_writing_cnf_time)
        
        if args.var_file:
            if not args.quiet: print "Writing var file..."
            var_file_name = os.path.join(args.tmp_path,
                args.exp_name + "_" + str(args.horizon) + ".var")
            encoding.write_variable_file(var_file_name)
        
        end_writing_all_cnf_time = time.time()
        if not args.quiet and args.var_file:
            print "Writing time:", (end_writing_all_cnf_time - end_writing_dbg_cnf_time)
        
        try:
            if not args.quiet: print "Solving..."

            solver_wrapper.instantiate_solver(args.solver, cnf_file_name,\
                args.tmp_path, args.exp_name, args.time_out, args.solver_args)
          
            (sln_res, sln_time, true_vars) = solver_wrapper.solver.solve()

            # Decomissioned after minisat_DRUP
            #problematic_literals = None
            #if args.solver == "minisat_DRUP":
            #    problematic_literals = solver_wrapper.solver.get_problematic_literals()
            
            if not args.quiet:
                print "SAT" if sln_res else "UNSAT"
                print "Solution time:", sln_time
            
        except SolvingException as e:
            raise PlanningException(e.message, solving_error_code)
        
        if sln_res:
            encoding.set_true_variables(true_vars)
            encoding.build_plan()

            print(str(encoding.plan))
            if not args.quiet:
                print "Plan, length:" + str(len(encoding.plan))
                for step, s_actions in enumerate(encoding.plan):
                    #mlayer = encoding.plan_metalayer[step]
                    
                    for (action, a_args) in s_actions:
                        a_str = action.name
                        if a_args:
                            a_str += " " + " ".join(a_args)
                        #print str(mlayer) + " - " + str(step) + ": " + a_str
                        print str(step) + ": " + a_str
            
            
            
            if not args.quiet:
                print "Simulating plan for validation."
            
            sim_res, plan_cost = problem.simulate_plan(encoding.plan)
            
            if sim_res:
                print "Plan valid. Cost:", plan_cost
                #with file(args.tmp_path + "/valid_plan_marker", 'w') as x: None # touch file saying plan is valid
            else:
                print "Error: INVALID PLAN!"
                assert False, "Error: INVALID PLAN!"

        else: # Extra option for UNSAT
            if "limited" in str(encoding):
                # Request the encoding module to read the UNSAT core and generate a text list of problmatic DTG edges for use later
                encoding.get_problematic_DTG_edges(args.tmp_path, args.exp_name)
                encoding.handle_problematic_edges(args.DTG_increment_parallel, args.DTG_sole_random_child, args.DTG_file_next_suffix)
            
        end_time = time.time()
        if not args.quiet:
            print "Total time:", end_time - start_time

    except PlanningException as e:
       print "Planning Error: ", e.message
       sys.exit(1)


    # Before closing save the edgeLimit file
    # In new system this shouldn't be touched
    #if "limited" in str(encoding):
    #    encoding.edgeLimitManager.save(args.tmp_path + problem.name + "PROBLEMS.txt")

    # Before exiting, delete cnf files
    print(runBash("rm " + args.tmp_path + "/" + args.exp_name + "_" + str(args.horizon) + ".cnf"))
    print(runBash("rm " + args.tmp_path + "/" + args.exp_name + "_" + str(args.horizon) + ".ordering"))
    print(runBash("rm " + args.tmp_path + "/" + args.exp_name + ".grd"))
    print(runBash("rm " + args.tmp_path + "/" + args.exp_name + "_" + args.solver + "_STDOUT.sln"))
    print(runBash("rm " + args.tmp_path + "/" + args.exp_name + ".pre"))
    print(runBash("rm " + args.tmp_path + "/" + args.exp_name + ".sln"))
    print(runBash("rm " + args.tmp_path + "/" + args.exp_name + "_UNSAT_core.cnf"))
    print(runBash("rm " + args.tmp_path + "/" + args.exp_name + "scratch_area_for_drup_trim.txt"))

    sys.exit(0)

    

if __name__ == '__main__':
    main()

