""" File:        encodings/encoding_base.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2013-
    Desctiption:

    Liscence:   [Refine this!]
"""
import abc, os, itertools, copy, operator

from utilities import CodeException
from importlib import import_module
from problem import Predicate, PredicateCondition

import networkx
from networkx.algorithms.components import strongly_connected_components

import collections
from collections import defaultdict, deque

from customDebugPrint import dprint

encoding_list_file_name = os.path.join(os.path.dirname(__file__), "encoding_list")

class EncodingException(CodeException):
    """ An exception to be raised in the even that something goes wrong with
        the encoding process. """


class Encoding(object):
    __metaclass__ = abc.ABCMeta
        
    def __init__(self, args, problem, quiet):
        """ Set encoding parameters

            (Encoding, { str : obj }, Problem, bool) -> None
        """
        self.args = args
        self.problem = problem
        self.quiet = quiet
        self.code = 1

        self.clauses = []
        self.clause_types = []

        self.invariants = []
        self.object_invariants = []

        #Descriptions of the variables (all vars are horizon independent)
        self.variables = {}
        self.variable_layers = {}

        self.variable_to_cnf_code = {}
        self.cnf_code_to_variable = {}

        self.layer_codes = defaultdict(list)

        # Counting clauses extra variables
        self.counting_clause_to_edge = {}
        self.counting_var_to_edge = {}

    @abc.abstractmethod
    def get_variable_name(self, step, var):
        """ Return a string containing a representation of the supplied variable

            (Encoding, int, int) -> str
        """

    @abc.abstractmethod
    def encode_base(self):
        """ Make the variables and sets of horizon dependent and independent
            clauses, which will be written out when required.

           (Encoding) -> None
        """

    def new_code(self, c_data, c_layers):
        """ Store the given information in the structures for the next code and return it.
            (Encoding, Object, (int, int)) -> int
        """
        self.variables[self.code] = c_data
        self.variable_layers[self.code] = c_layers

        lb, ub = c_layers
        if lb < 0: lb = self.horizon + lb + 1
        if ub < 0: ub = self.horizon + ub + 1
        else: ub = min(ub, self.horizon-1)

        for layer in xrange(lb, ub+1):
            self.layer_codes[layer].append(self.code)

        self.code += 1
        return self.code - 1

    def manual_new_cnf_code(self, UNUSEDX, UNUSEDY):
        self.cnf_code += 1
        return self.cnf_code -1

    def compute_cnf_codes(self, horizon):
        """ Use the layer information about each variable to compute the codes
            to be used in a cnf.
        """
        self.horizon = horizon
        self.cnf_code = 1
        step_vars = defaultdict(set)
        for var in xrange(1, self.code):
            lb, ub = self.variable_layers[var]
            if lb < 0: lb = self.horizon + lb + 1
            if ub < 0: ub = self.horizon + ub + 1
            for step in xrange(lb, ub + 1):
                #step_vars[step].add((step, var))

                sv_pair = (step, var)
                #print("encoding_base: (step, var) sv_pair: " + str(sv_pair) + " -> self.cnf_code: " + str(self.cnf_code))
                self.variable_to_cnf_code[sv_pair] = self.cnf_code
                self.cnf_code_to_variable[self.cnf_code] = sv_pair
                self.cnf_code += 1

        """
        steps = step_vars.keys()
        steps.sort(reverse=False)
        for step in steps:
            for sv_pair in step_vars[step]:
                self.variable_to_cnf_code[sv_pair] = self.cnf_code
                self.cnf_code_to_variable[self.cnf_code] = sv_pair
                self.cnf_code += 1
        """

    def write_variable_file(self, file_name):
        """ Write a file to file_name that describes the variables and clauses
            used in the current encoding.

            (FlatEncoding, str) -> None
        """
        try:
            with file(file_name, 'w') as var_file:
                for cnf_code in xrange(1, self.cnf_code):
                    step, var = self.cnf_code_to_variable[cnf_code]
                    var_file.write("v " + str(cnf_code) + " " +\
                        self.get_variable_name(step, var) + "\n")

                clause_no = 0
                for cid, ((lb, ub), clause) in enumerate(self.clauses):
                    if lb < 0: lb = self.horizon + lb + 1
                    if ub < 0: ub = self.horizon + ub + 1
                    else: ub = min(ub, self.horizon-1)
                    for step in xrange(lb, ub + 1):
                        var_file.write("c " + str(clause_no) + " " +\
                            self.clause_types[cid] + "\n")
                        clause_no += 1

        except IOError:
            raise EncodingException("Error: cannot open var file: " + file_name,
                encoding_error_code)


    def write_ordering(self, file_name):
        try:
            #clauses = set()
            with file(file_name, 'w') as ordering_file:
                for layer in reversed(xrange(self.horizon)):
                    a_thing = self.layer_codes[layer]
                    for x in a_thing:
                        if self.is_action(x):
                            try_this = self.variable_to_cnf_code[(layer, x)]
                            ordering_file.write(str(try_this) + " ")

        except IOError:
            raise EncodingException("Error: cannot open ORDERING file: " + file_name,
                encoding_error_code)


    def write_cnf(self, file_name):
        """Write a DIAMCS file to file_name representing the current encoding.

            (FlatEncoding, str) -> Nonee
        """
        try:
            with file(file_name, 'w') as cnf_file:
                if not self.quiet:
                    print "Total variables:", self.cnf_code
                    print "Total clauses:", self.get_num_clauses()
                cnf_file.write("p cnf " + str(self.cnf_code-1) + " " +\
                    str(self.get_num_clauses() + len(self.counting_clause_to_edge.keys())) + "\n")

                for cid, ((lb, ub), clause) in enumerate(sorted(self.clauses)): # Marshall Changed to sorted
                    if lb < 0: lb = self.horizon + lb + 1
                    if ub < 0: ub = self.horizon + ub + 1
                    else: ub = min(ub, self.horizon-1)
                    for step in xrange(lb, ub + 1):
                        cnf_codes = set()
                        for offset, lit in clause:
                             if lit < 0:
                                 cnf_codes.add(-self.variable_to_cnf_code[(step+offset, abs(lit))]) # Marshall Edit
                                 #c_set.add(-self.variable_to_cnf_code[(step+offset, abs(lit))])

                                 # Original
                                 #cnf_file.write(str(-self.variable_to_cnf_code[(step+offset, abs(lit))])+" ")

                                 #print("encoding_base: cnf_writing T (step+offset, abs(lit)): (" + str(step+offset) + ", " + str(abs(lit)) + ") -> " + str(-self.variable_to_cnf_code[(step+offset, abs(lit))]))
                             else:
                                 cnf_codes.add(self.variable_to_cnf_code[(step+offset, lit)]) # Marshall Edit
                                 #c_set.add(self.variable_to_cnf_code[(step+offset, abs(lit))])
                                 
                                 # Original
                                 #cnf_file.write(str(self.variable_to_cnf_code[(step+offset, lit)])+" ") 

                                 #print("encoding_base: cnf_writing F (step+offset, lit): (" + str(step+offset) + ", " + str(lit) + ") -> " + str(self.variable_to_cnf_code[(step+offset, abs(lit))]))

                        for lit in sorted(cnf_codes):
                            cnf_file.write(str(lit) + " ")
                        cnf_file.write("0\n")

                        """
                        c_set = frozenset(c_set)
                        if c_set in clauses:
                            print "DUP:",
                            for offset, lit in clause:
                                print str(-self.variable_to_cnf_code[(step+offset, abs(lit))])+" "+\
                                    "[" + self.get_variable_name(step+offset, abs(lit)) + "]",
                            print "-", self.clause_types[cid]
                        clauses.add(c_set)
                        """
        except IOError:
            raise EncodingException("Error: cannot open CNF file: " + file_name,
                encoding_error_code)

    def write_counters_to_cnf(self, file_name):
        """Write a DIAMCS file to file_name representing the current encoding.

            (FlatEncoding, str) -> Nonee
        """
        try:
            with file(file_name, 'a') as cnf_file:
                for clause in sorted([self.get_string_clause(sorted(self.get_list_clause(text_clause))) for text_clause in self.counting_clause_to_edge.keys()]):
                    cnf_file.write(clause + "\n")

        except IOError:
            raise EncodingException("Error: cannot open CNF file: " + file_name,
                encoding_error_code)

    def write_debug_cnf(self, file_name, horizon):
        """Write an annotated CNF file to file_name representing the current encoding.

            (Encoding, str) -> None
        """
        self.horizon = horizon
        try:
            with file(file_name, 'w') as cnf_file:
                cnf_file.write("p cnf " + str(self.cnf_code-1) +\
                    " " + str(self.get_num_clauses()) + "\n")
                for cid, ((lb, ub), clause) in enumerate(self.clauses):
                    if lb < 0: lb = self.horizon + lb + 1
                    if ub < 0: ub = self.horizon + ub + 1
                    else: ub = min(ub, self.horizon-1)
                    for step in xrange(lb, ub + 1):
                        for offset, lit in clause:
                            if lit < 0:
                                cnf_file.write(str(-self.variable_to_cnf_code[(step+offset, abs(lit))])+" "+\
                                    "[" + self.get_variable_name(step+offset, abs(lit)) + "] ")
                            else:
                                cnf_file.write(str(self.variable_to_cnf_code[(step+offset, lit)])+" "+\
                                    "[" + self.get_variable_name(step+offset, abs(lit)) + "] ")
                        cnf_file.write("0 - " + self.clause_types[cid] + "\n")

        except IOError:
            raise EncodingException("Error: cannot open CNF file: " + file_name,
                encoding_error_code)

    def get_num_clauses(self):
        """ Returns the total number of clauses in the problem, given the
            current horizon

            (Encoding) -> int
        """
        num_clauses = 0;
        for (lb, ub), clause in self.clauses:
            if lb < 0: lb = self.horizon + lb + 1
            if ub < 0: ub = self.horizon + ub + 1
            else: ub = min(ub, self.horizon-1)
            if (lb <= ub): num_clauses += ub - lb + 1
        return num_clauses

    def get_clause_types(self):
        """ Returns a dictionary mapping each clause type to the number
            of clauses of that type, given the current horizon

            (Encoding) -> { str : int }
        """
        clause_type_counts = defaultdict(int)
        for cid, ((lb, ub), clause) in enumerate(self.clauses):
            if lb < 0: lb = self.horizon + lb + 1
            if ub < 0: ub = self.horizon + ub + 1
            else: ub = min(ub, self.horizon-1)
            if (lb <= ub):
                ct = self.clause_types[cid]
                clause_type_counts[self.clause_types[cid]] += ub - lb + 1

        return clause_type_counts


    def set_true_variables(self, true_vars):
        """Set the true variables of the encoding.

            (Encoding, [int]) -> None
        """
        self.true_vars = true_vars

    def print_true_variables(self, v_type):
        """Print the true variables of the given type

            (Encoding, str) -> None
        """
        for cnf_code in self.true_vars:
            step, var = self.cnf_code_to_variable[cnf_code]
            print cnf_code, self.get_variable_name(step, var)

    @abc.abstractmethod
    def build_plan(self):
        """Build a plan from the true variables

            (Encoding) -> None
        """

    def make_invariant_clauses(self):
        """Add the stored invariants to the clauses of the encoding.

            (Encoding, [[int]]) -> None
        """
        self.clauses.extend(self.invariants)
        self.clause_types.extend(["inv"] * len(self.invariants))

        for bound, clause in self.object_invariants:
            v_clause = []
            for lit in clause:
                if isinstance(lit[0], Predicate):
                    code = self.fluent_codes[(lit[0], lit[1])]
                    if lit[2]:
                        code = -code
                    v_clause.append((0, code))
                else:
                    v_clause.append((0, -self.action_codes[lit]))
            self.clauses.append((bound, v_clause))
            self.clause_types.append("inv")

    def add_invariants(self, invariants):
        """Add the given invariants to the encoding.

            (Encoding, [[int]]) -> None
        """
        assert False, "NOT IMPLEMENTED, clauses have changed..."
        self.invariants.extend(invariants)

    def add_object_invariants(self, invariants):
        """ We have a bunch of invariants to add, but instead of codes we have
            fluents and actions.
            (Encoding, [((lb, ub), ((obj, (str,))),) ] -> None
        """
        self.object_invariants.extend(invariants)


    def make_conflict_cliques(self, conflicts_to_encode_set, conflicts_to_encode, codes):
        """ Make cliques out of the given conflicts. Use the variables from the
            supplied code dictionary.

            Warning - modifies conflicts_to_enocde!

            (Encoding, set([(obj, obj)]), { obj : set([obj]) }, { obj : int }) -> [set([int])]
        """
        cliques = []
        m_encoded = set()
        for c_pair in conflicts_to_encode_set:
            if c_pair in m_encoded: continue
            c1, c2 = c_pair
            clique = set(c_pair)
            conflicts_to_encode[c1].discard(c2)
            conflicts_to_encode[c2].discard(c1)
            m_encoded.add(c_pair)
            m_encoded.add((c2, c1))
            added = []
            for c3 in conflicts_to_encode[c1]:
                if conflicts_to_encode[c3].issuperset(clique):
                    added.append(c3)
                    clique.add(c3)
            for c3 in added:
                for c4 in clique:
                    if c3 != c4:
                        conflicts_to_encode[c3].discard(c4)
                        conflicts_to_encode[c4].discard(c3)
                        m_encoded.add((c3, c4))
                        m_encoded.add((c4, c3))
            #Make the clique out of codes
            cliques.append(set([codes[c] for c in clique]))

        if not self.quiet:
            c_hist = {}
            for c in cliques:
                lc = len(c)
                if lc not in c_hist: c_hist[lc] = 1
                else: c_hist[lc] += 1

            print "(size - number):",
            c_hist_v = sorted(c_hist.keys())
            for v in c_hist_v:
                print "(", v, "-", c_hist[v], ")",
            print

        return cliques


    def make_clique_clauses(self, cliques):
        """ Make the clauses for the given list of cliques

            (Encoding, [set([int])]) -> None
        """
        start_code = self.code
        for clique in cliques:
            #Special encoding with n log n clauses
            #Grab pairs of elements and make them mutex, with an aux var
            #put all aux vars in a set, repeat.
            clique = sorted(clique)
            while True:
                if self.min_mutex_clique_size is None or len(clique) < self.min_mutex_clique_size:
                    for (c1, c2) in itertools.combinations(clique, 2):
                        self.clauses.append(((0, -2), [(0, -c1), (0, -c2)]))
                        self.clause_types.append("cliquem")
                    break

                t_clique = []
                for c in range(len(clique))[::2]:
                    if c == len(clique)-1:
                        #odd one out - link to previous
                        t_clique.append(clique[c])
                    else:
                        code = self.new_code(('aux', (clique[c], clique[c+1])), (0, -2))
                        self.aux_codes.append(code)
                        t_clique.append(code)
                        self.clauses.append(((0, -2), [(0, -clique[c]), (0, -clique[c+1])]))
                        self.clause_types.append("cliquem")
                        self.clauses.append(((0, -2), [(0, -clique[c]), (0, self.code)]))
                        self.clause_types.append("cliquem")
                        self.clauses.append(((0, -2), [(0, -clique[c+1]), (0, self.code)]))
                        self.clause_types.append("cliquem")
                clique = t_clique

        if not self.quiet:
            print "Aux vars added:", self.code - start_code


    def make_enabling_disabling_sccs(self):
        """ Make an enabling disabling graph from the problem and then get the
            set of strongly connected components in this graph and save them.

            Edges in this graph when:
            if o1 adds something that o2 requires or o2 deletes something that o1 requires and
            They are not conflicting or mutex due to preconditions.

            (Encoding) -> None
        """
        self.wehrle_conflicts = {}
        broken = 0
        kept = 0
        if self.problem.ap_mutexes is None:
            self.problem.ap_mutexes = {}
            for ag_pair1 in self.action_codes:
                self.problem.ap_mutexes[ag_pair1] = set()
                self.wehrle_conflicts[ag_pair1] = set()
        else:
            for ag_pair1 in self.action_codes:
                self.wehrle_conflicts[ag_pair1] = set()
            #Two actions are conflicting if they have conflicting preconditons (are in ap_mutexes)
            #and there is no action that can be interleved between them
            for ag_pair1, ap_list in self.problem.ap_mutexes.iteritems():
                action1, grounding1 = ag_pair1
                for ag_pair2 in ap_list:
                    action2, grounding2 = ag_pair2
                    #These actions might be mutex for multiple reasons, lets find them all
                    some_mutex_unbreakable = False
                    for pg_pair1 in action1.strips_preconditions[grounding1]:
                        pred1, pgrounding1, sign1 = pg_pair1

                        if sign1: a1_list = pred1.ground_precs[pgrounding1]
                        else: a1_list = pred1.ground_nprecs[pgrounding1]

                        for pg_pair2 in action2.strips_preconditions[grounding2]:
                            pred2, pgrounding2, sign2 = pg_pair2

                            if sign2: a2_list = pred2.ground_precs[pgrounding2]
                            else: a2_list = pred2.ground_nprecs[pgrounding2]

                            if pg_pair2 in self.problem.state_mutexes[pg_pair1]:
                                #This pair of preconditions might be responsible for a conflict
                                mutex_breakable = False
                                #ag_pair1 < ag_pair2
                                for ag_pair3 in a2_list:
                                    if ag_pair3 in self.problem.eff_eff_conflicts[ag_pair1] or\
                                        ag_pair3 in self.problem.eff_eff_conflicts[ag_pair2]: continue
                                    action3, grounding3 = ag_pair3
                                    bad_effect = False
                                    for pg_pair3 in action3.strips_effects[grounding3]:
                                        pred3, pgrounding3, sign3 = pg_pair3
                                        if (pred3, pgrounding3, not sign3) in action2.strips_preconditions[grounding2]:
                                            bad_effect = True
                                            break
                                    if bad_effect:
                                        mutex_breakable = True
                                        break

                                if not mutex_breakable:
                                    #ag_pair2 < ag_pair1
                                    for ag_pair3 in a1_list:

                                        if ag_pair3 in self.problem.eff_eff_conflicts[ag_pair1] or\
                                            ag_pair3 in self.problem.eff_eff_conflicts[ag_pair2]: continue
                                        action3, grounding3 = ag_pair3
                                        bad_effect = False
                                        for pg_pair3 in action3.strips_effects[grounding3]:
                                            pred3, pgrounding3, sign3 = pg_pair3
                                            if (pred3, pgrounding3, not sign3) in action1.strips_preconditions[grounding1]:
                                                bad_effect = True
                                                break
                                        if bad_effect:
                                            mutex_breakable = True
                                            break

                                if not mutex_breakable:
                                    some_mutex_unbreakable = True
                                    break

                        if some_mutex_unbreakable:
                            break


                    if some_mutex_unbreakable:
                        self.wehrle_conflicts[ag_pair1].add(ag_pair2)
                        kept += 1
                    else:
                        broken += 1


        #print "Broken:", broken
        #print "Kept:", kept

        ed_graph = networkx.DiGraph()
        for ag_pair1 in self.action_codes:
            action1, grounding1 = ag_pair1
            ed_graph.add_node(ag_pair1)
            for fluent1 in action1.strips_effects[grounding1]:
                pred1, f_grounding1, sign1 = fluent1

                ######################################################### WHY DOES THIS IGNORE THE SIGN????????????

                for ag_pair2 in itertools.chain(pred1.ground_precs[f_grounding1],
                                                pred1.ground_nprecs[f_grounding1]):
                    if ag_pair2 not in self.problem.eff_eff_conflicts[ag_pair1] and\
                       ag_pair2 not in self.wehrle_conflicts[ag_pair1] and\
                       ag_pair1 not in self.wehrle_conflicts[ag_pair2]:

                       action2, grounding2 = ag_pair2
                       mutex_effects = False
                       for eff1 in action1.strips_effects[grounding1]:
                           for eff2 in action2.strips_effects[grounding2]:
                               if eff2 in self.problem.state_mutexes[eff1]:
                                   mutex_effects = True
                                   break
                           if mutex_effects: break
                       if not mutex_effects:
                           ed_graph.add_edge(ag_pair1, ag_pair2)
                       #else:
                       #    print "avoided line:", action1.name, grounding1, ":", action2.name, grounding2


        self.ed_sccs = strongly_connected_components(ed_graph)

        scc_hist = {}
        for scc in self.ed_sccs:
            scc_s = len(scc)
            if scc_s not in scc_hist:
                scc_hist[scc_s] = 1
            else:
                scc_hist[scc_s] += 1

        scc_sizes = sorted(scc_hist.keys())

        #print "Sccs:", len(self.ed_sccs)
        #for scc_s in scc_sizes:
        #    print scc_s, "-", scc_hist[scc_s], ",",


    def recursive_single_source_longest_path_no_cycles(self, graph, node, history, distances):
        """ Traverse the supplied graph from the start node and return a
            dictionary mapping nodes to their furthers distance from the start
            state not allowing cycles.

        """
        child_dist = distances[node] + 1
        for child in graph.neighbors_iter(node):
            if child in history or (child in distances and distances[child] <= child_dist):
                continue
            distances[child] = child_dist
            history.add(child)
            self.recursive_single_source_longest_path_no_cycles(graph, child, history, distances)
            history.remove(child)


    def totally_order_actions(self):
        """ Get a total ordering over actions.

            There are lots of possibilities here, for now, we will just make an
            arbitrary one, but eventually you want to use something like an
            enabling and disabling graph to get your orderings.

            (Encoding, int) -> None
        """
        actions = []
        for action in self.problem.actions.itervalues():
            for grounding in action.groundings:
               actions.append((action, grounding))

        self.total_order = []

        #It is possible that we can switch this out for simple reachability
        #In the case that we don't fully compute the plangraph - actually it
        #might be better to just have a non-mutex mode for the plangraph
        distances = {}
        for ag_pair in actions:
            distances[ag_pair] = self.problem.first_layer_actions[ag_pair]

        dist_to_nodes = defaultdict(list)
        for node, dist in distances.iteritems():
            dist_to_nodes[dist].append(node)

        dists = sorted(dist_to_nodes.iterkeys())
        for dist in dists:
            nodes = dist_to_nodes[dist]
            for node in nodes:
                self.total_order.append(node)


    #---------------------------------------------------------------------------
    #Hierarchical stuff
    #---------------------------------------------------------------------------

    def make_dtgs(self):
        """
            (Encoding) -> None
        """
        state_mutexes = self.problem.state_mutexes
        seen_edges = set()
        for prop1, mutexes in state_mutexes.iteritems():
            if not prop1[2]: continue
            for prop2 in mutexes:
                if not prop2[2]: continue
                if (prop2, prop1) not in seen_edges:
                    seen_edges.add((prop1[:2], prop2[:2]))

        mutex_graph = networkx.Graph()
        for predicate in self.problem.predicates.itervalues():
            for grounding in predicate.groundings:
                mutex_graph.add_node((predicate, grounding))

        for f1, f2 in seen_edges:
            mutex_graph.add_edge(f1, f2)

        self.mutex_cliques = list(networkx.find_cliques(mutex_graph))
        self.mutex_cliques.sort(key=len) #, reverse=True)


        """
        def replace_arg(arg):
            new_cliques = defaultdict(list)
            new_mutex_cliques = []
            for clique in self.mutex_cliques:
                if len(clique) == 1:
                   fact = clique[0]
                   pred, grounding = fact
                   if arg in grounding:
                       new_cliques[len(grounding)].append(fact)
                   else:
                       new_mutex_cliques.append(clique)
                else:
                    new_mutex_cliques.append(clique)
            self.mutex_cliques = new_mutex_cliques
            for clique in new_cliques.itervalues():
                self.mutex_cliques.append(clique)

        replace_arg("shaker1")
        replace_arg("shot1")
        replace_arg("shot2")
        replace_arg("shot3")
        replace_arg("shot4")
        """

        self.dtg_actions = []
        self.prop_to_clique = {}
        for cid, clique in enumerate(self.mutex_cliques):
            dtg_actions = set()
            for prop in clique:
                pred, grounding = prop
                self.prop_to_clique[prop] = cid
                for ag_pair in pred.ground_adds[grounding]:
                    dtg_actions.add(ag_pair)
                for ag_pair in pred.ground_dels[grounding]:
                    dtg_actions.add(ag_pair)
            self.dtg_actions.append(set(dtg_actions))

            print "CLIQUE:", cid, " ".join(["(" + f[0].name + " " + " ".join(f[1]) + ")" for f in clique])
            #print "Actions:"
            #for action in dtg_actions:
            #    print "    ", action[0].name, " ".join(action[1])



        #assert False


    def make_causal_graph(self, pure_effect_links):
        """ Make a causal graph from the DTGs. """

        if not pure_effect_links:
            pos_fluents = set()
            neg_fluents = set()
            for action in self.problem.actions.itervalues():
                for grounding in action.groundings:
                    ag_pair = (action, grounding)
                    for eff in action.strips_effects[grounding]:
                        if eff[2]: pos_fluents.add(eff[:2])
                        else: neg_fluents.add(eff[:2])
            pure_fluents = pos_fluents.symmetric_difference(neg_fluents)

        self.causal_graph = networkx.DiGraph()
        for action in self.problem.actions.itervalues():
            for grounding in action.groundings:
                ag_pair = (action, grounding)
                for eff1, eff2 in itertools.combinations(\
                    action.strips_effects[grounding], 2):
                    f1 = eff1[:2]
                    f2 = eff2[:2]
                    if not pure_effect_links and\
                        (f1 in pure_fluents or f2 in pure_fluents): continue

                    clique1 = self.prop_to_clique[f1]
                    clique2 = self.prop_to_clique[f2]
                    if clique1 == clique2: continue
                    self.causal_graph.add_edge(clique1, clique2)
                    self.causal_graph.add_edge(clique2, clique1)


                for eff in action.strips_effects[grounding]:
                    eff_clique = self.prop_to_clique[eff[0], eff[1]]
                    for prec in action.strips_preconditions[grounding]:
                        prec_clique = self.prop_to_clique[prec[0], prec[1]]
                        if prec_clique == eff_clique: continue
                        self.causal_graph.add_edge(prec_clique, eff_clique)


        self.next_block = 0
        self.block_components = { None : None }
        for block in self.causal_graph.nodes_iter():
            self.block_components[block] = block
            self.next_block = max(self.next_block, block)
        self.next_block += 1

        self.block_graph_root = None

        networkx.write_dot(self.causal_graph, "grid0.dot")


    def make_better_causal_graph(self):
        """ Make a graph that is better than the causal graph """

        component_preconditions = {}
        component_effects = {}
        for cid in xrange(len(self.mutex_cliques)):
            component_preconditions[cid] = set()
            component_effects[cid] = set()
            for action, grounding in self.dtg_actions[cid]:
                for prec in action.strips_preconditions[grounding]:
                    component_preconditions[cid].add(prec)
                for eff in action.strips_effects[grounding]:
                    component_effects[cid].add(eff)

        #self.dtg_actions = []
        #self.prop_to_clique = {}
        #for cid, clique in enumerate(self.mutex_cliques):

        self.causal_graph = networkx.DiGraph()

        self.next_block = 0
        self.block_graph_root = self.next_block

        self.block_components = { 0 : None }
        """
        def recursively_build_sup_graph(component, pending_precs, ancestors):
            #print "com:", component, len(pending_precs), len(ancestors)

            for cid in xrange(len(self.mutex_cliques)):
                if cid not in ancestors and pending_precs & component_effects[cid]:
                    ancestors_copy = set(ancestors)
                    ancestors_copy.add(cid)
                    if component is not None:
                        self.causal_graph.add_edge((self.next_block, cid), component)
                    self.block_components[self.next_block] = cid
                    self.next_block += 1
                    recursively_build_sup_graph(cid, component_preconditions[cid], ancestors_copy)
            #recursively_build_sup_graph(None, pending_precs, set())
        """
        pending_precs = set([(prec[0].pred, prec[0].ground_conditions[prec[1]], prec[2])\
            for prec in self.problem.flat_ground_goal_preconditions ])
        queue = deque([(0, self.block_graph_root, pending_precs, set())])

        level_comps = defaultdict(set)
        while queue:
            level, component, pp, ancestors = queue.popleft()

            #print "PP:", level, component
            #print "--------------------------------------------"

            #for p in pp:
            #    print p[0].name, " ".join(p[1]), p[2]

            for cid in xrange(len(self.mutex_cliques)):

                intersection = pp & component_effects[cid]
                #print "CID:", cid

                #print "Overlapping effects:"
                #for fact in intersection:
                #    print "    ", fact[0].name, " ".join(fact[1]), fact[2]

                seen_before = [ cid in level_comps[l] for l in xrange(level)]
                #print "Seen before:", seen_before

                if not any(seen_before) and intersection:
                    level_comps[level].add(cid)

                    ancestors_copy = set(ancestors)
                    ancestors_copy.add(cid)

                    if self.next_block != component:
                        self.causal_graph.add_edge(self.next_block, component)

                    queue.append((level+1, self.next_block,
                        component_preconditions[cid], ancestors_copy))
                    self.block_components[self.next_block] = cid
                    self.next_block += 1


        """
        for action in self.problem.actions.itervalues():
            for grounding in action.groundings:
                ag_pair = (action, grounding)
                for eff1, eff2 in itertools.combinations(\
                    action.strips_effects[grounding], 2):
                    f1 = eff1[:2]
                    f2 = eff2[:2]
                    if not pure_effect_links and\
                        (f1 in pure_fluents or f2 in pure_fluents): continue

                    clique1 = self.prop_to_clique[f1]
                    clique2 = self.prop_to_clique[f2]
                    if clique1 == clique2: continue
                    self.causal_graph.add_edge(clique1, clique2)
                    self.causal_graph.add_edge(clique2, clique1)


                for eff in action.strips_effects[grounding]:
                    eff_clique = self.prop_to_clique[eff[0], eff[1]]
                    for prec in action.strips_preconditions[grounding]:
                        prec_clique = self.prop_to_clique[prec[0], prec[1]]
                        if prec_clique == eff_clique: continue
                        self.causal_graph.add_edge(prec_clique, eff_clique)
        """

        networkx.write_dot(self.causal_graph, "grid0.dot")





    def cut_block_graph(self, cut_above, cycle_limit):

        #for cid, clique in enumerate(self.mutex_cliques):
        #    print "CID:", cid, " ".join(["(" + f[0].name + " " + " ".join(f[1]) +")" for f in clique])

        print "Cutting Graph:",
        #gi = 0

        while True:
            cycle_gen = networkx.cycles.simple_cycles(self.block_graph)
            cycles = []
            for cid, cycle in enumerate(cycle_gen):
                cycles.append(cycle)
                if cid == cycle_limit: break
            if not cycles: break

            cycle_count = defaultdict(int)
            for cycle in cycles:
                for block in cycle:
                    cycle_count[block] += 1

            max_count = max(cycle_count.itervalues())
            max_blocks = [block for block, count in cycle_count.iteritems()\
                if count == max_count]

            max_out_deg = 0
            cut_block = None
            for block in max_blocks:
                out_deg = self.block_graph.out_degree(block)
                if out_deg > max_out_deg:
                    max_out_deg = out_deg
                    cut_block = block

            print cut_block,

            if cut_above:   #we don't actually use -- i think it works on depots
                self.block_graph.remove_edges_from(\
                    self.block_graph.in_edges(cut_block))
                for succ in self.block_graph.successors(cut_block)[1:]:
                    self.block_graph.remove_edge(cut_block, succ)
                    self.block_graph.add_edge(self.next_block, succ)
                    self.block_components[self.next_block] =\
                        self.block_components[cut_block]
                    self.next_block += 1
            else:
                self.block_graph.remove_edges_from(\
                    self.block_graph.out_edges(cut_block))

            """
            self.block_graph_labeled = networkx.DiGraph()
            for edge in self.block_graph.edges_iter():
                block1 = tuple([edge[0]]+[self.block_components[edge[0]]])
                block2 = tuple([edge[1]]+[self.block_components[edge[1]]])
                self.block_graph_labeled.add_edge(block1, block2)
            networkx.write_dot(self.block_graph_labeled, "grid" + str(gi) + ".dot")
            gi += 1
            """

        print

    def make_children_unique(self):
        dirty_queue = deque([x[0]\
            for x in self.block_graph.out_degree().iteritems() if x[1] > 1])
        while dirty_queue:
            block = dirty_queue.popleft()
            if not self.block_graph.has_node(block): continue
            for succ in self.block_graph.successors(block):
                self.block_graph.remove_edge(block, succ)
                self.block_graph.add_edge(self.next_block, succ)
                for pred in self.block_graph.predecessors_iter(block):
                    self.block_graph.add_edge(pred, self.next_block)
                    dirty_queue.append(pred)
                self.block_components[self.next_block] =\
                    self.block_components[block]
                self.next_block += 1
            self.block_graph.remove_node(block)
            del self.block_components[block]


    def remove_extra_parents(self):
        non_parent_ancestors = defaultdict(set)
        def explore_decendants(ancestor, block, store):
            if store: non_parent_ancestors[block].add(ancestor)
            for succ in self.block_graph.successors_iter(block):
                explore_decendants(ancestor, succ, True)
        for block in self.block_graph.nodes_iter():
            component = self.block_components[block]
            for succ in self.block_graph.successors_iter(block):
                explore_decendants(component, succ, False)

        for block in self.block_graph.nodes():
            if block not in self.block_graph.nodes(): continue
            for parent in self.block_graph.predecessors(block):
                if self.block_components[parent] in non_parent_ancestors[block]:
                    self.block_graph.remove_node(parent)
                    #print "Removing:", parent
                    del self.block_components[parent]


    def compute_block_actions(self, prune_extra_actions):
        self.block_actions = {}
        for block in self.block_graph.nodes_iter():
            self.block_actions[block] = set()
            component = self.block_components[block]
            if component is not None:
                self.block_actions[block].update(self.dtg_actions[component])

        #Remove actions from a block if they exist in one of the blocks ancestors
        def recursive_prune_actions(block):
            actions = set()
            for parent in self.block_graph.predecessors_iter(block):
                actions.update(recursive_prune_actions(parent))
            self.block_actions[block].difference_update(actions)
            actions.update(self.block_actions[block])
            return actions

        recursive_prune_actions(self.block_graph_root)

        action_blocks = defaultdict(set)
        for block, actions in self.block_actions.iteritems():
            for action in actions:
                action_blocks[action].add(block)

        #Prune extra actions
        #----------------------------------------------------------------------
        # For each node N, look at its child N', now we have three sets of propositions.
        # props(N), props(N'), and props(N)\cap props(N'). Keep only those actions where
        # eff(a) \subseteq props(N) or eff(a) \subseteq(N') or
        #eff(a)\cap props(N)\cap props(N') != 0  (and there is at least one copy)

        if not prune_extra_actions:
            return

        num_removed = 0

        for block in self.block_graph.nodes_iter():
            #If the block has no actions or is the root block then continue
            if not self.block_actions[block] or\
                not self.block_graph.successors(block): continue

            child = self.block_graph.successors(block)[0]
            if child is None: continue
            block_fluents = set(self.mutex_cliques[self.block_components[block]]) #ancestor_facts[block]

            child_component = self.block_components[child]
            if child_component is None:
                child_fluents = set()
            else:
                child_fluents = set(self.mutex_cliques[self.block_components[child]])
            shared_fluents = block_fluents & child_fluents
            for ag_pair in list(self.block_actions[block]):
                action, grounding = ag_pair
                eff_set = set()
                for eff in action.strips_effects[grounding]:
                    eff_set.add(eff[:2])
                if not eff_set.issubset(block_fluents) and\
                    not eff_set.issubset(child_fluents) and\
                    eff_set.isdisjoint(shared_fluents):
                    if len(action_blocks[ag_pair]) > 1:
                        self.block_actions[block].remove(ag_pair)

                        #print "Removing:", action.name, grounding, "from block:", block
                        num_removed += 1

                        action_blocks[ag_pair].remove(block)

        if num_removed:
            print "We removed", num_removed, "actions from blocks because they were elsewhere"



    def copy_n_lowest_layers(self, copy_layer_n, block_layer_function):
        """ Starting at layer n, the deepest layer, make K copies where K
            is the number of actions at n+1 - skip a layer if K = 0.

            For the root node, just copy everything inside it x times where x
            is copy_layer_n - the root depth + 1.

            This function does not do the copying. It just marks nodes with
            either the number of copies (for the root node) or with the fact
            that the node will be copied.
        """
        layer_blocks = defaultdict(set)
        def compute_block_layers(block):
            layers = []
            for parent in self.block_graph.predecessors_iter(block):
                layers.append(compute_block_layers(parent))
            if not layers: block_layer = 0
            else:          block_layer = block_layer_function(layers)+1
            layer_blocks[block_layer].add(block)
            return block_layer
        compute_block_layers(self.block_graph_root)

        root_reached = False
        for cl in xrange(1, copy_layer_n+2):
            for block in layer_blocks[cl]:
                if not self.block_graph.out_degree(block):
                    root_reached = True
                    self.node_copies[block] = copy_layer_n+1-cl
                elif self.block_actions[block]:
                    self.node_copies[block] = True
            if root_reached: break


    def partition_on_actions(self):
        for block in self.block_graph.nodes():
            if not self.block_actions[block]: continue
            component = self.block_components[block]
            block_facts = set(self.mutex_cliques[component])

            action_partitions = defaultdict(set)
            for ag_pair in self.block_actions[block]:
                action, grounding = ag_pair
                assigned = False
                for eff in action.strips_effects[grounding]:
                    prop = eff[:2]
                    if prop in block_facts and eff[2]:
                        action_partitions[prop].add(ag_pair)
                        assigned = True
                if not assigned:
                    action_partitions[None].add(ag_pair)

            new_block_actions = set()
            for fact, actions in action_partitions.iteritems():
                if len(actions) > min_action_component_size:
                    self.block_components[self.next_block] = self.block_components[block]
                    self.block_graph.add_edge(self.next_block, block)
                    self.block_actions[self.next_block] = actions
                    self.next_block += 1
                else:
                    new_block_actions.update(actions)

            if len(new_block_actions) < len(self.block_actions[block]):
                self.block_actions[block] = new_block_actions


    def compute_block_interfaces(self):
        #Compute the facts in every block + its children
        self.block_preconditions = {}
        self.block_effects = {}
        def recursive_get_block_interface(block):
            self.block_preconditions[block] = set()
            self.block_effects[block] = set()
            for action, grounding in self.block_actions[block]:
                for prec in action.strips_preconditions[grounding]:
                    self.block_preconditions[block].add(prec[:2])
                for eff in action.strips_effects[grounding]:
                    self.block_effects[block].add(eff[:2])
            for parent in self.block_graph.predecessors_iter(block):
                recursive_get_block_interface(parent)
                self.block_preconditions[block].update(self.block_preconditions[parent])
                self.block_effects[block].update(self.block_effects[parent])

            #Every block has its effects as preconditions
            self.block_preconditions[block].update(self.block_effects[block])
        recursive_get_block_interface(self.block_graph_root)


    def assign_block_types(self):
        self.block_types = {}
        num_ordered = 0
        num_causal = 0
        for block in self.block_graph.nodes_iter():
            size = len(self.block_actions[block]) + self.block_graph.in_degree(block)
            if size >= linear_encoding_threshold:
                num_ordered += 1
                self.block_types[block] = "ordered"
            else:
                num_causal += 1
                self.block_types[block] = "causal"

        print "Number of ordered blocks:", num_ordered
        print "Number of causal blocks: ", num_causal



    def totally_order_block_actions(self, copy_layer_n, block_layer_function,
        redundant_sub_block_elim):
        """ Get a total ordering over actions and sub-blocks for each block.

            make any required copies... as specified by self.node_copies

            redundant_sub_block_elim reduces the number of copies by checking
            if interleving blocks between actions could actually be helpful
            given the known assignment from previous actions.

            (Encoding, bool) -> None

        """
        distances = {}
        def compute_block_distances_recursive(block):
            block_dists = []
            for ag_pair in self.block_actions[block]:
                dist = self.problem.first_layer_actions[ag_pair]
                distances[block, ag_pair] = dist
                block_dists.append(dist)
            for parent in self.block_graph.predecessors_iter(block):
                dist = compute_block_distances_recursive(parent)
                distances[block, parent] = dist
                block_dists.append(dist)
            return float(sum(block_dists)) / len(block_dists)

        compute_block_distances_recursive(self.block_graph_root)

        dist_to_actions = defaultdict(list)
        for action, dist in distances.iteritems():
            dist_to_actions[dist].append(action)
        dists = sorted(dist_to_actions.iterkeys())

        tmp_block_total_order = defaultdict(list)
        for dist in dists:
            for block, action in dist_to_actions[dist]:
                tmp_block_total_order[block].append(action)

        layer_blocks = defaultdict(set)
        def compute_block_layers(block):
            layers = []
            for parent in self.block_graph.predecessors_iter(block):
                layers.append(compute_block_layers(parent))
            if not layers: block_layer = 0
            else:          block_layer = max(layers)+1
            layer_blocks[block_layer].add(block)
            return block_layer
        compute_block_layers(self.block_graph_root)

        def copy_subgraph(block, parent):
            next_block = self.next_block
            self.block_components[next_block] = self.block_components[block]
            self.block_actions[next_block] = set(self.block_actions[block])
            self.block_preconditions[next_block] = set(self.block_preconditions[block])
            self.block_effects[next_block] = set(self.block_effects[block])
            self.block_total_order[next_block] = list(self.block_total_order[block])
            self.next_block += 1
            self.block_graph.add_edge(next_block, parent)
            for parent in self.block_graph.predecessors_iter(block):
                copy_subgraph(parent, next_block)

        #Get the real, signed effects of every block
        block_signed_effects = defaultdict(set)
        def recursive_get_ancestor_eff(block):
            aset = set()
            for parent in self.block_graph.predecessors(block):
                aset.update(recursive_get_ancestor_eff(parent))
            for action, grounding in self.block_actions[block]:
                aset.update(action.strips_effects[grounding])
            block_signed_effects[block] = aset
            return aset
        recursive_get_ancestor_eff(self.block_graph_root)

        self.block_total_order = defaultdict(list)
        root_reached = False
        cl = 0
        ################## ONE THING TO DO HERE - check to see if we make any
        #########copies and if not increase copy_layer_n until we do
        while True:
            for block in layer_blocks[cl]:
                if not self.block_graph.out_degree(block):
                    #Root block
                    self.block_total_order[block] = list(tmp_block_total_order[block])
                    root_reached = True
                    num_copies = max(0, copy_layer_n+2-cl)
                    parents = self.block_graph.predecessors(block)
                    for copy in xrange(num_copies-1):
                        for parent in parents:
                            copy_subgraph(parent, block)
                            self.block_total_order[block].append(parent)

                elif cl > 0 and cl <= copy_layer_n and self.block_actions[block]:
                    #A block to be copied
                    parents = self.block_graph.predecessors(block)
                    if redundant_sub_block_elim:
                        #We include fewer blocks here...

                        block_dist_to_actions = defaultdict(set)
                        for ag_pair in tmp_block_total_order[block]:
                            if isinstance(ag_pair, int): continue
                            block_dist_to_actions[distances[block, ag_pair]].add(ag_pair)
                        dists = sorted(block_dist_to_actions.iterkeys())

                        total_order = []
                        total_order_precs = []

                        effects = set()
                        for dist in dists:
                            dist_actions = block_dist_to_actions[dist]
                            for aid, ag_pair in enumerate(dist_actions):
                                action, grounding = ag_pair
                                bad_pre = False
                                for pre in action.strips_preconditions[grounding]:
                                    if pre in effects:
                                        bad_pre = True
                                        break
                                if bad_pre or aid == 0:
                                    total_order.append(None)
                                    total_order_precs.append(set())
                                    effects.clear()

                                total_order.append(ag_pair)
                                total_order_precs[-1].update(action.strips_preconditions[grounding])
                                for eff in action.strips_effects[grounding]:
                                    effects.add(eff)

                        pid = 0
                        for ag_pair in total_order:
                            if ag_pair is None:
                                precs = total_order_precs[pid]
                                pid += 1
                                for parent in parents:
                                    can_sup = False
                                    for eff in block_signed_effects[parent]:
                                        if eff in precs:
                                            can_sup = True
                                            break
                                    if can_sup:
                                        self.block_total_order[block].append(parent)
                                        copy_subgraph(parent, block)
                            else:
                                self.block_total_order[block].append(ag_pair)

                    else:
                        for ag_pair in tmp_block_total_order[block]:
                            if isinstance(ag_pair, int): continue
                            #Find the blocks that need to go before the action
                            action, grounding = ag_pair
                            for parent in parents:
                                can_sup = False
                                parent_effs = block_signed_effects[parent]
                                for eff in action.strips_preconditions[grounding]:
                                    if eff in parent_effs:
                                        can_sup = True
                                        break
                                if can_sup:
                                    self.block_total_order[block].append(parent)
                                    copy_subgraph(parent, block)
                            self.block_total_order[block].append(ag_pair)

                else:
                    self.block_total_order[block] = list(tmp_block_total_order[block])

            if root_reached: break
            cl +=1


    def get_tree_order(self):
        """ Get a list the actions by traversing the tree in the specified
            total orders. Save this in self.total_order.
        """
        self.total_order = []
        def recursive_tree_order(tblock):
            for action in self.block_total_order[tblock]:
                if isinstance(action, int):
                    recursive_tree_order(action)
                else:
                    self.total_order.append(action)
        recursive_tree_order(self.block_graph_root)


    def make_hierarchy(self, cut_above, cycle_limit, partition_on_actions,
        min_action_component_size, copy_layer_n, block_layer_function,
        redundant_sub_block_elim, linear_encoding_threshold,
        prune_extra_actions):
        """
            to_parent is if we put their parents (alternatively nodes inside
            their children)
            bi_directional is for a<->b if we go both ways
            absorb_useless is if children absorb their parents (which have

            (Encoding, bool, bool, bool, bool, int) -> None
        """
        self.block_graph = self.causal_graph.copy()

        #Cut the graph on nodes which occur in the most cycles
        self.cut_block_graph(cut_above, cycle_limit)


        self.block_graph_labeled = networkx.DiGraph()
        for edge in self.block_graph.edges_iter():
            block1 = tuple([edge[0]]+[self.block_components[edge[0]]])
            block2 = tuple([edge[1]]+[self.block_components[edge[1]]])
            self.block_graph_labeled.add_edge(block1, block2)
        networkx.write_dot(self.block_graph_labeled, "grid11.dot")


        #Copy blocks with more than one outgoing arc
        self.make_children_unique()

        #Remove the parents of a node that exist as non-parent ancestors of the node.
        self.remove_extra_parents()

        if self.block_graph_root is None:
            roots = [block for block in self.block_graph.nodes_iter()\
                if not self.block_graph.out_degree(block)]
            self.block_graph_root = self.next_block
            self.block_components[self.next_block] = None
            for root in roots:
                self.block_graph.add_edge(root, self.next_block)
            self.next_block += 1

        #Put actions in blocks
        self.compute_block_actions(prune_extra_actions)

        #Temp remove blocks with no actions or children
        bad_queue = deque()
        for block in self.block_graph.nodes_iter():
            if not self.block_actions[block] and not self.block_graph.in_degree(block):
                bad_queue.append(block)

        #Remove blocks with no actions and no parents, recursively
        while bad_queue:
            block = bad_queue.popleft()
            children = self.block_graph.successors(block)
            self.block_graph.remove_node(block)
            assert children
            child = children[0]
            if not self.block_actions[child] and not self.block_graph.in_degree(child):
                bad_queue.append(child)


        #Split block actions according to incoming arcs to DTG nodes
        #This won't work properly because we need to make it copy some extra
        #info
        #if partition_on_actions:
        #    self.partition_on_actions()

        #Compute block interface
        self.compute_block_interfaces()


        #Draw

        self.block_graph_labeled = networkx.DiGraph()
        for edge in self.block_graph.edges_iter():
            block1 = tuple([edge[0]]+[self.block_components[edge[0]]])
            block2 = tuple([edge[1]]+[self.block_components[edge[1]]])
            self.block_graph_labeled.add_edge(block1, block2)
        networkx.write_dot(self.block_graph_labeled, "grid1.dot")


        #for cid, clique in enumerate(self.mutex_cliques):
        #    print "CID:", cid, " ".join(["(" + f[0].name + " " + " ".join(f[1]) +")" for f in clique])


        #assert False


        #This also makes copies of nodes as required by self.node_copies
        self.totally_order_block_actions(copy_layer_n, block_layer_function,
            redundant_sub_block_elim)

        #Assign block types
        if linear_encoding_threshold is not None:
            self.assign_block_types()

    def get_list_clause(self, string_clause):
        return [int(x) for x in string_clause.split(" ")[:-1]]

    def get_string_clause(self, list_clause):
        string_clause = ""
        for var in list_clause:
            string_clause += str(var)
            string_clause += " "
        string_clause += "0"
        return string_clause






class EncodingWrapper(object):
    """ This class serves as a wrapper for the encodings implemented in the planner.
        It allows encodings to be registered and instantiated by putting them
        in the cnf_encodings package and adding their names to the registration list.
    """

    def __init__(self):
        """ Make a new EncodingWrapper

            (EncodingWrapper) -> None
        """
        self.valid_encodings = {}
        self.default_encoding = ''

    def read_encoding_list(self):
        """ Read the list of encodings and the default encoding. Register
            the listed encodings.

            (EncodingWrapper) -> None
        """
        try:
            with file(encoding_list_file_name, 'r') as encoding_list_file:
                encoding_lines = encoding_list_file.readlines()
                if len(encoding_lines) < 2:
                    raise EncodingException("Error: encoding list does not have enough lines.",\
                        encoding_error_code)
                self.default_encoding = encoding_lines[0].rstrip()
                for line in encoding_lines[1:]:
                    try:
                        line_tokens = line.rstrip().split(' ')
                        self.valid_encodings[line_tokens[0]] = line_tokens[1]
                    except IndexError:
                        raise EncodingException("Error: badly formed line in encoding list: {}".\
                            format(line.rstrip()), encoding_error_code)
        except IOError:
            raise EncodingException("Error: failed to load encoding list {}.".\
                format(encoding_list_file_name), encoding_error_code)


    def instantiate_encoding(self, encoding_name, encoding_options, problem, quiet):
        """ Instantiate the encoding with the given name and options.

            (EncodingWrapper, {str : str}, Problem, bool) -> None
        """
        if encoding_name not in self.valid_encodings:
            raise EncodingException("Error: invalid encoding: {}".format(encoding_name),\
                encoding_error_code)

        encoding_module = import_module(os.path.dirname(__file__).split("/")[-1] +\
            '.' + self.valid_encodings[encoding_name])
        self.encoding = getattr(encoding_module, encoding_module.encoding_class)\
            (encoding_options, problem, quiet)
