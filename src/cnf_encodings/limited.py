""" File:        encodings/flat.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2013-
    Desctiption:

    Liscence:   [Refine this!]
"""

from utilities import encoding_error_code

from encoding_base import EncodingException, Encoding

from problem   import Predicate, PredicateCondition, NotCondition, AndCondition,\
                      OrCondition, ForAllCondition, ExistsCondition,\
                      IncreaseCondition, ConditionalEffect, Action

from edgeLimitManager import edgeLimitManager

from customDebugPrint import dprint

import itertools
import pickle
import networkx as nx
#import matplotlib.pyplot as plt
import os
import random

encoding_class = 'LimitedEncoding'

def to_node(v):
    return "(" + str(v[0]) + ", " + str(v[1]) + ", " + str(v[2]) + ")"

class LimitedEncoding(Encoding):
    """ A simple flat encoding along the lines of SatPlan06 with full frame
        axioms.

        Variables and clauses are created once
    """

    def __init__(self, args, problem, quiet):
        """ Set encoding parameters

            (LimitedEncoding, { str : obj }, Problem, bool) -> None
        """
        super(LimitedEncoding, self).__init__(args, problem, quiet)

        self.execution_semantics = args["execution_semantics"]
        self.redundant_mutex = args["redundant_mutex"]
        self.min_mutex_clique_size = args["min_mutex_clique_size"]
        self.process_semantics = args["process_semantics"]

    def get_variable_name(self, step, var):
        """ Return a string containing a representation of the supplied variable
            (LimitedEncoding, int, int) -> str
        """
        v_obj, v_vars = self.variables[var]
        if v_obj == 'aux':
            return "AUX " + str(v_vars[0]) + " " + str(v_vars[1]) + " step" + str(step)
        elif isinstance(v_obj, Action):
            return v_obj.name + " " + " ".join(v_vars + ('step' + str(step),))
        elif isinstance(v_obj, Predicate):
            return v_obj.name + " " + " ".join(v_vars + ('step' + str(step),))
        else:
            return v_obj.cond_code + " " + v_obj.desc + " " +\
                " ".join(v_vars + ('step' + str(step),))


    def get_precondition_code(self, prec):
        """ Return the code used for the precondition condition

            (LimitedEncoding, (PredicateCondition, bool, (str,...))/
                (Condition, (str, ...))) -> int
        """
        if isinstance(prec[0], PredicateCondition):
            if prec[2]:
                return self.fluent_codes[(prec[0].pred, prec[0].ground_conditions[prec[1]])]
            return -self.fluent_codes[(prec[0].pred, prec[0].ground_conditions[prec[1]])]
        return self.condition_codes[prec]

    def get_effect_code(self, eff):
        """ Return the code used for the given effect condition.

            (LimitedEncoding, (PredicateCondition, bool, (str,...)) /
                (ConditionalEffect, (str, ...))) -> str / None
        """
        if isinstance(eff[0], PredicateCondition):
            if eff[2]:
                return self.fluent_codes[(eff[0].pred, eff[0].ground_conditions[eff[1]])]
            return -self.fluent_codes[(eff[0].pred, eff[0].ground_conditions[eff[1]])]
        elif isinstance(eff[0], ConditionalEffect):
            return self.cond_effect_codes[eff]
        return None


    def make_condition_clauses(self, cond, clause_list):
        """ Make the clauses for the given condition and add it to the given
            clause list.

            (LimitedEncoding, (Condition, (str,...)) -> str
        """
        assert False, "Not Implemented"
        """
        c_code = self.condition_codes[cond]
        if isinstance(cond[0], AndCondition) or isinstance(cond[0], ForAllCondition):
            #c -> ^_children
            sub_codes = []
            for cid, conj_groundings in enumerate(cond[0].ground_conditions):
                sub_cond = cond.conditions[cid]
                if isinstance(sub_cond, PredicateCondition):
                    sub_codes.append(self.get_precondition_code(\
                        (sub_cond, sub_cond.sign, conj_groundings[grounding])))
                else:
                    sub_codes.append(self.get_precondition_code(\
                        (sub_cond, conj_groundings[grounding])))

            clause_list.append([-c_code] + sub_codes)
            #v_children -> c
            for child in sub_codes:
                clause_list.append([-child, c_code])

        elif isinstance(cond[0], OrCondition) or isinstance(cond[0], ExistsCondition):
            #^_children -> c
            sub_codes = []
            for cid, conj_groundings in enumerate(cond[0].ground_conditions):
                sub_cond = cond.conditions[cid]
                if isinstance(sub_cond, PredicateCondition):
                    sub_codes.append(self.get_precondition_code(\
                        (sub_cond, sub_cond.sign, conj_groundings[grounding])))
                else:
                    sub_codes.append(self.get_precondition_code(\
                        (sub_cond, conj_groundings[grounding])))

            clause_list.append([-c_code] + sub_codes)
            #c -> v_children
            for child in sub_codes:
                clause_list.append([-child, c_code])


        else: assert False
        """

    def encode_base(self):
        """ Do any horizon-independent encoding that might be required.

           (LimitedEncoding, int, bool, bool) -> None
        """
        #Variables
        #Actions
        self.action_codes = {}
        for action in self.problem.actions.itervalues():
            for grounding in action.groundings:
                pair = (action, grounding)
                code = self.new_code(pair, (0, -2))
                #print("limited: Generated code: " + str(code))
                self.action_codes[pair] = code

        #Fluents
        self.fluent_codes = {}
        for predicate in self.problem.predicates.itervalues():
            for grounding in predicate.groundings:
                pair = (predicate, grounding)
                code = self.new_code(pair, (0, -1))
                self.fluent_codes[pair] = code

        #Fluents for DTG arc traversals
#        for predicates in self.ag_to_edge_predicates.itervalues():
#            for predicate in predicates:
#                for grounding in predicate.groundings: # only one grounding: ()
#                    pair = (predicate, grounding)
#                    code = self.new_code(pair, (0, -1))
#                    self.fluent_codes[pair] = code
            
        #Conditional Effects
        self.cond_effect_codes = {}
        for cond in self.problem.conditional_effects:
            assert False, "Conditional effects not implemented"
            for grounding in cond.groundings:
                pair = (cond, grounding)
                code = self.new_code(pair, (0, -2))
                self.cond_effect_codes[pair] = code

        #Conditions
        self.condition_codes = {}
        for cond in self.problem.conditions:
            for grounding in cond.groundings:
                pair = (cond, grounding)
                code = self.new_code(pair, (0, -2))
                self.condition_codes[pair] = code

        #Aux vars
        self.aux_codes = []

        #PREVIOUSLY STORED INVARIANTS
        self.make_invariant_clauses()

        #h_dependant clauses
        #Start State (other clauses will be added in other functions as needed)

        # Marshall edited
        for fluent, f_code in self.fluent_codes.iteritems():
            if ("DTG edge" in str(fluent[0])):
                assert False, "DTG edge in fluent affecting things.. problematic"
            if fluent in self.problem.initial_state or ("DTG edge" in str(fluent[0])): # set as true initially TODO ammend
                self.clauses.append(((0, 0), [(0, f_code)]))
            else:
                self.clauses.append(((0, 0), [(0, -f_code)]))
            self.clause_types.append("start")

        #goal clauses
        self.goal_conditions = set()

        for prec in self.problem.flat_ground_goal_preconditions:
            if not isinstance(prec[0], PredicateCondition):
                self.goal_conditions.add(prec)
            self.clauses.append(((-1, -1), [(0, self.get_precondition_code(prec))]))
            self.clause_types.append("goal")

        for gcond in self.goal_conditions:
            assert False, "Complex goal Not implemented"
            #need to turn these conditions in to clauses
            #self.make_condition_clauses(gcond, self.goal_clauses)


        #Mutex
        ac = len(self.clauses)
        conflicts_to_encode = {}
        conflicts_to_encode_list = []
        for ag_pair in self.action_codes:
            conflicts_to_encode[ag_pair] = set()

        if self.execution_semantics == "serial":
            for ag_pair1, ag_pair2 in itertools.combinations(self.action_codes, 2):
                if (self.redundant_mutex or ag_pair2 not in self.problem.eff_eff_conflicts[ag_pair1]):
                    conflicts_to_encode_list.append((ag_pair1, ag_pair2))
                    conflicts_to_encode[ag_pair1].add(ag_pair2)
                    conflicts_to_encode[ag_pair2].add(ag_pair1)
        else: #forall
            for ag_pair1, ag_pair2_list in self.problem.pre_eff_conflicts.iteritems():
                for ag_pair2 in ag_pair2_list:
                    if ag_pair1 != ag_pair2 and (self.redundant_mutex or\
                        ag_pair2 not in self.problem.eff_eff_conflicts[ag_pair1]):
                        conflicts_to_encode_list.append((ag_pair1, ag_pair2))
                        conflicts_to_encode[ag_pair1].add(ag_pair2)
                        conflicts_to_encode[ag_pair2].add(ag_pair1)

        if not self.quiet:
            print "Conflicts to encode:", len(conflicts_to_encode_list)
            print "Making conflict cliques..."
        cliques = self.make_conflict_cliques(conflicts_to_encode_list,
            conflicts_to_encode, self.action_codes)

        self.make_clique_clauses(cliques)

        if not self.quiet:
            print "Mutex clauses:", len(self.clauses) - ac

        #Action pre- and post-conditions
        #Also make the fluent pre- and post-condition lists for SSAs
        for (action, grounding), a_code in self.action_codes.iteritems():
            for prec in action.flat_ground_preconditions[grounding]:
                pre_code = self.get_precondition_code(prec)
                self.clauses.append(((0, -2), [(0, -a_code), (0, pre_code)]))
                self.clause_types.append("pre")

            #print("limited: AA example format of a precondition given for action: " + str(action) + " prec: " + str(prec[0]))
            
            # Marshall addition, add predicates for arc traversal flag
#            if (action, grounding) in self.ag_to_edge_predicates.keys():
#                for DTG_edge_predicate in self.ag_to_edge_predicates[(action, grounding)]:
#                    DTG_edge_predicate_condition = PredicateCondition(DTG_edge_predicate, DTG_edge_predicate.variables)
#                    DTG_edge_predicate_condition.ground_conditions[()] = ()
#
#                    # Getting the actual generated prec and add it to clauses
#                    prec = (DTG_edge_predicate_condition, (), True)
#                    pre_code = self.get_precondition_code(prec)
#                    self.clauses.append(((0, -2), [(0, -a_code), (0, pre_code)]))
#                    self.clause_types.append("pre")

            #print("limited: AA example format of a precondition artifiially generated: " + str(action) + " prec: " + str(prec[0]))

            for eff in action.flat_ground_effects[grounding]:
                if isinstance(eff, ConditionalEffect):
                    assert False
                    #eff_pre = [self.get_precondition_code(prec)\
                    #    for prec in eff.flat_ground_preconditions[grounding]]
                    ############# THIS NEEDS TO BE DONE
                else:
                    eff_code = self.get_effect_code(eff)
                    if eff_code is not None: #Not for increase effects
                        self.clauses.append(((0, -2), [(0, -a_code), (1, eff_code)]))
                        self.clause_types.append("eff")

            # Marshall addition, add predicates for arc traversal flag
#            DTG_edge_predicate = None
#            if (action, grounding) in self.ag_to_edge_predicates.keys():
#                for DTG_edge_predicate in self.ag_to_edge_predicates[(action, grounding)]:
#                    DTG_edge_predicate_condition = PredicateCondition(DTG_edge_predicate, DTG_edge_predicate.variables)
#                    DTG_edge_predicate_condition.ground_conditions[()] = ()
#                    eff = (DTG_edge_predicate_condition, (), False)
#                    eff_code = self.get_effect_code(eff)
#                    if eff_code is None:
#                        assert False
#                    self.clauses.append(((0, -2), [(0, -a_code), (1, eff_code)]))
#                    self.clause_types.append("eff")


        #SSAs
        #CURRENTLY JUST WITH FLUENTS - not conditional

        for (pred, grounding), f_code in self.fluent_codes.iteritems():
            add_clause = [(0, -f_code), (-1, f_code)]
            #print("123" + str(pred))
            for ag_pair in pred.ground_adds[grounding]:
                #print("123 limited: Example from ground_adds: " + str(ag_pair[0]))
                #print("456 groundings 2 types, from pred, from action: " + str(grounding) + " - " + str(ag_pair[1]))
                add_clause.append((-1, self.action_codes[ag_pair]))
            self.clauses.append(((1, -1), add_clause))
            self.clause_types.append("ssa")

            del_clause = [(0, f_code), (-1, -f_code)]
            for ag_pair in pred.ground_dels[grounding]:
                del_clause.append((-1, self.action_codes[ag_pair]))
            self.clauses.append(((1, -1), del_clause))
            self.clause_types.append("ssa")

        if self.process_semantics:
            #Process semantics clases
            #a^{t} -> V-pre(a^{t-1}) v Vmutex(a^{t-1})
            for (action, grounding), a_code in self.action_codes.iteritems():
                clause = [(0, -a_code)]
                for prec in action.flat_ground_preconditions[grounding]:
                    clause.append((-1, -self.get_precondition_code(prec)))

                #Here we want actions that are mutex with a, but which do not delete
                #The preconditions of a
                not_encode = set([a_code])
                for prec in action.flat_ground_preconditions[grounding]:
                    #Assuming STRIPS
                    if prec[2]: e_list = prec[0].pred.ground_dels[prec[0].ground_conditions[prec[1]]]
                    else: e_list = prec[0].pred.ground_adds[prec[0].ground_conditions[prec[1]]]
                    for ag_pair in e_list:
                        not_encode.add(self.action_codes[ag_pair])

                for eff in action.flat_ground_effects[grounding]:
                    if isinstance(eff[0], IncreaseCondition): continue
                    if eff[2]: e_list = eff[0].pred.ground_dels[eff[0].ground_conditions[eff[1]]]
                    else: e_list = eff[0].pred.ground_adds[eff[0].ground_conditions[eff[1]]]
                    for ag_pair in e_list:
                        a_code2 = self.action_codes[ag_pair]
                        if a_code2 not in not_encode:
                            clause.append((-1, a_code2))
                            not_encode.add(a_code2)
                    if eff[2]: e_list = eff[0].pred.ground_nprecs[eff[0].ground_conditions[eff[1]]]
                    else: e_list = eff[0].pred.ground_precs[eff[0].ground_conditions[eff[1]]]
                    for ag_pair in e_list:
                        a_code2 = self.action_codes[ag_pair]
                        if a_code2 not in not_encode:
                            clause.append((-1, a_code2))
                            not_encode.add(a_code2)
                self.clauses.append(((1, -2), clause))
                self.clause_types.append("process")

    def build_plan(self):
        """Build a plan from the true variables

            (LimitedEncoding) -> None
        """
        plan_actions = {}
        for t in xrange(self.horizon):
            plan_actions[t] = []
        for cnf_code in self.true_vars:
            if not cnf_code in self.cnf_code_to_variable.keys(): continue # Could be a counting additional variable
            
            step, var = self.cnf_code_to_variable[cnf_code]
            v_obj, v_vars = self.variables[var]
            if step < self.horizon and isinstance(v_obj, Action):
                plan_actions[step].append((v_obj, v_vars))
        self.plan = []
        for step in xrange(self.horizon):
            self.plan.append(plan_actions[step])

    # ==============================
    # Most of the Marshall Additions
    # ==============================

    def strag(self, ag):
        ret = "("
        for x in ag:
            ret+= str(x) + ", "

        return ret + ")"

    def print_cnf_code_meaning(self, cnf_code):
        dprint("==== PRINTING NEW CODE ====== " + str(cnf_code))
        if cnf_code in self.cnf_code_to_variable.keys():
            step, var = self.cnf_code_to_variable[cnf_code]
            v_obj, v_vars = self.variables[var]
            if True: #step < self.horizon:
                dprint("cnf_code: " + str(cnf_code))
                dprint("step: " + str(step))
                dprint("var: " + str(var))
                dprint("v_obj: " + str(v_obj))
                dprint("v_vars: " + str(v_vars))
        else:
            dprint("cnf_code: " + str(cnf_code))
            dprint("counting edge corresponding to: " + self.get_edge_name(self.counting_var_to_edge[cnf_code]))

    def print_all_cnf_code_meanings(self):
        dprint("==== PRINTING ALL CNF CODE MEANINGS ====")
        for cnf_code in sorted(self.cnf_code_to_variable.keys()):
            self.print_cnf_code_meaning(cnf_code)

        for cnf_code in sorted(self.counting_var_to_edge.keys()):
            self.print_cnf_code_meaning(cnf_code)

    def print_all_edge_counter_vars(self):
        # make reverse dictionary
        edge_to_vars = {}
        for var in self.counting_var_to_edge.keys():
            edge = self.counting_var_to_edge[var]
            if edge not in edge_to_vars.keys():
                edge_to_vars[edge] = set()
            edge_to_vars[edge].add(var)

        for edge in edge_to_vars.keys():
            edge_string = self.get_edge_name(edge)
            dprint("edge: " + edge_string + " has vars: " + str(sorted(edge_to_vars[edge])))

    def print_all_edge_counter_clauses(self):
        # make reverse dictionary
        edge_to_clauses = {}
        for clause in self.counting_clause_to_edge.keys():
            edge = self.counting_clause_to_edge[clause]
            if edge not in edge_to_clauses.keys():
                edge_to_clauses[edge] = set()
            edge_to_clauses[edge].add(clause)

        for edge in edge_to_clauses.keys():
            edge_string = self.get_edge_name(edge)
            dprint("edge: " + edge_string + " has clauses: " + str(sorted(edge_to_clauses[edge])))

    def print_UNSAT_core(self, UNSAT_core):
        dprint("UNSAT core:")
        for clause in UNSAT_core:
            dprint("== CLAUSE ==")
            for lit in self.get_list_clause(clause):
                if lit>0:
                    dprint(" + ")
                else:
                    dprint(" - ")
                self.print_cnf_code_meaning(abs(lit))

    def print_problematic_literals(self, problematic_literals = None):
        """Build a plan from the true variables

            (LimitedEncoding) -> None
        """
    
        dprint("========= printing all given problematic literals ======")
        if problematic_literals == None:
            problematic_literals = self.cnf_code_to_variable.keys()
            
        for cnf_code in problematic_literals:
            step, var = self.cnf_code_to_variable[cnf_code]
            v_obj, v_vars = self.variables[var]
            if step < self.horizon:# and isinstance(v_obj, Action):
                dprint("variable: " + str(cnf_code) + " corresponding to:")
                dprint("    actual step, v_obj, v_vars: " + str(step) + " " + str(v_obj) + " " + str(v_vars))

    # ================
    # Actual Functions
    # ================
    
    def set_relaxed_plan(self, relaxed_plan):
        self.relaxed_plan = relaxed_plan
    
    def longest_simple_path_length_helper(self, DTG, current_node, unvisited):
        longest_path_from_here = 0
        for neighbour in DTG.neighbors(current_node):
            if neighbour in unvisited:
                unvisited_without_neighbour = unvisited.copy()
                unvisited_without_neighbour.remove(neighbour)
                longest_path_from_here = max(longest_path_from_here, self.longest_simple_path_length_helper(DTG, neighbour, unvisited_without_neighbour))
        return longest_path_from_here + 1

    def longest_simple_path_length(self, DTG, initial_node):
        if initial_node:
            return self.longest_simple_path_length_helper(DTG, initial_node, set(DTG.nodes))
        return max([self.longest_simple_path_length_helper(DTG, node, set(DTG.nodes)) for node in DTG.nodes])

    def get_pgb_name(self, pgb):
        (p,g,b) = pgb
        return str(p) + " : " + str([str(x) for x in g]) + " : " + str(b)

    def get_ag_name(self, ag):
        (a,g) = ag
        return str(a) + " : " + str([str(x) for x in g])
    
    def get_edge_name(self, edge):
        (pre, eff) = edge
        return "Crossing DTG edge allowed for[" + str(pre[0]) + ", " + str(pre[1]) + ", " + str(pre[2]) + "] -> [" + str(eff[0]) + ", " + str(eff[1]) + ", " + str(eff[2]) + "] END_NAME"

    def get_edge_obj_name(self, edge):
        return "( " + self.get_edge_name(edge) + "  )"

    def is_DTG_edge(self, var):
        return "Crossing DTG edge allowed for" in var and "END_NAME" in var

    def is_action(self, var):
        v_obj, v_vars = self.variables[var]
        return isinstance(v_obj, Action)

    def literal_not(self,x):
        if isinstance(x, bool):
            return not x
        else:
            return -x

    def get_from_list(self, var_list, index):
        if index == -1: return True
        elif index == len(var_list): return False
        else: return var_list[index]

    # ====================
    # Main Large Functions
    # ====================
    def load_DTG_edges(self, DTG_use_existing, ag_to_edges_filename):
        with open(ag_to_edges_filename, "r") as ag_to_edges_file:
            string_ag_to_edges = pickle.load(ag_to_edges_file)
        
        # make a dictionary mapping the string encoding of ag's to the actual ag's
        string_ag_to_ag = {}
        string_pgb_to_pgb = {}
        for action in self.problem.actions.itervalues():
            for grounding in action.groundings:
                ag = (action, grounding)
                string_ag_to_ag[self.get_ag_name(ag)] = ag
                for pre in action.strips_preconditions[grounding]: # pre, eff same format as node
                    for eff in action.strips_effects[grounding]:
                        if eff[2] and pre[2]: # TODO REVIEW: always true. If positive proposition in pre and eff
                            string_pgb_to_pgb[self.get_pgb_name(pre)] = pre
                            string_pgb_to_pgb[self.get_pgb_name(eff)] = eff

        # Make a dictionary mapping ag to edges (convert from loaded one)
        self.ag_to_edges = {}
        for string_ag, set_string_edges in string_ag_to_edges.items():
            for (string_edge_0, string_edge_1) in set_string_edges:
                ag = string_ag_to_ag[string_ag]
                edge = (string_pgb_to_pgb[string_edge_0], string_pgb_to_pgb[string_edge_1])
                if ag not in self.ag_to_edges.keys():
                    self.ag_to_edges[ag] = set()
                self.ag_to_edges[ag].add(edge)

        # Create Reverse Dictionary
        self.edge_to_ag = {} # TODO fix naming scheme
        for ag in self.ag_to_edges.keys():
            for edge in self.ag_to_edges[ag]:
                if edge not in self.edge_to_ag.keys():
                    self.edge_to_ag[edge] = set()
                self.edge_to_ag[edge].add(ag)

        # Test, to know every action corresponds to at least one edge
        #for ag in string_ag_to_ag.values():
        #    assert ag in self.ag_to_edges.keys()
        #    assert len(self.ag_to_edges[ag])!=0

    def calculate_DTG_edges(self, state_mutexes, DTG_use_existing, ag_to_edges_filename):
        """ Analyses the state mutexes to create a set of edges, and the actions which cross them """

        # Generate state mutex graph just for visuaization
        '''
        G_VISUALIZE = nx.Graph()
        for v in state_mutexes.keys():
            if v[2]:
                G_VISUALIZE.add_node(to_node(v))
        for v1 in state_mutexes.keys():
            for v2 in state_mutexes[v1]:
                if v1[2] and v2[2]:
                    G_VISUALIZE.add_edge(to_node(v1),to_node(v2))
        pos = nx.spring_layout(G_VISUALIZE)
        nx.draw(G_VISUALIZE, pos, font_size=16, with_labels=False)
        for p in pos:
            pos[p][1] += 0.01
        nx.draw_networkx_labels(G_VISUALIZE, pos)
        #plt.show() # Actually show it
        '''

        # Generate state mutex Graph
        G = nx.Graph()
        for v in state_mutexes.keys():
            if v[2]:
                G.add_node(v)
        for v1 in state_mutexes.keys():
            for v2 in state_mutexes[v1]:
                if v1[2] and v2[2]:
                    G.add_edge(v1,v2)

        # Find DTGs from maximal cliques
        DTG_edge_constraints = 0 # Keep a counter to display later
        self.edge_to_ag = {} # dictionary from DTG edges to the action that caused them
        ag_to_edge_predicates = {} # dictionary from action grouding to DTG edge predicate
        DTGs = []

        # Find maximal cliques
        while True:
            # find approximate maximal clique
            cliques = list(nx.find_cliques(G))
            if len(cliques) == 0:
                break
            G_prime_list = sorted(cliques, key=len)[-1]
            if len(G_prime_list) == 1:
                break            
         
            # Found approximate max clique and use it
            G_prime = nx.DiGraph()
            G_prime.add_nodes_from(G_prime_list)

            # Dictionary of predicates to keep track of
            edge_to_predicate = {}
            
            for action in self.problem.actions.itervalues():
                for grounding in action.groundings:
                    for pre in action.strips_preconditions[grounding]: # pre, eff same format as node
                        if pre in G_prime:
                            for eff in action.strips_effects[grounding]:
                                if eff in G_prime:
                                    if eff[2] and pre[2]: # TODO REVIEW: always true. If positive proposition in pre and eff
                                        # pre and effect in G_prime, are "linked" already as come from clique
                                       
                                        DTG_edge_constraints += 1 # Increment counter

                                        # add all edges to universal edge list
                                        edge = (pre,eff)
                                        ag = (action, grounding)

                                        G_prime.add_edge(pre, eff)
                                        if not edge in self.edge_to_ag.keys():
                                            self.edge_to_ag[edge] = []
                                        self.edge_to_ag[edge].append(ag)

                DTGs.append(G_prime)
            [G.remove_node(node) for node in G_prime_list]

        # Create reverse dictionary for later retrieval
        self.ag_to_edges = {}
        for edge in self.edge_to_ag.keys():
            for ag in self.edge_to_ag[edge]:
                if not ag in self.ag_to_edges.keys():
                    self.ag_to_edges[ag] = set()
                self.ag_to_edges[ag].add(edge)

        # See if there are double ups
        #for ag in self.ag_to_edges.keys():
        #    if len(self.ag_to_edges[ag]) >= 2:
        #        assert False, "Found one: " + str([str(x) for x in ag]) + str([[[str(z) for z in x] for x in y] for y in self.ag_to_edges[ag]])

        # Save self.ag_to_edges to file for later use
        # Create a text version for saving
        text_ag_to_edges = {}
        for ag in self.ag_to_edges.keys():
            text_ag_to_edges[self.get_ag_name(ag)] = set((self.get_pgb_name(edge_0), self.get_pgb_name(edge_1)) for (edge_0, edge_1) in self.ag_to_edges[ag])
        with open(ag_to_edges_filename, "w") as ag_to_edges_file:
            pickle.dump(text_ag_to_edges, ag_to_edges_file)

        # Save found edges to edgeLimiteManager
        #self.add_initial_edge_crossing_limits()
        
        # Save for use later TODO review ue of this...
        #self.ag_to_edge_predicates = ag_to_edge_predicates

    def get_new_horizon(self):
        # naive method sum over all edge limits
        new_horizon = 0
        for edge in self.edgeLimitManager.edges.keys():
            new_horizon += self.edgeLimitManager.edges[edge]
        dprint("Calculate new horizon: " + str(max(1,new_horizon)))
        return max(1,new_horizon) # do max with 1 as 
        
    def set_initial_edge_crossing_limits(self):
        """ After finding edges, set them to to an initial value """
        # split into 1 or 0 depending on relaxed plangraph
        limit_0_edges = set(self.edge_to_ag.keys())
        limit_1_edges = set()


        
        for ags in self.relaxed_plan:
            for ag in ags:

                
                if ag not in self.ag_to_edges.keys():
                    #assert False, "ag in relaxed plan that does not have a corresponding edge: " + self.strag(ag)
                    continue #skip it TODO review might be dangerous

                
                edges = self.ag_to_edges[ag]
                for edge in edges:
                    if edge in limit_0_edges:
                        limit_0_edges.remove(edge)
                        limit_1_edges.add(edge)

        #dprint("limit_0_edges: " + str([self.get_edge_name(edge) for edge in limit_0_edges]))
        #dprint("limit_1_edges: " + str([self.get_edge_name(edge) for edge in limit_1_edges]))
        for edge in limit_1_edges:
            self.edgeLimitManager.edges[self.get_edge_obj_name(edge)] = 1
        for edge in limit_0_edges:
            self.edgeLimitManager.edges[self.get_edge_obj_name(edge)] = 0
#        self.edgeLimitManager.add_edges([(self.get_edge_obj_name(edge), 1) for edge in limit_1_edges])
#        if len(limit_0_edges) > 0:
#            self.edgeLimitManager.add_edges([(self.get_edge_obj_name(edge), 0) for edge in limit_0_edges])        
     
    def add_edge_counters(self, DTG_use_existing, tmp_path, args):
        ''' Add variables, clauses to encode the edge crossing limits '''
        # For debugging, special problematic edges
        #self.n_to_e = [x for x in self.edge_to_ag.keys() if "at_n  ), (), True] -> [( at_e" in self.get_edge_obj_name(x)][0]
        #self.s_to_n = [x for x in self.edge_to_ag.keys() if "at_s  ), (), True] -> [( at_n" in self.get_edge_obj_name(x)][0]
        
        counting_clauses = []

        # use edge to ag, and calculate ag to vars
        edge_to_cnf_codes = {}

        # Calculate edge to cnf var list
        for cnf_code in self.cnf_code_to_variable.keys():
            step, var = self.cnf_code_to_variable[cnf_code]
            v_obj, v_vars = self.variables[var]
            if isinstance(v_obj, Action):
                #dprint("ag_to_edge keys: " + str(["(" + str(x[0]) + ","+ str(x[1]) + ")" for x in self.ag_to_edges.keys()]))
                #dprint("edge: " + str(edge[0][0]) + str(edge[0][1]) + str(edge[1][0]) + str(edge[1][1]))



                if (v_obj, v_vars) not in self.ag_to_edges.keys(): # TODO quick fix review
                    #assert False, "TODO REVIEW THIS"
                    continue



                
                edges = self.ag_to_edges[(v_obj, v_vars)]
                for edge in edges:
                    if edge not in edge_to_cnf_codes.keys():
                        edge_to_cnf_codes[edge] = set()
                    edge_to_cnf_codes[edge].add(cnf_code)                

        # Testing that result
        #dprint("Testing result")
        for edge, cnf_codes in edge_to_cnf_codes.items():
            actions = []
            for cnf_code in cnf_codes:
                step, var = self.cnf_code_to_variable[cnf_code]
                v_obj, v_vars = self.variables[var]
                actions.append((v_obj,step))
            #dprint("edge to actions: " + self.get_edge_name(edge) + " : " + str([str(a) + str(b) for a,b in actions]))

#        for edge in self.edge_to_ag.keys():
#            assert len(self.edge_to_ag[edge]) <= 1, "An edge has more than 1 action grounding attached to it (Unseen)"
            
        # for each edge, encode it with clauses and variables
        for edge in sorted(edge_to_cnf_codes.keys(), key=self.get_edge_name):
            #dprint("dealing with edge: " + self.get_edge_name(edge))
            # Generate a tree for each set of cnf codes
            cnf_codes = edge_to_cnf_codes[edge]
            remaining_nodes = [[cnf_code] for cnf_code in sorted(cnf_codes)]
            #dprint("initial cnf codes for edge:" + self.get_edge_name(edge) + " : " + str(sorted(remaining_nodes)))
            additional_clauses = []
            count_limit_clauses = []
            
            # Find limit
            limit = self.edgeLimitManager.edges[self.get_edge_obj_name(edge)]

            # If relaxing edge:
            if args.DTG_nonzero_as_infinite:
                if limit != 0:
                    continue

            if limit in [0,1]: # special case when limit low, can encode simpler
                neg_cnf_codes = [-x for x in cnf_codes]
                new_clauses = itertools.combinations(neg_cnf_codes,limit+1)
                
                for clause in new_clauses:
                # Get cnf written form of clause
                    self.counting_clause_to_edge[self.get_string_clause(clause)] = edge
                    for lit in clause:
                        #all_vars_for_debug.add(abs(lit))
                        self.counting_var_to_edge[abs(lit)] = edge
                continue
                
            # iterate over parent nodes
            while len(remaining_nodes)>1: # when only one, don't need to combine any more
                # at least 2 exist
                alpha = remaining_nodes.pop()
                beta = remaining_nodes.pop()
                
                # assign new variables for the parent
                omega_length = min(limit+1, len(alpha) + len(beta))
                omega = [self.manual_new_cnf_code(("Intermediate for counting", ()), (0,0)) for i in range(omega_length)]

                for a_index in range(-1, len(alpha)):
                    for b_index in range(-1, len(beta)):
                        o_index = a_index + b_index + 1
                        
                        if o_index >= len(omega):
                            continue # ignore counting this high

                        a = self.get_from_list(alpha, a_index)
                        ap = self.get_from_list(alpha, a_index+1)
                        b = self.get_from_list(beta, b_index)
                        bp = self.get_from_list(beta, b_index+1)
                        o = self.get_from_list(omega, o_index)
                        op = self.get_from_list(omega, o_index+1)
                        
                        C_1 = [self.literal_not(a), self.literal_not(b), o]
                        C_2 = [ap, bp, self.literal_not(op)]
                        
                        additional_clauses.append(C_1)
                        additional_clauses.append(C_2)

                        # Do count limiting
                        if len(omega) > limit:
                            assert len(omega) == limit+1
                            count_limit_clauses.append([-omega[limit]]) # negative unit

                remaining_nodes.insert(0,omega)

            # Remove Falses, Trues
            additional_clauses_no_false = [[lit for lit in clause if not (lit == False and isinstance(lit, bool))] for clause in additional_clauses]
            additional_clauses_no_bool = [clause for clause in additional_clauses_no_false if (len([lit for lit in clause if (lit==True and isinstance(lit,bool))]) == 0) and (len(clause) > 0)]
            counting_clauses.extend(additional_clauses_no_bool)

            # Encode constraints
            # Limit applies to the root of the tree
            assert len(remaining_nodes) == 1
            root_counter = remaining_nodes[0]
            
            if limit > len(root_counter): # TODO review this
                assert False # too high ignore, shouldn't get to this point...

            counting_clauses.extend(count_limit_clauses)

            # Mark clauses relevent to this edge
            all_added_clauses = [] # TODO there are duplicates
            all_added_clauses.extend(additional_clauses_no_bool)
            all_added_clauses.extend(count_limit_clauses)
            all_vars_for_debug = set()
            for clause in all_added_clauses:
                # Get cnf written form of clause
                self.counting_clause_to_edge[self.get_string_clause(clause)] = edge
                for lit in clause:
                    all_vars_for_debug.add(abs(lit))
                    self.counting_var_to_edge[abs(lit)] = edge

            #dprint("all added clauses: " + str(len(sorted(all_added_clauses))) + " :"+ str(sorted(all_added_clauses)))
            #dprint("all_vars: " + str(sorted(all_vars_for_debug)))            
        
                
    def get_problematic_DTG_edges(self, tmp_path, exp_name):
        ''' Use known codes and the UNSAT file to generate set of problematic edges '''
        # Write problematic DTG edge file
        # Load UNSAT core file
        cnf_UNSAT_core_file_name = os.path.join(tmp_path, exp_name + "_UNSAT_core.cnf")
        with file(cnf_UNSAT_core_file_name, 'r') as UNSAT_core_file:
            UNSAT_core_lines = UNSAT_core_file.readlines()
            UNSAT_core = [line.rstrip() for line in UNSAT_core_lines[1:]]
            dprint("analyzing UNSAT core of length: " + str(len(UNSAT_core)))

            # find edges by clauses in unsat core
            problematic_edges = set()
            for clause in UNSAT_core:
                if clause in self.counting_clause_to_edge.keys():
                    problematic_edges.add(self.counting_clause_to_edge[clause])
            
            # get problematic edges by looking all vars (has problems)
            '''
            # Using another method to get problematic edges, because of the unsat cure extraction method, rely on the vars themself. TODO could use var info to relax further? eg higher ups relax more
            problematic_vars = set()
            problematic_edges = set()
            for unsat_core_clause in UNSAT_core:
                for var in unsat_core_clause.split(" ")[:-1]:
                    problematic_vars.add(abs(int(var)))

            # From the set, find responsible edges (as each clause only has ony one type)
            for problematic_var in problematic_vars:
                if problematic_var in self.counting_var_to_edge.keys():
                    # corresponds to a var used in counting clauses
                    problematic_edges.add(self.counting_var_to_edge[problematic_var])
            '''

        # For debugguing print what the vars are
        #self.print_UNSAT_core(UNSAT_core)
        #self.print_all_edge_counter_vars()
        #self.print_all_cnf_code_meanings()
        #self.print_all_edge_counter_clauses()
        self.problematic_edges = problematic_edges

    def handle_problematic_edges(self, DTG_increment_parallel, DTG_sole_random_child, new_save_start):
        """ After recieving problematic edges calculated seperately, decide here what to do with them """

        if not DTG_increment_parallel and DTG_sole_random_child:
            assert "Trying to get a sole random child without getting children in parallel"
            
        # Have problematic edges, need a scheme to increment them
        # Don't actually change THIS file, just affect later ones

        child_number = 0
        if len(self.problematic_edges) == 0:
            #dprint("no problematic edges found, creating next node with no changes
            #dprint("no_problematic_edges_found")
            children = self.edgeLimitManager.get_no_change_children([self.get_edge_obj_name(x) for x in self.problematic_edges]) # will be just one
        else: # have some, what to do with them...
            if DTG_increment_parallel:
                if DTG_sole_random_child:
                    # If random get them all and only choose one randomly
                    children = self.edgeLimitManager.get_1_inc_children([self.get_edge_obj_name(x) for x in self.problematic_edges])
                    children = [children[random.randint(0,len(children)-1)]]
                else:
                    children = self.edgeLimitManager.get_1_inc_children([self.get_edge_obj_name(x) for x in self.problematic_edges])
            else:
                children = self.edgeLimitManager.get_all_inc_children([self.get_edge_obj_name(x) for x in self.problematic_edges])

        new_save_number = new_save_start
        for child in children:
            child.filename = "@".join(child.filename.split("@")[:-1]) + "@" + str(new_save_number)
            dprint("creating_child_with_name:" + child.filename.split("/")[-1])
            child.save()
            child_number += 1
            new_save_number += 1

        dprint("new edgeLimitManager instances created: " + str(child_number))

        # Print out mapping:
        #for clause, edge in self.counting_clause_to_edge.items():
        #    dprint("Edge: " + self.get_edge_obj_name(edge) + " maps to: " + str(clause))
