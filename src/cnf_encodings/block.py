""" File:        encodings/block.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2014
    Desctiption: 

    Liscence:   [Refine this!]
"""

from utilities import encoding_error_code

from encoding_base import EncodingException, Encoding

from problem   import Predicate, PredicateCondition, NotCondition, AndCondition,\
                      OrCondition, ForAllCondition, ExistsCondition,\
                      IncreaseCondition, ConditionalEffect, Action

import itertools, sys,  networkx

from collections import defaultdict, deque

encoding_class = 'BlockEncoding'

tree = lambda: defaultdict(tree)

class BlockEncoding(Encoding):
    """ Variables and clauses are created once """
    
    def __init__(self, args, problem, quiet):
        """ Set encoding parameters
            
            (BlockEncoding, { str : obj }, Problem, bool) -> None
        """
        super(BlockEncoding, self).__init__(args, problem, quiet)


    def get_action_name(self, action):
        """ Return a string representing the action (str/int/(A/g))
            (BlockEncoding, str/int/(Action,(str))) -> str
        """
        if isinstance(action, str): return action
        elif isinstance(action, int): return "b" + str(action)
        return action[0].name + " " + " ".join(action[1])
    
    def is_action(self, var):
        v_objs = self.variables[var]
        return v_objs[0] == "act"

    def get_variable_name(self, step, var):
        """ Return a string containing a representation of the supplied variable
            
            (BlockEncoding, int, int) -> str
        """
        
        v_objs = self.variables[var]
        if v_objs[0] == "block":
            return "block " + str(v_objs[1]) + " step" + str(step)
        
        elif v_objs[0] in ["pre", "eff"]:
            block, (pred, grounding) = v_objs[1:]
            return v_objs[0] + " b" + str(block) + " " + pred.name +\
                " " + " ".join(grounding) + " step" + str(step)
        
        elif v_objs[0] == "act":
            block, (action, grounding) = v_objs[1:]
            return "act b" + str(block) + " " + action.name + " " + " ".join(grounding) +\
                " step" + str(step)
        
        elif v_objs[0] == "sup":
            block, action1, literal, action2 = v_objs[1:]
            literal_str = literal[0].name + " " + " ".join(literal[1])
            if not literal[2]: literal_str = "-" + literal_str
            return "sup b" + str(block) + " " + self.get_action_name(action1) + " : " +\
                literal_str + " : " + self.get_action_name(action2) + " step" + str(step)
        
        elif v_objs[0] == "ord":
           block, action1, action2 = v_objs[1:]
           return "ord b" + str(block) + " " + self.get_action_name(action1) + " : " +\
               self.get_action_name(action2) + " step" + str(step)
        
        elif v_objs[0] == "intra_b":
            block, ag_pair, fluent = v_objs[1:]   
            return "intra_b b" + str(block) + " " + self.get_action_name(ag_pair) + " : " +\
                fluent[0].name + " " + " ".join(fluent[1]) + " step" + str(step)
        
        elif v_objs[0] == "intra_a":
            block, ag_pair, eff = v_objs[1:]
            return "intra_a b" + str(block) + " " + self.get_action_name(ag_pair) + " : " +\
                eff[0].name + " " + " ".join(eff[1]) + " " + str(eff[2]) + " step" + str(step)
            
        
        assert False
 
    def get_action_code(self, block, action):
        """  Return the CNF code for the given action/block.
             Return None if a start/end action are passed.
        
            (BlockEncoding, int, (Action, (str,))/int) -> int
        """
        if isinstance(action, int):
            return self.block_codes[action]
        elif isinstance(action, tuple):
            return self.block_action_codes[block][action]
        return None
        
    
    def close_orderings(self, orderings, orderings_rev):
        """ Add the transitive closure to the ordering and reverse ordering dictionaries.
            The reverse ordering dictionary should be empty to start with.
            
            (BlockEncoding, defaultdict(set), defaultdict(set)) -> None
        """
        ordering_queue = deque()
        for action1, actions in orderings.iteritems():
            for action2 in actions:
                ordering_queue.append((action1, action2))
                orderings_rev[action2].add(action1)
        while ordering_queue:
            action1, action2 = ordering_queue.popleft()
            for action3 in orderings[action2]:
                if action1 != action3 and action3 not in orderings[action1]:
                    orderings[action1].add(action3)
                    orderings_rev[action3].add(action1)
                    ordering_queue.append((action1, action3))
            for action3 in orderings_rev[action1]:
                if action2 != action3 and action2 not in orderings[action3]:
                    orderings[action3].add(action2)
                    orderings_rev[action2].add(action3)
                    ordering_queue.append((action3, action2))

    def make_causal_block_codes(self, block):
        """ Make the CNF codes for the given block.
            
            This uses a causal encoding.
        
            (BlockEncoding, int) -> None
        """
        
        self.block_codes[block] = self.new_code(("block", block), self.hm1_steps)
        
        self.block_pre_codes[block] = {}
        for pre in self.block_preconditions[block]:
            self.block_pre_codes[block][pre] = self.new_code(("pre", block, pre), self.hm1_steps)
        
        self.block_eff_codes[block] = {}
        for eff in self.block_effects[block]:
            self.block_eff_codes[block][eff] = self.new_code(("eff", block, eff), self.hm1_steps)
        
        self.block_action_codes[block] = {}
        for action in self.block_actions[block]:
            self.block_action_codes[block][action] = self.new_code(("act", block, action), self.hm1_steps)
        
        action_orderings = defaultdict(set)
        
        self.block_sup_codes[block] = tree()
        self.block_sup_codes_rev[block] = tree()
        
        #a_start
        for pre in self.block_preconditions[block]:
            for sign in [True, False]:
                pre_sign = pre + (sign,)
                if sign: a_list = pre[0].ground_precs[pre[1]]
                else:    a_list = pre[0].ground_nprecs[pre[1]]
                for action in a_list:
                    if action in self.block_actions[block]:
                        code = self.new_code(("sup", block, self.a_start, pre_sign, action), self.hm1_steps)
                        self.block_sup_codes[block][self.a_start][pre_sign][action] = code
                        self.block_sup_codes_rev[block][action][pre_sign][self.a_start] = code 
                        action_orderings[self.a_start].add(action)
                
                if pre in self.block_effects[block]:
                    code = self.new_code(("sup", block, self.a_start, pre_sign, self.a_end), self.hm1_steps)
                    self.block_sup_codes[block][self.a_start][pre_sign][self.a_end] = code
                    self.block_sup_codes_rev[block][self.a_end][pre_sign][self.a_start] = code 
                    action_orderings[self.a_start].add(self.a_end)
                
                for parent in self.block_graph.predecessors_iter(block):
                    if pre in self.block_preconditions[parent]:
                        code = self.new_code(("sup", block, self.a_start, pre_sign, parent), self.hm1_steps)
                        self.block_sup_codes[block][self.a_start][pre_sign][parent] = code
                        self.block_sup_codes_rev[block][parent][pre_sign][self.a_start] = code
                        action_orderings[self.a_start].add(parent)
        
        #a_end
        for eff in self.block_effects[block]:
            for sign in [True, False]:
                eff_sign = eff + (sign,)
                if sign: a_list = eff[0].ground_adds[eff[1]]
                else:    a_list = eff[0].ground_dels[eff[1]]
                for action in a_list:
                    if action in self.block_actions[block]:
                        code = self.new_code(("sup", block, action, eff_sign, self.a_end), self.hm1_steps)
                        self.block_sup_codes[block][action][eff_sign][self.a_end] = code
                        self.block_sup_codes_rev[block][self.a_end][eff_sign][action] = code
                        action_orderings[action].add(self.a_end)
                
                for parent in self.block_graph.predecessors_iter(block):
                    if eff in self.block_effects[parent]:
                        code = self.new_code(("sup", block, parent, eff_sign, self.a_end), self.hm1_steps)
                        self.block_sup_codes[block][parent][eff_sign][self.a_end] = code
                        self.block_sup_codes_rev[block][self.a_end][eff_sign][parent] = code
                        action_orderings[parent].add(self.a_end)
     
        #Action to action/block
        for ag_pair in self.block_actions[block]:
            action, grounding = ag_pair
            for eff in action.strips_effects[grounding]:
                if eff[2]: a_list = eff[0].ground_precs[eff[1]]
                else:      a_list = eff[0].ground_nprecs[eff[1]]
                for ag_pair2 in a_list:
                    if ag_pair == ag_pair2 or ag_pair2 not in self.block_actions[block]: continue
                    code = self.new_code(("sup", block, ag_pair, eff, ag_pair2), self.hm1_steps)
                    self.block_sup_codes[block][ag_pair][eff][ag_pair2] = code
                    self.block_sup_codes_rev[block][ag_pair2][eff][ag_pair] = code
                    action_orderings[ag_pair].add(ag_pair2)
            
            for parent in self.block_graph.predecessors_iter(block):
                if eff in self.block_preconditions[parent]:
                    code = self.new_code(("sup", block, ag_pair, eff, parent), self.hm1_steps)
                    self.block_sup_codes[block][ag_pair][eff][parent] = code
                    self.block_sup_codes_rev[block][parent][eff][ag_pair] = code
                    action_orderings[ag_pair].add(parent)
        
        #Block to action/block
        for parent in self.block_graph.predecessors_iter(block):
            for eff in self.block_effects[parent]:
                for sign in [True, False]:
                    eff_sign = eff + (sign,)
                    if sign: a_list = eff[0].ground_precs[eff[1]]
                    else:    a_list = eff[0].ground_nprecs[eff[1]]
                    for ag_pair in a_list:
                        if ag_pair not in self.block_actions[block]: continue
                        code = self.new_code(("sup", block, parent, eff_sign, ag_pair), self.hm1_steps)
                        self.block_sup_codes[block][parent][eff_sign][ag_pair] = code
                        self.block_sup_codes_rev[block][ag_pair][eff_sign][parent] = code
                        action_orderings[parent].add(ag_pair)
            
                    for parent2 in self.block_graph.predecessors_iter(block):
                        if parent2 == parent or eff not in self.block_preconditions[parent2]: continue
                        code = self.new_code(("sup", block, parent, eff_sign, parent2), self.hm1_steps)
                        self.block_sup_codes[block][parent][eff_sign][parent2] = code
                        self.block_sup_codes_rev[block][parent2][eff_sign][parent] = code
                        action_orderings[parent].add(parent2)

        #Threats mean that we may require additional orders -- i.e. threatening
        #actions may be ordered before or after the start and end of a causal link
        for action1, sup_literals in self.block_sup_codes[block].iteritems():
            for lit, sup_actions in sup_literals.iteritems():
                prop = lit[:2]
                for action2, sup_code in sup_actions.iteritems():   
                    if action1 == self.a_start and action2 == self.a_end: continue
                    #Threatening actions
                    if lit[2]: a_list = lit[0].ground_dels[lit[1]]
                    else:      a_list = lit[0].ground_adds[lit[1]]
                    for action3 in a_list:
                        if action3 == action1 or action3 == action2 or\
                            action3 not in self.block_actions[block]: continue
                        if action1 != self.a_start:
                            action_orderings[action3].add(action1)
                        if action2 != self.a_end:
                            action_orderings[action2].add(action3) 
                    #Threatening blocks
                    for parent in self.block_graph.predecessors_iter(block):
                        if parent == action1 or parent == action2 or\
                            prop not in self.block_effects[parent]: continue
                        if action1 != self.a_start:
                            action_orderings[parent].add(action1)
                        if action2 != self.a_end:
                            action_orderings[action2].add(parent)

        reverse_orderings = defaultdict(set)
        self.close_orderings(action_orderings, reverse_orderings)

        self.block_ord_codes[block] = tree()
        for action1, actions in action_orderings.iteritems():
            for action2 in actions:
                self.block_ord_codes[block][action1][action2] =\
                    self.new_code(("ord", block, action1, action2), self.hm1_steps)

    
    def make_causal_block_clauses(self, block):
        """ Encode the causal block.
        
            (BlockEncoding, int) -> None
        """
        #Action implies causal support
        for ag_pair, a_code in self.block_action_codes[block].iteritems():
            action, grounding = ag_pair
            for pre in action.strips_preconditions[grounding]:
                clause = [(0, -a_code)]
                for sup_code in self.block_sup_codes_rev[block][ag_pair][pre].itervalues():
                    clause.append((0, sup_code))
                self.clauses.append(((0, -2), clause))
                self.clause_types.append("causal_support")
        
        """
        #Action must causally support something
        for action, sup_lits in self.block_sup_codes[block].iteritems():
            a_code = self.get_action_code(block, action)
            if a_code is None: continue
            clause = [(0, -a_code)]
            for lit, actions in sup_lits.iteritems():
                for sup_code in actions.itervalues():
                    clause.append((0, sup_code))
            self.clauses.append(((0, -2), clause))
            self.clause_types.append("must-support")
        
        
        #Blocks must have one of their children in them
        clause = [(0, -self.block_codes[block])]
        for a_code in self.block_action_codes[block].itervalues():
            clause.append((0, a_code))
        for parent in self.block_graph.predecessors_iter(block):
            clause.append((0, self.block_codes[parent]))
        self.clauses.append(((0, -2), clause))
        self.clause_types.append("non-empty-block")    
        """
        
        #Support implies actions and ordering axioms
        for action1, sup_literals in self.block_sup_codes[block].iteritems():
            a1_code = self.get_action_code(block, action1)
            for lit, sup_actions in sup_literals.iteritems():
                for action2, sup_code in sup_actions.iteritems():
                    a2_code = self.get_action_code(block, action2)
                    if a1_code is not None:
                        self.clauses.append(((0, -2), [(0, -sup_code), (0, a1_code)]))
                        self.clause_types.append("sup_imp_act")
                    if a2_code is not None:
                        self.clauses.append(((0, -2), [(0, -sup_code), (0, a2_code)]))
                        self.clause_types.append("sup_imp_act")
                    self.clauses.append(((0, -2),
                        [(0, -sup_code), (0, self.block_ord_codes[block][action1][action2])]))
                    self.clause_types.append("sup_imp_ord")

        #Causal support threat axioms
        for action1, sup_literals in self.block_sup_codes[block].iteritems():
            for lit, sup_actions in sup_literals.iteritems():
                prop = lit[:2]
                for action2, sup_code in sup_actions.iteritems():     
                    #Threatening actions
                    if lit[2]: a_list = lit[0].ground_dels[lit[1]]
                    else:      a_list = lit[0].ground_adds[lit[1]]
                    for action3 in a_list:
                        if action3 == action1 or action3 == action2 or\
                            action3 not in self.block_actions[block]: continue
                        clause = [(0, -sup_code), (0, -self.get_action_code(block, action3))]
                        if action1 in self.block_ord_codes[block][action3]:
                            clause.append((0, self.block_ord_codes[block][action3][action1]))
                        if action3 in self.block_ord_codes[block][action2]:
                            clause.append((0, self.block_ord_codes[block][action2][action3]))
                        self.clauses.append(((0, -2), clause))
                        self.clause_types.append("threat_action")
                        
                    #Threatening blocks
                    for parent in self.block_graph.predecessors_iter(block):
                        if parent == action1 or parent == action2 or\
                            prop not in self.block_effects[parent]: continue
                        clause = [(0, -sup_code), (0, -self.get_action_code(block, parent))]
                        if lit[2]: clause.append((0, self.block_eff_codes[parent][prop]))
                        else:      clause.append((0, -self.block_eff_codes[parent][prop]))
                        if action1 in self.block_ord_codes[block][parent]:
                            clause.append((0, self.block_ord_codes[block][parent][action1]))
                        if parent in self.block_ord_codes[block][action2]:
                            clause.append((0, self.block_ord_codes[block][action2][parent]))
                        self.clauses.append(((0, -2), clause))
                        self.clause_types.append("threat_block")
                        
        #Ordering implies actions
        for action1, action_ords in self.block_ord_codes[block].iteritems():
            a1_code = self.get_action_code(block, action1)
            for action2, a1_a2_ord_code in action_ords.iteritems():
                a2_code = self.get_action_code(block, action2)
                if a1_code is not None:
                    self.clauses.append(((0, -2), [(0, -a1_a2_ord_code), (0, a1_code)]))
                    self.clause_types.append("ord_imp_act")
                if a2_code is not None:
                    self.clauses.append(((0, -2), [(0, -a1_a2_ord_code), (0, a2_code)]))
                    self.clause_types.append("ord_imp_act")
         
           
        #Ordering transitivity  ################### FIGURE OUT WHY BLOCK_ORD_CODES CHANING SIZE
        trans_clauses = set()
        for action1, action_ords in self.block_ord_codes[block].items():
            for action2, a1_a2_ord_code in action_ords.iteritems():
                for action3, a2_a3_ord_code in self.block_ord_codes[block][action2].items():
                    clause = [(0, -a1_a2_ord_code), (0, -a2_a3_ord_code)]
                    if action3 in self.block_ord_codes[block][action1]:
                        clause.append((0, self.block_ord_codes[block][action1][action3]))
                    cset = frozenset(clause)
                    if cset not in trans_clauses:
                        self.clauses.append(((0, -2), clause))
                        self.clause_types.append("ord_trans")
                        trans_clauses.add(cset)
        
        
        #start action preconditions
        for lit, sup_actions in self.block_sup_codes[block][self.a_start].iteritems():
            pre_code = self.block_pre_codes[block][lit[:2]]
            for sup_code in sup_actions.itervalues():
                if lit[2]: clause = [(0, -sup_code), (0, pre_code)]
                else:      clause = [(0, -sup_code), (0, -pre_code)]
                self.clauses.append(((0, -2), clause))
                self.clause_types.append("start_pre")
                
        #end action effects
        for lit, sup_actions in self.block_sup_codes_rev[block][self.a_end].iteritems():
            eff_code = self.block_eff_codes[block][lit[:2]]
            for sup_code in sup_actions.itervalues():
                if lit[2]: clause = [(0, -sup_code), (0, eff_code)]
                else:      clause = [(0, -sup_code), (0, -eff_code)]
                self.clauses.append(((0, -2), clause))
                self.clause_types.append("end_eff")
        
        #end action, frame axioms
        for prop, eff_code in self.block_eff_codes[block].iteritems():
            for sign in [True, False]:
                lit = prop + (sign,)
                if sign: clause = [(0, -eff_code)]
                else:    clause = [(0, eff_code)]
                clause.append((0, -self.block_codes[block]))
                for sup_code in self.block_sup_codes_rev[block][self.a_end][lit].itervalues():
                    clause.append((0, sup_code))
                self.clauses.append(((0, -2), clause))
                self.clause_types.append("end_frame")
        
        #block support implies block effects
        for parent in self.block_graph.predecessors_iter(block):
            for lit, sup_actions in self.block_sup_codes[block][parent].iteritems():
                eff_code = self.block_eff_codes[parent][lit[:2]]
                for action, sup_code in sup_actions.iteritems():
                    if lit[2]: clause = ((0, -sup_code), (0, eff_code))
                    else:       clause = ((0, -sup_code), (0, -eff_code))
                    self.clauses.append(((0, -2), clause))
                    self.clause_types.append("sup_imp_eff")

        #block preconditions require support
        for parent in self.block_graph.predecessors_iter(block):
            for prop, pre_code in self.block_pre_codes[parent].iteritems():
                for sign in [True, False]:
                    lit = prop + (sign,)
                    if sign: clause = [(0, -self.block_codes[parent]), (0, -pre_code)]
                    else:    clause = [(0, -self.block_codes[parent]), (0, pre_code)]
                    for sup_code in self.block_sup_codes_rev[block][parent][lit].itervalues():
                        clause.append((0, sup_code))
                    self.clauses.append(((0, -2), clause))
                    self.clause_types.append("pre_req_sup")
                    

    def make_ordered_block_codes(self, block):
        """ Make the codes for the totally ordered block.
        
            (BlockEncoding, int) -> None
        """
        self.totally_order_block_actions(block)
        
        #print "Block:", block, "TOTAL ORDER:", self.total_order[block]
        
        self.block_codes[block] = self.new_code(("block", block), self.hm1_steps)
        
        self.block_action_codes[block] = {}
        for action in self.block_actions[block]:
            self.block_action_codes[block][action] = self.new_code(("act", block, action), self.hm1_steps)
        
        self.block_pre_codes[block] = {}
        for fluent in self.block_preconditions[block]:
            self.block_pre_codes[block][fluent] =\
                self.new_code(("pre", block, fluent), self.hm1_steps)
        
        self.block_eff_codes[block] = {}
        
        #Intra-Fluents
        self.intra_fluent_codes[block] = {}
        self.intra_fluent_codes[block][None] = {}
        for ag_pair in self.total_order[block]:
            self.intra_fluent_codes[block][ag_pair] = {}
            if isinstance(ag_pair, int): #block
                for fluent in self.block_effects[ag_pair]:
                    code = self.new_code(("intra_b", block, ag_pair, fluent), self.hm1_steps)
                    self.intra_fluent_codes[block][ag_pair][fluent] = code
                    self.block_eff_codes[block][fluent] = code
            else:
                action, grounding = ag_pair
                for eff in action.strips_effects[grounding]:
                    fluent = eff[:2]
                    if not eff[2] and fluent + (True,) in action.strips_effects[grounding]:
                        continue
                    code = self.new_code(("intra_a", block, ag_pair, eff), self.hm1_steps)
                    self.intra_fluent_codes[block][ag_pair][eff] = code
                    self.block_eff_codes[block][fluent] = code

    def make_ordered_block_clauses(self, block):
        """ Make the clauses for the totally ordered block.
        
            (BlockEncoding, int) -> None
        """
        last_occurance = dict(self.block_pre_codes[block])
        for ag_pair in self.total_order[block]:
            
            if isinstance(ag_pair, int): #block
                a_code = self.block_codes[ag_pair]
                #Preconditions
                for fluent, pre_code in self.block_pre_codes[ag_pair].iteritems():
                    last_code = last_occurance[fluent]
                    for sign in [1, -1]:
                        self.clauses.append(((0, -2),\
                             [(0, -a_code), (0, -1*sign*pre_code), (0, sign*last_code)]))
                        self.clause_types.append("ord_pre_b")
            
                #Effects and SSAs
                for fluent, f_code in self.intra_fluent_codes[block][ag_pair].iteritems():
                    last_code = last_occurance[fluent]
                    eff_code = self.block_eff_codes[ag_pair][fluent]
                    
                    #Effect
                    self.clauses.append(((0, -2), [(0, -a_code), (0, -eff_code), (0, f_code)]))
                    self.clause_types.append("ord_eff_b")
                    self.clauses.append(((0, -2), [(0, -a_code), (0, eff_code), (0, -f_code)]))
                    self.clause_types.append("ord_eff_b")
                    
                    #SSAs
                    self.clauses.append(((0, -2), [(0, -f_code), (0, a_code), (0, last_code)]))
                    self.clause_types.append("ord_ssa_b")
                    self.clauses.append(((0, -2), [(0, -f_code), (0, eff_code), (0, last_code)]))
                    self.clause_types.append("ord_ssa_b")
                    self.clauses.append(((0, -2), [(0, f_code), (0, a_code), (0, -last_code)]))
                    self.clause_types.append("ord_ssa_b")
                    self.clauses.append(((0, -2), [(0, f_code), (0, -eff_code), (0, -last_code)]))
                    self.clause_types.append("ord_ssa_b")
                    
                    last_occurance[fluent] = f_code
            
            else:
                action, grounding = ag_pair
                a_code = self.block_action_codes[block][ag_pair]
                
                #Precondition clauses
                for prec in action.strips_preconditions[grounding]:
                    last_code = last_occurance[prec[:2]]
                    if not prec[2]: last_code = -last_code
                    self.clauses.append(((0, -2), [(0, -a_code), (0, last_code)]))
                    self.clause_types.append("ord_pre_a")

                #Effects and SSAs
                for eff, f_code in self.intra_fluent_codes[block][ag_pair].iteritems():
                    fluent = eff[:2]
                    last_code = last_occurance[fluent]
                    if eff[2]:
                        #Effect
                        self.clauses.append(((0, -2), [(0, -a_code), (0, f_code)]))
                        self.clause_types.append("ord_eff_a")
                        
                        #SSAs
                        self.clauses.append(((0, -2), [(0, -f_code), (0, a_code), (0, last_code)]))
                        self.clause_types.append("ord_ssa_a")
                        self.clauses.append(((0, -2), [(0, f_code), (0, -last_code)]))
                        self.clause_types.append("ord_ssa_a")
                        
                    else:
                        #Effect
                        self.clauses.append(((0, -2), [(0, -a_code), (0, -f_code)]))
                        self.clause_types.append("ord_eff_a")
                    
                        #SSAs
                        self.clauses.append(((0, -2), [(0, -f_code), (0, last_code)]))
                        self.clause_types.append("ord_ssa_a")
                        self.clauses.append(((0, -2), [(0, f_code), (0, a_code), (0, -last_code)]))
                        self.clause_types.append("ord_ssa_a")
                    last_occurance[fluent] = f_code


    def make_interface_mutex(self, block):
        """ Make fluent mutex for the given block.
            Don't make precondition mutex for the root block.
            
            (BlockEncoding, int) -> None
        """
        if block != self.block_graph_root:
            for prop1, prop_code1 in self.block_pre_codes[block].iteritems():
                for sign in [True, False]:
                    lit1 = prop1 + (sign,)
                    if sign: prop_code1_sign = -prop_code1
                    else: prop_code1_sign = prop_code1
                    for lit2 in self.problem.state_mutexes[lit1]:
                        prop2 = lit2[:2]
                        if prop1 == prop2: continue
                        if prop2 in self.block_pre_codes[block]:
                            prop_code2 = self.block_pre_codes[block][prop2]
                            if lit2[2]: prop_code2 = -prop_code2
                            self.clauses.append(((0, -2), [(0, prop_code1_sign), (0, prop_code2)]))
                            self.clause_types.append("pre_mutex")
        
        for prop1, prop_code1 in self.block_eff_codes[block].iteritems():
            for sign in [True, False]:
                lit1 = prop1 + (sign,)
                if sign: prop_code1_sign = -prop_code1
                else: prop_code1_sign = prop_code1
                for lit2 in self.problem.state_mutexes[lit1]:
                    prop2 = lit2[:2]
                    if prop1 == prop2: continue
                    if prop2 in self.block_eff_codes[block]:
                        prop_code2 = self.block_eff_codes[block][prop2]
                        if lit2[2]: prop_code2 = -prop_code2
                        self.clauses.append(((0, -2), [(0, prop_code1_sign), (0, prop_code2)]))
                        self.clause_types.append("eff_mutex")
    
    
    
    def encode_base(self):
        """ Do any horizon-independent encoding that might be required.
        
           (BlockEncoding, int, bool, bool) -> None
        """
        
        assert self.horizon == 1, "This encoding assumes that the horizon is 1"
        
        #To do in making the initial causal_graph - do not have bi-directed arcs
        
        self.make_dtgs(False, False)
        self.make_causal_graph(False)
        self.make_hierarchy(cut_above=False, #false is what we want
                            cycle_limit=2000,
                            partition_on_actions=False,
                            min_action_component_size=5,
                            copy_layer_n=2, #None
                            num_layer_copies=10,
                            min_subnode_size=100,
                            block_layer_function=max,
                            linear_encoding_threshold=4)
        
        self.a_start = "start"
        self.a_end = "end"
        self.hm1_steps = (0, -2)
        
        self.block_codes = {}
        self.block_pre_codes = {}
        self.block_eff_codes = {}
        self.block_action_codes = {}
        self.block_sup_codes = {}
        self.block_sup_codes_rev = {}
        self.block_ord_codes = {}
        
        self.intra_fluent_codes = {}
        
        for block in self.block_graph.nodes_iter():
            if self.block_types[block] == "ordered":
                self.make_ordered_block_codes(block)
            else:
                self.make_causal_block_codes(block)
            
        
        for block in self.block_graph.nodes_iter():
            if self.block_types[block] == "ordered":
                self.make_ordered_block_clauses(block)
            else:
                self.make_causal_block_clauses(block)
                self.make_interface_mutex(block)
            
            #self.make_interface_mutex(block)
            
        #Make clauses for the main block
        self.clauses.append(((0, -2), [(0, self.block_codes[self.block_graph_root])]))
        self.clause_types.append("main_block")
        
        #Start 
        for prop, prop_code in self.block_pre_codes[self.block_graph_root].iteritems():
            if prop in self.problem.initial_state:
                self.clauses.append(((0, 0), [(0, prop_code)]))
            else:
                self.clauses.append(((0, 0), [(0, -prop_code)]))
            self.clause_types.append("start")

        #goal clauses
        for prec in self.problem.flat_ground_goal_preconditions:
            assert isinstance(prec[0], PredicateCondition)
            goal_code = self.block_eff_codes[self.block_graph_root]\
                [(prec[0].pred, prec[0].ground_conditions[prec[1]])]
            if prec[2]: self.clauses.append(((-2, -2), [(0, goal_code)]))
            else:       self.clauses.append(((-2, -2), [(0, -goal_code)]))
            self.clause_types.append("goal")

    
    
    def build_plan(self):
        """Build a plan from the true variables
        
            (BlockEncoding) -> None
        """
        if not self.quiet:
            print "Building plan..."

        orderings = defaultdict(lambda: defaultdict(set))
        actions = defaultdict(set)
        blocks = set()
        
        def get_start_end_action(block, action, st_end):
            if isinstance(action, int): return action, st_end
            return block, action
        
        for cnf_code in self.true_vars:
            step, var = self.cnf_code_to_variable[cnf_code]
            objects = self.variables[var]
            
            if objects[0] == "block":
                blocks.add(objects[1])
            
            elif objects[0] == "act":
                block, action = objects[1:]
                actions[block].add(action)

            elif self.variables[var][0] == "ord":
                block, a1, a2 = objects[1:]
                if a1 == self.a_start or a2 == self.a_end: continue
                orderings[block][a1].add(a2)
                #print "B:", self.get_action_name(a1), ":::", self.get_action_name(a2)
            
        for block, b_ords in orderings.iteritems():
          rev = defaultdict(set)
          self.close_orderings(b_ords, rev)
        
        
        def make_plan_for_block(block, plan, state, states):
            """ First totally order the actions/sub-blocks within the block.
                Next traverse them in order. If a duplicate state is found
                pop actions from the plan as required.
                
                (int, [(Action, (str,))], set((Prop, (str,)),
                    {frozenset((Prop, (str,))) : int}) -> None
            """
            
            if self.block_types[block] == "ordered":
                order = [x for x in self.total_order[block]\
                    if x in blocks or x in actions[block]]
            else:
                layer_queue = deque(actions[block])
                for parent in self.block_graph.predecessors_iter(block):
                    if parent in blocks: layer_queue.append(parent)
                layers = defaultdict(int)
                while layer_queue:
                    a1 = layer_queue.pop()
                    la1 = layers[a1]  
                    for a2 in orderings[block][a1]:
                        la2 = layers[a2]
                        if la2 <= la1:
                            layers[a2] = layers[a1]+1
                            layer_queue.append(a2)
                layer_actions = defaultdict(set)
                for action, layer in layers.iteritems():
                    layer_actions[layer].add(action)
                layers = sorted(layer_actions)
                order = [action for layer in layers for action in layer_actions[layer]]

            for ag_pair in order:
                if isinstance(ag_pair, int):
                    make_plan_for_block(ag_pair, plan, state, states)
                else:
                    action, grounding = ag_pair
                    #Make successor state
                    pos = set()
                    neg = set()
                    for eff in action.strips_effects[grounding]:
                        fluent = eff[:2]
                        if eff[2]: pos.add(fluent)
                        else: neg.add(fluent)
                    state.difference_update(neg)
                    state.update(pos)
                    frozen_state = frozenset(state)
                    if frozen_state in states:
                        layer = states[frozen_state]
                        while len(plan) > layer:
                            rem_a, rem_s = plan.pop()
                            del states[rem_s]
                    else:
                        plan.append((ag_pair, frozen_state))
                        states[frozen_state] = len(plan)
        
        self.plan = []
        state = set(self.problem.initial_state_set)
        states = {frozenset(self.problem.initial_state_set) : 0}
        make_plan_for_block(self.block_graph_root, self.plan, state, states)
        self.plan = [[act[0]] for act in self.plan]

     
