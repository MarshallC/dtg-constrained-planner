""" File:        solvers/__init__.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2013-
    Desctiption: Exists to allow the importing of the solver subsystem as a
                 module.

    Liscence:   [Refine this!]
"""

from solver_base import SolvingException, SolverWrapper
#solver_sat_code, solver_unsat_code, solver_time_out_code, solver_error_code, 
