from itertools import combinations
import copy
import sys, subprocess, os, itertools, time

from solver_base import SolvingException, Solver, solver_sat_code, solver_unsat_code,\
    solver_time_out_code, solver_error_code
from customDebugPrint import dprint
import time
solver_class = 'LingelingDrupTrim'

class LingelingDrupTrim(Solver):
    problematic_literals = None

    solver_sat_code = 10
    solver_unsat_code = 20
    
    solver_path = os.path.join(os.path.dirname(__file__), "lingeling")

    def minisat_to_lingeling(self, instring):
        [base, cnf_file_name]
        

    def __init__(self, cnf_file_name, tmp_path, exp_name, time_out, solver_args):
        self.cnf_file_name = cnf_file_name
        self.tmp_path = tmp_path
        self.exp_name = exp_name
        self.time_out = time_out
        self.solver_args = solver_args
        
        self.sln_file_name = os.path.join(tmp_path, exp_name + '.sln')
        self.out_file_name = os.path.join(tmp_path, exp_name + '_lingelingDrupTrim_STDOUT.sln')
        
        if solver_args:
            print "Warning: LingelingDrupTrim solver args currently ignored."
        
        self.run_str = self.solver_path
        if time_out is not None:
            self.run_str += " -timeout " + time_out
        self.run_str += " " + cnf_file_name + " -t " + self.sln_file_name + " > " + self.out_file_name

    def solve(self):
        try:
            runtime = 0.0
            with file(self.sln_file_name, 'w') as sln_file:
                print self.run_str
                before = time.time()
                solver_res = subprocess.call(self.run_str, stdout=sln_file, stderr=sln_file, shell=True)
                after = time.time()
                runtime = after - before
            #if solver_res not in [self.solver_sat_code, self.solver_unsat_code]: # TODO commented out by Marshall?
               # raise SolvingException("Error: There was a problem running LingelingDrupTrim. Return code: " +\
                #    str(solver_res), solver_error_code)
        except IOError as e:
            raise SolvingException("Error: could not open solution file: " + self.sln_file_name,
                solver_error_code)
        except OSError as e:
            sys.stdout.flush()

            raise SolvingException("Error: There was a problem running the solver: " + self.run_str,
                solver_error_code)
        
        try:
            out_file = file(self.out_file_name, 'r')
            out_lines = out_file.readlines()
            cpu_time = float(runtime)
            # Result parseing UNSATISFIABLE/SATISFIABLE/None
            result = None
            result_line = [x for x in out_lines if "SATISFIABLE" in x]
            if len(result_line)==1:
                [c,result] = result_line[0].split()

            if result == None:
                print("PROBLEM:")
                print("\n".join(out_lines))
                assert False, "Problem reading lingeling output"

            if result == "SATISFIABLE": # get true vars list from out file
                true_vars = set()
                sat_var_lines = [x for x in out_lines if x.split()[0] == 'v']
                for sat_var_line in sat_var_lines:
                    all_vars_on_line = sat_var_line.split()[1:]
                    true_vars_on_line = [int(x) for x in all_vars_on_line if int(x) > 0]
                    
                    for true_var in true_vars_on_line:
                        true_vars.add(true_var)

                return (True, cpu_time, true_vars)

            if result == "UNSATISFIABLE": # Need to process drup proof
                #with file(self.sln_file_name, 'r') as sln_file: # drup or true_vars
                self.drup_trim_path = os.path.join(os.path.dirname(__file__), "drup-trim")
                self.cnf_core_file_name = os.path.join(self.tmp_path, self.exp_name + '_UNSAT_core.cnf')
                self.scrap_file_name = os.path.join(self.tmp_path, self.exp_name + 'scratch_area_for_drup_trim.txt')
                    
                # drup-trim cnf drup-proof -c cnf-core
                self.drup_trim_str = self.drup_trim_path + " " + self.cnf_file_name + " " + self.sln_file_name + " -c " + self.cnf_core_file_name
                
                with file(self.scrap_file_name, 'w') as scrap_file_name:
                    before2 = time.time()
                    subprocess.call(self.drup_trim_str, stdout=scrap_file_name, stderr=scrap_file_name, shell=True)
                    dprint("drup-trim time: " + str(time.time() - before2))
                    #minimiser_run = "python " + os.path.join(os.path.dirname(__file__), "minimiser.py") + " " + self.cnf_core_file_name
                    #subprocess.call(minimiser_run, stdout=scrap_file_name, stderr=scrap_file_name, shell=True)
                return (False, cpu_time, [])

            assert False, "result:" + result
            
            result = out_lines[-1] # "SATISFIABLE" or "UNSATISFIABLE"
            result = result.rstrip()













            
            with file(self.sln_file_name, 'r') as sln_file: # drup or true_vars
                sln_lines = sln_file.readlines()
                linepos = 0
                if linepos < len(sln_lines):
                    # These are implemented Before, in different file
                    #result = sln_lines[linepos] 
                    #result = result.rstrip()
                    if result == 'SATISFIABLE':
                        true_vars = filter(lambda x: x>0, \
                            map(int, sln_lines[linepos][:-1].split(' ')[1:]))
                        cpu_time = float(runtime)
                        return (True, cpu_time, set(true_vars))
                    elif result == 'UNKNOWN':
                        raise SolvingException("ACTUALLY POSSIBLY ERROR>>>>FIX>>>>> Error: solving CNF instance {} timed out!".\
                            format(self.cnf_file_name), solver_time_out_code)

#                    Commented out to correct flow
#                    raise SolvingException("Error: solving CNF instance {} resulted in an error: {}".\ 
#                            format(self.cnf_file_name, sln_lines), solver_error_code)


                # Should be unsat if here #                    elif result == 'UNSATISFIABLE':
            # Should be unsat if here
            # ASSUME! that an UNSAT proof has been derived
            cpu_time = float(runtime)            
            self.drup_trim_path = os.path.join(os.path.dirname(__file__), "drup-trim")
            self.cnf_core_file_name = os.path.join(self.tmp_path, 'UNSAT_core.cnf')
            self.scrap_file_name = os.path.join(self.tmp_path, self.exp_name + 'scratch_area_for_drup_trim.txt')
                        
            # drup-trim cnf drup-proof -c cnf-core
            self.drup_trim_str = self.drup_trim_path + " " + self.cnf_file_name + " " + self.sln_file_name + " -c " + self.cnf_core_file_name
            with file(self.scrap_file_name, 'w') as scrap_file_name:
                subprocess.call(self.drup_trim_str, stdout=scrap_file_name, stderr=scrap_file_name, shell=True)
            return (False, cpu_time, [])
        
            
        except IOError:
            raise SolvingException("Error: could not open solution file: {}".\
                format(self.sln_file_name), solver_error_code)

    def get_problematic_literals(self):
        raise NotImplementedError
        return self.problematic_literals

    def compute_literals_causing_conflict(self, drup_lines, cnf_lines):
        raise NotImplementedError
        '''
        # Set up
        problem_vars = set()
        base_cnf = []
        drup_clauses = []

        # parsing of list of string into list of sets
        for cnf_line in cnf_lines:
            if len(cnf_line)>0:
                base_cnf.append(set([int(x) for x in cnf_line.split("\n")[0].split(" ") if not x == "0"]))
        for drup_line in drup_lines:
            if len(drup_line)>0:
                if not drup_line[0] == "d":
                    drup_clauses.append(set([int(x) for x in drup_line.split("\n")[0].split(" ") if not x == "0"]))
                    
        for drup_clause in drup_clauses:
            print(drup_clause)

        # main computation
        all_cnf_subsets = []

        # For each drup clause find a minimal subset that can justify its existence
        for drup_clause_index in range(0,len(drup_clauses)):
            drup_clause = drup_clauses[drup_clause_index]
            print("dealing with drup clause:" + str(drup_clause) + " : " + str(drup_clause_index) + "/" + str(len(drup_clauses)))

            # To try reduce the size of what clauses a checked, find a subset that is affected by setting drup clause false and unit prop-ing
            working_cnf_formulation = copy.deepcopy(base_cnf)

            true_lits = []
            true_lits.extend([-lit for lit in drup_clause])

            # do assignments and unit prop to get the empty list of clauses
            while len(true_lits)>0:
                # make assignments
                while len(true_lits)>0:
                    true_lit = true_lits.pop()
                    clauses_to_remove = []
                    for clause in working_cnf_formulation:
                        if true_lit in clause: # clause now satisfied
                            clauses_to_remove.append(clause)
                        if -true_lit in clause: # remove literal from clause as false
                            clause.remove(-true_lit)
                            if len(clause) == 0:
                                print("    found empty clause in preprocessing")
                    for clause in clauses_to_remove:
                        working_cnf_formulation.remove(clause)

                # Identify unit clauses
                true_lits.extend([list(clause)[0] for clause in working_cnf_formulation if len(clause) == 1])

            # after this is done, remove any clause untouched here from the working_cnf
            working_cnf_clean = copy.deepcopy(base_cnf)
            working_cnf = [clause for clause in working_cnf_clean if not (clause in working_cnf_formulation)]
            
            for num_clauses in range(1,len(working_cnf)):
                print("working with subset size: " + str(num_clauses) + " time:" + str(time.time()))
                for working_cnf_subset_base in combinations(working_cnf,num_clauses):
                    working_cnf_subset = copy.deepcopy(working_cnf_subset_base)
                    working_cnf_subset_clean = copy.deepcopy(working_cnf_subset)
                    working_cnf_subset = list(working_cnf_subset)

                    # try prove that THIS subset causes drup_clause
                    found_subset = False # try prove it does
                    true_lits = []

                    # assign all literals in drup false
                    true_lits.extend([-lit for lit in drup_clause])

                    # do assignments and unit prop to get the empty list of clauses
                    while len(true_lits)>0 and not found_subset:
                        # make assignments
                        while len(true_lits)>0:
                            true_lit = true_lits.pop()
                            clauses_to_remove = []
                            for clause in working_cnf_subset:
                                if true_lit in clause: # clause now satisfied
                                    clauses_to_remove.append(clause)
                                if -true_lit in clause: # remove literal from clause as false
                                    clause.remove(-true_lit)
                                    if len(clause) == 0:
                                        print("    found empty clause")
                                        found_subset = True
                                        break
                            for clause in clauses_to_remove:
                                working_cnf_subset.remove(clause)

                        # Identify unit clauses
                        true_lits.extend([list(clause)[0] for clause in working_cnf_subset if len(clause) == 1])
                    
                    if found_subset:
                        break
                if found_subset:
                    break

            # Now process working_cnf_subset as this set of clauses caused the drup clause
            print("found cnf subset responsible for a drup clause: " + str(working_cnf_subset_clean))
            all_cnf_subsets.append(working_cnf_subset_clean)

            # Need to add 
            base_cnf.append(drup_clause)

        # extract every claue mentioned in proof
        problem_vars = set()
        for clause_set in all_cnf_subsets:
            for clause in clause_set:
                for lit in clause:
                    problem_vars.add(abs(lit))

        self.problematic_literals = sorted(list(problem_vars))
        print("Found collection of problematic vars: " + str(self.problematic_literals))
        return
        '''
    def head(self, string):
        return str(string)[0:min(30,len(str(string)))] + "..."
