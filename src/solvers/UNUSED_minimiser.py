import copy
import shlex
from RunBash import runBash
from subprocess import Popen, PIPE
import os
import sys

def try_solve(lines):
    temp_file = open("temp_cnf", "w")
    temp_file.writelines(lines)
    temp_file.close()
    
    cmd = "./src/solvers/minisat temp_cnf"
    (exitcode, out, err, total_time) = runBash(cmd)
    word = out.split("\n")[-2]
    sat = word == "SATISFIABLE"
    return sat

def get_exitcode_stdout_stderr(cmd):
    """
    Execute the external command and get its exitcode, stdout and stderr.
    """
    args = shlex.split(cmd)

    proc = Popen(args, stdout=PIPE, stderr=PIPE)
    out, err = proc.communicate()
    exitcode = proc.returncode
    #
    return exitcode, out, err

def print_lines(lines):
    for clause in lines:
        print(clause)

cnf_file = open(sys.argv[1], "r")
working_cnf_lines = cnf_file.readlines()
cnf_file.close()

try_delete_another = True
while try_delete_another:
    print("try delete another")
    try_delete_another = False # assume last one, prove otherwise
    for delete_line_index in range(1,len(working_cnf_lines)):
        #print("  deleting line " + str(delete_line_index))
        temp_delete_cnf = copy.copy(working_cnf_lines)
        print("deleting: " + str(temp_delete_cnf.pop(delete_line_index)))

        # Try to solve this smaller cnf
        sat = try_solve(temp_delete_cnf)
        if not sat:
            # success, found a subset which is still UNSAT
            working_cnf_lines = temp_delete_cnf
            try_delete_another = True
            #print("make new subset cnf")
            #print_lines(working_cnf_lines)
            break

print("ended with cnf:")
print_lines(working_cnf_lines)

out_file = open(sys.argv[1], "w")
out_file.writelines(working_cnf_lines)
out_file.close()


