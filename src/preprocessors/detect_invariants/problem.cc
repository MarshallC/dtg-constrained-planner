
#include "problem.hh"

bool Action::is_effect(int in){
    bool answer =  basic_effects.find(in) != basic_effects.end() || 
        basic_effects.find(-in) != basic_effects.end();

    if(answer)return answer;

    for (auto eff =  other_effects.begin()
             ; eff != other_effects.end()
             ; eff++){
        if(eff->is_effect(in))return true;
    }

    return false;
}





bool Planning_Problem::is_fluent(int i) const{
    int in = std::abs(i);

    return fluents.find(in) != fluents.end();
}
