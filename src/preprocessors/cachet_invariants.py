""" File:        preprocessors/detect_invariants.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2013-
    Desctiption: 

    Liscence:   [Refine this!]
"""

import os
from utilities import preprocessing_error_code, neg_prec_prefix, cachet_path, split_id_conds

from preprocessor_base import Preprocessor, PreprocessingException

from problem   import Object, Type, Function, Predicate, Condition,\
                      PredicateCondition, NotCondition, AndCondition,\
                      OrCondition, ForAllCondition, ExistsCondition,\
                      IncreaseCondition, EqualsCondition, ConditionalEffect,\
                      Action

from subprocess import Popen, PIPE, STDOUT
   
import itertools
   
class CachetPreprocessor(Preprocessor):

    preprocessor_path = os.path.join(os.path.dirname(__file__),
        "detect_invariants", "invariants")

    def __init__(self, problem, clauses, variables, vars_per_layer, tmp_path,
        exp_name, unary_only, quiet):
        """ Set the preprocessor parameters
            
            (CachetPreprocessor, Problem, [[int]], [[int]], [[int]], int, str, str) -> None
        """
        super(CachetPreprocessor, self).__init__(problem, quiet)
        self.clauses = clauses
        self.variables = variables
        self.vars_per_layer = vars_per_layer
        self.tmp_path = tmp_path
        self.cachet_file_name = os.path.join(self.tmp_path, exp_name + "_cachet_input.cnf")
        self.cachet_out_file_name = os.path.join(self.tmp_path, exp_name + "_cachet_output.cnf")
        self.invariants = []
        self.unary_only = unary_only

    
    def run(self):
        """ Run the preprocessor and generate the invariants.
        
           (CachetPreprocessor) -> None
        """
        try:
            
            with file(self.cachet_file_name, 'w') as cachet_file:
                #Write comments describing the variable types
                nvars = 2 * self.vars_per_layer
                cachet_file.write("c N " + str(self.vars_per_layer) + "\n")
                for var in xrange(1, self.vars_per_layer+1):
                    v_obj, v_vars = self.variables[((var - 1) % self.vars_per_layer) + 1]
                    if v_obj == 'aux':
                        cachet_file.write("c x " + str(var) + " " +\
                            str(v_vars[0]) + " " + str(v_vars[1]) + "\n")
                    elif v_obj == 'split':
                        var_str = "c s"
                        for (pred, grounding, cond_type) in v_vars:
                            var_str += " " + split_id_conds[cond_type] + " " +\
                                pred.name + " " + " ".join(grounding)
                        cachet_file.write(var_str)
                    elif isinstance(v_obj, Action):
                        if v_vars:
                            cachet_file.write("c a " + str(var) + " " + v_obj.name +\
                                " " + " ".join(v_vars) + "\n")
                        else:
                            cachet_file.write("c a " + str(var) + " " + v_obj.name + "\n")
                    elif isinstance(v_obj, Predicate):
                        if v_vars:
                            cachet_file.write("c f " + str(var) + " " + v_obj.name +\
                                " " + " ".join(v_vars) + "\n")
                        else:
                            cachet_file.write("c f " + str(var) + " " + v_obj.name + "\n")
                    else:
                        if v_vars:
                            cachet_file.write("c c " + str(var) + " " + v_obj.cond_code +\
                                " " + " ".join(v_vars) + "\n")
                        else:
                            cachet_file.write("c c " + str(var) + " " + v_obj.cond_code + "\n")
                #Write the header and clauses
                num_clauses = 0
                for (lb, ub), clause in self.clauses:
                    if ub == 0 or ub == -2: num_clauses += 1
                cachet_file.write("p cnf " + str(nvars) + " " + str(num_clauses) + "\n")
                for (lb, ub), clause in self.clauses:
                    if ub == 0 or ub == -2:
                        #This covers the initial clauses and the clauses not at the goal
                        for lit in clause:
                            cachet_file.write(str(lit) + " ")
                    cachet_file.write("0\n")
        except IOError as e:
            raise PreprocessingException("IOError writing to cachet file: " + str(e),
                preprocessing_error_code)
  
        try:
            process = Popen([self.preprocessor_path] +\
                [self.cachet_file_name, self.cachet_out_file_name, self.tmp_path, cachet_path],
                #[k + "=" + v for k, v in self.args.iteritems()],
                stdout=PIPE, bufsize=-1) #stderr=STDOUT
            #Get the data back
            for line in process.stdout:
                print line.rstrip()
            process.wait()
            process.stdout.close()
        except IOError as e:
            raise PreprocessingException("IOError running cachet",
                preprocessing_error_code)
        
        try:
            with file(self.cachet_out_file_name) as cachet_file:
                seen_steps = set()
                step = None
                max_step = None
                for line in cachet_file:
                    line = line.strip()
                    if not line: continue
                    tokens = line.split(" ")
                    if tokens[0] == "c":
                        step = tokens[-1]
                        if step in seen_steps and self.unary_only: break
                        seen_steps.add(step)
                        try: 
                            step = int(step)
                            max_step = max(step, max_step)
                        except Exception: pass
                        continue
                    if step == "TIMELESS":
                        #if step not in seen_steps:
                        bounds = (0, -2)
                        self.invariants.append((bounds, [int(lit) for lit in tokens if lit != "0"]))
                    elif step == "n": 
                        if step in seen_steps: continue
                        bounds = (max_step+1, -2)
                        self.invariants.append((bounds, [int(lit) for lit in tokens if lit != "0"]))
                    else:
                        bounds = (step, step)
                        self.invariants.append((bounds, [int(lit) for lit in tokens if lit != "0"]))
                    
                   

        except IOError as e:
            raise PreprocessingException("IOError error reading cachet output",
                preprocessing_error_code)
        
        return True

        

       
