""" File:        preprocessors/plangraph_invariants.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2014
    Desctiption: 

    Liscence:   [Refine this!]
"""

import os, time, itertools, random, sys

from utilities import preprocessing_error_code, neg_prec_prefix

from preprocessor_base import Preprocessor, PreprocessingException

from problem   import Object, Type, Function, Predicate,\
                      PredicateCondition, NotCondition, AndCondition,\
                      OrCondition, ForAllCondition, ExistsCondition,\
                      IncreaseCondition, EqualsCondition, ConditionalEffect,\
                      Action

class PlangraphPreprocessor(Preprocessor):
    """ Generate a plangraph to perform reachability analysis and produce a set
        of invariants.
    """
    
    def __init__(self, problem, quiet, timeless_only):
        """ Make a new plangraph preprocessor
            
            (Encoding, Problem, ArgProcessor, bool) -> None
        """
        super(PlangraphPreprocessor, self).__init__(problem, quiet)
        self.timeless_only = timeless_only


    def run(self):
        """ Run the preprocessor and modify the problem in the required way.
            Return iff the goal might be achievable.
        
           (PlangraphPreprocessor) -> bool
        """
        
        #start_time = time.time()

        first_layer_actions = {}
        first_layer_fluents = {}
        
        conflicts = self.problem.conflicts
            
        state_mutexes = {}
        action_mutexes = {}
        
        output_state_mutexes = []
        state = set()
        fluent_list = []
        
        for pred in self.problem.predicates.itervalues():
            for grounding in pred.groundings:
                pg_pair = (pred, grounding)
                fluent = (pred, grounding, (pg_pair in self.problem.initial_state))
                not_fluent = (pred, grounding, not fluent[2])
                fluent_list.append(fluent)
                fluent_list.append(not_fluent)
                
                first_layer_fluents[fluent] = 0
                first_layer_fluents[not_fluent] = None
                state.add(fluent)
                state_mutexes[fluent] = set()
                state_mutexes[not_fluent] = set()
 
        remaining_actions = set()
        for action in self.problem.actions.itervalues():
            for grounding in action.groundings:
                ag_pair = (action, grounding)
                remaining_actions.add(ag_pair)
                first_layer_actions[ag_pair] = None
                action_mutexes[ag_pair] = {} 
                        
        #pre_noop_time = time.time()
        #print "Setup time:", pre_noop_time - start_time
        
        #Add noop actions
        noops = []
        for pred in self.problem.predicates.itervalues():
            for sign in [True, False]:
                
                if sign: noop_name = "noop_" + pred.name
                else: noop_name = "noop_not_" + pred.name
                
                noop_act = Action(noop_name, [], [], None, None, False)
                noop_act.is_noop = True
                noop_act.strips_preconditions = {}
                noop_act.strips_effects = {}

                for grounding in pred.groundings:
                    ag_pair1 = (noop_act, grounding)
                    noops.append(ag_pair1)
                    pre_eff = (pred, grounding, sign)
                    remaining_actions.add(ag_pair1)
                    
                    action_mutexes[ag_pair1] = {}
                    
                    first_layer_actions[ag_pair1] = None
                    conflicts[ag_pair1] = set()
                    noop_act.strips_preconditions[grounding] = set([pre_eff])
                    noop_act.strips_effects[grounding] = set([pre_eff])
                    
                    #Inform preds
                    if sign:
                        pred.ground_precs[grounding].add(ag_pair1)
                        pred.ground_adds[grounding].add(ag_pair1)
                    else:
                        pred.ground_nprecs[grounding].add(ag_pair1)
                        pred.ground_dels[grounding].add(ag_pair1)
                    
                    #Make conflicts
                    neg_pre_eff = (pred, grounding, not sign)
                    for ag_pair2 in remaining_actions:
                        if ag_pair2 == ag_pair1: continue
                        a2, g2 = ag_pair2
                        if neg_pre_eff in a2.strips_preconditions[g2] or\
                            neg_pre_eff in a2.strips_effects[g2]:
                            conflicts[ag_pair1].add(ag_pair2)
                            conflicts[ag_pair2].add(ag_pair1)
        
        
        #noop_time = time.time()
        #print "Noop time:", noop_time - pre_noop_time
        
        
        #Make a fluent mutex white list - currently we only allow two fluents
        #to be mutex if there is a pair of non-conflicting actions which they are
        
        """
        allowed_fluent_mutexes = {}
        for fluent in fluent_list:
            allowed_fluent_mutexes[fluent] = set()
        allowed = 0
        not_allowed = 0
        for fluent1, fluent2 in itertools.combinations(fluent_list, 2):
            pred1, grounding1, sign1 = fluent1
            pred2, grounding2, sign2 = fluent2

            if sign1: p1_list = pred1.ground_precs[grounding1]
            else: p1_list = pred1.ground_nprecs[grounding1]
            if sign2: p2_list = pred2.ground_precs[grounding2]
            else: p2_list = pred2.ground_nprecs[grounding2]

            allowed_mutex = False
            
            #allowed_mutex = random.random() < 0.8
            
            for a1, a2 in itertools.product(p1_list, p2_list):
                if a1 == a2 or ((not a1[0].is_noop or not a2[0].is_noop) and a2 not in conflicts[a1]):
                    allowed_mutex = True
                    break
            
            if allowed_mutex:
                allowed += 1
                allowed_fluent_mutexes[fluent1].add(fluent2)
                allowed_fluent_mutexes[fluent2].add(fluent1)
            else:
                #print "Not allowed:", pred1.name, grounding1, sign1, ":", pred2.name, grounding2, sign2
                not_allowed += 1
        
        print "Fraction of fluent mutexes allowed:", allowed / float(allowed + not_allowed)
        """
        

        actions = set()
        step = 0
        new_actions = set([-1])
        broken_mutexes = set()
        if not self.quiet:
            print "Step:",
            sys.stdout.flush()
        while new_actions or broken_mutexes:
            
#            print("plangraph: new actions:" + str([action[0].__dump__() if type(action) is tuple else str(action) for action in new_actions]))
#            print("plangraph: broken mutexes:" + str([[prop[0].__dump__() for prop in mutex] for mutex in broken_mutexes]))
            step_time = time.time()
            
            if not self.quiet:
                print step,
                sys.stdout.flush()
                
            
            new_fluents = set()
            new_actions = set()
            #Try and execute actions

            #pre_time = 0
            #am_time = 0
            #break_e_time = 0
            
            for ag_pair1 in remaining_actions:
                action1, grounding1 = ag_pair1
                
                #print "Trying:", action1.name, grounding1
                
                #st_pre_time = time.time()
                
                exec_action = True
                for prec in action1.strips_preconditions[grounding1]:
                    if prec not in state:
                        
                        #print "Prec failed:", prec[0].name, prec[1], prec[2]
                        
                        exec_action = False
                        break
                if not exec_action: continue
                for f1, f2 in itertools.combinations(action1.strips_preconditions[grounding1], 2):
                    if f1 in state_mutexes[f2]:
                        
                        #print "Precs mutex:", f1[0].name, f1[1], f1[1],":", f2[0].name, f2[1], f2[2]
                        
                        exec_action = False
                        break
                if not exec_action: continue
                
                first_layer_actions[ag_pair1] = step
                actions.add(ag_pair1)
                new_actions.add(ag_pair1)
                
                #print "New action:", ag_pair1[0].name, ag_pair1[1]
                
                #end_pre_time = time.time()
                #pre_time += end_pre_time - st_pre_time
                
                #compute mutexes with existing actions

                #print("plangraph invariants: grounding1 from ag_pair1 from remaining_actions: " + str(grounding1))
                for prec1 in action1.strips_preconditions[grounding1]:
                    for prec2 in state_mutexes[prec1]:
                        #print("plangraph invariants: Example prec1: " + str(prec1))
                        #print('plangraph invariants: state_mutex instance: ' + str(prec1[0].name) + str(prec1[1]) + ' ' + str(prec2[0].name) + str(prec2[1]))
                        prec_pair = (prec1, prec2)
                        rev_prec_pair = (prec2, prec1)
                        
                        if prec2[2]: p_list = prec2[0].ground_precs[prec2[1]]
                        else: p_list = prec2[0].ground_nprecs[prec2[1]]
                        for ag_pair2 in p_list:
                            if ag_pair2 not in actions: continue
                            
                            if ag_pair1 < ag_pair2:
                                if ag_pair2 not in action_mutexes[ag_pair1]:   
                                    action_mutexes[ag_pair1][ag_pair2] = 1
                                else:
                                    action_mutexes[ag_pair1][ag_pair2] += 1
                            else:
                                if ag_pair1 not in action_mutexes[ag_pair2]:
                                    action_mutexes[ag_pair2][ag_pair1] = 1
                                else:
                                    action_mutexes[ag_pair2][ag_pair1] += 1
                            
                #end_am_time = time.time()
                #am_time += end_am_time - end_pre_time
                for f1 in action1.strips_effects[grounding1]:
                    #break existing mutexes
                    if f1 in state:
                        for f2 in state_mutexes[f1]:
                            break_pair = (f1, f2)
                            break_mutex = f2 in action1.strips_effects[grounding1]
                            if not break_mutex:    
                                if f2[2]: e_list = f2[0].ground_adds[f2[1]]
                                else: e_list = f2[0].ground_dels[f2[1]]
                                for ag_pair2 in e_list:
                                    if ag_pair2 in actions and ag_pair1 != ag_pair2 and\
                                        ag_pair2 not in conflicts[ag_pair1]:
                                        if ag_pair2 not in action_mutexes[ag_pair1] and\
                                            ag_pair1 not in action_mutexes[ag_pair2]:
                                            break_mutex = True
                                            break
                            if break_mutex:
                                if (f2, f1) not in broken_mutexes:
                                    broken_mutexes.add((f1, f2))
                    else:
                        new_fluents.add(f1)
                
                #break_e_time += time.time() - end_am_time

            #act_ex_time = time.time()
            #print "Action execution time:", act_ex_time - step_time
            #print "   pre time:", pre_time
            #print "   a mutex time:", am_time
            #print "   break e time:", break_e_time

            #cm_time = time.time()
            #print "Copy mutex time...", cm_time - act_ex_time

            new_broken_mutexes = set()
            for f_mutex in broken_mutexes:
                f1, f2 = f_mutex
                rev_f_mutex = (f2, f1)

                state_mutexes[f1].remove(f2)
                state_mutexes[f2].remove(f1)
                
                if f1[2]: p1_list = f1[0].ground_precs[f1[1]]
                else: p1_list = f1[0].ground_nprecs[f1[1]]
                
                if f2[2]: p2_list = f2[0].ground_precs[f2[1]]  
                else: p2_list = f2[0].ground_nprecs[f2[1]] 

                for ag_pair1 in p1_list:
                    if ag_pair1 not in actions: continue
                    action1, grounding1 = ag_pair1
                    for ag_pair2 in p2_list:
                        
                        if ag_pair2 not in actions or ag_pair1 == ag_pair2: continue
                        action2, grounding2 = ag_pair2

                        m_pair = (ag_pair1, ag_pair2)
                        rev_m_pair = (ag_pair2, ag_pair1)
                        
                        if ag_pair1 < ag_pair2:
                            action_mutexes[ag_pair1][ag_pair2] -= 1
                            
                            mutex_broken = not action_mutexes[ag_pair1][ag_pair2]
                            if mutex_broken:
                                del action_mutexes[ag_pair1][ag_pair2]
                        else:
                            action_mutexes[ag_pair2][ag_pair1] -= 1
                            
                            mutex_broken = not action_mutexes[ag_pair2][ag_pair1]
                            if mutex_broken:
                                del action_mutexes[ag_pair2][ag_pair1]

                        if mutex_broken and ag_pair2 not in conflicts[ag_pair1]:
                            #Here we may additionally break a set of fluent mutexes in the next step
                            for eff1 in action1.strips_effects[grounding1]:
                                for eff2 in action2.strips_effects[grounding2]:
                                    if eff1 != eff2 and eff2 in state_mutexes[eff1]:
                                        new_broken_mutexes.add((eff1, eff2))

            #bm_time = time.time()
            #print "Break mutex time:", bm_time - cm_time

            for f1 in new_fluents:
                if f1[2]: e_list1 = f1[0].ground_adds[f1[1]]
                else: e_list1 = f1[0].ground_dels[f1[1]]
                
                #make new fluent and action mutexes
                for f2 in state:
                    #if f2 not in allowed_fluent_mutexes[f1]: continue

                    if f2[2]: e_list2 = f2[0].ground_adds[f2[1]]
                    else: e_list2 = f2[0].ground_dels[f2[1]]

                    any_adder = False
                    for ag_pair1 in e_list1:
                        if ag_pair1 not in actions: continue
                        for ag_pair2 in e_list2:
                            if ag_pair2 not in actions: continue
                            
                            if ag_pair1 == ag_pair2 or\
                                (ag_pair2 not in conflicts[ag_pair1] and\
                                ag_pair2 not in action_mutexes[ag_pair1] and\
                                ag_pair1 not in action_mutexes[ag_pair2]):
                                
                                any_adder = True
                                break
                        if any_adder: break

                    if not any_adder:
                        state_mutexes[f1].add(f2)
                        state_mutexes[f2].add(f1)

                state.add(f1)
                first_layer_fluents[f1] = step+1
 
            
            #nf_time = time.time()
            #print "New fluents time:", nf_time - bm_time

            broken_mutexes = set()
            for f_pair in new_broken_mutexes:
                f1, f2 = f_pair
                if f2 in state_mutexes[f1] and (f2, f1) not in broken_mutexes:
                    broken_mutexes.add(f_pair)
            
            remaining_actions.difference_update(new_actions)
            
            #ds_time = time.time()
            #print "new bm, new act, etc time:", ds_time - nf_time
            
            #Save the fluent mutexes
            layer_mutexes = []
            for f1, fs in state_mutexes.iteritems():
                for f2 in fs:
                    if f1 < f2 and (f1[0] != f2[0] or f1[1] != f2[1]):
                        layer_mutexes.append((f1, f2))
            
            output_state_mutexes.append(layer_mutexes)
            step += 1

            #end_step_time = time.time()
            
            #print "Saving mutexes time:", end_step_time - ds_time
            
            #if not self. quiet:
                #print "time:", end_step_time - step_time
                #print "fluent mutexes:", len(layer_mutexes)
        
        
        #Check to see if the goal is possibly satisfiable
        for prec in self.problem.flat_ground_goal_preconditions:
            if not isinstance(prec[0], PredicateCondition):
                assert False, "Complex goal not implemented"
            
            fluent = (prec[0].pred, prec[0].ground_conditions[prec[1]], prec[2])
            if fluent not in state:
                print "Goal is not relaxed reachable:", fluent[0].name, fluent[1], fluent[2]
                print("state: " + str([str(i[0]) + ", " + str(i[1]) + "," + str(i[2]) for i in state]))
                print("fluent not in state: " + str(fluent[0]) + ", " + str(fluent[1]) + ", " + str(fluent[2]))

                return False

        # Marshall addition to find a relaxed reachable plan
        # At this point I believe the checks for relaxed reachable have been checked, so presume it is
        action = list(actions)[0]
        print("M actions " + str(list(actions)[0]))
        print("M action groundings " + str(action[0].groundings))
        #print("M goal_normal " + str(list(self.problem.goal)[0]))
        print("M goal " + str([x for x in self.problem.goal.conditions][0]))
        print("M initial " + str([str(x) for x in self.problem.initial_state_set][0]))
        #action1.groundings
        #print()
        #for eff1 in action1.strips_effects[grounding1]:


        # Predicatecondition object
        
        remaining_goals = [x for x in self.problem.goal.conditions]
        #propositions = sef.problem.
        step = 1
        #while True:
            

        assert False
        


        
        for pred in self.problem.predicates.itervalues():
            for grounding in pred.groundings:
                if sign:
                    pred.ground_precs[grounding].pop()
                    pred.ground_adds[grounding].pop()
                else:
                    pred.ground_nprecs[grounding].pop()
                    pred.ground_dels[grounding].pop()

        #Now we want to find those fluents and actions which never become true
        
        mutex_clauses = []
        
        invalid_actions = set()
        invalid_fluents = set()
        static_fluents = set()
        
        for fluent, step in first_layer_fluents.iteritems():
            if step is None:
                if fluent[2]: invalid_fluents.add(fluent[:2])
                else: static_fluents.add(fluent[:2])
        
        for action, step in first_layer_actions.iteritems():
            if step is None and not action[0].is_noop:
                invalid_actions.add(action) 

        if not self.timeless_only:
            for fluent, step in first_layer_fluents.iteritems():
                abs_fluent = fluent[:2]
                if step is not None and step > 0 and abs_fluent not in invalid_fluents and\
                    abs_fluent not in static_fluents:
                    mutex_clauses.append(((0, step-1), (fluent,)))

            for action, step in first_layer_actions.iteritems():
                if not action[0].is_noop and step is not None and step > 0 and\
                    action not in invalid_actions:
                    mutex_clauses.append(((0, step-1), (action,)))

        if static_fluents:
            self.problem.static_preds.update(static_fluents)
        
        for pred, grounding in invalid_fluents:
            pred.groundings.remove(grounding)
        
        for action, grounding in invalid_actions:
            action.groundings.remove(grounding)

        self.problem.compute_static_preds()
        self.problem.link_groundings()
        self.problem.make_flat_preconditions()
        self.problem.make_flat_effects()
        self.problem.get_encode_conds()
        self.problem.make_cond_and_cond_eff_lists()
        self.problem.link_conditions_to_actions()
        self.problem.make_strips_conditions()
        self.problem.compute_conflict_mutex()
       

        for s, mutexes in enumerate(output_state_mutexes):
            step = s + 1
            if s < len(output_state_mutexes) - 1:
                if self.timeless_only: continue
                bound = (step, step)
            else:
                if not self.timeless_only: bound = (1, -1)
                else: bound = (step, -1)
            
            for mutex in mutexes:
                fluent1 = mutex[0][:2]
                fluent2 = mutex[1][:2]
                #nfluent1 = (self.problem.predicates[mutex[0][0].name], mutex[0][1], mutex[0][2])
                #nfluent2 = (self.problem.predicates[mutex[1][0].name], mutex[1][1], mutex[1][2])
                if fluent1 in invalid_fluents or fluent2 in invalid_fluents or\
                    fluent1 in static_fluents or fluent2 in static_fluents: continue
                mutex_clauses.append((bound, (fluent1, fluent2)))
        
        for ag_pair in noops:
            del action_mutexes[ag_pair]
        for ag_pair1 in action_mutexes:
            m_set = action_mutexes[ag_pair1]
            new_m_set = set()
            for ag_pair2 in m_set:
                if not ag_pair2[0].is_noop:
                    new_m_set.add(ag_pair2)
            action_mutexes[ag_pair1] = new_m_set
        
        self.problem.ap_mutexes = action_mutexes   #This might still refer to old actions, but who cares at the moment
        
        self.problem.state_mutexes = state_mutexes
        
        """
        self.problem.state_mutexes = {}
        for (pred, args, sign), mutexes in state_mutexes.iteritems():
            new_pred = (self.problem.predicates[pred.name], args, sign)
            self.problem.state_mutexes[new_pred] = set()
            for (pred1, args1, sign1) in mutexes:
                new_pred1 = (self.problem.predicates[pred1.name], args1, sign1)
                self.problem.state_mutexes[new_pred].add(new_pred1)
        """
        
        #print state_mutexes
        
        self.problem.first_layer_fluents = first_layer_fluents
        self.problem.first_layer_actions = first_layer_actions


        #print "First layer a"
        #for action in self.problem.actions.itervalues():
        #    for grounding in action.groundings:
        #        ag_pair = (action, grounding)
        #        print action.name, grounding, first_layer_actions[ag_pair]
                
        #print "ffl:"
        #for pred in self.problem.predicates.itervalues():
        #    for grounding in pred.groundings:
        #        print pred.name, grounding, "True", first_layer_fluents[(pred, grounding, True)]
        #        print pred.name, grounding, "False", first_layer_fluents[(pred, grounding, False)]
        

        #for s, mutexes in enumerate(output_state_mutexes):
        #    for mutex in mutexes:
        #        print mutex[0][0].name + " " + str(mutex[0][1]) + " " +\
        #            str(mutex[0][2]) + " : " + mutex[1][0].name + " " + str(mutex[1][1]) +\
        #            " " + str(mutex[1][2]) + "\n")



            
        self.state_mutexes = state_mutexes

        return mutex_clauses

    def get_state_mutexes(self): # only run after being generated and saved with main run()
        return self.state_mutexes
