import math
import os
import sys
import pickle
import networkx as nx
import matplotlib.pyplot as plt
from wrapper_node import Node
import random

class Analysis:
    def __init__(self, first_lines):
        if len(first_lines) == 0:
            return "when empty file"
        
        val0 = first_lines[0].rstrip().split(": ")[-1]
        val1 = first_lines[1].rstrip().split(": ")[-1]
        val2 = first_lines[2].rstrip().split(": ")[-1]
        val3 = first_lines[3].rstrip().split(": ")[-1]
        val4 = first_lines[4].rstrip().split(": ")[-1]
        val5 = first_lines[5].rstrip().split(": ")[-1]
        val6 = first_lines[6].rstrip().split(": ")[-1]

        for val in [val4,val5,val6]:
            assert val in ["True","False"]
            
        val4bool = val4 == "True"
        val5bool = val5 == "True"
        val6bool = val6 == "True"
        
        self.domain = val0
        self.problem = val1
        self.encoding = val2
        self.increment_parallel = val4bool
        self.only_fastest_path = val6bool

def get_bool(string):
    if string == "True":
        return True
    elif string == "False":
        return False
    else:
        assert False
        
def remove_timestamp(string):
    if len(string)==0:
        return string
    
    if string[0] == "[":
        return " ".join(string.split(" ")[1:])
    else:
        return string

def get_nodes_from_lines(all_lines):
    # from all lines in logfile
    acknowledged_nodes = []
    id_to_node = {}
    set_initial_node = False

    for index in range(len(all_lines)-1): # TODO note: removed the last line
        line_raw = all_lines[index]
        line = remove_timestamp(line_raw.rstrip())
        
        if "Starting" in line:
            # Start a new node
            working_node_id = int(line.split(" ")[-1])
            # Check if node for id has already been created
            if working_node_id in id_to_node.keys():
                working_node = id_to_node[working_node_id]
            else:
                # initial node
                if set_initial_node: assert False, "line" + "  ===AT===  " + str(index)
                set_initial_node = True

                # Set properties
                working_node = Node()
                working_node.run_name = str(working_node_id)
                working_node.SAT_time_to_node = 0

                # Register
                acknowledged_nodes.append(working_node)
                id_to_node[working_node_id] = working_node

        elif "optimal_path_SAT_time_sum: " in line:
            None
        elif "DTG_ag_to_edge" in line:
            None
        elif "running_command" in line:
            working_node.run_command = line.split("running_command: ")[1]
        elif "running command" in line:
            working_node.run_command = line.split("running command: ")[1]
        elif "Finished" in line:
            None
        elif "total_time" in line:
            working_node.total_time = float(line.split("total_time: ")[1])
        elif "SAT_time" in line:
            working_node.SAT_time = float(line.split("SAT_time: ")[1])
        elif "solution_found" in line:
            working_node.solution_found = get_bool(line.split("solution_found: ")[1])
        elif "children_count" in line:
            None
        elif "children: " in line:
            if "children: []" in line:
                child_ids = []
            else:
                child_ids = [int(child_id) for child_id in line.split("children: [")[1].split("]")[0].split(", ")]

            for child_id in child_ids:
                child = Node()
                child.run_name = str(child_id)
                child.SAT_time_to_node = working_node.SAT_time_to_node + working_node.SAT_time

                working_node.children.append(child)

                acknowledged_nodes.append(child)
                id_to_node[child_id] = child
        elif "horizon: " in line:
            working_node.horizon = int(line.split("horizon: ")[1])
        elif "log file: " in line:
            working_node.log_file = line.split("log file: ")[1]
        elif "unique problem name: " in line:
            None
        elif "==========================" in line:
            # End of node
            node = None
        elif "self." in line:
            None # args
        elif "" == line:
            None
        elif "run_name: " in line:
            None
        elif "DONE" in line:
            None
        elif "START" in line:
            None
        elif "END" in line:
            None
        elif "optimal path last step run_command: " in line:
            None
        elif "args_parsed" in line:
            None
        elif "{\'domain\': \'" in line:
            None
        else:
            # error if end of line
            if index == len(all_lines):
                # invalid line on incomplete logfile
                None
            else:
                assert False, "Error in logfile: " + line

    return acknowledged_nodes

def draw_graph(nodes):
    print("starting draw graph")
    root_node = nodes[0]
    solution_nodes = [node for node in nodes if node.solution_found]
    
    completed_nodes_graph = nx.DiGraph()
    completed_nodes = [node for node in nodes if node.SAT_time != None]
#    print("draw_graph: completed_nodes: " + str(completed_nodes))
    for node in completed_nodes:
        for child in [node for node in node.children if node.SAT_time != None]:
            completed_nodes_graph.add_edge(node, child)

    print("creating pos")
    #pos = nx.spectral_layout(completed_nodes_graph)
    pos = {}
    
    print("overide pos")
    # Override pos
    for node_i in range(len(completed_nodes)):
        node = completed_nodes[node_i]
        pos[node] = [None, None]
        if not node in pos:
            assert False, node.full_print()
        else:
            pos[node][0] = node_i
            pos[node][1] = node.SAT_time_to_node + node.SAT_time
        

    nx.draw(completed_nodes_graph, pos, font_size=16, with_labels=False)
    #nx.draw_networkx_nodes(completed_nodes_graph, pos, font_size=16, with_labels=False)
    nx.draw_networkx_nodes(completed_nodes_graph, pos, nodelist=[root_node], node_color='g', node_size=500, alpha=0.8)
    nx.draw_networkx_nodes(completed_nodes_graph, pos, nodelist=solution_nodes, node_color='y', node_size=500, alpha=0.8)
    nx.draw_networkx_labels(completed_nodes_graph, pos)
    plt.show() # Actually show it

def get_random_path_time(root_node):
    node = root_node
    while len(node.children) != 0:
        node = node.children[random.randint(0,len(node.children)-1)]
    if node.SAT_time == None:
        return None
    return node.SAT_time_to_node + node.SAT_time

def get_analysis(analysis, all_nodes):
    # Perform analysis and get all sorts of stats.. so what do we want?

    # Get best time overall
    SAT_leaf_nodes = [node for node in all_nodes if len(node.children) == 0 and node.solution_found]
    if len(SAT_leaf_nodes) == 0:
        analysis.best_time_node = None
        return
    
    best_node = Node()
    best_node.SAT_time_to_node = 0
    best_node.SAT_time = float("inf")
    for node in SAT_leaf_nodes:
        if node.SAT_time + node.SAT_time_to_node < best_node.SAT_time + best_node.SAT_time_to_node:
            best_node = node
    analysis.best_time_overall = best_node.SAT_time + best_node.SAT_time_to_node
    analysis.best_time_node = best_node

    # For limited all: All leaf nodes in the finishing band, the total time to get to them
    if analysis.encoding == "limited" and not analysis.only_fastest_path:
        leaf_nodes = [node for node in all_nodes if len(node.children) == 0]
        #SAT_leaf_nodes = [node for node in leaf_nodes if node.solution_found]
        analysis.leaf_nodes = leaf_nodes


    # For limited all: if taking random path, probability of finishing in band, distribution of time if finished in (just take mean for now)
    if analysis.encoding == "limited" and not analysis.only_fastest_path:
        # random runs for time if random
        runs_to_try = int(math.sqrt(len(all_nodes)) * 1000) # TODO Arbitraary, a guess..
        runs_completed = 0
        time_sum = 0
        for run in range(runs_to_try):
            
            res = get_random_path_time(all_nodes[0])
            if res:
                runs_completed += 1
                time_sum += res
                
        analysis.expected_random_path_SAT_time = time_sum / runs_completed
        analysis.prob_SAT_in_band = runs_completed / runs_completed

    # Average branching factor for non child
    non_child_node_count = 0
    branching_factor_sum = 0
    frontier = [all_nodes[0]]
    while len(frontier) != 0:
        node = frontier.pop()
        if len(node.children) != 0:
            non_child_node_count += 1
            branching_factor_sum += len(node.children)
            frontier.extend(node.children)
    analysis.average_branching_factor = branching_factor_sum / non_child_node_count
    # Do branching factor things?, position of SAT when not found
    

def get_nodes_from_filename(nodes_filename):
    with open(nodes_filename, "rb") as nodes_file:
        return pickle.load(nodes_file)

def get_local_path(path_root, old_path):
    return path_root + "/tmp_files/" + old_path.split("/tmp_files/")[-1]

def print_online_node_analysis(analysis):
    print(analysis.problem, end=', ')
    print(analysis.encoding, end=', ')
    print(analysis.only_fastest_path, end=', ')
    print(analysis.best_time_overall, end=', ')
    print(analysis.average_branching_factor, end=', ')
    if not analysis.only_fastest_path and analysis.encoding == "limited":
        print(analysis.prob_SAT_in_band, end=', ')
        print(analysis.expected_random_path_SAT_time, end='')
    else:
        print('', end= ' , ')

    #endline
    print('')

def print_problem_analysis(analysiss):
    problem_to_analysis_set = {}
    for analysis in analysiss:
        if analysis.problem not in problem_to_analysis_set.keys():
            problem_to_analysis_set[analysis.problem] = set()
        problem_to_analysis_set[analysis.problem].add(analysis)

    print("problem, all time, some time, flat time")
    for problem in sorted(problem_to_analysis_set.keys()):
        a_set = problem_to_analysis_set[problem]
        
        print(problem, end=', ')
        alls = []
        somes = []
        flats = []
        
        # all
        alls = [a for a in a_set if a.encoding == "limited" and not a.only_fastest_path]
        if len(alls) == 1:
            print(alls[0].best_time_overall, end=', ')
        else:
            print('', end=', ')        
            
        # some
        somes = [a for a in a_set if a.encoding == "limited" and a.only_fastest_path]
        if len(somes) == 1:
            print(somes[0].best_time_overall, end=', ')
        else:
            print('', end=', ')        

        # all
        flats = [a for a in a_set if a.encoding == "flat"]
        if len(flats) == 1:
            print(flats[0].best_time_overall, end='')
        else:
            print('', end='')        

        print('')


##########
## MAIN ##
##########


for wrapper_log_filename in sys.argv[1:]:
    with open(wrapper_log_filename, "r") as in_file:
        all_lines = in_file.readlines()
        nodes = get_nodes_from_lines(all_lines)
        
        analysis = Analysis(all_lines[0:7])
        analysis.nodes = nodes
        get_analysis(analysis, nodes)
        if analysis.best_time_node == None:
            print("No SAT runs")
        else:
            print("global optimal SAT time: " + str(analysis.best_time_node.SAT_time_to_node + analysis.best_time_node.SAT_time) + ", horizon: " + str(analysis.best_time_node.horizon))
