cd ..
echo solving depot prob01.pddl
timeout 3600 python3 -u ./src/limited_1_wrapper.py ../AI_benchmarks/depot/domain.pddl ../AI_benchmarks/depot/problem01.pddl tmp_files/ > scrap > log_depot_limited_1_prob01

echo solving depot prob02.pddl
timeout 3600 python3 -u ./src/limited_1_wrapper.py ../AI_benchmarks/depot/domain.pddl ../AI_benchmarks/depot/problem02.pddl tmp_files/ > scrap > log_depot_limited_1_prob02

echo solving depot prob03.pddl
timeout 3600 python3 -u ./src/limited_1_wrapper.py ../AI_benchmarks/depot/domain.pddl ../AI_benchmarks/depot/problem03.pddl tmp_files/ > scrap > log_depot_limited_1_prob03

echo solving depot prob04.pddl
timeout 3600 python3 -u ./src/limited_1_wrapper.py ../AI_benchmarks/depot/domain.pddl ../AI_benchmarks/depot/problem04.pddl tmp_files/ > scrap > log_depot_limited_1_prob04

echo solving depot prob05.pddl
timeout 3600 python3 -u ./src/limited_1_wrapper.py ../AI_benchmarks/depot/domain.pddl ../AI_benchmarks/depot/problem05.pddl tmp_files/ > scrap > log_depot_limited_1_prob05

echo solving depot prob06.pddl
timeout 3600 python3 -u ./src/limited_1_wrapper.py ../AI_benchmarks/depot/domain.pddl ../AI_benchmarks/depot/problem06.pddl tmp_files/ > scrap > log_depot_limited_1_prob06

echo solving depot prob07.pddl
timeout 3600 python3 -u ./src/limited_1_wrapper.py ../AI_benchmarks/depot/domain.pddl ../AI_benchmarks/depot/problem07.pddl tmp_files/ > scrap > log_depot_limited_1_prob07

echo solving depot prob08.pddl
timeout 3600 python3 -u ./src/limited_1_wrapper.py ../AI_benchmarks/depot/domain.pddl ../AI_benchmarks/depot/problem08.pddl tmp_files/ > scrap > log_depot_limited_1_prob08

echo solving depot prob09.pddl
timeout 3600 python3 -u ./src/limited_1_wrapper.py ../AI_benchmarks/depot/domain.pddl ../AI_benchmarks/depot/problem09.pddl tmp_files/ > scrap > log_depot_limited_1_prob09

echo solving depot prob10.pddl
timeout 3600 python3 -u ./src/limited_1_wrapper.py ../AI_benchmarks/depot/domain.pddl ../AI_benchmarks/depot/problem10.pddl tmp_files/ > scrap > log_depot_limited_1_prob10

echo Done
