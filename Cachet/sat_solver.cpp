/* =========FOR INTERNAL USE ONLY. NO DISTRIBUTION PLEASE ========== */

/*********************************************************************
 Copyright 2000-2001, Princeton University.  All rights reserved. 
 By using this software the USER indicates that he or she has read, 
 understood and will comply with the following:

 --- Princeton University hereby grants USER nonexclusive permission 
 to use, copy and/or modify this software for internal, noncommercial,
 research purposes only. Any distribution, including commercial sale 
 or license, of this software, copies of the software, its associated 
 documentation and/or modifications of either is strictly prohibited 
 without the prior consent of Princeton University.  Title to copyright
 to this software and its associated documentation shall at all times 
 remain with Princeton University.  Appropriate copyright notice shall 
 be placed on all software copies, and a complete copy of this notice 
 shall be included in all copies of the associated documentation.  
 No right is  granted to use in advertising, publicity or otherwise 
 any trademark,  service mark, or the name of Princeton University. 


 --- This software and any associated documentation is provided "as is" 

 PRINCETON UNIVERSITY MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS 
 OR IMPLIED, INCLUDING THOSE OF MERCHANTABILITY OR FITNESS FOR A 
 PARTICULAR PURPOSE, OR THAT  USE OF THE SOFTWARE, MODIFICATIONS, OR 
 ASSOCIATED DOCUMENTATION WILL NOT INFRINGE ANY PATENTS, COPYRIGHTS, 
 TRADEMARKS OR OTHER INTELLECTUAL PROPERTY RIGHTS OF A THIRD PARTY.  

 Princeton University shall not be liable under any circumstances for 
 any direct, indirect, special, incidental, or consequential damages 
 with respect to any claim by USER or any third party on account of 
 or arising from the use, or inability to use, this software or its 
 associated documentation, even if Princeton University has been advised
 of the possibility of those damages.
*********************************************************************/
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <set>
#include <vector>
#include <cstring>
#include "SAT.h"

using namespace std;

#define MAX_LINE_LENGTH    65536
#define MAX_WORD_LENGTH    64

#include<set>

set<int> unit_false;
set<int> unit_true;


//This cnf parser function is based on the GRASP code by Joao Marques Silva
void read_cnf(SAT_Manager mng, char * filename )
{
    char line_buffer[MAX_LINE_LENGTH];
    char word_buffer[MAX_WORD_LENGTH];
    set<int> clause_vars;
    set<int> clause_lits;
    int line_num = 0;

    ifstream inp (filename, ios::in);
    if (!inp) {
	cerr << "Can't open input file" << endl;
	exit(1);
    }
    while (inp.getline(line_buffer, MAX_LINE_LENGTH)) {
	++ line_num;
	if (line_buffer[0] == 'c') { 
	    continue; 
	}
	else if (line_buffer[0] == 'p') {
	    int var_num;
	    int cl_num;

	    int arg = sscanf (line_buffer, "p cnf %d %d", &var_num, &cl_num);
	    if( arg < 2 ) {
		cerr << "Unable to read number of variables and clauses"
		     << "at line " << line_num << endl;
		exit(3);
	    }
	    SAT_SetNumVariables(mng, var_num); //first element not used.
	}
	else if (line_buffer[0] == 'w')		// added by sang
	{
		;	// skip lines starting with w
	}
	else {                             // Clause definition or continuation
	    char *lp = line_buffer;
	    do {
		char *wp = word_buffer;
		while (*lp && ((*lp == ' ') || (*lp == '\t'))) {
		    lp++;
		}
		while (*lp && (*lp != ' ') && (*lp != '\t') && (*lp != '\n')) {
		    *(wp++) = *(lp++);
		}
		*wp = '\0';                                 // terminate string

		if (strlen(word_buffer) != 0) {     // check if number is there
		    int var_idx = atoi (word_buffer);
		    int sign = 0;

		    if( var_idx != 0) {
			if( var_idx < 0)  { var_idx = -var_idx; sign = 1; }
			clause_vars.insert(var_idx);
			clause_lits.insert( (var_idx << 1) + sign);
		    } 	
		    else {
			//add this clause
			if (clause_vars.size() != 0 && (clause_vars.size() == clause_lits.size())) { //yeah, can add this clause
			    vector <int> temp;
			    for (set<int>::iterator itr = clause_lits.begin();
				 itr != clause_lits.end(); ++itr)
				{
					temp.push_back (*itr);
				}

                            if(temp.size() == 1){

                                int var_idx = (*clause_lits.begin())>>1;
                                int var_sign = (*clause_lits.begin())&0x1;

                                if(0 == var_sign){
                                    // cerr<<"T"<<(var_idx)<<endl;
                                    unit_true.insert(var_idx);
                                } else {
                                    // cerr<<"F"<<-1 * (var_idx)<<endl;
                                    unit_false.insert(-1 * (var_idx));
                                }
                            }
			    SAT_AddClause(mng, & temp.begin()[0], temp.size() );
			}
			else {} //it contain var of both polarity, so is automatically satisfied, just skip it
			clause_lits.clear();
			clause_vars.clear();
		    }
		}
	    }
	    while (*lp);
	}
    }
    if (!inp.eof()) {
	cerr << "Input line " << line_num <<  " too long. Unable to continue..." << endl;
	exit(2);
    }
//    assert (clause_vars.size() == 0); 	//some benchmark has no 0 in the last clause
    if (clause_lits.size() && clause_vars.size()==clause_lits.size() ) {
	vector <int> temp;
	for (set<int>::iterator itr = clause_lits.begin();
	     itr != clause_lits.end(); ++itr)
	    temp.push_back (*itr);
	SAT_AddClause(mng, & temp.begin()[0], temp.size() );
    }
    clause_lits.clear();
    clause_vars.clear();
}


void handle_result(SAT_Manager mng, int outcome, char * filename, bool quiet)
{
    char * result = "UNKNOWN";
    switch (outcome) {
    case SATISFIABLE:
	//cout << "Instance satisfiable" << endl;
//following lines will print out a solution if a solution exist
	/* for (int i=1, sz = SAT_NumVariables(mng); i<= sz; ++i) {
	    switch(SAT_GetVarAsgnment(mng, i)) {
	    case -1:	
		cout <<"("<< i<<")"; break;
	    case 0:
		cout << "-" << i; break;
	    case 1:
		cout << i ; break;
	    default:
		cerr << "Unknown variable value state"<< endl;
		exit(4);
	    }
	    cout << " ";
	} */
	result  = "SAT";
	cout << endl;
	break;
    case UNSATISFIABLE:
	result  = "UNSAT";
	cout << endl;
	//cout << "Instance unsatisfiable" << endl << endl;
	break;
    case TIME_OUT:
	result  = "ABORT : TIME OUT"; 
	cout << "Time out, unable to determing the satisfiablility of the instance";
	cout << endl;
	break;
    case MEM_OUT:
	result  = "ABORT : MEM OUT"; 
	cout << "Memory out, unable to determing the satisfiablility of the instance";
	cout << endl;
	break;
    default:
	cerr << "Unknown outcome" << endl;
	exit (5);
    }	
    if (!quiet)
	{
    cout << "Number of Decisions\t\t\t" << SAT_NumDecisions(mng)<< endl;	// order changed
	cout << "Max Decision Level\t\t\t" << SAT_MaxDLevel(mng) << endl;	// order changed
    cout << "Number of Variables\t\t\t" << SAT_NumVariables(mng) << endl;
    cout << "Original Num Clauses\t\t\t" << SAT_InitNumClauses(mng) << endl;
    cout << "Original Num Literals\t\t\t" << SAT_InitNumLiterals(mng) << endl;
    cout << "Added Conflict Clauses\t\t\t" << SAT_NumAddedClauses(mng)- SAT_InitNumClauses(mng)<< endl;
    cout << "Added Conflict Literals\t\t\t" << SAT_NumAddedLiterals(mng) - SAT_InitNumLiterals(mng) << endl;
    cout << "Deleted Unrelevant clauses\t\t" << SAT_NumDeletedClauses(mng) <<endl;
    cout << "Deleted Unrelevant literals\t\t" << SAT_NumDeletedLiterals(mng) <<endl;
    cout << "Number of Implications\t\t\t" << SAT_NumImplications(mng)<< endl;
    //other statistics comes here
    cout << "Total Run Time\t\t\t\t" << SAT_GetCPUTime(mng) << endl << endl;
	}
//    cout << "RESULT:\t" << filename << " " << result << " RunTime: " << SAT_GetCPUTime(mng)<< endl;
	//if (result  == "UNSAT")
	//	cout  << "UNSAT" << endl;
	//else 
	if (SAT_GMP(mng))
		{
			long double solutions = SAT_NumSolutions(mng);
			if (solutions > 1000000 && !quiet)
				cout << "In scientific number form\t\t" << solutions << endl;
		}
	else 
	{
		if (!quiet)
			cout << "Satisfying probability\t\t\t" << SAT_SatProb(mng) << endl;
		cout << "Number of solutions\t\t\t" << SAT_NumSolutions(mng) << endl;
	}
	cout << endl << endl;
}
void output_status(SAT_Manager mng)
{
    cout << "Dec: " << SAT_NumDecisions(mng)<< "\t ";
    cout << "AddCl: " << SAT_NumAddedClauses(mng) <<"\t";
    cout << "AddLit: " << SAT_NumAddedLiterals(mng)<<"\t";
    cout << "DelCl: " << SAT_NumDeletedClauses(mng) <<"\t";
    cout << "DelLit: " << SAT_NumDeletedLiterals(mng)<<"\t";
    cout << "NumImp: " << SAT_NumImplications(mng) <<"\t";
    cout << "AveBubbleMove: " << SAT_AverageBubbleMove(mng) <<"\t";
    //other statistics comes here
    cout << "RunTime:" << SAT_GetElapsedCPUTime(mng) << endl;
}

void verify_solution(SAT_Manager mng)
{
    int num_verified = 0;
    for ( int cl_idx = SAT_GetFirstClause (mng); cl_idx >= 0; 
	  cl_idx = SAT_GetNextClause(mng, cl_idx)) {
	int len = SAT_GetClauseNumLits(mng, cl_idx);
	int * lits = new int[len+1];
	SAT_GetClauseLits( mng, cl_idx, lits);
	int i;
	for (i=0; i< len; ++i) {
	    int v_idx = lits[i] >> 1;
	    int sign = lits[i] & 0x1;
	    int var_value = SAT_GetVarAsgnment( mng, v_idx);
	    if( (var_value == 1 && sign == 0) ||
		(var_value == 0 && sign == 1) ) break;
	}
	if (i >= len) {
	    cerr << "Verify Satisfiable solution failed, please file a bug report, thanks. " << endl;
	    exit(6);
	}
	delete [] lits;
	++ num_verified;
    }
    cout << num_verified << " Clauses are true, Verify Solution successful. ";
}

#include<cassert>

#include <set>
#include <map>
#include <sstream>


#include"binary_tracker.h"

set<pair<int, int> > mutexes;

bool reporting_file_not_open;
ofstream reporting_file;
std::string cachet_output_file;
int variables_at_step_zero;

set<int> unit_clauses;

set<int> variables_for_which_we_have_no_positive_evidence;
set<int> variables_for_which_we_have_no_negative_evidence;

int main(int argc, char ** argv)
{
    SAT_Manager mng = SAT_InitManager();
    bool quiet = false;	// for output control
    bool static_heuristic = false;
    if (argc < 2) {
	cout << "cachet version 1.2, December 2005" << endl
             << "copyright 2005, University of Washington" << endl
             << "incorporating code from zchaff, copyright 2004, Princeton University" << endl
             << "Usage: "<< argv[0] << " cnf_file [-t time_limit]" << endl
             << " [-c cache_size]" 
             << " [-o oldest_cache_entry]" 
             << " [-l cache_clean_limit]" 
             << " [-h heuristic_selection]"
             << " [-b backtrack_factor]"
             << " [-f far_backtrack_enabled]"
             << " [-r cross_implication_off] "
             << " [-a adjusting_component_ordering_off] "
             << " [-n max_num_learned_clause] " 
             << " [-q quiet] " << endl;
	return 2;
    }
    cout << "cachet version 1.2, December 2005" << endl
         << "copyright 2005, University of Washington" << endl
         << "incorporating code from zchaff, copyright 2004, Princeton University" << endl;
    cout <<"Solving " << argv[1] << " ......" << endl;
    //if (argc == 2) {
    //read_cnf (mng, argv[1] );
    //}
    //else {
    //read_cnf (mng, argv[1] );
    //SAT_SetTimeLimit(mng, atoi(argv[2]));
    //}

    assert(argc >= 4);

    cachet_output_file = string(argv[2]);
    //cerr<<"Cachet is writing output to : "<<cachet_output_file<<endl;
    variables_at_step_zero = atoi(argv[3]);

        
    int max_variables =0;// atoi(argv[4]);
    int first_fluent_var =0;// atoi(argv[5]);
    int last_fluent_var =0;// atoi(argv[5]);
        
    int current = 0;
    if(0 != variables_at_step_zero && (isalpha(argv[4][0]) || ispunct(argv[4][0]))){
        ifstream input;
        input.open(argv[4]);
        current = 4;
        
        if ( input.fail() ) {
            cerr<<"UNRECOVERABLE ERROR : No input file. \n"; exit(-1);
        }
    
        string tmp;
        while(!input.eof()){
            getline(input, tmp);
            istringstream iss(tmp);
            int i,j;
            iss>>i>>j;
                
            //cerr<<i<<" "<<j<<endl;
            mutexes.insert(pair<int, int>(i,j));
        }
            
        input.close();
    } else if (0 != variables_at_step_zero) {
        max_variables = atoi(argv[4]);/*number of variables at step 0*/
        first_fluent_var = atoi(argv[5]);
        last_fluent_var = atoi(argv[6]);
        current = 6;

        for(int i = first_fluent_var; i <= last_fluent_var; i++){
            for(int j = i + 1; j <=  last_fluent_var; j++){
                //cerr<<i<<" "<<j<<endl;
                mutexes.insert(pair<int, int>(i + max_variables, j + max_variables));
            }
        }
    } else {
         current = 6;
    }

    read_cnf (mng, argv[1] );
    
    if(max_variables > 0){

        set<int> _unit_false;
        set<int> _unit_true;
        assert(unit_true.size());
        for(set<int>::const_iterator i = unit_false.begin()
                ; i != unit_false.end()
                ; i++){
            _unit_false.insert(*i + max_variables);
        }
        unit_false = _unit_false;

        for(set<int>::const_iterator i = unit_true.begin()
                ; i != unit_true.end()
                ; i++){
            // cerr<<"Unit true "<<(*i + max_variables)<<endl;
            // char ch; cin>>ch;
            _unit_true.insert(*i + max_variables);
        }
        unit_true = _unit_true;

        set<pair<int, int> > to_keep;
        for(set<pair<int, int> >::const_iterator mutex = mutexes.begin()
                ; mutex != mutexes.end()
                ; mutex++){
            if (unit_true.find(mutex->first) != unit_true.end() 
                && unit_true.find(mutex->second) != unit_true.end()){
#define TRANSFORM_FOR_NATHAN(X) ((variables_at_step_zero > 0)?(((X - 1) % variables_at_step_zero) + 1):X)
                cerr<<"Throwing "<<mutex->first<<" "<<mutex->second<<endl;
                cerr<<"Throwing "<<TRANSFORM_FOR_NATHAN(mutex->first)<<" "<<TRANSFORM_FOR_NATHAN(mutex->second)<<endl;
            }
            
            // if(unit_false.find(mutex->first) != unit_false.end() 
            //    || unit_false.find(mutex->second) != unit_false.end()){
            //     cerr<<"Throwing "<<mutex->first<<" "<<mutex->second<<endl;
            // } else if (unit_true.find(mutex->first) != unit_true.end() 
            //            || unit_true.find(mutex->second) != unit_true.end()){
            //     cerr<<"Throwing "<<mutex->first<<" "<<mutex->second<<endl;
            // } 
            else {
                // cerr<<"Keeping "<<mutex->first<<" "<<mutex->second<<endl;
                to_keep.insert(*mutex);
                // variables_for_which_we_have_no_positive_evidence.insert(mutex->first);
                // variables_for_which_we_have_no_positive_evidence.insert(mutex->second);
            }
        }
        mutexes = to_keep;
    }    
    // char ch; cin>>ch;


    initialize__mutex_management(mutexes);


    // variables_for_which_we_have_no_negative_evidence 
    //     = variables_for_which_we_have_no_positive_evidence;
    

    int option;
    while (++current < argc)
    {
        switch (argv[current][1])
        {
        case 't':	SAT_SetTimeLimit(mng, atoi(argv[++current]));
            break;
        case 'c':	SAT_SetCacheSize(mng, atoi(argv[++current]));
            break;
            //case 'e':	SAT_SetMaxEntry(mng, atoi(argv[++current]));
            //			break;
            //case 'm':	SAT_SetMaxDistance(mng, atoi(argv[++current]));
            //			break;
            //case 'd':	SAT_SetDynamicHeuristic(mng, true);
            //			break;
        case 's':	SAT_SetStaticHeuristic(mng, true);	// false -> true
            static_heuristic = true;
            break;
        case 'h':	option = atoi(argv[++current]);
            if (option < 0 || option > 7)
            {
                cout << "invalid heuristic selection, must be between 0 and 7" << endl;
                exit(0);
            }
            SAT_SetDynamicHeuristic(mng, option);
            break;
        case 'n':	SAT_SetMaxNumLearnedClause(mng, atoi(argv[++current]));
            break;
        case 'o':	SAT_SetOldestEntry(mng, atoi(argv[++current]));
            break;
        case 'l':	SAT_SetCleanLimit(mng, atoi(argv[++current]));
            break;
        case 'r':	SAT_SetCrossFlag(mng, false);	// true -> false
            break;
        case 'a':	SAT_SetAdjustFlag(mng, false);	// true -> false
            break;
        case 'b':	SAT_SetBacktrackFactor(mng, atof(argv[++current]));
            break;
        case 'f':	SAT_SetFarBacktrackFlag(mng, true);
            break;
        case 'q':	SAT_SetQuietFlag(mng, true);	// true -> false
            quiet = true;
            break;
        default:
            cout << "unknown option! " << argv[current] << endl;
            exit(0);
            //break;
        }	// end switch
    }
    int result = SAT_Solve(mng);
    // cout << "Number of solutions: " << _stats.num_solutions << endl;
#ifndef KEEP_GOING
//    if (result == SATISFIABLE)
//		verify_solution(mng);
#endif
    handle_result (mng, result,  argv[1], quiet);


    if(!reporting_file_not_open){ 

        //cerr<<"Cachet is opening output file : "<<cachet_output_file<<endl;
        reporting_file_not_open = true;
        reporting_file.open(cachet_output_file.c_str());
        if (reporting_file.fail()) {
            cerr<<"UNRECOVERABLE ERROR : Cannot write to specified output file. "
                <<cachet_output_file.c_str()<<"  --  "<<cachet_output_file<<"\n"; 
            exit(-1);
        };//"CACHET_LEARNT_CLAUSES.cnf");
    }

    
    // if(!mutexes.size()) {cerr<<"No fluent mutex from solver stack...\n";}

    //Put this back in.
    // cerr<<variables_for_which_we_have_no_positive_evidence.size()<<"\n";
    // cerr<<variables_for_which_we_have_no_negative_evidence.size()<<"\n";
    // char ch; cin>>ch;
    report__mutex_management();
    
// #define TRANSFORM_FOR_NATHAN(X) ((variables_at_step_zero > 0)?(((X - 1) % variables_at_step_zero) + 1):X)
//     if(reporting_file_not_open && mutexes.size()){ 
//         reporting_file<<"c Inferred fluent mutex clauses.\n";
//         for(set<pair<int, int> >::const_iterator mutex = mutexes.begin()
//                 ; mutex != mutexes.end()
//                 ; mutex++){
//             reporting_file<<-1*TRANSFORM_FOR_NATHAN(mutex->first)<<" "<<-1*TRANSFORM_FOR_NATHAN(mutex->second)<<" 0"<<endl;
//         }
        
//     }

    if(reporting_file_not_open){ 
        reporting_file.close();
    }
    
    return 0;
}
