#!/bin/bash

if [ -a  tmp_files ] ; then 
    echo "Already have a folder named tmp_files, so I shall remove it" ; 

    rm -r tmp_files/*
    sudo umount tmp_files/
    rm -r tmp_files
fi
 
mkdir -p tmp_files
sudo mount -t tmpfs -o size=4096M tmpfs tmp_files/
