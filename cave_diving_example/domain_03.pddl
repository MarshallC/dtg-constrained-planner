;; Cave Diving ADL
;; Authors: Nathan Robinson,
;;          Christian Muise, and
;;          Charles Gretton

(define (domain cave-diving-adl)
  (:requirements :typing :action-costs :adl)
  (:types location diver tank quantity)
  (:predicates
    (in-storage ?t - tank)
    (at-diver ?d - diver ?l - location)
    (available ?d - diver)
    (at-surface ?d - diver)
    (decompressing ?d - diver)
    (cave-entrance ?l - location)
    (holding ?d - diver ?t - tank)
    (have-photo ?l - location)
    (woo)
    (hoo)
  )

  (:functions
    (hiring-cost ?d - diver) - number
    (other-cost) - number
    (total-cost) - number
  )

  (:action hire-diver
    :parameters (?d1 - diver)
    :precondition (and      (available ?d1)
                  )
    :effect (and (at-surface ?d1)
                 (not (available ?d1))
                 (increase (total-cost) (hiring-cost ?d1))
            )
  )

  (:action prepare-tank
    :parameters (?d - diver ?t1 - tank)
    :precondition (and (at-surface ?d)
                       (in-storage ?t1)
                  )
    :effect (and (not (in-storage ?t1))
                      (holding ?d ?t1)
                 (increase (total-cost) (other-cost ))
            )
  )

  (:action enter-water
    :parameters (?d - diver ?l - location)
    :precondition (and (at-surface ?d)
                       (cave-entrance ?l)
                  )
    :effect (and (not (at-surface ?d))
                      (at-diver ?d ?l)
                 (increase (total-cost) (other-cost ))
            )
  )

  (:action photograph
    :parameters (?d - diver ?l - location ?t - tank)
    :precondition (and (at-diver ?d ?l)
                       (holding ?d ?t)
                  )
    :effect (and 
                      (have-photo ?l)
                 (increase (total-cost) (other-cost ))
            )
  )

  (:action decompress
    :parameters (?d - diver ?l - location)
    :precondition (and (at-diver ?d ?l)
                       (cave-entrance ?l)
                  )
    :effect (and (not (at-diver ?d ?l))
                      (decompressing ?d)
                 (increase (total-cost) (other-cost ))
            )
  )

)
