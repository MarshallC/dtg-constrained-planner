;; Cave Diving ADL
;; Authors: Nathan Robinson,
;;          Christian Muise, and
;;          Charles Gretton

(define (problem cave-diving-adl-01)
  (:domain cave-diving-adl)
  (:objects
    l0 l1 l2 - location
    d0 - diver
    t0 t1 t2 t3 t4 dummy - tank
    zero one two three four - quantity
  )

  (:init
    (available d0)
    (capacity d0 four)
    (in-storage t0)
    (next-tank t0 t1)
    (next-tank t1 t2)
    (next-tank t2 t3)
    (next-tank t3 dummy)
    
    (cave-entrance l0)
    (connected l0 l1)
    (connected l1 l0)
    (connected l1 l2)
    (connected l2 l1)
    (next-quantity zero one)
    (next-quantity one two)
    (next-quantity two three)
    (next-quantity three four)
    (at-tank t4 l1)
    (full t4)

    (= (hiring-cost d0) 1)
    (= (other-cost ) 1)
    (= (total-cost) 0)
  )

  (:goal
    (and
      (have-photo l2)
      (decompressing d0)
    )
  )

  (:metric minimize (total-cost))

)
