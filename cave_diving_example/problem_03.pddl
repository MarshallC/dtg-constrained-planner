;; Cave Diving ADL
;; Authors: Nathan Robinson,
;;          Christian Muise, and
;;          Charles Gretton

(define (problem cave-diving-adl-01)
  (:domain cave-diving-adl)
  (:objects
    l0 - location
    d0 - diver
    t0 - tank
  )

  (:init
    (available d0)
    (in-storage t0)
    (cave-entrance l0)
    (= (hiring-cost d0) 1)
    (= (other-cost ) 1)
    (= (total-cost) 0)
  )

  (:goal
    (and
      (have-photo l0)
      (decompressing d0)
    )
  )

  (:metric minimize (total-cost))

)
