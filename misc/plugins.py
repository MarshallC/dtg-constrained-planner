""" File:        plugins.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2013-
    Desctiption: 

    Liscence:   [Refine this!]
"""

from importlib import import_module
from utilities import CodeException

class PluginException(CodeException):
    """ An exception to be raised in the even that something goes wrong with
        the plugin setup process. """

class PluginWrapper(object):
    """ PluginWrapper is used to read a plugin file and dynamically load plugins.
    """

    def __init__(self, plugin_path, plugin_list_file_name):
        """ Make a new PluginWrapper.
            (PluginWrapper,  CodeException, int) -> None
        """
        self.plugin_path = plugin_path
        self.plugin_list_file_name = plugin_list_file_name
        self.error_code = "PLU"
        
        self.valid_plugins = {}
        self.default_plugins = []

    def read_plugin_list(self):
        """ Read the plugin list file and populate valid_plugins and default_plugin
            (PluginWrapper) -> None
        """
        try:
            with file(self.plugin_list_file_name) as plugin_list_file:
                plugin_lines = plugin_list_file.readlines()
                if plugin_lines:
                    self.default_plugins = plugin_lines[0].strip().split(' ')   
                else:
                    self.default_plugins = []
                for line in plugin_lines[1:]:
                    try:
                        line_tokens = line.strip().split(' ')
                        self.valid_plugins[line_tokens[0]] = line_tokens[1]
                    except IndexError:
                        raise PluginException("Error: badly formed line in plugin list: {}"\
                            .format(line.strip()), self.error_code)
        except IOError:
            raise PluginException("Error: failed to load plugin list {}.".\
                format(self.plugin_list_file_name), self.error_code)

    def instantiate_plugins(self, plugin_name, plugins_option):
        """ Make an instance of the plugins specified by plugin_names with the
            corresponding options in plugin_options. Raise the appropriate exception if required.
            (PluginWrapper, str, Object) -> None
        """
        self.plugins = []
        if plugin_name not in self.valid_plugins:
            raise PluginException("Error: invalid plugin: {}".format(plugin_name),\
                self.error_code)

            plugin_module = import_module(plugin_path.split("/")[-1] + '.' +\
                self.valid_plugins[plugin_name])
            self.plugins.append(getattr(plugin_module, plugin_module.plugin_class)(plugin_options))

