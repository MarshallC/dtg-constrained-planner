#ifndef PLANGRAPH_GEN_H
#define PLANGRAPH_GEN_H

#include "Global.h"

class PropT
{
public:
    string print();
    
    bool operator ==(const PropT& p2)
    {
        return index == p2.index;
    }
    
    int index;
    
    string name;
    
    int layer;
    
    vector<int> pre;
    vector<int> add;
    vector<int> del;
};

class ActionT
{
public:  
    
    string print();
    
    bool operator == (const ActionT& a2)
    {
        return index == a2.index;
    }
    
    int index;

    string name;
    
    int layer;
    
    long cost;

    vector<int> pre;
    vector<int> add;
    vector<int> del;
    
    int noopProp;
};

extern vector<PropT>     propTemplates;
extern vector<ActionT>   actionTemplates;

extern vector<int>       initialProps;
extern vector<int>       goalProps;

extern vector<vector<vector<bool> > > prop_mutexes;

extern vector<vector<set<int> > > action_pre_mutexes;

extern vector<set<int> > action_conf_mutexes;

extern int currentLayer;

void add_noops();
void initialise_plangraph(set<int> &trueProps, vector<int> &untrueActions,
    bool compute_mutex);
void build_graph_layers(set<int> &trueProps, vector<int> &untrueActions,
    bool compute_mutex);
bool check_goals();

bool actions_are_mutex(int t, int a1, int a2);




#endif
