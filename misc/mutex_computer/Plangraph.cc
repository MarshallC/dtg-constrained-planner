
#include "Plangraph.h"

vector<PropT>     propTemplates;
vector<ActionT>   actionTemplates;

vector<int>       initialProps;
vector<int>       goalProps;

vector<vector<vector<bool> > > prop_mutexes;

vector<vector<set<int> > > action_pre_mutexes;

vector<set<int> > action_conf_mutexes;

int currentLayer;

int int_in_vec(int tInt, const vector<int> &tVec)
{
    for (unsigned int tPos = 0; tPos < tVec.size(); tPos++) { 
        if (tVec[tPos] == tInt) return tPos;
    }
    return -1;
}

bool conf_mutex(int a1, int a2) 
{
    for (unsigned int d1Pos = 0;d1Pos < actionTemplates[a1].del.size();d1Pos++){
        if (int_in_vec(actionTemplates[a1].del[d1Pos], 
            actionTemplates[a2].pre) != -1) return true;
        if (int_in_vec(actionTemplates[a1].del[d1Pos],
            actionTemplates[a2].add) != -1) return true;
    }
    for (unsigned int d2Pos = 0;d2Pos < actionTemplates[a2].del.size();d2Pos++){
        if (int_in_vec(actionTemplates[a2].del[d2Pos],
            actionTemplates[a1].pre) != -1) return true;
        if (int_in_vec(actionTemplates[a2].del[d2Pos],
            actionTemplates[a1].add) != -1) return true;
    }
    return false;
}

//only actions of lower index have a relationship recorded with actions of a
//higher index - this saves half of the space.
void compute_conflict_mutex()
{
    for (unsigned int aPos = 0; aPos < actionTemplates.size(); aPos++) {
        for (unsigned int aPos2 = aPos + 1; aPos2 < actionTemplates.size();
            aPos2++) {
            if (conf_mutex(aPos, aPos2)) {
                action_conf_mutexes[aPos].insert(aPos2);
            }
        }
    }
}

bool actions_are_mutex(int t, int a1, int a2)
{
    if (a1 < a2) {
        return action_conf_mutexes[a1].find(a2)!=action_conf_mutexes[a1].end()|| 
            action_pre_mutexes[t][a1].find(a2)!=action_pre_mutexes[t][a1].end();
    }
    return action_conf_mutexes[a2].find(a1) != action_conf_mutexes[a2].end() ||
        action_pre_mutexes[t][a2].find(a1) != action_pre_mutexes[t][a2].end();
}

void add_noops()
{
    for (unsigned int pPos = 0; pPos < propTemplates.size(); pPos++) {
        actionTemplates.push_back(ActionT());
        actionTemplates.back().index = actionTemplates.size() - 1;
        actionTemplates.back().pre.push_back(pPos);
        propTemplates[pPos].pre.push_back(actionTemplates.back().index);
        actionTemplates.back().add.push_back(pPos);
        propTemplates[pPos].add.push_back(actionTemplates.back().index);
        actionTemplates.back().noopProp = pPos;
        actionTemplates.back().name = propTemplates[pPos].name;
        actionTemplates.back().layer = -1;
    }
}


void initialise_plangraph(set<int> &trueProps, vector<int> &untrueActions,
    bool compute_mutex)
{
    currentLayer = 0;
    
    if (compute_mutex) {
        action_conf_mutexes = vector<set<int> >(actionTemplates.size());
        action_pre_mutexes.push_back(vector<set<int> >(actionTemplates.size()));
        
        prop_mutexes.push_back(vector<vector<bool> >(propTemplates.size()));
        for (unsigned int pPos = 0; pPos < propTemplates.size(); pPos++) {
            prop_mutexes[0][pPos] = vector<bool>(propTemplates.size());
        }
        
        compute_conflict_mutex();
    }
    
    for (unsigned int ssP = 0; ssP < initialProps.size(); ssP++) {
        trueProps.insert(initialProps[ssP]);
        propTemplates[initialProps[ssP]].layer = 0;
    }
    for (unsigned int aPos = 0; aPos < actionTemplates.size(); aPos++) {
        untrueActions.push_back(aPos);
    }
}

void build_graph_layers(set<int> &trueProps, vector<int> &untrueActions,
    bool compute_mutex)
{
    set<int> trueActions;
    set<int> newTrueProps = trueProps;
    currentLayer = 0;
    while (true) {
        //1. apply actions
        //check to add new actions
        int copyBack = 0;
        for (unsigned int aPos = 0; aPos < untrueActions.size(); aPos++) {
            bool addA = true;
            int tA = untrueActions[aPos];
            //check preconditions are present and not mutex
            for (unsigned int prePos = 0; prePos < actionTemplates[tA].pre.size();
                prePos++) {
                int tPre = actionTemplates[tA].pre[prePos];
                if (trueProps.find(tPre) == trueProps.end()) {
                    addA = false;
                    break;
                }
                //check the mutex with previous pres
                if (!addA) break;
                if (compute_mutex) {
                    for (unsigned int prePos2 = 0; prePos2 < prePos; prePos2++) {
                        int oPre = actionTemplates[tA].pre[prePos2];
                        if (prop_mutexes[currentLayer][tPre][oPre]) {
                            addA = false;
                            break;
                        }
                    }
                    if (!addA) break;
                }
            }
            if (addA) {
                actionTemplates[tA].layer = currentLayer;
                trueActions.insert(tA);
                copyBack++;
                for (unsigned int prePos = 0; prePos < 
                    actionTemplates[tA].add.size(); prePos++) {
                    int tP = actionTemplates[tA].add[prePos];
                    if (newTrueProps.find(tP) == newTrueProps.end()) {
                        newTrueProps.insert(tP);
                        propTemplates[tP].layer = currentLayer + 1;
                    }
                }
            } else if (copyBack > 0) {
                untrueActions[aPos - copyBack] = untrueActions[aPos];
            }
        }
        bool anyActions = copyBack != 0;
        while (copyBack-- > 0) untrueActions.pop_back();
        
        if (compute_mutex) {
            //2. Compute action preMutexes
            for (set<int>::const_iterator tAIt = trueActions.begin(); tAIt !=
                trueActions.end(); tAIt++) {
                int tA = *tAIt;
                for (set<int>::const_iterator oAIt = tAIt; oAIt !=
                    trueActions.end(); oAIt++) {
                    if (tAIt == oAIt) continue;
                    int oA = *oAIt;
                    for (unsigned int prePos = 0; prePos < 
                        actionTemplates[tA].pre.size(); prePos++) {
                        int tP = actionTemplates[tA].pre[prePos];
                        for (unsigned int oPrePos = 0; oPrePos <
                            actionTemplates[oA].pre.size(); oPrePos++) {
                            int oP = actionTemplates[oA].pre[oPrePos];
                            if (oP != oA && prop_mutexes[currentLayer][tP][oP]) {
                                if (tA < oA) {
                                    action_pre_mutexes[currentLayer][tA].insert(oA);
                                } else {
                                    action_pre_mutexes[currentLayer][oA].insert(tA);
                                }
                            }    
                        } 
                    }
                }
            }
        }
        trueProps = newTrueProps;
        //2. make the next prop layer and copy the prop mutexes over
        if (compute_mutex) {
            prop_mutexes.push_back(vector<vector<bool> >(propTemplates.size()));
            action_pre_mutexes.push_back(vector<set<int> >(actionTemplates.size()));
            for (unsigned int pPos = 0; pPos < propTemplates.size(); pPos++) {
                prop_mutexes.back()[pPos] = vector<bool>(propTemplates.size());
            }
    
            //3. compute prop mutexes
            for (set<int>::const_iterator tPIt = trueProps.begin(); 
                tPIt != trueProps.end(); tPIt++) {
                int tP = *tPIt;  
                for (set<int>::const_iterator oPIt = tPIt; oPIt != trueProps.end(); 
                    oPIt++) {
                    if (tPIt == oPIt) continue;
                    int oP = *oPIt;
                    //can we achieve both of these together
                    bool canAchieve = false;
                    for (unsigned int tAddPos = 0; tAddPos < 
                        propTemplates[tP].add.size(); tAddPos++) {
                        int tA = propTemplates[tP].add[tAddPos];
                        if (trueActions.find(tA) == trueActions.end()) continue;
                        for (unsigned int oAddPos = 0; oAddPos < 
                            propTemplates[oP].add.size(); oAddPos++) {
                            int oA = propTemplates[oP].add[oAddPos];
                            if (trueActions.find(oA) == trueActions.end()) continue;

                            if (tA == oA || 
                                !actions_are_mutex(currentLayer, tA, oA)) {
                                canAchieve = true;
                                break;
                            }
                        }
                        if (canAchieve) break;
                    }
                    if (!canAchieve) {
                        prop_mutexes[currentLayer+1][tP][oP] = true;
                        prop_mutexes[currentLayer+1][oP][tP] = true;
                    }
                }
            }
            action_pre_mutexes[currentLayer].clear();
        }
        
        ++currentLayer;
        
        if (compute_mutex) {
            if (!anyActions && 
               prop_mutexes[currentLayer-1] == prop_mutexes[currentLayer])  break;
        } else {
            if (!anyActions) break;
        }
    }
}


