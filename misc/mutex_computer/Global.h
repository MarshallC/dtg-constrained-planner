#ifndef GLOBAL_H
#define GLOBAL_H

/* Author:      Nathan Robinson
 * Date:        
 * Description: 
 */

using namespace std;

#include <iostream>
#include <fstream>
#include <set>
#include <vector>
#include <map>
#include <list>
#include <sstream>
#include <string>
#include <cmath>
#include <cassert>
#include <cstdlib>
#include <algorithm>

#define MAX_LINE_LENGTH 1000000

/* This is a simple template for converting objects to strings */
template <typename T> string toString(const T &thing) {
    stringstream os; os << thing; return os.str();
}

/* Convert strings to objects. Example of use:
 * from_string<int>(i, string("ff"), hex)
 */
template <class T>
bool from_string(T& t, const string& s, ios_base& (*f)(ios_base&))
{
  istringstream iss(s);
  return !(iss >> f >> t).fail();
}

#endif


