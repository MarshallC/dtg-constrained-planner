/* Author:      Nathan Robinson
 * Date:        
 * Description: 
 */
 
using namespace std;

#include "Global.h"
#include "Plangraph.h"


bool parse_desc_file(const string &descFileName)
{
    ifstream descFile(descFileName.c_str());
    if (!descFile.is_open()) {
        cerr << "Error: cannot open problem description file: " << descFileName
            << endl;
        return false;
    }
    char tLine[MAX_LINE_LENGTH];
    while (descFile.getline(tLine, MAX_LINE_LENGTH)) {
        string sLine (tLine);
        stringstream ssLine(sLine);
        string lineTypeStr;
        ssLine >> lineTypeStr;
        
        if (lineTypeStr == "action") {
            int aNumber;
            ssLine >> aNumber;
            if (aNumber != actionTemplates.size()) {
                cerr << "Error: action number " << aNumber << " does not match "
                    << actionTemplates.size() << endl;
                return false;
            }
            long aCost;
            ssLine >> aCost;
            if (aCost < 0) {
                cerr << "Error: action number " << aNumber << " has invalid " <<
                    "cost: " << aCost << endl;
                return false;
            }
            int noopInt;
            ssLine >> noopInt;
            assert(noopInt == -1);
            int pref_act;
            ssLine >> pref_act;  //CURRENTLY DO NOTHING WITH THIS INFO
            actionTemplates.push_back(ActionT());
            actionTemplates[aNumber].index = aNumber;
            actionTemplates[aNumber].cost = aCost;
            actionTemplates[aNumber].layer = -1;
            actionTemplates[aNumber].noopProp = -1;
            int noPrecs;
            ssLine >> noPrecs;
            actionTemplates[aNumber].pre = vector<int>(noPrecs);
            for (int pNo = 0; pNo < noPrecs; pNo++) {
                int tPre;
                ssLine >> tPre;
                if (tPre < 0) {
                    cerr << "Error: action " << aNumber <<
                        " has bad precondition " << tPre << endl;
                    return false;
                }
                actionTemplates[aNumber].pre[pNo] = tPre;
            }
            int noAdds;
            ssLine >> noAdds;
            actionTemplates[aNumber].add = vector<int>(noAdds);
            for (int aNo = 0; aNo < noAdds; aNo++) {
                int tAdd;
                ssLine >> tAdd;
                if (tAdd < 0) {
                    cerr << "Error: action " << aNumber << 
                        " has bad add effect " << tAdd << endl;
                    return false;
                }
                actionTemplates[aNumber].add[aNo] = tAdd;
            }
            int noDels;
            ssLine >> noDels;
            actionTemplates[aNumber].del = vector<int>(noDels);
            for (int dNo = 0; dNo < noDels; dNo++) {
                int tDel;
                ssLine >> tDel;
                if (tDel < 0) {
                    cerr << "Error: action " << aNumber << 
                        " has bad delete effect " << tDel << endl;
                    return false;
                }
                actionTemplates[aNumber].del[dNo] = tDel;
            }
            
            string aName;
            string tStr;
            ssLine >> aName;
            while (ssLine >> tStr) {
                aName += " " + tStr;
            }
            if (aName == "") {
                cerr << "Error: action " << aNumber << " has no name." << endl;
                return false;
            }
            actionTemplates[aNumber].name = aName;
        } else if (lineTypeStr == "fluent") {
            int pNumber;
            ssLine >> pNumber;
            if (pNumber != propTemplates.size()) {
                cerr << "Error: fluent number " << pNumber << 
                    " does not match " << propTemplates.size() << endl;
                return false;
            }
            int pref_prop;
            ssLine >> pref_prop; // CURRENTLY DO NOTHING WITH THIS
            propTemplates.push_back(PropT());
            propTemplates[pNumber].index = pNumber;
            propTemplates[pNumber].layer = -1;
            int noPrecs;
            ssLine >> noPrecs;
            propTemplates[pNumber].pre = vector<int>(noPrecs);
            for (int pNo = 0; pNo < noPrecs; pNo++) {
                int tPre;
                ssLine >> tPre;
                if (tPre < 0) {
                    cerr << "Error: fluent " << pNumber <<
                        " has bad precondition " << tPre << endl;
                    return false;
                }
                propTemplates[pNumber].pre[pNo] = tPre;
            }
            int noAdds;
            ssLine >> noAdds;
            propTemplates[pNumber].add = vector<int>(noAdds);
            for (int aNo = 0; aNo < noAdds; aNo++) {
                int tAdd;
                ssLine >> tAdd;
                if (tAdd < 0) {
                    cerr << "Error: fluent " << pNumber << " has bad add effect "
                        << tAdd << endl;
                    return false;
                }
                propTemplates[pNumber].add[aNo] = tAdd;
            }
            int noDels;
            ssLine >> noDels;
            propTemplates[pNumber].del = vector<int>(noDels);
            for (int dNo = 0; dNo < noDels; dNo++) {
                int tDel;
                ssLine >> tDel;
                if (tDel < 0) {
                    cerr << "Error: fluent " << pNumber << 
                        " has bad delete effect " << tDel << endl;
                    return false;
                }
                propTemplates[pNumber].del[dNo] = tDel;
            }
            string pName;
            string tStr;
            while (ssLine >> tStr) {
                if (pName == "") pName += tStr;
                else pName += " " + tStr;
            }
            if (pName == "") {
                cerr << "Error: fluent " << pNumber << " has no name." << endl;
                return false;
            }
            propTemplates[pNumber].name = pName;
        } else if (lineTypeStr == "start") {
            int tInt;
            while (ssLine >> tInt) {
                initialProps.push_back(tInt);
            }
        } else if (lineTypeStr == "goal") {
            int tInt;
            while (ssLine >> tInt) {
                goalProps.push_back(tInt);
            }
        } else {
            if (lineTypeStr != "operator" && lineTypeStr != "cond" &&
                lineTypeStr != "type" && lineTypeStr != "object" &&
                lineTypeStr != "static" && lineTypeStr != "pred" &&
                lineTypeStr != "problem" && lineTypeStr != "pref" &&
                lineTypeStr != "requirements" && lineTypeStr != "multiplier") {
                cerr << "Error parsing problem description. Bad line: " <<
                    tLine << endl;
                return false;
            }
        }
    }
    return true;
}

bool output_desc_file(const string &descFileName, bool keepNoops,
    bool compute_mutex)
{
    ofstream descFile(descFileName.c_str(), fstream::app);
    if (!descFile.is_open()) return false;
    
    if (keepNoops) {
        for (unsigned int aPos = 0; aPos < actionTemplates.size(); aPos++) {
            if (actionTemplates[aPos].noopProp == -1) continue;
            descFile << "action " << aPos << " 0 " << 
              actionTemplates[aPos].noopProp << " 0 1 " <<
              actionTemplates[aPos].noopProp << " 1 " <<
              actionTemplates[aPos].noopProp << " 0 " <<
              actionTemplates[aPos].name << endl;
        }
    }
    for (unsigned int aPos = 0; aPos < actionTemplates.size(); aPos++) {
        if (!keepNoops && actionTemplates[aPos].noopProp != -1) continue;
        descFile << "alayer " << aPos << " " << actionTemplates[aPos].layer << endl;
    }
    for (unsigned int pPos = 0; pPos < propTemplates.size(); pPos++) {
        descFile << "player " << pPos << " " << propTemplates[pPos].layer << endl;
    }
    if (compute_mutex) {
        for (unsigned int t = 0; t < prop_mutexes.size(); t++) {
            for (unsigned int pPos = 0; pPos < prop_mutexes[t].size(); pPos++) {
                vector<int> mVec;
                for (unsigned int oPPos = pPos + 1; oPPos < prop_mutexes[t].size(); oPPos++) {
                    if (prop_mutexes[t][pPos][oPPos]) {
                        mVec.push_back(oPPos);
                    }
                }
                if (mVec.size() > 0) {
                    descFile << "pmutex " << pPos << " " << t;
                    for (unsigned int mPos = 0; mPos < mVec.size(); mPos++) {
                        descFile << " " << mVec[mPos];
                    }
                    descFile << endl;
                }
            }
        }
    }
    return true;
}


int main(int argc, char** argv)
{
    if (argc != 3) {
        cerr << "Error: wrong number of arguments.\n";
        cerr << "usage: ./MutexComputer problemDescription computeMutex" << endl;
        return 1;
    }
    
    string descFileName = string(argv[1]);
    bool compute_mutex = (string(argv[2]) == "1");
    if (!compute_mutex)  assert(string(argv[2]) == "0");

    //cout << "Computing mutex and reachability..." << endl;

    if (!parse_desc_file(descFileName)) return 1;
       
    set<int> trueProps;
    set<int> trueActions;
    vector<int> untrueActions;
    add_noops();
    initialise_plangraph(trueProps, untrueActions, compute_mutex);

    build_graph_layers(trueProps, untrueActions, compute_mutex);
    
    if (!output_desc_file(descFileName, true, compute_mutex)) return 1;
    
    return 0;
}


