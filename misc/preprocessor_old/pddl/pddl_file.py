#! /usr/bin/env python
# -*- coding: latin-1 -*-

import sys
import os.path
import re

import parser

import tasks

def parse_pddl_file(type, filename):
    try:
        return parser.parse_nested_list(file(filename))
    except IOError, e:
        raise SystemExit("Error: Could not read file: %s\nReason: %s." %
                         (e.filename, e))
    except parser.ParseError, e:
        raise SystemExit("Error: Could not parse %s file: %s\n" % (type, filename))

def open(domain_file_name, task_file_name, prefs):
    
    if not os.path.exists(domain_file_name):
        raise SystemExit("Error: domain file does not exist: %s" % domain_file_name)
    if not os.path.exists(task_file_name):
        raise SystemExit("Error: problem file does not exist: %s" % task_file_name)
    domain_pddl = parse_pddl_file("domain", domain_file_name)
    task_pddl = parse_pddl_file("task", task_file_name)
    return tasks.Task.parse(domain_pddl, task_pddl, prefs)

if __name__ == "__main__":
    open().dump()
