import sys

import actions
import axioms
import conditions
import predicates
import pddl_types
import functions
import f_expression
#import preprocessor.pddl as pddl

import string


class Task(object):
    FUNCTION_SYMBOLS = dict()
    def __init__(self, domain_name, task_name, requirements,\
                 types, objects, predicates, functions, init, goal, actions,\
                 axioms, preferences,  pref_weights, use_metric, multiplier):
        self.domain_name = domain_name
        self.task_name = task_name
        self.requirements = requirements
        self.types = types
        self.objects = objects
        self.predicates = predicates
        self.functions = functions
        self.init = init
        self.goal = goal
        self.actions = actions
        self.axioms = axioms
        self.axiom_counter = 0
        self.preferences = preferences
        self.use_metric = use_metric
        self.pref_weights = pref_weights
        self.multiplier = multiplier

    def add_axiom(self, parameters, condition):
        name = "new-axiom@%d" % self.axiom_counter
        self.axiom_counter += 1
        axiom = axioms.Axiom(name, parameters, condition)
        self.predicates.append(predicates.Predicate(name, parameters))
        self.axioms.append(axiom)
        return axiom


    @staticmethod
    def parse(domain_pddl, task_pddl, prefs):
        domain_name, requirements, types, constants, predicates, functions, actions, axioms \
                     = parse_domain(domain_pddl)

        #Modified by Nathan Robinson 2011/10/19 -- use_metric now can be False,
        #"minimize-total-cost", "minimize-violated-preferences", or
        #"maximize-net-benifit"
        #preferences is a list of preference name and weight pairs
        task_name, task_domain_name, objects, init, goal, preferences, \
            pref_weights, use_metric, multiplier = parse_task(task_pddl)

        assert domain_name == task_domain_name
        objects = constants + objects
        init += [conditions.Atom("=", (obj.name, obj.name)) for obj in objects]

        if preferences and not prefs == '0':
            assert prefs == 'kg' or prefs == 'kgo' or prefs == 'maratea'
            
            if prefs == 'kg' or prefs == 'kgo':
            
                predicates.append(predicates.Predicate("__normal_mode", [], True))
                normal_mode = conditions.Atom("__normal_mode", [])
                #add normal_mode to the start state
                init.append(normal_mode)
                
                predicates.append(predicates.Predicate("__end_mode", [], True))
                end_mode = conditions.Atom("__end_mode", [])
                
                enter_end_mode_effect = [effects.Effect([], conditions.Truth(), normal_mode.negate() ),\
                    effects.Effect([], conditions.Truth(), end_mode )]
                actions.append(actions.Action("__enter_end_mode", [], \
                    conditions.Truth(), enter_end_mode_effect,
                    f_expression.Increase(f_expression.PrimitiveNumericExpression("total-cost", []), \
                        f_expression.NumericConstant(0)), True, -1))
                
                for action in actions:
                    if action.precondition.__class__ == conditions.Truth:
                        action.precondition = normal_mode
                    elif action.precondition.__class__ == conditions.Conjunction:
                        action.precondition = conditions.Conjunction(list(action.precondition.parts) + [normal_mode])
                    else:
                        action.precondition = conditions.Conjunction([action.precondition, normal_mode])
            
            #1. we achieve a preference 
            
            pref_id = 0
            pref_ids = {}
            pref_goals = {}
            new_goal_parts = []
            pref_copies = {}
            for pref_tuple in preferences:
                pref = pref_tuple[0]
                pref_name = pref_tuple[1]

                if pref_name not in pref_ids:
                    pref_ids[pref_name] = pref_id
                    pid = pref_id
                    pref_id += 1
                    predicates.append(predicates.Predicate("__pref_" + pref_name, [], True))
                    tGoal = conditions.Atom("__pref_" + pref_name, [])
                    new_goal_parts.append(tGoal)
                    pref_goals[pid] = tGoal
                    pref_copies[pid] = 0
                else:
                    pid = pref_ids[pref_name]
                    pref_copies[pid] += 1
                
                achieve_prec = pref
                effect_list = [effects.Effect([], conditions.Truth(), tGoal)]
                if prefs == 'kg' or (prefs == 'kgo' and pid == 0):
                    assert not achieve_prec.__class__ == conditions.Truth, "Trivially true preference %s" % pref_name
                    if achieve_prec.__class__ == conditions.Conjunction:
                        achieve_prec = conditions.Conjunction(list(achieve_prec.parts) + [end_mode])
                    else:
                        achieve_prec = conditions.Conjunction([achieve_prec, end_mode])
                elif prefs == 'kgo':
                    assert not achieve_prec.__class__ == conditions.Truth, "Trivially true preference %s" % pref_name 
                    if achieve_prec.__class__ == conditions.Conjunction:
                        achieve_prec = conditions.Conjunction(list(achieve_prec.parts) + [pref_goals[pid-1]])
                    else:
                        achieve_prec = conditions.Conjunction([achieve_prec, pref_goals[pid-1]])
                        
                if pref_copies[pid] == 0:
                    achieve_name = "__achieve_pref_" + pref_name
                else:
                    achieve_name = "__achieve_pref_" + pref_name + "_copy_" + str(pref_copies[pid])
                achieve_cost = f_expression.Increase(f_expression.PrimitiveNumericExpression("total-cost", []), \
                        f_expression.NumericConstant(0))
                actions.append(actions.Action(achieve_name, [], \
                    achieve_prec, effect_list, achieve_cost, True, pref_name))
                
                if pref_copies[pid] == 0:
                
                    if prefs == 'kg' or (prefs == 'kgo' and pid == 0):
                        fail_pre = end_mode
                        fail_cost = f_expression.Increase(f_expression.PrimitiveNumericExpression("total-cost", []), \
                            f_expression.NumericConstant(pref_weights[pref_name]))
                    elif prefs == 'kgo':
                        fail_pre = pref_goals[pid-1]
                        fail_cost = f_expression.Increase(f_expression.PrimitiveNumericExpression("total-cost", []), \
                            f_expression.NumericConstant(pref_weights[pref_name]))
                    else:
                        fail_pre = conditions.Truth()
                        fail_cost = f_expression.Increase(f_expression.PrimitiveNumericExpression("total-cost", []), \
                            f_expression.NumericConstant(0))
                            
                    actions.append(actions.Action("__fail_pref_" + pref_name, [], fail_pre, effect_list, fail_cost, True, -1))
            
            goal = conditions.Conjunction(list(goal.parts) + new_goal_parts)
            
        return Task(domain_name, task_name, requirements, types, objects,
                    predicates, functions, init, goal, actions, axioms, preferences, pref_weights, use_metric, multiplier)

 
    def dump(self):
        print "Problem %s: %s [%s]" % (self.domain_name, self.task_name,
                                       self.requirements)
        print "Types:"
        for type in self.types:
            print "  %s" % type
        print "Objects:"
        for obj in self.objects:
            print "  %s" % obj
        print "Predicates:"
        for pred in self.predicates:
            print "  %s" % pred
        print "Functions:"
        for func in self.functions:
            print "  %s" % func
        print "Init:"
        for fact in self.init:
            print "  %s" % fact
        print "Goal:"
        self.goal.dump()
        print "Actions:"
        for action in self.actions:
            action.dump()
        if self.axioms:
            print "Axioms:"
            for axiom in self.axioms:
                axiom.dump()
                
        if self.preferences:
            print "Preferences:"
            for pref in self.preferences:
                print 'Pref:', pref[1]
                pref[0].dump()
        if self.pref_weights:
            print "Preference weights:"
            for pref, weight in self.pref_weights.iteritems():
                print "%s: %s" % (pref, weight)
        print "Metric: ", self.use_metric


    def dump_formatted(self, ground_atoms, ground_actions, out_file, prefs):
        print >> out_file, "problem %s %s" % (self.domain_name, self.task_name)
        print >> out_file, "requirements %s" % self.requirements
        
        typeStrToObjects = {}
        typeStrToType = {}
        typeStrToTypeIndex = {}
        type_strs = []
        type_set = set()   # FOR SOME REASON A TYPE APPEARS TWICE HERE?????
        tid = 0
        for type in self.types:
            if type.name in type_set: continue
            type_set.add(type.name)
            typeStrToObjects[type.name] = []
            typeStrToType[type.name] = type
            typeStrToTypeIndex[type.name] = tid
            tid += 1
            type_strs.append(type.name)
        #Objects and types  
        for oid, obj in enumerate(self.objects):
            print >> out_file, "object %s %s" % (oid, obj.name)
            typeStrToObjects["object"].append(oid)
            if not obj.type == "object":
                typeStrToObjects[obj.type].append(oid)
            for otype in typeStrToType[obj.type].supertype_names:
                if not otype == "object":
                    typeStrToObjects[otype].append(oid)
        for type in type_strs:
            print >> out_file, "type %s %s" % (typeStrToTypeIndex[type], type),
            for obj in typeStrToObjects[type]:
                print >> out_file, "%s" % obj,
            print >> out_file
        #We do not print out the = predicates
        
        pid = 0
        for pred in self.predicates:
            if pred.name == '=' or \
                (prefs != 'kg' and prefs != 'kgo' and pred.name[:7] == "__pref_"):
                continue
            print >> out_file, "pred %s %s" % (pid, pred.name),
            for arg in pred.arguments:
                if arg.type.__class__ == str:
                    print >> out_file, "1 %s" % typeStrToTypeIndex[arg.type], 
                else:
                    assert arg.type.__class__ == list
                    print >> out_file, len(arg.type),
                    for atype in arg.type:
                        if atype == 'either': continue
                        print >> out_file, typeStrToTypeIndex[atype], 
            pid += 1
            print >> out_file
        #operator 0 load-truck 1 obj 1 obj 1 truck 1 truck 1 location 1 loc
        #cond 0 load-truck PRE at 2 obj loc
        condCtr = 0
        operatorToID = {}
        oid = 0
        for operator in self.actions:
            if prefs != 'kg' and prefs != 'kgo' and operator.pref_action: continue
            operatorToID[operator] = oid
            print >> out_file, "operator %s %s" % (oid, operator.name),
            oid += 1
            for param in operator.parameters:
                assert param.type.__class__ == str
                assert param.name.__class__ == str
                print >> out_file, "1 %s 1 %s" % (param.name, param.type),
            print >> out_file
            if isinstance(operator.precondition, conditions.Literal):
                operator.precondition = conditions.Conjunction([operator.precondition])
                
            assert operator.precondition.__class__ == conditions.Conjunction, \
                "Precondition must be a conjunction: %s" % operator.precondition.__class__
            
            for prec in operator.precondition.parts:
                assert prec.__class__ == conditions.Atom, "PREC: %s" % prec.__class__
                print >> out_file, "cond %s %s PRE %s" % (condCtr, operator.name, prec.predicate),
                for arg in prec.args:
                    print >> out_file, arg,
                print >> out_file 
                condCtr += 1
            for eff in operator.effects:
                assert eff.condition.__class__ == conditions.Truth, \
                   "We do not currently handle conditional effects: %s" % eff.condition.__class__
                print >> out_file, "cond %s %s" % (condCtr, operator.name),
                if eff.literal.negated:
                    print >> out_file, "DEL",
                else:
                    print >> out_file, "ADD",
                print >> out_file, eff.literal.predicate,
                for arg in eff.literal.args:
                    print >> out_file, arg,
                print >> out_file
                condCtr += 1
        
        #make the ids for the actions
        actToID = {}
        aid = 0

        for aindex, ground_action in enumerate(ground_actions):
            if prefs != 'kg' and prefs != 'kgo' and ground_action.pref_action:
                continue

            actToID[ground_action] = aid
            aid+=1
        
        #make the ids for the fluents
        fluentToID = {}
        fluentToStr = {}
        fluentPre = {}
        fluentAdd = {}
        fluentDel = {}
        fid = 0
        
        for fluent in ground_atoms:
            fname = ' '.join([fluent.predicate] + list(fluent.args))
            if prefs != 'kg' and prefs != 'kgo' and \
                fname[:min(7, len(fname))] == '__pref_': continue
            
            assert fluent.__class__ == conditions.Atom 
            
            fluentToStr[fluent] = fname
            fluentToID[fluent] = fid
            fid+=1
            fluentPre[fluent] = []
            fluentAdd[fluent] = []
            fluentDel[fluent] = []
        
        #populate the fluent pre, and, and del lists
        for ground_action in ground_actions:
            if not ground_action in actToID: continue
            tid = actToID[ground_action]
            for prec in ground_action.precondition:
                assert not prec.negated
                if not tid in fluentPre[prec]:
                    fluentPre[prec].append(tid)
            for add_eff in ground_action.add_effects:
                #AFFECTS COND EFFECTS
                if not tid in fluentAdd[add_eff[1]]:
                    fluentAdd[add_eff[1]].append(tid)
            for del_eff in ground_action.del_effects:
                #AFFECTS COND EFFECTS
                if not tid in fluentDel[del_eff[1]]:
                    fluentDel[del_eff[1]].append(tid)
                
        #print the fluents
        #fluent id noPre pres noAdd adds noDel dels pred args
        for fluent in ground_atoms:
            if not fluent in fluentToStr: continue
            print >> out_file, "fluent", fluentToID[fluent],
            fname = fluentToStr[fluent]
            if fname[:min(7, len(fname))] == '__pref_' or fname == '__end_mode'\
                or fname == '__normal_mode' or fname[:min(14, len(fname))] == '__current_pref':
                print >> out_file, "1",
            else:
                print >> out_file, "0",
            print >> out_file, ' '.join([str(len(fluentPre[fluent]))] + map(str, fluentPre[fluent])),
            print >> out_file, ' '.join([str(len(fluentAdd[fluent]))] + map(str, fluentAdd[fluent])),
            print >> out_file, ' '.join([str(len(fluentDel[fluent]))] + map(str, fluentDel[fluent])),
            print >> out_file, fluentToStr[fluent]
        
        #print the actions
        #action id cost noopFor(or-1) noPrecs precs noAdds adds noDels dels pred args
        for ground_action in ground_actions:
            if not ground_action in actToID: continue
            
            print >> out_file, "action %s %s -1" % (actToID[ground_action], ground_action.cost),
            if ground_action.pref_action:
                print >> out_file, "1",
            else:
                print >> out_file, "0",
            print >> out_file, len(ground_action.precondition),
            
            #a_precs = []
            for prec in ground_action.precondition:
                #if not prec in a_precs:
                #a_precs.append(prec)
                print >> out_file, fluentToID[prec],
            print >> out_file, len(ground_action.add_effects),
            #a_adds = []
            for add_eff in ground_action.add_effects:
                #if not add_eff in a_adds:
                #a_adds.append(add_eff)
                print >> out_file, fluentToID[add_eff[1]],
            print >> out_file, len(ground_action.del_effects),
            #a_dels = []
            for del_eff in ground_action.del_effects:
                #if not del_eff in a_dels:
                #a_dels.append(del_eff)
                print >> out_file, fluentToID[del_eff[1]],  
            print >> out_file, ground_action.name[1:-1]
            
        #detect static propositions
        for fact in self.init:
            if fact.__class__ == conditions.Atom and \
                not fact in fluentToID and \
                not fact.predicate == "=" :
                print >> out_file, "static %s" % ' '.join([fact.predicate] + list(fact.args))
        
        #start state
        print >> out_file, "start",
        for fact in self.init:
            if fact.__class__ == conditions.Atom and fact in fluentToID:
                print >> out_file, fluentToID[fact],
        print >> out_file
        
        #goal
        assert self.goal.__class__ == conditions.Conjunction, "Goal must be a conjunction."
        print >> out_file, "goal",
        for fact in self.goal.parts:
            if fact in fluentToID:
                print >> out_file, fluentToID[fact],
        print >> out_file

        #multiplier
        print >> out_file, "multiplier", self.multiplier

        #preferences
        #indicate the special preference actions -- we need both achieve and fail
        #so that the preprocessing does not consider the problem unsolvable
        
        if prefs == 'maratea' or prefs == 'kg' or prefs == 'kgo':
            pref_to_succ_act = {}
            for ground_action in ground_actions:
                if str(ground_action.achieve_pref) != "-1":
                    pref_name = str(ground_action.achieve_pref)
                    if not pref_name in pref_to_succ_act:
                        pref_to_succ_act[pref_name] = []
                    pref_to_succ_act[pref_name].append(ground_action)
            
            prefid = 0
            for fluent in ground_atoms:
                fname = fluent.predicate
                if fname[:min(7, len(fname))] == "__pref_":
                    assert len(fluent.args) == 0
                    fname = fname[min(7, len(fname)):]
                    print >> out_file, "pref %s %s %s" % \
                    (prefid, fname, int(self.pref_weights[fname])),
                    pref_set = set()
                    if fname in pref_to_succ_act:
                        for prefact in pref_to_succ_act[fname]:
                            tpres = []
                            tset = set()
                            for prec in prefact.precondition:
                                if prec.predicate != '__end_mode' and\
                                   prec.predicate[:min(7, len(prec.predicate))] != "__pref_":
                                    tpres.append(fluentToID[prec])
                                    tset.add(fluentToID[prec])
                            tset = frozenset(tset)
                            if tset not in pref_set:
                                print >> out_file, len(tpres), ' '.join(map(str,tpres)),
                                pref_set.add(tset)
                    print >> out_file
                    prefid += 1

        assert not self.axioms, "Axioms not currently supported"
        #if self.axioms:
        #    print "Axioms:"
        #    for axiom in self.axioms:
        #        axiom.dump()

class Requirements(object):
    def __init__(self, requirements):
        self.requirements = requirements
        for req in requirements:
            assert req in (
              ":strips", ":adl", ":typing", ":negation", ":equality",
              ":negative-preconditions", ":disjunctive-preconditions",
              ":existential-preconditions", ":universal-preconditions",
              ":quantified-preconditions", ":conditional-effects",
              ":derived-predicates", ":action-costs", ":goal-utilities",
              ":preferences"), req
    def __str__(self):
        #comma removed by Nathan Robinson 2011/10/22
        return " ".join(self.requirements)

def parse_domain(domain_pddl):
    iterator = iter(domain_pddl)

    assert iterator.next() == "define"
    domain_line = iterator.next()
    assert domain_line[0] == "domain" and len(domain_line) == 2
    yield domain_line[1]

    ## We allow an arbitrary order of the requirement, types, constants,
    ## predicates and functions specification. The PDDL BNF is more strict on
    ## this, so we print a warning if it is violated.
    requirements = Requirements([":strips"])
    the_types = [pddl_types.Type("object")]
    constants, the_predicates, the_functions = [], [], []
    correct_order = [":requirements", ":types", ":constants", ":predicates",
                     ":functions"]
    seen_fields = []
    for opt in iterator:
        field = opt[0]
        if field not in correct_order:
            first_action = opt
            break
        if field in seen_fields:
            raise SystemExit("Error in domain specification\n" +
                             "Reason: two '%s' specifications." % field)
        if (seen_fields and 
            correct_order.index(seen_fields[-1]) > correct_order.index(field)):
            msg = "\nWarning: %s specification not allowed here (cf. PDDL BNF)" % field
            print >> sys.stderr, msg
        seen_fields.append(field)
        if field == ":requirements":
            requirements = Requirements(opt[1:])
        elif field == ":types":
            the_types.extend(pddl_types.parse_typed_list(opt[1:],
                        constructor=pddl_types.Type))
        elif field == ":constants":
            constants = pddl_types.parse_typed_list(opt[1:])
        elif field == ":predicates":
            the_predicates = [predicates.Predicate.parse(entry) 
                              for entry in opt[1:]]
            the_predicates += [predicates.Predicate("=",
                                 [pddl_types.TypedObject("?x", "object"),
                                  pddl_types.TypedObject("?y", "object")])]
        elif field == ":functions":
            the_functions = pddl_types.parse_typed_list(opt[1:],
                    constructor=functions.Function.parse_typed, functions=True)
            for function in the_functions:
                Task.FUNCTION_SYMBOLS[function.name] = function.type
    pddl_types.set_supertypes(the_types)
    # for type in the_types:
    #   print repr(type), type.supertype_names
    yield requirements
    yield the_types
    yield constants
    yield the_predicates
    yield the_functions
    
    entries = [first_action] + [entry for entry in iterator]
    the_axioms = []
    the_actions = []
    for entry in entries:
        if entry[0] == ":derived":
            axiom = axioms.Axiom.parse(entry)
            the_axioms.append(axiom)
        else:
            action = actions.Action.parse(entry)
            the_actions.append(action)
    yield the_actions
    yield the_axioms


#Nathan Robinson 2011 11 23
#Multiply the weight by the multiplier.
#if it is not an integer (+-epislon) keep multiplying it by 10 and increading the
#multiplier until it is.
def find_multiplier(pref_weight, multiplier, epsilon):
    t_weight = pref_weight * multiplier
    t_multiplier = multiplier
    while t_weight % 1 > epsilon:
        t_weight *= 10
        t_multiplier *= 10
    return t_multiplier
       

def parse_task(task_pddl):
    iterator = iter(task_pddl)

    assert iterator.next() == "define"
    problem_line = iterator.next()
    assert problem_line[0] == "problem" and len(problem_line) == 2, "%s" % problem_line
    yield problem_line[1]
    domain_line = iterator.next()
    assert domain_line[0] == ":domain" and len(domain_line) == 2
    yield domain_line[1]

    objects_opt = iterator.next()
    if objects_opt[0] == ":objects":
        yield pddl_types.parse_typed_list(objects_opt[1:])
        init = iterator.next()
    else:
        yield []
        init = objects_opt

    assert init[0] == ":init"
    initial = []
    for fact in init[1:]:
        if fact[0] == "=":
            try:
                initial.append(f_expression.parse_assignment(fact))
            except ValueError, e:
                raise SystemExit("Error in initial state specification\n" +
                                 "Reason: %s." %  e)
        else:
            initial.append(conditions.Atom(fact[0], fact[1:]))
            
    yield initial

    #Changes by Nathan Robinson 2011/10/20
       #Modified to handle simple preferences and the net benifit case
    #We assume that the net benifit case maximises a constant minus the sum of
    #the action costs and the weights of the violated preferences. This constant
    #is ignored.
    #We assume that the simple preferences case minimises the sum of the weights
    #of violated preferences.
    
    goal = iterator.next()
    assert goal[0] == ":goal" and len(goal) == 2
    condition = conditions.parse_condition(goal[1])
    
    #We require this to be a conjunction at the top level
    assert condition.__class__ == conditions.Conjunction, 'We require a conjunctive goal'
    new_parts = []
    preferences = []
    for part in condition.parts:
        t_pref = part.has_preference()
        if t_pref:
            #strip the preference out of part
            part = part.strip_preference()
            preferences.append((part, t_pref))
        else:
            assert issubclass(part.__class__, conditions.Literal), 'Only simple literals in goals'
            new_parts.append(part)
    
    condition = conditions.Conjunction(new_parts)
    #Goal
    yield condition
    #Preferences
    yield preferences

    use_metric = False
    for entry in iterator:
        if string.lower(entry[0]) == ":metric":
            assert not use_metric, "Cannot have more than one metric."
            use_metric = True
            min_or_max = string.lower(entry[1])
            assert min_or_max == "minimize" or min_or_max == "maximize", "Unknown function in metric."
            assert len(entry) == 3, "Unknown metric."
            entry = entry[2]       
    if use_metric:
        is_minus = False
        if entry[0] == "total-cost":
            assert min_or_max == "minimize", "Unknown metric: %s total-cost" % min_or_max
            yield []
            yield "minimise-total-cost"
            yield 1
        else:
            multiplier = 1
            if entry[0] == "-":
                try:
                    is_minus = int(entry[1])
                except ValueError:
                    assert False, "Unknown metric format (- %s ..." % entry[1]
                #We dont care about the constant
                entry = entry[2]
            if entry[0] == "+":
                entry = entry[1:]
            else:
                entry = [entry]
            is_cost = False
            pref_weights = {}
            for pref in entry:
                if pref[0] == "total-cost":
                    is_cost = True
                else:
                    pref_weight = 1
                    if pref[0] == "*":
                        assert len(pref) == 3, "Unknown preference %s" % pref
                        try:
                            pref_weight = float(pref[2])
                            pref = pref[1]
                        except TypeError, ValueError:
                            try:
                                pref_weight = float(pref[1])
                                pref = pref[2]
                            except TypeError, ValueError:
                                assert False, "Bad preference weight %s" % pref
                    assert string.lower(pref[0]) == "is-violated", "Unknown preference %s" % pref
                    multiplier = find_multiplier(pref_weight, multiplier, 0.00001)
                    pref_weights[pref[1]] = pref_weight
            yield pref_weights
            if is_cost == False:
                assert min_or_max == "maximize" and is_minus or \
                    min_or_max == "minimize" and not is_minus, "Cannot maximise violated preferences."
                yield "minimize-violated-preferences"
            else:
                assert min_or_max == "maximize" and is_minus, "Unsupported net-benifit metric format."
                yield "maximize-net-benifit"
            
            for prefk in pref_weights:
                pref_weights[prefk] *= multiplier
            
            yield multiplier
    else:
        yield []
        yield use_metric 
        yield 1

    
    for entry in iterator:
        assert False, entry
      
        
