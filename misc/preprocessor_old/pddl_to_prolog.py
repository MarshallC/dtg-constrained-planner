#! /usr/bin/env python
# -*- coding: latin-1 -*-

from __future__ import with_statement

import itertools

import normalize
import pddl
import timers

class PrologProgram:
    def __init__(self):
        self.facts = []
        self.rules = []
        self.objects = set()
        def predicate_name_generator():
            for count in itertools.count():
                yield "p$%d" % count
        self.new_name = predicate_name_generator()
    def add_fact(self, atom):
        self.facts.append(Fact(atom))
        self.objects |= set(atom.args)
    def add_rule(self, rule):
        self.rules.append(rule)
    def dump(self):
        for fact in self.facts:
            print fact
        for rule in self.rules:
            print getattr(rule, "type", "none"), rule
    def normalize(self):
        # Normalized prolog programs have the following properties:
        # 1. Each variable that occurs in the effect of a rule also occurs in its
        #    condition.
        # 2. The variables that appear in each effect or condition are distinct.
        # 3. There are no rules with empty condition.
        self.remove_free_effect_variables()
        self.split_duplicate_arguments()
        self.convert_trivial_rules()
    def split_rules(self):
        import split_rules
        # Splits rules whose conditions can be partitioned in such a way that
        # the parts have disjoint variable sets, then split n-ary joins into
        # a number of binary joins, introducing new pseudo-predicates for the
        # intermediate values.
        new_rules = []
        for rule in self.rules:
            new_rules += split_rules.split_rule(rule, self.new_name)
        self.rules = new_rules
    def remove_free_effect_variables(self):
        """Remove free effect variables like the variable Y in the rule
        p(X, Y) :- q(X). This is done by introducing a new predicate
        @object, setting it true for all objects, and translating the above
        rule to p(X, Y) :- q(X), @object(Y).
        After calling this, no new objects should be introduced!"""

        # Note: This should never be necessary for typed domains.
        # Leaving it in at the moment regardless.
        must_add_predicate = False
        for rule in self.rules:
            eff_vars = get_variables([rule.effect])
            cond_vars = get_variables(rule.conditions)
            if not eff_vars.issubset(cond_vars):
                must_add_predicate = True
                eff_vars -= cond_vars
                for var in eff_vars:
                    rule.add_condition(pddl.Atom("@object", [var]))
        if must_add_predicate:
            print "Unbound effect variables: Adding @object predicate."
            self.facts += [Fact(pddl.Atom("@object", [obj])) for obj in self.objects]
    def split_duplicate_arguments(self):
        """Make sure that no variable occurs twice within the same symbolic fact,
        like the variable X does in p(X, Y, X). This is done by renaming the second
        and following occurrences of the variable and adding equality conditions.
        For example p(X, Y, X) is translated to p(X, Y, X@0) with the additional
        condition =(X, X@0); the equality predicate must be appropriately instantiated
        somewhere else."""
        printed_message = False
        for rule in self.rules:
            if rule.rename_duplicate_variables() and not printed_message:
                print "Duplicate arguments: Adding equality conditions."
                printed_message = True
    def convert_trivial_rules(self):
        """Convert rules with an empty condition into facts.
        This must be called after bounding rule effects, so that rules with an
        empty condition must necessarily have a variable-free effect.
        Variable-free effects are the only ones for which a distinction between
        ground and symbolic atoms is not necessary."""
        must_delete_rules = []
        for i, rule in enumerate(self.rules):
            if not rule.conditions:
                assert not get_variables([rule.effect])
                self.add_fact(pddl.Atom(rule.effect.predicate, rule.effect.args))
                must_delete_rules.append(i)
        if must_delete_rules:
            print "Trivial rules: Converted to facts."
            for rule_no in must_delete_rules[::-1]:
                del self.rules[rule_no]

def get_variables(symbolic_atoms):
    variables = set()
    for sym_atom in symbolic_atoms:
        variables |= set([arg for arg in sym_atom.args if arg[0] == "?"])
    return variables

class Fact:
    def __init__(self, atom):
        self.atom = atom
    def __str__(self):
        return "%s." % self.atom

class Rule:
    def __init__(self, conditions, effect):
        self.conditions = conditions
        self.effect = effect
    def add_condition(self, condition):
        self.conditions.append(condition)
    def get_variables(self):
        return get_variables(self.conditions + [self.effect])
    def _rename_duplicate_variables(self, atom, new_conditions):
        used_variables = set()
        for i, var_name in enumerate(atom.args):
            if var_name[0] == "?":
                if var_name in used_variables:
                    new_var_name = "%s@%d" % (var_name, len(new_conditions))
                    atom = atom.rename_variables({var_name: new_var_name})
                    new_conditions.append(pddl.Atom("=", [var_name, new_var_name]))
                else:
                    used_variables.add(var_name)
        return atom
    def rename_duplicate_variables(self):
        new_conditions = []
        self.effect = self._rename_duplicate_variables(self.effect, new_conditions)
        for condition in self.conditions:
            condition = self._rename_duplicate_variables(condition, new_conditions)
        self.conditions += new_conditions
        return bool(new_conditions)
    def __str__(self):
        cond_str = ", ".join(map(str, self.conditions))
        return "%s :- %s." % (self.effect, cond_str)

def translate_typed_object(prog, obj, type_dict):
    supertypes = type_dict[obj.type].supertype_names
    for type_name in [obj.type] + supertypes:
        prog.add_fact(pddl.Atom(type_name, [obj.name]))

def translate_facts(prog, task):
    type_dict = dict((type.name, type) for type in task.types)
    for obj in task.objects:
        translate_typed_object(prog, obj, type_dict)
    for fact in task.init:
        assert isinstance(fact, pddl.Atom) or isinstance(fact, pddl.Assign)
        if isinstance(fact, pddl.Atom):
            prog.add_fact(fact)

def translate(task):

    with timers.timing("Normalizing task"):
        normalize.normalize(task)
    
    #Nathan Robinson 2011/10/25 -- wrap conditions in conjunctions if they arer
    #bare literals (make Truth an empty conjunction)
    
    for action in task.actions:
        if action.precondition.__class__ == pddl.conditions.Truth:
            action.precondition = pddl.conditions.Conjunction([])
        elif not action.precondition.__class__ == pddl.conditions.Conjunction:
            assert issubclass(action.precondition.__class__, pddl.conditions.Literal), "PC: %s" % action.precondition.__class__
            action.precondition = pddl.conditions.Conjunction([action.precondition])
    
    #Nathan Robinson 2011/10/24 -- Ensure that no operators have the same name 
    #after normalisation
    n_dict = {}
    for action in task.actions:
        if not action.name in n_dict:
            n_dict[action.name] = 0
        else:
            n_dict[action.name] += 1
            action.name = "__copy_" + str(n_dict[action.name]) + "_" + action.name
    
    #Nathan Robinson 2010/10/24
    #Compile away negative preconditions
    #First pass: find the negative precs, make precs representing their inverse,
    #and replace the original preconditions
    p_dict = {}
    for action in task.actions:
        assert action.precondition.__class__ == pddl.conditions.Conjunction
        action.precondition = action.precondition.collect_neg(p_dict)
    
    assert task.goal.__class__ == pddl.conditions.Conjunction
    task.goal = task.goal.collect_neg(p_dict)

    copied_preds = {}
    for o_prec, n_prec in p_dict.iteritems():
            #We need to find the appropriate predicate in main pred list and copy its types
            for t_pred in task.predicates:
                if t_pred not in copied_preds and \
                    t_pred.name == o_prec.predicate and len(t_pred.arguments) == len(o_prec.args):
                    task.predicates.append(pddl.predicates.Predicate(n_prec.predicate, t_pred.arguments ))
                    copied_preds[t_pred] = task.predicates[-1]
    
    #Second pass: add additional add effects and delete effects
    for action in task.actions:
        new_effects = []
        for eff in action.effects:
            assert eff.condition.__class__ == pddl.conditions.Truth, \
                "We do not currently handle conditional effects: %s" % eff.condition.__class__
            for o_pred, n_pred in copied_preds.iteritems():
                if o_pred.name == eff.literal.predicate and\
                    len(o_pred.arguments) == len(eff.literal.args):
                    if eff.literal.__class__ == pddl.conditions.Atom:
                        #delete the new atom
                        new_effects.append(pddl.effects.Effect(eff.parameters, \
                            eff.condition, pddl.conditions.NegatedAtom(n_pred.name, eff.literal.args)))
                    else:
                        assert eff.literal.__class__ == pddl.conditions.NegatedAtom
                        #add the new atom
                        new_effects.append(pddl.effects.Effect(eff.parameters, \
                            eff.condition, pddl.conditions.Atom(n_pred.name, eff.literal.args)))
        action.effects += new_effects
       
                
    #Add the new negation atoms to the start state if their inverse is not there
    #first make tmp type lists
    typeStrToObjects = {}
    typeStrToType = {}
    for type in task.types:
        typeStrToObjects[type.name] = []
        typeStrToType[type.name] = type
    for obj in task.objects:
        typeStrToObjects["object"].append(obj)
        if not obj.type == "object":
            typeStrToObjects[obj.type].append(obj)
        for otype in typeStrToType[obj.type].supertype_names:
            if not otype == "object":
                typeStrToObjects[otype].append(obj)
    
  
    new_init = []
    for o_pred, n_pred in copied_preds.iteritems():
        
        if len(o_pred.arguments) == 0:
            #no args
            #is there an atom with this predicate in the start state
            found = False
            for atom in task.init:
                if atom.__class__ == pddl.conditions.Atom and \
                    atom.predicate == o_pred.name:
                    found = True
                    break
            if not found:
                new_init.append(pddl.conditions.Atom(n_pred.name, []));
        else:
            arg_doms = []
            #find the matching predicate
            found = False
            for pred in task.predicates:
                if pred.name == o_pred.name:
                    found = True
                    break
            assert found
            for arg in pred.arguments:
                o_names = []
                if arg.type.__class__ == str:
                    for obj in typeStrToObjects[arg.type]:
                        o_names.append(obj.name)
                else:
                    assert arg.type.__class__ == list
                    for ttype in arg.type:
                        if ttype == 'either': continue
                        for obj in typeStrToObjects[ttype]:
                            if not obj.name in o_names:
                                o_names.append(obj.name)
                    
                arg_doms.append(o_names)
            arg_prod = list(itertools.product(*arg_doms))
            for argc in arg_prod:
                if not pddl.conditions.Atom(o_pred.name, argc) in task.init:
                    new_init.append(pddl.conditions.Atom(n_pred.name, argc))
    task.init += new_init

    with timers.timing("Generating Datalog program"):
        prog = PrologProgram()
        translate_facts(prog, task)
        for conditions, effect in normalize.build_exploration_rules(task):
            prog.add_rule(Rule(conditions, effect))
            
    with timers.timing("Normalizing Datalog program", block=True):
        # Using block=True because normalization can output some messages
        # in rare cases.
        prog.normalize()
        prog.split_rules()
    return prog

def test_normalization():
    prog = PrologProgram()
    prog.add_fact(pddl.Atom("at", ["foo", "bar"]))
    prog.add_fact(pddl.Atom("truck", ["bollerwagen"]))
    prog.add_fact(pddl.Atom("truck", ["segway"]))
    prog.add_rule(Rule([pddl.Atom("truck", ["?X"])], pddl.Atom("at", ["?X", "?Y"])))
    prog.add_rule(Rule([pddl.Atom("truck", ["X"]), pddl.Atom("location", ["?Y"])],
                  pddl.Atom("at", ["?X", "?Y"])))
    prog.add_rule(Rule([pddl.Atom("truck", ["?X"]), pddl.Atom("location", ["?Y"])],
                  pddl.Atom("at", ["?X", "?X"])))
    prog.add_rule(Rule([pddl.Atom("p", ["?Y", "?Z", "?Y", "?Z"])],
                  pddl.Atom("q", ["?Y", "?Y"])))
    prog.add_rule(Rule([], pddl.Atom("foo", [])))
    prog.add_rule(Rule([], pddl.Atom("bar", ["X"])))
    prog.normalize()
    prog.dump()

if __name__ == "__main__":
    # test_normalization()

    task = pddl.open()
    prog = translate(task)
    prog.dump()
