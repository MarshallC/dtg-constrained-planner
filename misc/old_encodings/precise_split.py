""" File:        encodings/precise_split.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2013-
    Desctiption: 

    Liscence:   [Refine this!]
"""


from encodings import EncodingException, Encoding, encoding_success_code,\
    encoding_error_code

solver_class = 'PreciseSplit'

class PreciseSplit(Encoding):


    def encode(self, horizon):
        """Encode the problem with the given horizon and return a list of
           clauses or raise an EncodingException with the appropriate code from 
           above.
        
           (Encoding, int) -> [[int]]
        """
        return [[]]

    def extend(self, horizon):
        """Extend the encoding to the new horizon and return the new list of
           clauses or raise an EncodingException with the appropriate code from 
           above.
        
            (Encoding, int) -> [[int]]
        """
        return [[]]
        
    def write_variable_file(self, file_name):
        """Write a file to file_name that describes the variables used in the
            current encoding. Raise an EncodingException if no encoding has
            been generated.
        
            (Encoding, str) -> None
        """

     
    def print_true_variables(self, v_type):
        """Print the true variables of the given type
        
            (Encoding, str) -> None
        """
        
     
    def build_plan(self):
        """Build a plan from the true variables
        
            (Encoding) -> None
        """
        
    

