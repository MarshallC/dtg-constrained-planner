""" File:        encodings/encodings.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2013-
    Desctiption: 

    Liscence:   [Refine this!]
"""


from utils import CodeException
import abc
from importlib import import_module

encoding_path = "./encodings/"
encoding_list_file_name = encoding_path + "encoding_list"

encoding_success_code = 0
encoding_error_code = 1

class EncodingException(CodeException):
    """ An exception to be raised in the even that something goes wrong with
        the encoding process. """

class Encoding(object):
    __metaclass__ = abc.ABCMeta
    
    @abc.abstractmethod
    def __init__(self, problem, args):
        """ Set encoding parameters
            
            (Encoding, Problem, ArgProcessor) -> None
        """
        self.problem = problem
        self.args = args

    @abc.abstractmethod
    def encode(self, horizon):
        """Encode the problem with the given horizon and return a list of
           clauses or raise an EncodingException with the appropriate code from 
           above.
        
           (Encoding, int) -> [[int]]
        """

    @abc.abstractmethod
    def extend(self, horizon):
        """Extend the encoding to the new horizon and return the new list of
           clauses or raise an EncodingException with the appropriate code from 
           above.
        
            (Encoding, int) -> [[int]]
        """
        
    @abc.abstractmethod
    def write_variable_file(self, file_name):
        """Write a file to file_name that describes the variables used in the
            current encoding. Raise an EncodingException if no encoding has
            been generated.
        
            (Encoding, str) -> None
        """
     
    def set_true_variables(self, true_vars):
        """Set the true variables of the encoding.
        
            (Encoding, [int]) -> None
        """
        self.true_vars = true_vars
    
    @abc.abstractmethod   
    def print_true_variables(self, v_type):
        """Print the true variables of the given type
        
            (Encoding, str) -> None
        """
    
    @abc.abstractmethod   
    def build_plan(self):
        """Build a plan from the true variables
        
            (Encoding) -> None
        """
      
    def print_plan(self):
        """Print a plan to the given file (or stdout if None)
        
            (Encoding, str) -> None
        """
        #IMPLEMENT
     
    
    def validate_plan(self):
        """Vakidate the computed plan.
        
            (Encoding) -> bool
        """
        #IMPLEMENT
 

class EncodingWrapper(object):
    def __init__(self):
        self.valid_encodings = {}
        self.default_encoding = ''

    def read_encoding_list(self):
        try:
            with file(encoding_list_file_name, 'r') as encoding_list_file:
                encoding_lines = encoding_list_file.readlines()
                if len(encoding_lines) < 2:
                    raise EncodingException("Error: encoding list does not have enough lines.",\
                        encoding_error_code)
                self.default_encoding = encoding_lines[0].rstrip()   
                for line in encoding_lines[1:]:
                    try:
                        line_tokens = line.rstrip().split(' ')
                        self.valid_encodings[line_tokens[0]] = line_tokens[1]
                    except IndexError:
                        raise EncodingException("Error: badly formed line in encoding list: {}".\
                            format(line.rstrip()), encoding_error_code)
        except IOError:
            raise EncodingException("Error: failed to load encoding list {}.".\
                format(encoding_list_file_name), encoding_error_code)

    def instantiate_encoding(self, encoding_name, encoding_options, problem):
        if encoding_name not in self.valid_encodings:
            raise EncodingException("Error: invalid encoding: {}".format(encoding_name),\
                encoding_error_code)
        encoding_module = import_module(encoding_path[:-1] + '.' +\
             self.valid_encodings[encoding_name])
        self.encoding = getattr(encoding_module, encoding_module.encoding_class)\
            (problem, encoding_options)

