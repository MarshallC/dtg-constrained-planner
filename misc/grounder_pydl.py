""" File:        parser/grounder.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2013-
    Desctiption: 


    Liscence:   [Refine this!]
"""


from utilities import grounding_error_code, grounder_run_success_code,\
    grounder_path, var_alphabet, neg_prec_prefix, equality_prefix,\
    inequality_prefix, default_type_name, asp_convert
    
from parser import ParsingException
from problem import Object, Type, Function, Predicate,\
                    PredicateCondition, NotCondition, AndCondition,\
                    OrCondition, ForAllCondition, ExistsCondition,\
                    IncreaseCondition, EqualsCondition, ConditionalEffect

import os, subprocess, itertools

#from pyDatalog import pyDatalog
#pyDatalog.create_terms('X,Y,reachable_goal,reachable,reachable_a,reachable_f,dtype,action')

from pyDatalog.pyDatalog import assert_fact, load, ask

asp_types = {}
asp_objects = {}
asp_predicates = {}
asp_functions = {}
asp_actions = {}
asp_dp = {}


def rename_asp_components(problem):
    """ Rename the appropriate problem elements using the function asp_convert
        defined in utilities.
   
        (Grounder) -> None
    """
    #Types
    for ttype in problem.types.itervalues():
        ttype.asp_name = asp_convert(ttype.name)
        asp_types[ttype.asp_name] = ttype
    
    #Objects
    for obj in problem.objects.itervalues():
        obj.asp_name = asp_convert(obj.name)
        asp_objects[obj.asp_name] = obj
    
    #Predicates
    for pred in problem.predicates.itervalues():
        pred.asp_name = asp_convert(pred.name)
        asp_predicates[pred.asp_name] = pred
    
    #Functions
    for func in problem.functions.itervalues():
        func.asp_name = asp_convert(func.name)
        asp_functions[func.asp_name] = func
    
    #Actions
    for action in problem.actions.itervalues():
        action.asp_name = asp_convert(action.name)
        asp_actions[action.asp_name] = action

       

def write_prec_asp(condition, problem):
    """ Write the description of this conditon as a precondition into the
        datalog program.
    
        (Condition) -> None
    """
    if isinstance(condition, PredicateCondition):
        pred_name = condition.pred.asp_name
        if not condition.sign:
            pred_name = neg_prec_prefix + pred_name
        if condition.variables:
            v_strs, p_strs, alph_pos = [], [], 0
            for v in condition.variables:
                if v in condition.relevant_vars:
                    v_strs.append(var_alphabet[alph_pos])
                    p_strs.append(var_alphabet[alph_pos])
                    alph_pos += 1
                else:
                    p_strs.append(problem.objects[v].asp_name)
            
            x = "reachable(" + ", ".join([condition.cond_code] + v_strs) +\
                ") <= reachable_f(" + pred_name + ", " + ", ".join(p_strs) + ")"
            load(x)
            print x
            
        else:
            x = "reachable(" + condition.cond_code + ") <= reachable_f(" +\
                pred_name + ")"
            load(x)
            print x
    
    elif isinstance(condition, AndCondition):
        cond_strs = []
        for cond in condition.conditions:
            cond_strs.append("reachable(" + ", ".join([cond.cond_code] +\
                [var_alphabet[condition.var_indices[v][0]] for v in cond.relevant_vars]) + ")")

        x = "reachable(" + ", ".join([condition.cond_code] +\
            list(var_alphabet[:len(condition.relevant_vars)])) + ") <= " + " & ".join(cond_strs)
        print x
        load(x)

        for cond in condition.conditions:
            write_prec_asp(cond, problem)
    
    elif isinstance(condition, OrCondition):
        for cond in condition.conditions:
            reachable_str = "reachable(" + ", ".join([condition.cond_code] +\
                var_alphabet[:len(condition.relevant_vars)]) + ") <= " +\
                "reachable(" + ", ".join([cond.cond_code] +\
                [var_alphabet[condition.var_indices[v][0]] for v in cond.relevant_vars]) + ")"
            extra_bindings = ["obj(" + condition.var_types[v].asp_name + ", " + var_alphabet[vid] + ")"\
                for vid, v in enumerate(condition.relevant_vars) if v not in cond.relevant_vars]
            if extra_bindings:
                reachable_str += " & " + " & ".join(extra_bindings)
            print reachable_str
            load(reachable_str)

        for cond in condition.conditions:
            write_prec_asp(cond, problem)
        
    elif isinstance(condition, ForAllCondition):
        
        reachable_str = "reachable(" + ", ".join([condition.cond_code] +\
            list(var_alphabet[:len(condition.relevant_vars)])) + ") <= "
        
        cond_strs = []
        if condition.condition.relevant_vars:
            for obj in condition.v_type.objects:
                cond_strs.append("reachable(" + condition.condition.cond_code + ", " +\
                    ", ".join([var_alphabet[condition.var_indices[v][0]]\
                    if v in condition.relevant_vars else obj.asp_name\
                    for v in condition.condition.relevant_vars]) + ")")
        else:
            cond_strs.append("reachable(" + condition.condition.cond_code + ")")
        reachable_str += " & ".join(cond_strs)
        
        print reachable_str
        load(reachable_str)
        
        write_prec_asp(condition.condition, problem)
    
    elif isinstance(condition, ExistsCondition):
        reachable_str = "reachable(" +  ", ".join([condition.cond_code] +\
            list(var_alphabet[:len(condition.relevant_vars)])) + ") <= "
      
        if condition.condition.relevant_vars:
            reachable_str += "reachable(" + condition.condition.cond_code + ", " +\
                ", ".join([var_alphabet[condition.var_indices[v][0]]\
                if v in condition.relevant_vars\
                else var_alphabet[len(condition.relevant_vars)]\
                for v in condition.condition.relevant_vars]) + ")"
        else:
            reachable_str += "reachable(" + condition.condition.cond_code + ")"
        
        print reachable_str
        load(reachable_str)

        write_prec_asp(condition.condition, problem)
    
    elif isinstance(condition, EqualsCondition):
        prefix = inequality_prefix if not condition.sign else equality_prefix 
        v_strs, p_strs, alph_pos = [], [], 0
        for v in condition.variables:
            if v in condition.relevant_vars:
                v_strs.append(var_alphabet[alph_pos])
                p_strs.append(var_alphabet[alph_pos])
                alph_pos += 1
            else:
                p_strs.append(problem.objects[v].asp_name)
        x = "reachable(" + condition.cond_code + ", " +\
            ", ".join(v_strs) + ") <= " + prefix + "(" + ", ".join(p_strs) + ")"
        
        print x
        load(x)
    
    else:
        raise ParsingException("Invalid condition type in precondion: " +\
            str(condition), grounding_error_code)


def write_eff_asp(condition, problem):
    """ Write the effect of this condition into asp.
    
        (Condition, Problem) -> None
    """
    if isinstance(condition, PredicateCondition):
        if condition.sign or condition.pred in problem.neg_precs:
            pred_name = condition.pred.asp_name
            if not condition.sign:
                pred_name = neg_prec_prefix + pred_name
            if condition.variables:
                v_strs, p_strs, alph_pos = [], [], 0
                for v in condition.variables:
                    if v in condition.relevant_vars:
                        v_strs.append(var_alphabet[alph_pos])
                        p_strs.append(var_alphabet[alph_pos])
                        alph_pos += 1
                    else:
                        p_strs.append(problem.objects[v].asp_name)
                
                x = "reachable_f(" + pred_name + ", " + ", ".join(p_strs) +\
                    ") <= reachable(" + condition.cond_code + ", " + ", ".join(v_strs) + ")"
                print x
                load(x)
            else:
                x = "reachable_f(" + pred_name + ") <= reachable(" + condition.cond_code + ")"
                print x
                load(x)

    elif isinstance(condition, AndCondition):
        and_str = "reachable(" + ", ".join([condition.cond_code] +\
            list(var_alphabet[:len(condition.relevant_vars)])) + ")"

        for cond in condition.conditions:
            x = "reachable(" + ", ".join([cond.cond_code] +\
                [var_alphabet[condition.var_indices[v][0]]\
                for v in cond.relevant_vars]) + ") <= " + and_str
            
            print x
            load(x)

        for cond in condition.conditions:
            write_eff_asp(cond, problem)
        
    elif isinstance(condition, ForAllCondition):
        forall_str = "reachable(" + ", ".join([condition.cond_code] +\
            list(var_alphabet[:len(condition.relevant_vars)])) + ")" +\
            ", obj(" + condition.v_type.asp_name + ", " +\
            var_alphabet[len(condition.relevant_vars)] + ") <= reachable(" +\
            ", ".join( [condition.condition.cond_code] +\
            [var_alphabet[condition.var_indices[v][0]]\
            if v in condition.relevant_vars else\
            var_alphabet[len(condition.relevant_vars)]\
            for v in condition.condition.relevant_vars]) + ")"

        print forall_str
        load(forall_str)
        
        write_eff_asp(condition.condition, problem)
    
    elif isinstance(condition, IncreaseCondition):
        #print "Increase: ", condition.cond_code
        pass
    elif isinstance(condition, ConditionalEffect):
        eff_str = "reachable(" + ", ".join([condition.effect.cond_code] +\
            [var_alphabet[condition.var_indices[v][0]]\
            for v in condition.effect.relevant_vars]) + ") <= reachable(" +\
            ", ".join([condition.cond_code] +\
            list(var_alphabet[:len(condition.relevant_vars)])) +\
            ") & reachable(" + ", ".join([condition.condition.cond_code] +\
            [var_alphabet[condition.var_indices[v][0]]\
            for v in condition.condition.relevant_vars]) + ")"
        
        print eff_str
        load(eff_str)

        write_prec_asp(condition.condition, problem)
        write_eff_asp(condition.effect, problem)
    
    else:
        raise ParsingException("Invalid condition type in effect: " +\
            str(condition), grounding_error_code)


def ground_problem(problem):
    """ Write a datalog representation of the problem using pyDatalog and
        then generate all ground actions and fluents and test for relaxed goal
        reachability.

        (Problem) -> None
    """
    rename_asp_components(problem)
    
    #Objects
    for obj in problem.objects.itervalues():
        assert_fact("obj", obj.otype.asp_name, obj.asp_name)
    
    #Typing
    for ttype in problem.types.itervalues():
        if ttype.parent is not None:
            load("obj(" + ttype.parent.asp_name + ", " + var_alphabet[0] + ") <= " +\
                "obj(" + ttype.asp_name + ", " + var_alphabet[0] + ")")
    
    print "Types:"
    print ask("obj(X, Y)")
    
    #Actions and derived predicates
    for action in problem.actions.itervalues():
        action_str = ", ".join([action.asp_name] + list(var_alphabet[:len(action.parameters)]))

        #Precondition
        if action.precondition:
            if not action.parameters:
                x = "reachable_a(" + action_str + ") <= " +\
                   "reachable(" + action.precondition.cond_code + ")\n"
                load(x)
            else:
                pre_str = "reachable_a(" + action_str + ") <= " +\
                   "reachable(" + action.precondition.cond_code + ", " +\
                   ", ".join([var_alphabet[action.var_indices[v]]\
                   for v in action.precondition.relevant_vars]) + ")"
                for pid, (param, ptype) in enumerate(action.parameters):
                   if param not in action.precondition.relevant_vars:
                      pre_str += " & " + ptype.asp_name + "(" + var_alphabet[pid] + ")"
                
                print pre_str
                load(pre_str)
                
                write_prec_asp(action.precondition, problem)

        #Effect
        if not action.parameters:
            x = "reachable(" + action.effect.cond_code + ") <= " +\
                "reachable_a(" + action_str + ")"
            print x
            load(x)
        else:
            x = "reachable(" + ", ".join([action.effect.cond_code] +\
                [var_alphabet[action.var_indices[v]] for v in action.effect.relevant_vars]) +\
                ") <= " + "reachable_a(" + action_str + ")"  
            load(x)
            
            write_eff_asp(action.effect, problem)
       

    #start state
    for init_fact, init_args in problem.initial_state:
        x = "reachable_f(" + ", ".join([init_fact.asp_name] +\
            [problem.objects[v].asp_name for v in init_args]) + ")"
        print x
        load(x)
        
    x = equality_prefix + "(" + var_alphabet[0] +\
        ", " + var_alphabet[1] + ") <= obj(" + default_type_name + ", " +\
        var_alphabet[0] + ") & obj(" + default_type_name + ", " + var_alphabet[1] +\
        ") & (" + var_alphabet[0] + "==" + var_alphabet[1] + ")"
    print x
    load(x)
   
    '''
    #Inequalities
    for (v1, v2) in problem.inequalities:
        x = inequality_prefix + "(" + problem.objects[v1].asp_name +\
            ", " + problem.objects[v2].asp_name + ")"
        print x
        load(x)
    '''
    x = inequality_prefix + "(" + var_alphabet[0] +\
        ", " + var_alphabet[1] + ") <= obj(" + default_type_name + ", " +\
        var_alphabet[0] + ") & obj(" + default_type_name + ", " + var_alphabet[1] +\
        ") & (" + var_alphabet[0] + "!=" + var_alphabet[1] + ")"
    print x
    load(x)

    #negated preconditions in start state
    for pred, grounding in problem.neg_initial_state:
        x = "reachable_f(" + ", ".join([neg_prec_prefix + pred.asp_name] +\
            [problem.objects[v].asp_name for v in grounding]) + ")"
        print x
        load(x)

    #goal
    goal = "reachable_goal() <= reachable(" + problem.goal.cond_code + ")"
    print goal
    load(goal)

    write_prec_asp(problem.goal, problem)
        

    reachable_goal = False
    
    print "SOLUTION"
    print "------------------------------"
    
    res = ask("obj(tank,X) obj(diver,Y)")
    
    print res
    
    """
    for line in grounding_file:

        if tokens[0] == "reachable":
            cond_name = tokens[1]
            if len(tokens) == 3:
                cond_args = tokens[2].split(",")
            else:
                cond_args = []
            cond_args = [self.asp_objects[arg].name for arg in cond_args]
            self.problem.code_conds[cond_name].groundings.append(tuple(cond_args))
            
        elif tokens[0] == "reachable_a":
            pred_name = tokens[1]
            if len(tokens) == 3:
                pred_args = tokens[2].split(",")
            else:
                pred_args = []
            
            pred_args = [self.asp_objects[arg].name for arg in pred_args]
            self.asp_actions[pred_name].groundings.append(tuple(pred_args))
            
        elif tokens[0] == "reachable_f":
            pred_name = tokens[1]
            if len(tokens) == 3:
                pred_args = tokens[2].split(",")
            else:
                pred_args = []
             pred_args = [self.asp_objects[arg].name for arg in pred_args]
            if pred_name.startswith(neg_prec_prefix):
                self.asp_predicates[pred_name[len(neg_prec_prefix):]].\
                    neg_groundings.append(tuple(pred_args))
            else:
                 self.asp_predicates[pred_name].groundings.append(tuple(pred_args))
                
        elif tokens[0] == "reachable_goal":
            reachable_goal = True

    """
        
    if not reachable_goal:
        raise ParsingException("Error: the goal is not relaxed reachable.",
        grounding_error_code)


