# What?

SAT-based planning system written in Python

# How do I run it?

```bash
python3 python3 ./src/wrapper.py
```

## For an example run:
```bash
./test_block.sh
```

The planner uses several binaries, for lingeling, druplig and drat-trim, these can be built by running:
```bash
./build_external_tools.sh
```

Or manually from:
drat-trim - http://www.cs.utexas.edu/~marijn/drat-trim/
And:
The SAT solver lingeling is used here, the source code can be found at: https://github.com/arminbiere/lingeling
Also, as DRUP proof output is needed:
(As per lineling specification)
Add and compile the 'druplig' library in '../druplig/' from 'http://fmv.jku.at/druplig'.


# Where is the important output?

The program prints its main logs to STDOUT.
Also, In tmp_files. Each run produces 3 folders with suffixes:
_edges
_cnfs
_logs
which stores files used in execution

# Things to worry about

## Folder structure

The system will not run unless you have the following folder structure:

```bash
mkdir tmp_files
cd tmp_files
mkdir wrapper_logs
```

## Disk usage


The system writes lots of files to the tmp_files subfolder. So, in a cluster environment, you may want to deploy and execute the tool of a scratch volume, and then:

```bash
tar -zcf tmp_files.tar.gz tmp_files
cp tmp_files.tar.gz $NFS_VOLUME_LOCATION
cp *.log $NFS_VOLUME_LOCATION
cp *.e $NFS_VOLUME_LOCATION
```

Where, using the above wildcards, special care is made to ensure the filenames are distinct for each domain+problem run.
