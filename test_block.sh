python2.7 ./src/wrapper.py --domain ./domains/blocks/p03-domain.pddl --problem ./domains/blocks/p03.pddl --solver lingelingDrupTrim --encoding limited --DTG_increment_parallel True --DTG_simulate_only_fastest_path True --DTG_increment_horizon_0 False --DTG_nonzero_as_infinite False | tee tmp_files/wrapper_logs/test_block.log
./analyser.sh tmp_files/wrapper_logs/test_block.log
