#!/bin/bash

if [ -a  tmp_files ] ; then 
    echo "I shall unmount tmp_files and then re-create the directory as a non-RAM filesystem." ; 

    rm -r tmp_files/*
    sudo umount tmp_files/
    rm -r tmp_files
    mkdir tmp_files
fi

