""" File:        encodings/flat_metalayer.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2014
    Desctiption: 

    Liscence:   [Refine this!]
"""

from utilities import encoding_error_code

from encoding_base import EncodingException, Encoding

from problem   import Predicate, PredicateCondition, NotCondition, AndCondition,\
                      OrCondition, ForAllCondition, ExistsCondition,\
                      IncreaseCondition, ConditionalEffect, Action

import itertools, networkx

from collections import defaultdict
tree = lambda: defaultdict(tree)

encoding_class = 'FlatMetalayerEncoding'

class FlatMetalayerEncoding(Encoding):
    """ 
    
        Variables and clauses are created once 
    """
    
    def __init__(self, args, problem, quiet):
        """ Set encoding parameters
            
            (FlatMetalayerEncoding, { str : obj }, Problem, bool) -> None
        """
        super(FlatMetalayerEncoding, self).__init__(args, problem, quiet)


    def is_action(self, var):
        return self.variables[var][0] == "act"

    def get_variable_name(self, step, var):
        """ Return a string containing a representation of the supplied variable
            
            (FlatMetalayerEncoding, int, int) -> str
        """
        vtype, v_objs = self.variables[var]
        if vtype == "act":
            return "ACT aid " + str(v_objs[0]) + " " + v_objs[1][0].name + " " +\
                " ".join(v_objs[1][1]) + " step" + str(step)
        elif vtype == "irf":
            return "INT aid " + str(v_objs[0]) + " " +\
                v_objs[1][0][0].name + " " + " ".join(v_objs[1][0][1]) + " - " +\
                v_objs[1][1][0].name + " " + " ".join(v_objs[1][1][1]) + "step" + str(step)
        else: assert False
           

    def encode_base(self):
        """ Do any horizon-independent encoding that might be required.
        
           (FlatMetalayerEncoding, int, bool, bool) -> None
        """
        
        #The block copying needs a bit of work within make_heirarchy
        
        tree_order = True
        if tree_order: 
            self.make_dtgs()
            
            self.make_causal_graph(pure_effect_links=False)
            
            #self.make_better_causal_graph()
            
            
            self.make_hierarchy(cut_above=self.cg_cut_above,
                                cycle_limit=2000,
                                partition_on_actions=False,
                                min_action_component_size=1,
                                copy_layer_n=self.metalayer_copy_layer,
                                block_layer_function=max,
                                redundant_sub_block_elim=True,
                                linear_encoding_threshold=None,
                                prune_extra_actions=False)

            self.get_tree_order()
        else:
            self.totally_order_actions()
        
        
        print "Total order:"
        for action, grounding in self.total_order:
            print action.name, grounding

        #Uncomment to make grid1.dot to see the resulting graph
        self.block_graph_labeled = networkx.DiGraph()
        for edge in self.block_graph.edges_iter():
            block1 = tuple([edge[0]]+[self.block_components[edge[0]]])
            block2 = tuple([edge[1]]+[self.block_components[edge[1]]])
            self.block_graph_labeled.add_edge(block1, block2)
        networkx.write_dot(self.block_graph_labeled, "grid1.dot") 
        
        #Variables
        #Actions
        self.action_codes = {}
        for aid_ag_pair in enumerate(self.total_order):
            self.action_codes[aid_ag_pair] = self.new_code(("act", aid_ag_pair), (0, -2))

        #Intra-Fluents
        final_occurance = {}
        self.intra_fluent_codes = {}
        for aid_ag_pair in enumerate(self.total_order):
            aid, ag_pair = aid_ag_pair
            action, grounding = ag_pair
            self.intra_fluent_codes[aid_ag_pair] = {}
            for eff in action.strips_effects[grounding]:
                fluent = eff[:2]
                if not eff[2] and fluent + (True,) in\
                    action.strips_effects[grounding]: continue
                code = self.new_code(("irf", (aid, (ag_pair, eff))),  (0, -2))
                self.intra_fluent_codes[aid_ag_pair][eff] = code
                final_occurance[fluent] = code
          
        last_occurance = {}
        for predicate in self.problem.predicates.itervalues():
            for grounding in predicate.groundings:
                pg_pair = (predicate, grounding)
                last_occurance[pg_pair] = None

        num_prec_warnings = 0
        
        
        for aid_ag_pair in enumerate(self.total_order):
            aid, ag_pair = aid_ag_pair
            action, grounding = ag_pair
            a_code = self.action_codes[aid_ag_pair]
            
            #Precondition clauses
            for prec in action.strips_preconditions[grounding]:
                fluent = prec[:2]
                last_code = last_occurance[fluent]
                if last_code is None:
                    #Special clauses for layer 0
                    if prec[2] != (fluent in self.problem.initial_state):
                        
                        #print "Warning: action at layer 0, has impossible precondition:",\
                        #    aid, action.name, grounding
                        num_prec_warnings += 1
                        
                        self.clauses.append(((0, 0), [(0, -a_code)]))
                        self.clause_types.append("pre")
                    
                    """
                    #Clauses for layers > 0
                    if prec[2]: self.clauses.append(((1, -2),\
                        [(0, -a_code), (-1, final_occurance[fluent])]))
                    else:       self.clauses.append(((1, -2),\
                        [(0, -a_code), (-1, -final_occurance[fluent])]))
                    self.clause_types.append("pre")
                    """

                else:
                    if prec[2]: self.clauses.append(((0, -2), [(0, -a_code), (0, last_code)]))
                    else:       self.clauses.append(((0, -2), [(0, -a_code), (0, -last_code)]))
                    self.clause_types.append("pre")

            #Effects and SSAs
            for eff, f_code in self.intra_fluent_codes[aid_ag_pair].iteritems():
                fluent = eff[:2]
                last_code = last_occurance[fluent]
                final_code = final_occurance[fluent]
                if eff[2]:
                    #Effect
                    self.clauses.append(((0, -2), [(0, -a_code), (0, f_code)]))
                    self.clause_types.append("eff")
                    
                    #SSAs
                    if last_code is None:
                        #Clauses for layer 0
                        if fluent in self.problem.initial_state:
                            self.clauses.append(((0, 0), [(0, f_code)]))
                            self.clause_types.append("ssa")
                        else:
                            self.clauses.append(((0, 0), [(0, -f_code), (0, a_code)]))
                            self.clause_types.append("ssa")

                        #Clauses for layers > 0
                        self.clauses.append(((1, -2), [(0, -f_code), (0, a_code), (-1, final_code)]))
                        self.clause_types.append("ssa")
                        self.clauses.append(((1, -2), [(0, f_code), (-1, -final_code)]))
                        self.clause_types.append("ssa")
                    
                    else:
                        self.clauses.append(((0, -2), [(0, -f_code), (0, a_code), (0, last_code)]))
                        self.clause_types.append("ssa")
                        self.clauses.append(((0, -2), [(0, f_code), (0, -last_code)]))
                        self.clause_types.append("ssa")
                    
                else:
                    #Effect
                    self.clauses.append(((0, -2), [(0, -a_code), (0, -f_code)]))
                    self.clause_types.append("eff")
                
                    #SSAs
                    if last_code is None:
                        #Clauses for step 0
                        if fluent not in self.problem.initial_state:
                            self.clauses.append(((0, 0), [(0, -f_code)]))
                            self.clause_types.append("ssa")
                        else:
                            self.clauses.append(((0, 0), [(0, f_code), (0, a_code)]))
                            self.clause_types.append("ssa")

                        #Clauses for step > 0
                        self.clauses.append(((1, -2), [(0, -f_code), (-1, final_code)]))
                        self.clause_types.append("ssa")
                        self.clauses.append(((1, -2), [(0, f_code), (0, a_code), (-1, -final_code)]))
                        self.clause_types.append("ssa")
                    
                    else:
                        self.clauses.append(((0, -2), [(0, -f_code), (0, last_code)]))
                        self.clause_types.append("ssa")
                        self.clauses.append(((0, -2), [(0, f_code), (0, a_code), (0, -last_code)]))
                        self.clause_types.append("ssa")

                last_occurance[fluent] = f_code

            """
            made_clauses = set()
            for eff1, eff_code in self.intra_fluent_codes[ag_pair1].iteritems():
                for sign in [True, False]:
                    fluent1_sign = (eff1[0], eff1[1], sign)
                    if sign: f_code1 = -eff_code
                    else: f_code1 = eff_code
                    for eff2 in self.problem.state_mutexes[fluent1_sign]:
                        if eff2[0] == eff1[0] and eff2[1] == eff1[1]: continue
                        if (fluent1_sign, eff2) in made_clauses: continue
                        made_clauses.add((eff2, fluent1_sign))
                        fluent = (eff2[0], eff2[1])
                        f_code2 = last_occurance[fluent]
                        if f_code2 is None:
                            #Clause for step 0
                            if eff2[2] == (fluent in self.problem.initial_state):
                                if f_code1 in made_clauses: continue
                                made_clauses.add(f_code1)
                                self.clauses.append(((0, 0), [(0, f_code1)]))
                                self.clause_types.append("fmutex")
                            
                            #Clause for step > 0
                            f_code2 = final_occurance[fluent]
                            if eff2[2]: f_code2 = -f_code2
                            self.clauses.append(((1, -2), [(0, f_code1), (-1, f_code2)]))
                            self.clause_types.append("fmutex")

                        else:
                            if eff2[2]: f_code2 = -f_code2
                            self.clauses.append(((0, -2), [(0, f_code1), (0, f_code2)]))
                            self.clause_types.append("fmutex")
            """

        #goal clauses
        self.goal_conditions = set()
        for prec in self.problem.flat_ground_goal_preconditions:
            if not isinstance(prec[0], PredicateCondition):
                self.goal_conditions.add(prec)
            final_code = final_occurance[(prec[0].pred, prec[0].ground_conditions[prec[1]])]
            if prec[2]: self.clauses.append(((-2, -2), [(0, final_code)]))
            else:       self.clauses.append(((-2, -2), [(0, -final_code)]))
            self.clause_types.append("goal")
        
        if self.goal_conditions:
            assert False, "Complex goal not implemented"
            
            
        if num_prec_warnings:
            print "Warning!!!:", num_prec_warnings, "have impossible preconditions at layer 0"


    def build_plan(self):
        """Build a plan from the true variables
        
            (FlatMetalayerEncoding) -> None
        """
        
        print "Plan extraction:!"
        
        plan = []
        state = set(self.problem.initial_state_set)
        states = {frozenset(self.problem.initial_state_set) : 0}
        for step in xrange(self.horizon):
            for aid_ag_pair in enumerate(self.total_order):
                code = self.variable_to_cnf_code[(step, self.action_codes[aid_ag_pair])]
                if code not in self.true_vars: continue
                aid, ag_pair = aid_ag_pair
                action, grounding = ag_pair
                #Make successor state
                pos = set()
                neg = set()
                for eff in action.strips_effects[grounding]:
                    fluent = eff[:2]
                    if eff[2]: pos.add(fluent)
                    else: neg.add(fluent)
                state.difference_update(neg)
                state.update(pos)
                frozen_state = frozenset(state)
                if frozen_state in states:
                    layer = states[frozen_state]
                    while len(plan) > layer:
                        rem_a, rem_s = plan.pop()
                        del states[rem_s]
                else:
                    plan.append((ag_pair, frozen_state))
                    states[frozen_state] = len(plan)
        self.plan = [[act[0]] for act in plan]
        

