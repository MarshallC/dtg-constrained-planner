""" File:        cnf_encodings/__init__.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2013-
    Desctiption: Exists to allow the importing of the encoding subsystem as
                 a module.

    Liscence:   [Refine this!]
"""

from encoding_base import EncodingException, EncodingWrapper

