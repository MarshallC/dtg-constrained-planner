""" File:        encodings/flat.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2013-
    Desctiption: 

    Liscence:   [Refine this!]
"""

from utilities import encoding_error_code

from encoding_base import EncodingException, Encoding

from problem   import Predicate, PredicateCondition, NotCondition, AndCondition,\
                      OrCondition, ForAllCondition, ExistsCondition,\
                      IncreaseCondition, ConditionalEffect, Action


import itertools

encoding_class = 'FlatEncoding'

class FlatEncoding(Encoding):
    """ A simple flat encoding along the lines of SatPlan06 with full frame
        axioms.
    
        Variables and clauses are created once 
    """
    
    def __init__(self, args, problem, quiet):
        """ Set encoding parameters
            
            (FlatEncoding, { str : obj }, Problem, bool) -> None
        """
        super(FlatEncoding, self).__init__(args, problem, quiet)
        
        self.execution_semantics = args["execution_semantics"]
        self.redundant_mutex = args["redundant_mutex"]
        self.min_mutex_clique_size = args["min_mutex_clique_size"]
        self.process_semantics = args["process_semantics"]


    def is_action(self, var):
        v_obj, v_vars = self.variables[var]
        return isinstance(v_obj, Action)
    
    def get_variable_name(self, step, var):
        """ Return a string containing a representation of the supplied variable
            (FlatEncoding, int, int) -> str
        """
        v_obj, v_vars = self.variables[var]
        if v_obj == 'aux':
            return "AUX " + str(v_vars[0]) + " " + str(v_vars[1]) + " step" + str(step)
        elif isinstance(v_obj, Action):
            return v_obj.name + " " + " ".join(v_vars + ('step' + str(step),))
        elif isinstance(v_obj, Predicate):
            return v_obj.name + " " + " ".join(v_vars + ('step' + str(step),))
        else:
            return v_obj.cond_code + " " + v_obj.desc + " " +\
                " ".join(v_vars + ('step' + str(step),))


    def get_precondition_code(self, prec):
        """ Return the code used for the precondition condition
            
            (FlatEncoding, (PredicateCondition, bool, (str,...))/
                (Condition, (str, ...))) -> int
        """
        if isinstance(prec[0], PredicateCondition):
            if prec[2]:
                return self.fluent_codes[(prec[0].pred, prec[0].ground_conditions[prec[1]])]
            return -self.fluent_codes[(prec[0].pred, prec[0].ground_conditions[prec[1]])]
        return self.condition_codes[prec]

    def get_effect_code(self, eff):
        """ Return the code used for the given effect condition.
        
            (FlatEncoding, (PredicateCondition, bool, (str,...)) /
                (ConditionalEffect, (str, ...))) -> str / None
        """
        if isinstance(eff[0], PredicateCondition):
            if eff[2]:
                return self.fluent_codes[(eff[0].pred, eff[0].ground_conditions[eff[1]])]
            return -self.fluent_codes[(eff[0].pred, eff[0].ground_conditions[eff[1]])]
        elif isinstance(eff[0], ConditionalEffect):
            return self.cond_effect_codes[eff]
        return None
    
    
    def make_condition_clauses(self, cond, clause_list):
        """ Make the clauses for the given condition and add it to the given
            clause list.
        
            (FlatEncoding, (Condition, (str,...)) -> str
        """
        assert False, "Not Implemented"
        """
        c_code = self.condition_codes[cond]
        if isinstance(cond[0], AndCondition) or isinstance(cond[0], ForAllCondition):
            #c -> ^_children
            sub_codes = []
            for cid, conj_groundings in enumerate(cond[0].ground_conditions):
                sub_cond = cond.conditions[cid]
                if isinstance(sub_cond, PredicateCondition):
                    sub_codes.append(self.get_precondition_code(\
                        (sub_cond, sub_cond.sign, conj_groundings[grounding])))
                else:
                    sub_codes.append(self.get_precondition_code(\
                        (sub_cond, conj_groundings[grounding])))
            
            clause_list.append([-c_code] + sub_codes)
            #v_children -> c
            for child in sub_codes:
                clause_list.append([-child, c_code])
                
        elif isinstance(cond[0], OrCondition) or isinstance(cond[0], ExistsCondition):
            #^_children -> c
            sub_codes = []
            for cid, conj_groundings in enumerate(cond[0].ground_conditions):
                sub_cond = cond.conditions[cid]
                if isinstance(sub_cond, PredicateCondition):
                    sub_codes.append(self.get_precondition_code(\
                        (sub_cond, sub_cond.sign, conj_groundings[grounding])))
                else:
                    sub_codes.append(self.get_precondition_code(\
                        (sub_cond, conj_groundings[grounding])))
            
            clause_list.append([-c_code] + sub_codes)
            #c -> v_children
            for child in sub_codes:
                clause_list.append([-child, c_code])
            
            
        else: assert False
        """

    def encode_base(self):
        """ Do any horizon-independent encoding that might be required.
        
           (FlatEncoding, int, bool, bool) -> None
        """
        #Variables
        #Actions
        self.action_codes = {}
        for action in self.problem.actions.itervalues():
            for grounding in action.groundings:
                pair = (action, grounding)
                code = self.new_code(pair, (0, -2))
                self.action_codes[pair] = code
                
        #Fluents
        self.fluent_codes = {}
        for predicate in self.problem.predicates.itervalues():
            for grounding in predicate.groundings:
                pair = (predicate, grounding)
                code = self.new_code(pair, (0, -1)) 
                self.fluent_codes[pair] = code
        
        #Conditional Effects
        self.cond_effect_codes = {}
        for cond in self.problem.conditional_effects:
            assert False, "Conditional effects not implemented"
            for grounding in cond.groundings:
                pair = (cond, grounding)
                code = self.new_code(pair, (0, -2))
                self.cond_effect_codes[pair] = code
        
        #Conditions
        self.condition_codes = {}
        for cond in self.problem.conditions:
            for grounding in cond.groundings:
                pair = (cond, grounding)
                code = self.new_code(pair, (0, -2))
                self.condition_codes[pair] = code
        
        #Aux vars
        self.aux_codes = []

        #PREVIOUSLY STORED INVARIANTS
        self.make_invariant_clauses()
        
        #h_dependant clauses
        #Start State (other clauses will be added in other functions as needed)

        for fluent, f_code in self.fluent_codes.iteritems():
            if fluent in self.problem.initial_state:
                self.clauses.append(((0, 0), [(0, f_code)]))
            else:
                self.clauses.append(((0, 0), [(0, -f_code)]))
            self.clause_types.append("start")

        #goal clauses
        
        self.goal_conditions = set()
        
        for prec in self.problem.flat_ground_goal_preconditions:
            if not isinstance(prec[0], PredicateCondition):
                self.goal_conditions.add(prec)
            self.clauses.append(((-1, -1), [(0, self.get_precondition_code(prec))]))
            self.clause_types.append("goal")
        
     
        for gcond in self.goal_conditions:
            assert False, "Complex goal Not implemented"
            #need to turn these conditions in to clauses
            #self.make_condition_clauses(gcond, self.goal_clauses)

        
        #Mutex
        ac = len(self.clauses)
        conflicts_to_encode = {}
        conflicts_to_encode_list = []
        for ag_pair in self.action_codes:
            conflicts_to_encode[ag_pair] = set()
 
        if self.execution_semantics == "serial":
            for ag_pair1, ag_pair2 in itertools.combinations(self.action_codes, 2):
                if (self.redundant_mutex or ag_pair2 not in self.problem.eff_eff_conflicts[ag_pair1]):
                    conflicts_to_encode_list.append((ag_pair1, ag_pair2))
                    conflicts_to_encode[ag_pair1].add(ag_pair2)
                    conflicts_to_encode[ag_pair2].add(ag_pair1)
        else: #forall
            for ag_pair1, ag_pair2_list in self.problem.pre_eff_conflicts.iteritems():
                for ag_pair2 in ag_pair2_list:
                    if ag_pair1 != ag_pair2 and (self.redundant_mutex or\
                        ag_pair2 not in self.problem.eff_eff_conflicts[ag_pair1]):
                        conflicts_to_encode_list.append((ag_pair1, ag_pair2))
                        conflicts_to_encode[ag_pair1].add(ag_pair2)
                        conflicts_to_encode[ag_pair2].add(ag_pair1)

        if not self.quiet:
            print "Conflicts to encode:", len(conflicts_to_encode_list)
            print "Making conflict cliques..."
        cliques = self.make_conflict_cliques(conflicts_to_encode_list,
            conflicts_to_encode, self.action_codes)
     
        self.make_clique_clauses(cliques)

        if not self.quiet:
            print "Mutex clauses:", len(self.clauses) - ac

        #Action pre- and post-conditions
        #Also make the fluent pre- and post-condition lists for SSAs
        for (action, grounding), a_code in self.action_codes.iteritems():
            for prec in action.flat_ground_preconditions[grounding]:
                pre_code = self.get_precondition_code(prec)
                self.clauses.append(((0, -2), [(0, -a_code), (0, pre_code)]))
                self.clause_types.append("pre")
                
            for eff in action.flat_ground_effects[grounding]:
                if isinstance(eff, ConditionalEffect):
                    assert False
                    #eff_pre = [self.get_precondition_code(prec)\
                    #    for prec in eff.flat_ground_preconditions[grounding]]
                    ############# THIS NEEDS TO BE DONE
                else:
                    eff_code = self.get_effect_code(eff) 
                    if eff_code is not None: #Not for increase effects
                        self.clauses.append(((0, -2), [(0, -a_code), (1, eff_code)]))
                        self.clause_types.append("eff")

        #SSAs
        #CURRENTLY JUST WITH FLUENTS - not conditional

        
        for (pred, grounding), f_code in self.fluent_codes.iteritems():
            add_clause = [(0, -f_code), (-1, f_code)]
            for ag_pair in pred.ground_adds[grounding]:
                add_clause.append((-1, self.action_codes[ag_pair]))
            self.clauses.append(((1, -1), add_clause))
            self.clause_types.append("ssa")
            
            del_clause = [(0, f_code), (-1, -f_code)]
            for ag_pair in pred.ground_dels[grounding]:
                del_clause.append((-1, self.action_codes[ag_pair]))
            self.clauses.append(((1, -1), del_clause))
            self.clause_types.append("ssa")
        
        if self.process_semantics:
            #Process semantics clases
            #a^{t} -> V-pre(a^{t-1}) v Vmutex(a^{t-1})
            for (action, grounding), a_code in self.action_codes.iteritems():
                clause = [(0, -a_code)]
                for prec in action.flat_ground_preconditions[grounding]:
                    clause.append((-1, -self.get_precondition_code(prec)))
                
                #Here we want actions that are mutex with a, but which do not delete
                #The preconditions of a
                not_encode = set([a_code])
                for prec in action.flat_ground_preconditions[grounding]:
                    #Assuming STRIPS
                    if prec[2]: e_list = prec[0].pred.ground_dels[prec[0].ground_conditions[prec[1]]]
                    else: e_list = prec[0].pred.ground_adds[prec[0].ground_conditions[prec[1]]]
                    for ag_pair in e_list:
                        not_encode.add(self.action_codes[ag_pair])
                
                for eff in action.flat_ground_effects[grounding]:
                    if isinstance(eff[0], IncreaseCondition): continue
                    if eff[2]: e_list = eff[0].pred.ground_dels[eff[0].ground_conditions[eff[1]]]
                    else: e_list = eff[0].pred.ground_adds[eff[0].ground_conditions[eff[1]]]
                    for ag_pair in e_list:
                        a_code2 = self.action_codes[ag_pair]
                        if a_code2 not in not_encode:
                            clause.append((-1, a_code2))
                            not_encode.add(a_code2)
                    if eff[2]: e_list = eff[0].pred.ground_nprecs[eff[0].ground_conditions[eff[1]]]
                    else: e_list = eff[0].pred.ground_precs[eff[0].ground_conditions[eff[1]]]
                    for ag_pair in e_list:
                        a_code2 = self.action_codes[ag_pair]
                        if a_code2 not in not_encode:
                            clause.append((-1, a_code2))
                            not_encode.add(a_code2)
                self.clauses.append(((1, -2), clause))
                self.clause_types.append("process")
     
    def build_plan(self):
        """Build a plan from the true variables
        
            (FlatEncoding) -> None
        """
        plan_actions = {}
        for t in xrange(self.horizon):
            plan_actions[t] = []            
        for cnf_code in self.true_vars:
            step, var = self.cnf_code_to_variable[cnf_code]
            v_obj, v_vars = self.variables[var]
            if step < self.horizon and isinstance(v_obj, Action):
                plan_actions[step].append((v_obj, v_vars))
        self.plan = []
        for step in xrange(self.horizon):
            self.plan.append(plan_actions[step])


