""" File:        encodings/precise_split.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2013-
    Desctiption: 

    Liscence:   [Refine this!]
"""

from utilities import encoding_error_code, PRE_COND, POST_COND, split_id_conds

from encoding_base import EncodingException, Encoding

from problem   import Predicate, PredicateCondition, NotCondition, AndCondition,\
                      OrCondition, ForAllCondition, ExistsCondition,\
                      IncreaseCondition, ConditionalEffect, Action

import itertools

encoding_class = 'PreciseSplitEncoding'

class PreciseSplitEncoding(Encoding):
    """ A precise split encoding along the lines of the one described in
        my thesis, but with the new mutex clauses.
    """
    
    def __init__(self, args, problem, quiet):
        """ Set encoding parameters
            
            (PreciseSplitEncoding, { str : obj }, Problem, bool) -> None
        """
        super(PreciseSplitEncoding, self).__init__(args, problem, quiet)
        
        self.execution_semantics = args["execution_semantics"]
        self.redundant_mutex = args["redundant_mutex"]
        self.min_mutex_clique_size = args["min_mutex_clique_size"]
        self.process_semantics = args["process_semantics"]

    def get_variable_name(self, step, var):
        """ Return a string containing a representation of the supplied variable
            
            (PreciseSplitEncoding, int, int) -> str
        """
        v_obj, v_vars = self.variables[var]
        if v_obj == 'aux':
            return "AUX " + str(v_vars[0]) + " " + str(v_vars[1]) + " step" + str(step)
        elif v_obj == 'split':
            var_str = "SPLIT"
            for (pred, grounding, cond_type) in v_vars.scs:
                var_str += " " + split_id_conds[cond_type] + " " + pred.name + " " +\
                " ".join(grounding + ('step' + str(step),))
            return var_str
        elif isinstance(v_obj, Action):
            return v_obj.name + " " + " ".join(v_vars + ('step' + str(step),))
        elif isinstance(v_obj, Predicate):
            return v_obj.name + " " + " ".join(v_vars + ('step' + str(step),))
        else:
            return v_obj.cond_code + " " + v_obj.desc + " " +\
                " ".join(v_vars + ('step' + str(step),))
    
    def get_precondition_code(self, prec):
        """ Return the code used for the precondition condition
            
            (PreciseSplitEncoding, (PredicateCondition, bool, (str,...))/
                (Condition, (str, ...))) -> int
        """
        if isinstance(prec[0], Predicate):
            if prec[2]:
                return self.fluent_codes[(prec[0], prec[1])]
            return -self.fluent_codes[(prec[0], prec[1])]
        elif isinstance(prec[0], PredicateCondition):
            if prec[2]:
                return self.fluent_codes[(prec[0].pred, prec[0].ground_conditions[prec[1]])]
            return -self.fluent_codes[(prec[0].pred, prec[0].ground_conditions[prec[1]])]    
        
        assert False, "Not supposed to have conditions..."
        return self.condition_codes[prec]
        
    def get_goal_precondition_code(self, prec):
        """ Return the code used for the precondition condition
            
            (PreciseSplitEncoding, (PredicateCondition, bool, (str,...))/
                (Condition, (str, ...))) -> int
        """
        if isinstance(prec[0], PredicateCondition):
            if prec[2]:
                return self.fluent_codes[(prec[0].pred, prec[0].ground_conditions[prec[1]])]
            else:
             return -self.fluent_codes[(prec[0].pred, prec[0].ground_conditions[prec[1]])]
        assert False, "Not supposed to have complex preconditions"
        return self.condition_codes[prec]

    def get_effect_code(self, eff):
        """ Return the code used for the given effect condition.
        
            (PreciseSplitEncoding, (PredicateCondition, bool, (str,...)) /
                (ConditionalEffect, (str, ...))) -> str / None
        """
        if isinstance(eff[0], Predicate):
            if eff[2]:
                return self.fluent_codes[(eff[0], eff[1])]
            return -self.fluent_codes[(eff[0], eff[1])]       
        elif isinstance(eff[0], ConditionalEffect):
            assert False, "Conditional effects not implemented"
            return self.cond_effect_codes[eff]
        return None
    
    def make_condition_clauses(self, cond, clause_list):
        """ Make the clauses for the given condition and add it to the given
            clause list.
        
            (PreciseSplitEncoding, (Condition, (str,...)) -> str
        """
        assert False, "Not implemented"
        """
        c_code = self.condition_codes[cond]
        if isinstance(cond[0], AndCondition) or isinstance(cond[0], ForAllCondition):
            #c -> ^_children
            sub_codes = []
            for cid, conj_groundings in enumerate(cond[0].ground_conditions):
                sub_cond = cond.conditions[cid]
                if isinstance(sub_cond, PredicateCondition):
                    sub_codes.append(self.get_precondition_code(\
                        (sub_cond, sub_cond.negated, conj_groundings[grounding])))
                else:
                    sub_codes.append(self.get_precondition_code(\
                        (sub_cond, conj_groundings[grounding])))
            
            clause_list.append([-c_code] + sub_codes)
            #v_children -> c
            for child in sub_codes:
                clause_list.append([-child, c_code])
                
        elif isinstance(cond[0], OrCondition) or isinstance(cond[0], ExistsCondition):
            #^_children -> c
            sub_codes = []
            for cid, conj_groundings in enumerate(cond[0].ground_conditions):
                sub_cond = cond.conditions[cid]
                if isinstance(sub_cond, PredicateCondition):
                    sub_codes.append(self.get_precondition_code(\
                        (sub_cond, sub_cond.negated, conj_groundings[grounding])))
                else:
                    sub_codes.append(self.get_precondition_code(\
                        (sub_cond, conj_groundings[grounding])))
            
            clause_list.append([-c_code] + sub_codes)
            #c -> v_children
            for child in sub_codes:
                clause_list.append([-child, c_code])

        else: assert False
        """

    def encode_base(self):
        """ Do any horizon-independent encoding that might be required.
        
           (PreciseSplitEncoding, int, bool, bool) -> None
        """
        #Variables
        #Actions
        self.action_codes = {}
        for action in self.problem.actions.itervalues():
            for grounding in action.groundings:
                pair = (action, grounding)
                self.action_codes[pair] = self.code
                self.variables[self.code] = pair
                self.variable_layers[self.code] = (0, -2)
                self.code += 1
        
        #Split variables 
        self.split_codes = {}
        for scs in self.problem.split_conditions.itervalues():
            self.split_codes[scs] = self.code
            self.variables[self.code] = ('split', scs)
            self.variable_layers[self.code] = (0, -2)
            self.code += 1
        
        if not self.quiet:
            print "Number of action vars:", len(self.action_codes)
            print "Number of split vars: ", len(self.split_codes)
        
        #Continue making the variables
        #Fluents
        self.fluent_codes = {}
        for predicate in self.problem.predicates.itervalues():
            for grounding in predicate.groundings:
                pair = (predicate, grounding)
                self.fluent_codes[pair] = self.code
                self.variables[self.code] = pair
                self.variable_layers[self.code] = (0, -1)
                self.code += 1
        
        #Conditional Effects
        self.cond_effect_codes = {}
        for cond in self.problem.conditional_effects:
            assert False, "Conditional effects not implemented"
            for grounding in cond.groundings:
                pair = (cond, grounding)
                self.cond_effect_codes[pair] = self.code
                self.variables[self.code] = pair
                self.variable_layers[self.code] = (0, -2)
                self.code += 1
        
        #Conditions
        self.condition_codes = {}
        for cond in self.problem.conditions:
            for grounding in cond.groundings:
                pair = (cond, grounding)
                self.condition_codes[pair] = self.code
                self.variables[self.code] = pair
                self.variable_layers[self.code] = (0, -2)
                self.code += 1
        
        #Aux vars
        self.aux_codes = []
        self.vars_per_layer = self.code-1
        
        #PREVIOUSLY STORED INVARIANTS
        self.make_invariant_clauses()
        
        
        
        #h_dependant clauses
        #Start State (other clauses will be added in other functions as needed)
        for fluent, f_code in self.fluent_codes.iteritems():
            if fluent in self.problem.initial_state:
                self.clauses.append(((0, 0), [(0, f_code)]))
            else:
                self.clauses.append(((0, 0), [(0, -f_code)]))
            self.clause_types.append("start")

        #goal clauses
        self.goal_conditions = set()
        for prec in self.problem.flat_ground_goal_preconditions:
            if not isinstance(prec[0], PredicateCondition):
                self.goal_conditions.add(prec)
            self.clauses.append(((-1, -1), [(0, self.get_goal_precondition_code(prec))]))
            self.clause_types.append("goal")
        
        for gcond in self.goal_conditions:
            assert False, "Not implemented"
            #need to turn these conditions in to clauses
            self.make_condition_clauses(gcond, self.goal_clauses)
        
        pre_mutex_num_clauses = len(self.clauses)
        
        #h_independent clauses
        if self.execution_semantics in ["serial", "forall"]:
            conflicts_to_encode = {}
            conflicts_to_encode_set = set()
            split_conflicts_to_encode = {}
            split_conflicts_to_encode_set = set()
            for ag_pair in self.action_codes:
                conflicts_to_encode[ag_pair] = set()
            for scs in self.split_codes:
                split_conflicts_to_encode[scs] = set()
     
            for s_cond1 in self.split_codes:
                for (pred1, grounding1, (cond_type1, cond_sign1)) in\
                    itertools.chain(s_cond1.adds, s_cond1.dels):
                    if cond_sign1: p_list = pred1.ground_split_nprecs[grounding1]
                    else: p_list = pred1.ground_split_precs[grounding1]
                    for s_cond2 in p_list:
                        if s_cond1 != s_cond2 and s_cond2 not in\
                          self.problem.split_cond_cooccur[s_cond1]:
                            split_conflicts_to_encode[s_cond1].add(s_cond2)
                            split_conflicts_to_encode[s_cond2].add(s_cond1)
                            split_conflicts_to_encode_set.add((s_cond1, s_cond2))
                    
                    if self.redundant_mutex:
                        if cond_sign1: e_list = pred1.ground_split_dels[grounding1]
                        else: e_list = pred1.ground_split_adds[grounding1]
                        for s_cond2 in e_list:
                            if s_cond1 < s_cond2 and s_cond2 not in\
                              self.problem.split_cond_cooccur[s_cond1]:
                                split_conflicts_to_encode[s_cond1].add(s_cond2)
                                split_conflicts_to_encode[s_cond2].add(s_cond1)
                                split_conflicts_to_encode_set.add((s_cond1, s_cond2))

            if self.execution_semantics == "serial":
                for ag_pair1 in self.action_codes:
                    for ag_pair2 in self.action_codes:
                        if ag_pair1 < ag_pair2 and (self.redundant_mutex or\
                            ag_pair2 not in self.problem.eff_eff_conflicts[ag_pair1]):
                            conflicts_to_encode_set.add((ag_pair1, ag_pair2))
                            conflicts_to_encode[ag_pair1].add(ag_pair2)
                            conflicts_to_encode[ag_pair2].add(ag_pair1)
            
            else: #forall
                for s_cond1 in self.split_codes:
                    for (pred1, grounding1, (cond_type1, cond_sign1)) in\
                        itertools.chain(s_cond1.adds, s_cond1.dels):
                        if cond_sign1: p_list = pred1.ground_split_nprecs[grounding1]
                        else: p_list = pred1.ground_split_precs[grounding1]
                        
                        for s_cond2 in p_list:
                            if s_cond1 == s_cond2:
                                for (ag_pair1, ag_pair2) in itertools.combinations(\
                                    self.problem.split_cond_groundings[s_cond1], 2):
                                    if (self.redundant_mutex or ag_pair1 not in\
                                        self.problem.eff_eff_conflicts[ag_pair2]) and\
                                        (ag_pair2, ag_pair1) not in conflicts_to_encode:
                                        conflicts_to_encode_set.add((ag_pair1, ag_pair2))
                                        conflicts_to_encode[ag_pair1].add(ag_pair2)
                                        conflicts_to_encode[ag_pair2].add(ag_pair1)
                            
                            elif s_cond2 in self.problem.split_cond_cooccur[s_cond1]:
                                for ag_pair1 in self.problem.split_cond_groundings[s_cond1]:
                                    for ag_pair2 in self.problem.split_cond_groundings[s_cond2]:
                                        if (self.redundant_mutex or ag_pair1 not in\
                                        self.problem.eff_eff_conflicts[ag_pair2]) and\
                                        (ag_pair2, ag_pair1) not in conflicts_to_encode:
                                            conflicts_to_encode_set.add((ag_pair1, ag_pair2))
                                            conflicts_to_encode[ag_pair1].add(ag_pair2)
                                            conflicts_to_encode[ag_pair2].add(ag_pair1)

            if not self.quiet:
                print "Action conflicts to encode:", len(conflicts_to_encode_set)
                print "Split conflicts to encode: ", len(split_conflicts_to_encode_set)

            if not self.quiet: print "Making action cliques..."
            action_cliques = self.make_conflict_cliques(conflicts_to_encode_set,
                conflicts_to_encode, self.action_codes)
            
            if not self.quiet: print "Making split cliques..."
            split_cliques = self.make_conflict_cliques(split_conflicts_to_encode_set,
                split_conflicts_to_encode, self.split_codes)

            self.make_clique_clauses(action_cliques)
            self.make_clique_clauses(split_cliques)

        else:
            #exists step
            assert False, "Not implemented"
        
        
        if not self.quiet:
            print "Mutex clauses added:", (len(self.clauses) - pre_mutex_num_clauses)

        #We might add aux vars during mutex construction so we set this here
        self.vars_per_layer = self.code-1
        
        #Actions imply their split conditions
        for ag_pair, split_conds in self.problem.grounding_split_conds.iteritems():
            for s_cond in split_conds:
                self.clauses.append(((0, -2),
                    [(0, -self.action_codes[ag_pair]), (0, self.split_codes[s_cond])]))
                self.clause_types.append("a->s")

        #Split conditions imply their actions
        for s_cond, a_conds in self.problem.split_cond_groundings.iteritems():
            self.clauses.append(((0, -2),[(0, -self.split_codes[s_cond])] +\
                [(0, self.action_codes[a_cond]) for a_cond in a_conds]))
            self.clause_types.append("s->a")
        
        #MAKE SURE WE DONT ENFORCE BOTH AND ADD AND A DELETE (OR N AND PPRE)
        #TO THE SAME FLUENT FROM THE SAME VARIABLE

        #Action pre- and post-conditions
        for s_cond, s_code in self.split_codes.iteritems():
            pre_set = set()
            post_set = set()
            
            for (pred, grounding, (cond_type, cond_sign)) in s_cond.scs:
                if cond_type == PRE_COND:
                    pre_set.add((pred, grounding, cond_sign))
                else:
                    if cond_sign:
                        post_set.add((pred, grounding, cond_sign))
                        post_set.discard((pred, grounding, not cond_sign))
                    else:
                        if (pred, grounding, not cond_sign) not in post_set:
                            post_set.add((pred, grounding, cond_sign))
            for prec in pre_set:  
                pre_code = self.get_precondition_code(prec)
                self.clauses.append(((0, -2), [(0, -s_code), (0, pre_code)]))
                self.clause_types.append("pre")
                    
            for eff in post_set:
                if isinstance(eff, ConditionalEffect):
                    assert False
                else:
                    self.clauses.append(((0, -2), [(0, -s_code), (1, self.get_effect_code(eff))]))
                    self.clause_types.append("eff")

        #SSAs (Assumes STRIPS)
        for pg_pair, f_code in self.fluent_codes.iteritems():
            pred, grounding = pg_pair
            f_code = self.fluent_codes[pg_pair]
            add_clause = [(0, -f_code), (-1, f_code)]
            for s_cond in pred.ground_split_adds[grounding]:
                add_clause.append((-1, self.split_codes[s_cond]))
            self.clauses.append(((1, -1), add_clause))
            self.clause_types.append("ssa")
            del_clause = [(0, f_code), (-1, -f_code)]
            for s_cond in pred.ground_split_dels[grounding]:
                del_clause.append((-1, self.split_codes[s_cond]))
            self.clauses.append(((1, -1), del_clause))
            self.clause_types.append("ssa")
        
        if self.process_semantics:
            #Process semantics clases
            #a^{t} -> V-pre(a^{t-1}) v Vmutex(a^{t-1})
            av = 0
            for (action, grounding), a_code in self.action_codes.iteritems():
                clause = [(0, -a_code)]
                
                for prec in action.flat_ground_preconditions[grounding]:
                    clause.append((-1, -self.get_precondition_code(prec)))
                
                #Here we want actions that are mutex with a, but which do not delete
                #The preconditions of a
                not_encode = set()
                for prec in action.flat_ground_preconditions[grounding]:
                    if prec[2]: e_list = prec[0].pred.ground_split_dels[prec[0].ground_conditions[prec[1]]]
                    else: e_list = prec[0].pred.ground_split_adds[prec[0].ground_conditions[prec[1]]]
                    for s_cond in e_list:
                        not_encode.add(s_cond)
                
                for eff in action.flat_ground_effects[grounding]:
                    if isinstance(eff[0], IncreaseCondition): continue
                    if eff[2]: e_list = eff[0].pred.ground_split_dels[eff[0].ground_conditions[eff[1]]]
                    else: e_list = eff[0].pred.ground_split_adds[eff[0].ground_conditions[eff[1]]]
                    for s_cond in e_list:
                        if s_cond not in not_encode:
                            clause.append((-1, self.split_codes[s_cond]))
                        av += 1
                        
                    if eff[2]: e_list = eff[0].pred.ground_split_nprecs[eff[0].ground_conditions[eff[1]]]
                    else: e_list = eff[0].pred.ground_split_precs[eff[0].ground_conditions[eff[1]]]
                    for s_cond in e_list:
                        if s_cond not in not_encode:
                            clause.append((-1, self.split_codes[s_cond]))
                        else: av += 1

                self.clauses.append(((1, -2), clause))
                self.clause_types.append("process")
    
    
    def build_plan(self):
        """Build a plan from the true variables
        
            (PreciseSplitEncoding) -> None
        """
        plan_actions = {}
        for t in xrange(self.horizon):
            plan_actions[t] = []            
        for cnf_code in self.true_vars:
            step, var = self.cnf_code_to_variable[cnf_code]
            v_obj, v_vars = self.variables[var]
            if step < self.horizon and isinstance(v_obj, Action):
                plan_actions[step].append((v_obj, v_vars))
        self.plan = []
        for step in xrange(self.horizon):
            self.plan.append(plan_actions[step])
    


