""" File:        encodings/flat.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2013-
    Desctiption: 

    Liscence:   [Refine this!]
"""

from utilities import encoding_error_code

from encoding_base import EncodingException, Encoding

from problem   import Predicate, PredicateCondition, NotCondition, AndCondition,\
                      OrCondition, ForAllCondition, ExistsCondition,\
                      IncreaseCondition, ConditionalEffect, Action


import itertools, networkx

encoding_class = 'FlatExistsStepEncoding'

class FlatExistsStepEncoding(Encoding):
    """ A simple flat encoding along the lines of SatPlan06 with full frame
        axioms.
    
        Variables and clauses are created once 
    """
    
    def __init__(self, args, problem, quiet):
        """ Set encoding parameters
            
            (FlatExistsStepEncoding, { str : obj }, Problem, bool) -> None
        """
        super(FlatExistsStepEncoding, self).__init__(args, problem, quiet)
        
        self.execution_semantics = args["execution_semantics"]
        self.redundant_mutex = args["redundant_mutex"]
        self.min_mutex_clique_size = args["min_mutex_clique_size"]
        self.process_semantics = args["process_semantics"]

    def get_variable_name(self, var):
        """ Return a string containing a representation of the supplied variable
            
            (int) -> str
        """
        v_obj, v_vars = self.variables[((abs(var) - 1) % self.vars_per_layer) + 1]
        step = (abs(var) - 1) / self.vars_per_layer
        if v_obj is None:
            return "AUX " + str(v_vars[0]) + " " + str(v_vars[1]) + " step" + str(step)
        elif isinstance(v_obj, Action):
            return v_obj.name + " " + " ".join(v_vars + ('step' + str(step),))
        elif isinstance(v_obj, Predicate):
            return v_obj.name + " " + " ".join(v_vars + ('step' + str(step),))
        else:
            return v_obj.cond_code + " " + v_obj.desc + " " +\
                " ".join(v_vars + ('step' + str(step),))


    def get_precondition_code(self, prec):
        """ Return the code used for the precondition condition
            
            (FlatExistsStepEncoding, (PredicateCondition, bool, (str,...))/
                (Condition, (str, ...))) -> int
        """
        if isinstance(prec[0], PredicateCondition):
            if prec[2]:
                return self.fluent_codes[(prec[0].pred, prec[0].ground_conditions[prec[1]])]
            return -self.fluent_codes[(prec[0].pred, prec[0].ground_conditions[prec[1]])]
        return self.condition_codes[prec]

    def get_effect_code(self, eff, var_offset):
        """ Return the code used for the given effect condition.
        
            (FlatExistsStepEncoding, (PredicateCondition, bool, (str,...)) /
                (ConditionalEffect, (str, ...)), int) -> str / None
        """
        if isinstance(eff[0], PredicateCondition):
            if eff[2]:
                return self.fluent_codes[(eff[0].pred, eff[0].ground_conditions[eff[1]])] + var_offset
            return -self.fluent_codes[(eff[0].pred, eff[0].ground_conditions[eff[1]])] - var_offset
        elif isinstance(eff[0], ConditionalEffect):
            return self.cond_effect_codes[eff] + var_offset
        return None
    
    def encode_base(self):
        """ Do any horizon-independent encoding that might be required.
        
           (FlatExistsStepEncoding, int, bool, bool) -> None
        """
        #Variables
        #Actions
        self.action_codes = {}
        for action in self.problem.actions.itervalues():
            for grounding in action.groundings:
                pair = (action, grounding)
                self.action_codes[pair] = self.code
                self.variables[self.code] = pair
                self.code += 1
                
        #Fluents
        self.fluent_codes = {}
        for predicate in self.problem.predicates.itervalues():
            for grounding in predicate.groundings:
                pair = (predicate, grounding)
                self.fluent_codes[pair] = self.code
                self.variables[self.code] = pair
                self.code += 1
        
        print "ACTION CODES:", len(self.action_codes)
        print "FLUENT CODES:", len(self.fluent_codes)
        
        """
        #Conditional Effects
        self.cond_effect_codes = {}
        for cond in self.problem.conditional_effects:
            assert False, "Conditional effects Not implemented"
            for grounding in cond.groundings:
                pair = (cond, grounding)
                self.cond_effect_codes[pair] = self.code
                self.variables[self.code] = pair
                self.code += 1
        
        
        #Conditions
        self.condition_codes = {}
        for cond in self.problem.conditions:
            for grounding in cond.groundings:
                pair = (cond, grounding)
                self.condition_codes[pair] = self.code
                self.variables[self.code] = pair
                self.code += 1
        """
        
        #Aux vars
        self.aux_codes = []
        self.vars_per_layer = self.code-1
        
        #h_dependant clauses
        #Start State (other clauses will be added in other functions as needed)

        for fluent, f_code in self.fluent_codes.iteritems():
            if fluent in self.problem.initial_state:
                self.clauses.append(((0, 0), [f_code]))
            else:
                self.clauses.append(((0, 0), [-f_code]))
            self.clause_types.append("start")

        #goal clauses
        self.goal_conditions = set()
        for prec in self.problem.flat_ground_goal_preconditions:
            if not isinstance(prec[0], PredicateCondition):
                self.goal_conditions.add(prec)
            self.clauses.append(((-1, -1), [self.get_precondition_code(prec)]))
            self.clause_types.append("goal")
        
        for gcond in self.goal_conditions:
            assert False, "Complex goal Not implemented"
            #need to turn these conditions in to clauses
            self.make_condition_clauses(gcond, self.goal_clauses)
        
        #Mutex
        ac = len(self.clauses)
       
        #exists step
        self.make_enabling_disabling_sccs()
        
        
        
        
        
        
 
        if not self.quiet:
            print "Mutex clauses:", len(self.clauses) - ac

        #We might add aux vars during mutex construction so we set this here
        self.vars_per_layer = self.code-1

        #Action pre- and post-conditions
        #Also make the fluent pre- and post-condition lists for SSAs
        
        for (action, grounding), a_code in self.action_codes.iteritems():
            for prec in action.flat_ground_preconditions[grounding]:
                pre_code = self.get_precondition_code(prec)
                self.clauses.append(((0, -2), [-a_code, pre_code]))
                self.clause_types.append("pre")
                
            for eff in action.flat_ground_effects[grounding]:
                if isinstance(eff, ConditionalEffect):
                    assert False
                    #eff_pre = [self.get_precondition_code(prec)\
                    #    for prec in eff.flat_ground_preconditions[grounding]]
                    ############# THIS NEEDS TO BE DONE
                else:
                    eff_code = self.get_effect_code(eff, self.vars_per_layer) 
                    if eff_code is not None: #Not for increase effects
                        self.clauses.append(((0, -2), [-a_code, eff_code]))
                        self.clause_types.append("eff")

        #SSAs
        #CURRENTLY JUST WITH FLUENTS - THIS NEEDS TO BE FIXED
        #SSAs talk about the next layer (so that way all clauses can talk about the
        #current layer and next layer
        
        for (pred, grounding), f_code in self.fluent_codes.iteritems():
            add_clause = [-f_code - self.vars_per_layer, f_code]
            for ag_pair in pred.ground_adds[grounding]:
                add_clause.append(self.action_codes[ag_pair])
            self.clauses.append(((0, -2), add_clause))
            self.clause_types.append("ssa")
            
            del_clause = [f_code + self.vars_per_layer, -f_code]
            
            for ag_pair in pred.ground_dels[grounding]:
                del_clause.append(self.action_codes[ag_pair])
            
            self.clauses.append(((0, -2), del_clause))
            self.clause_types.append("ssa")
        
        if self.process_semantics:
            #Process semantics clases
            #a^{t+1} -> V-pre(a^{t}) v Vmutex(a^{t})
            for (action, grounding), a_code in self.action_codes.iteritems():
                clause = [-a_code - self.vars_per_layer]
                for prec in action.flat_ground_preconditions[grounding]:
                    clause.append(-self.get_precondition_code(prec))
                
                #Here we want actions that are mutex with a, but which do not delete
                #The preconditions of a
                not_encode = set([a_code])
                for prec in action.flat_ground_preconditions[grounding]:
                    #Assuming STRIPS
                    if prec[2]: e_list = prec[0].pred.ground_dels[prec[0].ground_conditions[prec[1]]]
                    else: e_list = prec[0].pred.ground_adds[prec[0].ground_conditions[prec[1]]]
                    for ag_pair in e_list:
                        not_encode.add(self.action_codes[ag_pair])
                
                for eff in action.flat_ground_effects[grounding]:
                    if isinstance(eff[0], IncreaseCondition): continue
                    if eff[2]: e_list = eff[0].pred.ground_dels[eff[0].ground_conditions[eff[1]]]
                    else: e_list = eff[0].pred.ground_adds[eff[0].ground_conditions[eff[1]]]
                    for ag_pair in e_list:
                        a_code2 = self.action_codes[ag_pair]
                        if a_code2 not in not_encode:
                            clause.append(a_code2)
                            not_encode.add(a_code2)
                    if eff[2]: e_list = eff[0].pred.ground_nprecs[eff[0].ground_conditions[eff[1]]]
                    else: e_list = eff[0].pred.ground_precs[eff[0].ground_conditions[eff[1]]]
                    for ag_pair in e_list:
                        a_code2 = self.action_codes[ag_pair]
                        if a_code2 not in not_encode:
                            clause.append(a_code2)
                            not_encode.add(a_code2)
                self.clauses.append(((0, -3), clause))
                self.clause_types.append("process")
   
    
    def build_plan(self):
        """Build a plan from the true variables
        
            (FlatExistsStepEncoding) -> None
        """
        plan_actions = {}
        for t in xrange(self.horizon):
            plan_actions[t] = []            
        for var in self.true_vars:
            v_obj, v_vars = self.variables[((abs(var) - 1) % self.vars_per_layer) + 1]
            step = (abs(var) - 1) / self.vars_per_layer
            if step < self.horizon and isinstance(v_obj, Action):
                plan_actions[step].append((v_obj, v_vars))
        self.plan = []
        for step in xrange(self.horizon):
            self.plan.append(plan_actions[step])


