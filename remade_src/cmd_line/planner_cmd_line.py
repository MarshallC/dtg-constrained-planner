""" File:        planner_cmd_line.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        
    Desctiption: 
"""

import sys, os
from cmd_line import ArgProcessor, ArgDefinition, FlagDefinition,\
    range_validator, enum_validator, bool_validator, InputException

def print_citation_info(arg_processor):
    """ 
    
    """
    print "When citing this planner please use the following publication:"
    print "TBA"
    sys.exit(0)


def process_args(valid_encodings, default_encoding, valid_solvers, default_solver):
    """ Process the command line arguments for the planner.
        
        ({str : str }, str, { str : str },  str) -> ArgProcessor
    """
    
    arg_processor = ArgProcessor()
    
    arg_processor.add_program_arg('-domain',
        ArgDefinition('domain_file_name',
            True,
            None,
            None,
            None,
            "Domain file name"))
    
    arg_processor.add_program_arg('-problem',
        ArgDefinition('problem_file_name',
            True,
            None,
            None,
            None,
            "Problem file name"))
    
    arg_processor.add_program_arg('-exp_name',
        ArgDefinition('exp_name',
            True,
            None,
            None,
            None,
            "Experiment name"))
            
    arg_processor.add_program_arg('-plan_file',
        ArgDefinition('plan_file',
            False,
            None,
            None,
            None,
            "The output plan file"))
    
    arg_processor.add_program_arg('-soln_file',
        ArgDefinition('soln_file',
            False,
            None,
            None,
            None,
            "The output solution file"))
    
    arg_processor.add_program_arg('-query',
        ArgDefinition('query_strategy',
            False,
            enum_validator,
            [['fixed', 'rampup'], "Error: invalid query strategy: "],
            'rampup',
            "Query strategy"))
    
    arg_processor.add_program_arg('-horizon',
        ArgDefinition('horizon',
            False,
            range_validator,
            [int, 0, None, True, "Error: invalid initial or fixed horizon: "],
            1,
            "Initial or fixed horizon"))
            
    arg_processor.add_program_arg('-copy_layer',
        ArgDefinition('copy_layer',
            False,
            range_validator,
            [int, 0, None, True, "Error: invalid copy layer: "],
            1,
            "Metalayer encoding copy layer"))
    
    arg_processor.add_program_arg('-cut_above',
        ArgDefinition('cut_above',
            False,
            bool_validator,
            ["Error: invalid cut above setting: "],
            False,
            "Causal graph cutting position"))
    
    arg_processor.add_program_arg('-maxhorizon',
        ArgDefinition('max_horizon',
            False,
            range_validator,
            [int, 0, None, True, "Error: invalid maximum horizon: "],
            300,
            "Maximum horizon"))
   
    arg_processor.add_program_arg('-plangraph_invariants',
        ArgDefinition('plangraph_invariants',
            False,
            bool_validator,
            ["Error: invalid Plangraph intariants option: "],
            False,
            "Compute Plangraph invariants"))
            
    arg_processor.add_program_arg('-plangraph_timeless_only',
        ArgDefinition('plangraph_timeless_only',
            False,
            bool_validator,
            ["Error: invalid Plangraph timeless only option: "],
            False,
            "Only include timeless plangraph invariants")) 
    
    arg_processor.add_program_arg('-sas_invariants',
        ArgDefinition('sas_invariants',
            False,
            bool_validator,
            ["Error: invalid SAS+ intariants option."],
            False,
            "Compute SAS+ invariants"))
            
    arg_processor.add_program_arg('-cachet_invariants',
        ArgDefinition('cachet_invariants',
            False,
            bool_validator,
            ["Error: invalid intariants option."],
            False,
            "Compute cachet invariants"))
    
    arg_processor.add_program_arg('-cachet_unary_only',
        ArgDefinition('cachet_unary_only',
            False,
            bool_validator,
            ["Error: invalid unary cachet intariants option: "],
            False,
            "Only compute unary cachet invariants")) 
    
    arg_processor.add_program_arg('-encoding',
        ArgDefinition('encoding',
            False,
            enum_validator,
            [valid_encodings.keys(), "Error: invalid encoding: "],
            default_encoding,
            "Encoding"))

    arg_processor.add_program_arg('-exec_semantics',
        ArgDefinition('execution_semantics',
            False,
            enum_validator,
            [['forall', 'serial'], "Error: invalid action execution semantics: "],
            'forall',
            "Action execution semantics"))   
    
    arg_processor.add_program_arg('-redundant_mutex',
        ArgDefinition('redundant_mutex',
            False,
            bool_validator,
            ["Error: invalid redundant mutex setting: "],
            False,
            "Include redundant mutex clauses (good with tree mutex):"))
    
    arg_processor.add_program_arg('-min_mutex_clique_size',
        ArgDefinition('min_mutex_clique_size',
            False,
            range_validator,
            [int, 2, None, True, "Error: invalid min mutex clique size: "],
            None,
            "Minimum mutex clique size for tree encoding"))
    
    arg_processor.add_program_arg('-process_semantics',
        ArgDefinition('process_semantics',
            False,
            bool_validator,
            ["Error: invalid process semantics setting"],
            False,
            "Use the process semantics"))
    
    arg_processor.add_program_arg('-costs',
        ArgDefinition('action_costs',
            False,
            enum_validator,
            [['True', 'False', 'unit'], "Error: invalid action cost setting: "],
            'True',
            "Action costs"))      
    
    arg_processor.add_program_arg('-solver',
        ArgDefinition('solver',
            False,
            enum_validator,
            [valid_solvers.keys(), "Error: invalid solver"],
            default_solver,
            "Solver"))
    
    arg_processor.add_program_arg('-solver_args',
        ArgDefinition('solver_args',
            False,
            None,
            None,
            None,
            "Solver args (arg=val;...)"))  
    
    arg_processor.add_program_arg('-time',
        ArgDefinition('time_out',
            False,
            range_validator,
            [int, 0, None, True, "Error: invalid time out: "],
            None,
            "Time out"))
    
    arg_processor.add_program_arg('-tmp_path',
        ArgDefinition('tmp_path',
            False,
            None,
            None,
            os.path.join("/".join(os.path.dirname(__file__).split("/")[:-2]), "tmp_files"),
            "Tmp file path"))
    
    arg_processor.add_program_arg('-remove_tmp',
        ArgDefinition('remove_tmp_files',
            False,
            bool_validator,
            ["Error: invalid action variables setting: "],
            False,
            "Remove tmp files"))
    
    arg_processor.add_program_arg('-var_file',
        ArgDefinition('var_file',
            False,
            bool_validator,
            ["Error: invalid var file option."],
            False,
            "If variable description files should be written: "))
    
    arg_processor.add_program_arg('-debug_cnf',
        ArgDefinition('debug_cnf',
            False,
            bool_validator,
            ["Error: invalid debug cnf option: "],
            False,
            "If annotated CNF files should be written for debugging"))
            
    arg_processor.add_program_flag('--cnf_stats',
        FlagDefinition('cnf_stats', 
            None, 
            "Print stats about generated CNF files"))
    
    arg_processor.add_program_flag('--debug',
        FlagDefinition('debug', 
            None, 
            'Print extended debugging output'))
    
    arg_processor.add_program_flag('--cite',
        FlagDefinition('cite',
            print_citation_info,
            "Display citation information"))
                  
    try:
        arg_processor.parse_args()
        
        if arg_processor.max_horizon != None and arg_processor.horizon != None\
            and arg_processor.max_horizon < arg_processor.horizon:
            raise InputException("Error: maximum horizon < init horizon")
            
        arg_processor.encoding_args = {}
        arg_processor.encoding_args["execution_semantics"] = arg_processor.execution_semantics
        arg_processor.encoding_args["redundant_mutex"] = arg_processor.redundant_mutex     
        arg_processor.encoding_args["min_mutex_clique_size"] = arg_processor.min_mutex_clique_size
        arg_processor.encoding_args["process_semantics"] = arg_processor.process_semantics 

    
    except InputException as e:
        print e.message
        print "Use --help flag to display usage information."
        sys.exit(1)
    return arg_processor


