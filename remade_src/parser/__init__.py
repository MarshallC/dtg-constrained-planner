""" File:        parser/__init__.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2013-
    Desctiption: Exists to allow the importing of the parser subsystem as
                 a module.

    Liscence:   [Refine this!]
"""

from parser import ParsingException, Parser
from grounder import Grounder
