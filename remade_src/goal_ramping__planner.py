""" File:        goal_ramping__planner.py
    Author:      Charles Gretton and Nathan Robinson
    Contact:     charles.gretton@nicta.com.au
    Date:        2014-
    Desctiption: 

    Liscence:   [Refine this!]
"""

import sys, os, time, subprocess, errno

from problem import AndCondition
from problem import Predicate

from utilities import CodeException, remove, PRE_SUFFIX, GROUND_SUFFIX,\
    solving_error_code, extracting_error_code
from cmd_line import process_args
from problem import ProblemException
from parser import ParsingException, Parser, Grounder
from preprocessors import PreprocessingException, SASPlusPreprocessor,\
    CachetPreprocessor, PlangraphPreprocessor
    
from cnf_encodings import EncodingException, EncodingWrapper
from solvers import SolvingException, SolverWrapper

class PlanningException(CodeException):
    def __init__(self, message, code):
        self.message = message
        self.code = code

def write_error_sln(output_file_name, exp_name, code):
    """ Write the given experiment name and error code to the error file
        
        (str, str, int) -> None
    """
    try:
        with file(output_file_name, 'w') as sln_file:
            print >> sln_file, exp_name
            print >> sln_file, 'NSAT'
            print >> sln_file, code
    except IOError:
        print "Error opening sln file:", out_name

final_state=0
set_of_static_names=0

def main(): 
    global final_state
    global set_of_static_names
    
    start_time = time.time()
    
    #Process the possible encodings, and solvers so we can provide useful
    #cmd line advice
    try:
        encoding_wrapper = EncodingWrapper()
        encoding_wrapper.read_encoding_list()
        
        solver_wrapper = SolverWrapper()
        solver_wrapper.read_solver_list()
    
    except (EncodingException, SolvingException) as e:
        print e.message
        sys.exit(1)
    
    #Process the cmd line args
    args = process_args(\
        encoding_wrapper.valid_encodings, encoding_wrapper.default_encoding,
        solver_wrapper.valid_solvers, solver_wrapper.default_solver)
    
    arg_processing_time = time.time()
    
    #Report on system start up
    if not args.quiet:
        print "Encodings registered:    ", len(encoding_wrapper.valid_encodings)
        print "Solvers registered:      ", len(solver_wrapper.valid_solvers)
        print "Cmd line arg processing time:", (arg_processing_time - start_time)
    
    #Ensure that the tmp_dir exists
    try:
        os.makedirs(args.tmp_path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            print "Error: could not create temporary directory:", args.tmp_path
            sys.exit(1)
    
    #Parse the input PDDL
    try:
        parser = Parser(args.domain_file_name, args.problem_file_name, args.quiet)
        #parser = Parser(args.domain_file_name, args.problem_file_name, args.quiet)
        if not args.quiet: print "Parsing the PDDL domain"
        parser.parse_domain()
        if not args.quiet: print "Parsing the PDDL problem"
        parser.parse_problem()
        
        the_list_of_goal_elements = []
        if isinstance(parser.problem.goal, AndCondition):
            for goal in parser.problem.goal.conditions:
                the_list_of_goal_elements.append(goal)
        else:
            assert 0
            exit
                
        if not args.quiet: print "Simplifying the problem representation"
        
        number_of_distinct_goal_elements=len(the_list_of_goal_elements)
        for a_goal in xrange(0, number_of_distinct_goal_elements):
            

            parser = Parser(args.domain_file_name, args.problem_file_name, args.quiet)
            if not args.quiet: print "Parsing the PDDL domain a second time"
            parser.parse_domain()
            if not args.quiet: print "Parsing the PDDL problem a second time"
            parser.parse_problem()

            print "In case the final state is non-zero I am going to treat is as the starting state: ",final_state
            
            if final_state != 0:
                print "We have initially the following state: "

                for fact in parser.problem.initial_state_set:
                    print fact[0].name, fact[1]
                    
                print "And now I am adding the following facts."
                parser.reset_initial_state(set_of_static_names)#static_predicates)
                
                for fact in final_state:
                    print fact[0].name, fact[1]
                    parser.add_fact_to_initial_state(fact[0].name, fact[1])
                
                parser.re_initialise_starting_state_set()


            the_list_of_goal_elements = []
            if isinstance(parser.problem.goal, AndCondition):
                for goal in parser.problem.goal.conditions:
                    the_list_of_goal_elements.append(goal)
            
            goals_we_are_planning_for_ = []
            for i in xrange(0, a_goal+1):
                print "Including goal",the_list_of_goal_elements[i]
                goals_we_are_planning_for_.append(the_list_of_goal_elements[i])
                
            
            assert  (len(goals_we_are_planning_for_) != 0)

            goals_we_are_planning_for=AndCondition(goals_we_are_planning_for_)
            parser.problem.goal=goals_we_are_planning_for
            
            problem=parser.problem
            parser.problem.simplify()
            parser.problem.assign_cond_codes()

            if not args.quiet: print "Grounding the problem"
            grounder = Grounder(problem, os.path.join(args.tmp_path, args.exp_name + PRE_SUFFIX),
                                os.path.join(args.tmp_path, args.exp_name + GROUND_SUFFIX), args.quiet)
            grounder.ground()
        
        
            if not args.quiet: print "Simplifying the ground encoding..."
            problem.compute_static_preds()
            
            static_predicates=problem.static_preds
            
            set_of_static_names = set()
            for f in static_predicates :
                print "A static name is : ", f[0].name
                set_of_static_names.add(f[0].name)
            print "Static predicates are: ", set_of_static_names

            problem.link_groundings()
            problem.make_flat_preconditions()
            problem.make_flat_effects()
            problem.get_encode_conds()
            problem.make_cond_and_cond_eff_lists()
            problem.link_conditions_to_actions()
            problem.make_strips_conditions()
        
            problem.prune_useless_strips_actions()

            problem.compute_conflict_mutex()
        
        
            object_invariants = []
            if args.plangraph_invariants:
                if not args.quiet: print "Generating Plangraph invariants..."
                plangraph_preprocessor = PlangraphPreprocessor(problem, args.quiet, args.plangraph_timeless_only)
                object_invariants = plangraph_preprocessor.run()
                if object_invariants == False:
                    raise PreprocessingError()
            
        
            if args.sas_invariants:
                if not args.quiet: print "Generating SAS+ invariants..."
                sas_invariants = SASPlusPreprocessor(problem, args.quiet)
                sas_invariants.run()
        
            end_sas_time = time.time()    
            if args.sas_invariants and not args.quiet:
                print "SAS+ invariants time:", (end_sas_time - end_plangraph_time)
        
            if not args.quiet:
                print "Generating base encoding:", args.encoding, "..."

            problem.precisely_split()
    
            encoding_wrapper.instantiate_encoding(args.encoding, args.encoding_args, problem, args.quiet)
            encoding = encoding_wrapper.encoding
            encoding.horizon = args.horizon
            encoding.metalayer_copy_layer = args.copy_layer
            encoding.cg_cut_above = args.cut_above
            encoding.encode_base()
        
            #Consider moving this stuff after the cachet invariants code
            encoding.add_object_invariants(object_invariants)
        
            end_encoding_base_time = time.time()
            if not args.quiet: print "Encoding generation time:",\
                    (end_encoding_base_time - end_sas_time)

            if args.cachet_invariants:
                cachet_invariants = CachetPreprocessor(problem, encoding.clauses,
                                                       encoding.variables, encoding.vars_per_layer, args.tmp_path,
                                                   args.exp_name, args.cachet_unary_only, args.quiet)
                cachet_invariants.run()
                encoding.add_invariants(cachet_invariants.invariants)
        
            end_cachet_time = time.time()
        
            if args.cachet_invariants and not args.quiet:
                print "Cachet invariants time:", (end_cachet_time - end_encoding_base_time)

    
            cnf_file_name = os.path.join(args.tmp_path,
                                         args.exp_name + "_" + str(args.horizon) + ".cnf")

            variable_order_file_name = os.path.join(args.tmp_path,
                                                    args.exp_name + "_" + str(args.horizon) + ".ordering")

            if not args.quiet: print "Writing CNF file..."
            encoding.compute_cnf_codes(args.horizon)
            encoding.write_cnf(cnf_file_name)
            encoding.write_ordering(variable_order_file_name)
            end_writing_cnf_time = time.time()
            if not args.quiet:
                print "Writing time:", (end_writing_cnf_time - end_cachet_time)
                
                print "Clause statistics:"
                for ctype, count in encoding.get_clause_types().iteritems():
                    print ctype, ":", count

            if args.debug_cnf:
                if not args.quiet: print "Writing debug CNF..."
                encoding.write_debug_cnf(cnf_file_name + "_dbg", args.horizon)
            end_writing_dbg_cnf_time = time.time()
            if not args.quiet and args.debug_cnf:
                print "Writing time:", (end_writing_dbg_cnf_time - end_writing_cnf_time)
        
            if args.var_file:
                if not args.quiet: print "Writing var file..."
                var_file_name = os.path.join(args.tmp_path,
                                             args.exp_name + "_" + str(args.horizon) + ".var")
                encoding.write_variable_file(var_file_name)
        
            end_writing_all_cnf_time = time.time()
            if not args.quiet and args.var_file:
                print "Writing time:", (end_writing_all_cnf_time - end_writing_dbg_cnf_time)
        
            if not args.quiet: print "Solving..."
            try:
                solver_wrapper.instantiate_solver(args.solver, cnf_file_name,\
                                                      args.tmp_path, args.exp_name, args.time_out, args.solver_args)
                
                (sln_res, sln_time, true_vars) = solver_wrapper.solver.solve()
                
                if not args.quiet:
                    print "SAT" if sln_res else "UNSAT"
                    print "Solution time:", sln_time
            
            except SolvingException as e:
                raise PlanningException(e.message, solving_error_code)
        
            if sln_res:
                encoding.set_true_variables(true_vars)
                encoding.build_plan()

            #encoding.print_true_variables()
            
            if not args.quiet:
                print "Plan:"
                for step, s_actions in enumerate(encoding.plan):
                    #mlayer = encoding.plan_metalayer[step]
                    
                    for (action, a_args) in s_actions:
                        a_str = action.name
                        if a_args:
                            a_str += " " + " ".join(a_args)
                        #print str(mlayer) + " - " + str(step) + ": " + a_str
                        print str(step) + ": " + a_str
            
            
            
            if not args.quiet:
                print "Simulating plan for validation."
            
            final_state=problem.get_state_resulting_from_simulation_of_plan(encoding.plan)
            

            print "Final state is: ",final_state

            sim_res, plan_cost = problem.simulate_plan(encoding.plan)
            
            if sim_res: print "Plan valid. Cost:", plan_cost
            else: print "Error: INVALID PLAN!"
            
                
    except (ParsingException, PreprocessingException, ProblemException, PlanningException) as e:
       print "Parsing or planning Error: ", e.message
       sys.exit(1)

    sys.exit(0)

if __name__ == '__main__':
    main()

