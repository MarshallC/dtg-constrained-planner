
#ifndef PARSER_HH
#define PARSER_HH

#include "problem.hh"

using namespace std;

class Parser{
public:

    Planning_Problem problem;
    

    void action(const string& );
    void effect(const string& );
    void conditional_effect(const string& );
    void fluent(const string& );
    void condition(const string& );
    void precondition(const string& );
    void starting_state(const string& );
    void goal_conditions(const string& );
};

#endif
