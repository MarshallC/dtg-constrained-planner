/* 
   This module determines a set of Boolean mutex constraints between problem variables.
 
   Date: Tue Dec 17 15:34:12 EST 2013
   Author: Charles Gretton
   
   Licence: NICTA License (free for research and non-commercial use)
   
*/

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <cassert>

#include "parser.hh"
#include <cstdlib>
#include <cstdio>
#include <cstring>

using namespace std;

string prefix = "";
string location_of_cachet = "";
string input_to_cachet__filename;// = "../../../tmp_files/invariants_via_cachet.cnf";
string output_from_cachet__filename;// = "../../../tmp_files/invariants_from_cachet.cnf";
string stack_derived_mutex__filename;

bool convergence = false;

int variable_count_N = 0;
int number_of_CNF_variables;

typedef set<set<int> > Problem;

ostream& operator<<(ostream& output, const Problem& problem){

    for (auto clause = problem.begin()
             ; clause != problem.end()
             ; clause++){
        if(!clause->size())continue;
        for (auto literal = clause->begin()
                 ; literal != clause->end()
                 ; literal++){
            output<<*literal<<" ";

            //assert(abs(*literal) < 1145);
        }
        output<<"0"<<endl;
    }

    return output;
}

vector<Problem >  timestep_i__constraints;
vector<Problem >  timestep_i__stack_derived_mutex;
Problem initial__Problem;
Problem timeless_constraints;
Problem only_one_action;
Problem at_least_one_action;

vector<int>  _timestep_1__fluents;
set<int> timestep_1__fluents;

vector<int>  _timestep_1__actions;
set<int> timestep_1__actions;

vector<int>  _timestep_2__fluents;
set<int> timestep_2__fluents;


void complete__only_one_action(){
    for(auto i =  _timestep_1__actions.begin()
            ; i !=  _timestep_1__actions.end()
            ; i++){
        for(auto j = i+1; j <  _timestep_1__actions.end() ; j++){
            set<int> clause;
            clause.insert(-1 * (*i));
            clause.insert(-1 * (*j));
            only_one_action.insert(clause);
        }
    }
}

void complete__at_least_one_action(){
    set<int> clause;
    for(auto i =  _timestep_1__actions.begin()
            ; i !=  _timestep_1__actions.end()
            ; i++){
        clause.insert(*i);
    }
    
    at_least_one_action.insert(clause);
}



void parse(ifstream& in){
    string tmp;
    string junk; /* Text that this program does not need to read. */
    bool parsed_variable_count_N = false;
    bool parsing_CNF = false;
    
    while(!in.eof()){
        getline(in, tmp);
        istringstream iss(tmp);

        if(!parsed_variable_count_N){
            parsed_variable_count_N = true;
            
            iss>>junk>>junk>>variable_count_N;
            cerr<<"Parsing number of variables at timestep 0 is :: "<<variable_count_N<<endl;
        } else if ('c' == tmp[0] && !parsing_CNF){ // DIMACS SAT comment line
            //cerr<<"Comment line: "<<tmp<<endl;
            if ('a' == tmp[2]){
                int action_symbol_index;
                iss>>junk>>junk>>action_symbol_index;
                _timestep_1__actions.push_back(action_symbol_index);
                timestep_1__actions.insert(action_symbol_index);
                //cerr<<"Interpreting comment line, action symbol :: "<<action_symbol_index<<endl;
            } else if ('f' == tmp[2]) {
                int fluent_symbol_index;
                iss>>junk>>junk>>fluent_symbol_index;
                _timestep_1__fluents.push_back(fluent_symbol_index);
                timestep_1__fluents.insert(fluent_symbol_index);
                //cerr<<"Interpreting comment line, fluent symbol :: "<<fluent_symbol_index<<endl;
            }
        } else if ('p' == tmp[0]){
            parsing_CNF = true;
            iss>>junk>>junk>>number_of_CNF_variables;

            cerr<<"Parsing CNF component of problem\n"
                <<"Number of variables is : "<<number_of_CNF_variables<<endl;
            //exit(-1);
            
        } else if ('c' != tmp[0] &&  parsing_CNF){
            int literal;
            set<int> clause;
            while(!iss.eof()){
                iss>>literal;
                if(0 != literal){
                    clause.insert(literal);
                }
            }
            
            if(1 == clause.size()){
                if(0 == timestep_i__constraints.size()){
                    timestep_i__constraints.resize(1);
                }
                timestep_i__constraints[0].insert(clause);
                continue;
            }

            initial__Problem.insert(clause);
        }
    }
    
    timestep_i__stack_derived_mutex.resize(1);
    timestep_i__stack_derived_mutex[0] = Problem();
    
    complete__only_one_action();
    //complete__at_least_one_action();
}

void parse(const string& in)
{
    ifstream input;
    input.open(in);
    
    if ( input.fail() ) {
        cerr<<"UNRECOVERABLE ERROR : No input file. \n"; exit(-1);
    }
    
    parse(input);

    input.close();
}

void write_to_obtain_timeless_constraints(){

    ofstream output;
    output.open(input_to_cachet__filename.c_str());
    
    assert(number_of_CNF_variables);

    int size = initial__Problem.size() + at_least_one_action.size() + only_one_action.size();
    output<<"p cnf "<<number_of_CNF_variables<<" "<<size<<endl;
    
    
    output<<at_least_one_action;
    output<<only_one_action;
    output<<initial__Problem;
    
    output.close();
}


void write_cnf(){
    ofstream output;
    output.open(input_to_cachet__filename.c_str());

    int size = initial__Problem.size() + 
        at_least_one_action.size() +
        only_one_action.size() + 
        timeless_constraints.size() +
        timestep_i__stack_derived_mutex.back().size() + 
        timestep_i__constraints.back().size() ;

    
    output<<"p cnf "<<number_of_CNF_variables<<" "<<size<<endl;

    output<<at_least_one_action;
    output<<only_one_action;
    output<<timeless_constraints;
    output<<timestep_i__constraints.back();
    output<<timestep_i__stack_derived_mutex.back();
    output<<initial__Problem;
    
    output.close();
}

void run_cachet(bool no_variable_limits = false){
    ostringstream oss;
    assert(number_of_CNF_variables);

    oss<<((no_variable_limits)?0:variable_count_N);
    
    if(timestep_i__constraints.size() >= 2){
        
        ofstream output;
        output.open(stack_derived_mutex__filename.c_str());
        
        if ( output.fail() ) {
            cerr<<"UNRECOVERABLE ERROR : Cannot open output file. \n"; exit(-1);
        }

        assert(timestep_i__stack_derived_mutex.size());
        const Problem& problem = timestep_i__stack_derived_mutex.back();
        
        //cerr<<"Now have the following stack derived mutex:\n";
        //cerr<<problem<<endl;

        for (auto clause = problem.begin()
                 ; clause != problem.end()
                 ; clause++){
            assert(clause->size() == 2);
            auto literal = clause->begin();
            int first = -1 * (*literal);
            literal++;
            int second = -1 * (*literal);
            output<<first + variable_count_N<<" "<<second + variable_count_N<<endl;

            
            //cerr<<first<<" "<<second<<endl;
            //cerr<<first + variable_count_N<<" "<<second + variable_count_N<<endl;
            
            assert(first >= _timestep_1__fluents.front());
            assert(first <= _timestep_1__fluents.back());
            assert(second >= _timestep_1__fluents.front());
            assert(second <= _timestep_1__fluents.back());
        }
        
        output.close();

        oss<<" "<<stack_derived_mutex__filename;
    } else {
        oss<<" "<<number_of_CNF_variables
           <<" "<<_timestep_1__fluents.front()
           <<" "<<_timestep_1__fluents.back();//_timestep_1__actions.size();
    }
    

    string run = location_of_cachet + " "
        + input_to_cachet__filename+" "
        + output_from_cachet__filename+" "
        + oss.str() + "  > /dev/null";
    
    cerr<<run<<endl;
    // char ch;
    // cin>>ch;

    //system(string("cp "+output_from_cachet__filename+" ./PROBLEM.cnf").c_str());

    int res = system(run.c_str());
    assert(res == 0);
    if(res != 0){
        cerr<<"UNRECOVERABLE ERROR : Invocation of Cachet failed!\n";
        exit(-1);
    }
}

bool read_learnt_clauses(ifstream& in){
    set<set<int> > new_clauses;
    set<set<int> > new_stack_clauses;

    bool parsing_stack_derived_knowledge = false;
    string tmp;

    cerr<<"Parsing Cachet output.\n";
    while(!in.eof()){
        getline(in, tmp);

        if(tmp[0] == 'c' && tmp[2] == 'I'){
            parsing_stack_derived_knowledge = true;
            continue;
        }

        if(!tmp.size())continue;

        istringstream iss(tmp);
        //cerr<<"Parsing line of learnt clauses: "<<tmp<<endl;
        

        //cerr<<"reading line : "<<tmp<<" "<<tmp.size()<<endl;
        

        int literal;
        set<int> clause;
        while(!iss.eof()){
            
            iss>>literal;
            if(0 != literal){
                //cerr<<literal<<" ";
                clause.insert(literal);

                //assert(abs(literal) < 1145);
                
            }
        }//cerr<<endl;
        
        if(clause.size()){
            //cerr<<"Learnt a new clause\n";
            if(!parsing_stack_derived_knowledge){
                new_clauses.insert(clause);
            } else {
                assert(clause.size() == 2);
                assert(-1 * (*clause.begin()) >= _timestep_1__fluents.front());
                assert(-1 * (*(++clause.begin())) <= _timestep_1__fluents.back());
                assert(-1 * (*clause.begin()) <= _timestep_1__fluents.back());
                assert(-1 * (*(++clause.begin())) >= _timestep_1__fluents.front());
                new_stack_clauses.insert(clause);
            }
        }
    }

    if(!new_clauses.size() &&  !new_stack_clauses.size()) return false;

    timestep_i__constraints.push_back(new_clauses);
    timestep_i__stack_derived_mutex.push_back(new_stack_clauses);

    return true;
}


bool read_learnt_clauses(){
    ifstream input;
    input.open(output_from_cachet__filename);
    
    if ( input.fail() ) {
        cerr<<"UNRECOVERABLE ERROR : No file from cachet found. \n"; exit(-1);
    }
    bool answer = read_learnt_clauses(input);
    
    input.close();

    return answer;
}

int iteration_count = 0;

bool pump(){
    cerr<<"We are at iteration : "<<++iteration_count<<endl;
    //char ch; cin>>ch;
    write_cnf();
    run_cachet();
    if(!read_learnt_clauses()) {
        cerr<<"Dropping out, as no clauses were learnt.\n";
        return false;
    }
    
    //cerr<<"TESTING CONVERGENCE\n";
    if(timestep_i__constraints.size() > 2){

        if((timestep_i__constraints.back() == *(--(--timestep_i__constraints.end())))){

            if((timestep_i__stack_derived_mutex.back() == *(--(--timestep_i__stack_derived_mutex.end())))){
                cerr<<"CONVERGENCE!\n";
                convergence = true;
                return false;
            }
        }
    }
    
    return true;
}

void write_all_learnt_clauses(ofstream& output){
    int timestep = 0;

    timestep_i__constraints.push_back(timeless_constraints);

    for(auto i = timestep_i__constraints.begin()
            ; i != timestep_i__constraints.end()
            ; i++){
        if(convergence && timestep_i__constraints.size() >=2 && i == --(--timestep_i__constraints.end())){
            output<<"c timestep is = "<<'n'<<endl;
        } else if(timestep_i__constraints.size() >=1 && i == --timestep_i__constraints.end() ){
            output<<"c timestep is = "<<"TIMELESS"<<endl;
        }else {
            output<<"c timestep is = "<<timestep++<<endl;
        }

        output<<*i;
    }

    timestep = 0;
    for(auto i = timestep_i__stack_derived_mutex.begin()
            ; i != timestep_i__stack_derived_mutex.end()
            ; i++){
        if(convergence && timestep_i__stack_derived_mutex.size() >=2 && i == --(--timestep_i__stack_derived_mutex.end())){
            output<<"c timestep is = "<<'n'<<endl;
        } else if(timestep_i__stack_derived_mutex.size() >=1 && i == --timestep_i__stack_derived_mutex.end() ){
            output<<"c timestep is = "<<"TIMELESS"<<endl;
        }else {
            output<<"c timestep is = "<<timestep++<<endl;
        }

        output<<*i;
    }
}

void write_all_learnt_clauses(const string& in){
    
    ofstream output;
    output.open(in);
    if ( output.fail() ) {
        cerr<<"UNRECOVERABLE ERROR : No output file. \n"; exit(-1);
    }
    
    write_all_learnt_clauses(output);

    output.close();
}

int main(int argc, char **argv)
{
    if(argc != 5){
        cerr<<"UNRECOVERABLE ERROR : Invariants learning needs an input and output file. \n";
        assert(argc == 5);
        exit(-1);
    }

    string intput_file;
    string output_file;
    
    intput_file = string(argv[1]);
    output_file = string(argv[2]);
    prefix = string(argv[3]);
    location_of_cachet = string(argv[4]);
    
    input_to_cachet__filename = prefix + "/invariants_via_cachet.cnf";// ../../../tmp_files/
    output_from_cachet__filename = prefix + "/invariants_from_cachet.cnf";// ../../../tmp_files/
    stack_derived_mutex__filename = prefix + "/stack_derived_mutex.cnf";


    parse(intput_file);

    /*First get all the timeless constraints.*/
    write_to_obtain_timeless_constraints();
    run_cachet(true /*Without limits of the learnt clauses.*/);
    read_learnt_clauses();
    if(2 == timestep_i__constraints.size()){
        cerr<<"We obtained some timeless constraints.\n";
        timeless_constraints = timestep_i__constraints.back();
        timestep_i__constraints.resize(timestep_i__constraints.size() - 1);
    }

    /*Now get all the timestep constraints.*/
    while(pump()){}

    write_all_learnt_clauses(output_file);

    return 0;
}
