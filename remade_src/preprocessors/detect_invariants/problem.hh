
#ifndef PROBLEM_HH
#define PROBLEM_HH

#include <string>
#include <set>
#include <map>
#include <vector>
#include <sstream>

using namespace std;

class Action{
public:
    Action():id(0){}
    Action(int id):id(id){}

    int id;

    bool is_effect(int in);
    void prevails();

    set<int> precondition;
    set<int> basic_effects;
    vector<Action> other_effects;
};

class Planning_Problem {
public:
    map<int, Action> actions;
    map<int, Action> conditional_effects;

    set<int> fluents;
    bool is_fluent(int i) const;
};

#endif
