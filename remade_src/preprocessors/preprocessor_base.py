""" File:        encodings/encoding_base.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2013-
    Desctiption: 

    Liscence:   [Refine this!]
"""


import abc, os
from utilities import CodeException

class PreprocessingException(CodeException):
    """ An exception to be raised in the even that something goes wrong with
        the preprocessing process. """

        
class Preprocessor(object):
    __metaclass__ = abc.ABCMeta
    
    def __init__(self, problem, quiet):
        """ Set preprocessor parameters
            
            (Preprocessor, Problem, bool) -> None
        """
        self.problem = problem
        self.quiet = quiet

    @abc.abstractmethod
    def run(self):
        """ Run the preprocessor and modify the problem in the required way.
            Return iff the problem might still be satisfiable.
        
           (Preprocessor) -> bool
        """
 
