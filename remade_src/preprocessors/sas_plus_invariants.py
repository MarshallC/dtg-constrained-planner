""" File:        preprocessors/sas_plus_invariants.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2013-
    Desctiption: 

    Liscence:   [Refine this!]
"""

import os
from utilities import preprocessing_error_code, neg_prec_prefix

from preprocessor_base import Preprocessor, PreprocessingException

from problem   import Object, Type, Function, Predicate,\
                      PredicateCondition, NotCondition, AndCondition,\
                      OrCondition, ForAllCondition, ExistsCondition,\
                      IncreaseCondition, EqualsCondition, ConditionalEffect

from subprocess import Popen, PIPE, STDOUT
   
import itertools
   
class SASPlusPreprocessor(Preprocessor):

    def run(self):
        """ Run the preprocessor and modify the problem in the required way.
        
           (SASPlusPreprocessor) -> None
        """
        
        print "#################### RUNNING SAS"
        
        
        pred = self.problem.predicates["on"]
        grounding = ('a', 'a')
        
        print "Pre:"
        for action, grounding in pred.ground_precs[grounding]:
            print action.name, grounding
        print "NPre:"
        for action, grounding in pred.ground_nprecs[grounding]:
            print action.name, grounding
        
        print "Add:"
        for action, grounding in pred.ground_adds[grounding]:
            print action.name, grounding
            
            for prec in action.flat_ground_preconditions[grounding]:
                print "    ", prec[0].pred.name, prec[1]
            
            
        print "Del:"
        for action, grounding in pred.ground_dels[grounding]:
            print action.name, grounding
        
        
        print """ Step 1: do a first step which is like the last step of the plangraph
          for all pairs of fluents -- are all ways of achieving them mutex.
          
          Use this to prune bad groundings from my actions.
          
          Now look at malte's procedure - 
        
        
        """
        
        
        #pred.ground_nprecs = {}
        #pred.ground_adds = {}
        #pred.ground_dels = {}
        
        """
        state = set(self.problem.initial_state)
        next_state = set(state)
        remaining_actions = set()
        for action in self.problem.actions:
            for grounding in action.groundings:
                remaining_actions.add((action, grounding))
        actions = state()
        
        state_mutexes = set()
        
        conflicts = set()
        mutexes
        
        for action1 in self.problem.actions:
            for grounding1 in action1.groundings:
                for prec in action1.flat_ground_preconditions[grounding1]:
                    if prec[2]: e_list = prec[0].pred.ground_adds[prec[0].ground_conditions[prec[1]]]
                    else: e_list = prec[0].pred.ground_dels[prec[0].ground_conditions[prec[1]]]
                    for ag_pair2 in e_list:
                        if a_code1 != a_code2:
                            if (a_code1, a_code2) not in m_to_not_encode:
                                mutexes[a_code1].add(a_code2)
                                mutexes[a_code2].add(a_code1)
                                m_to_encode.add((a_code1, a_code2))
                            else: m_avoided += 1
        
        
        """
                
        
        
        #self.invariants.append((bounds, [int(lit) for lit in tokens if lit != "0"]))

        #Action, derived predicate, and conditional effect Pre- and post-conditions
        #for action in itertools.chain(self.problem.actions.itervalues(),
        #        self.problem.conditional_effects): #self.problem.derived_predicates.itervalues(), 
                
      
      
        #for ss_fact in self.problem.initial_state]
           
        assert False    
        
        return True 


        

       
