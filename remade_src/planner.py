""" File:        planner.py
    Author:      Nathan Robinson
    Contact:     nathan.m.robinson@gmail.com
    Date:        2013-
    Desctiption: 

    Liscence:   [Refine this!]
"""

import sys, os, time, subprocess, errno

from utilities import CodeException, remove, PRE_SUFFIX, GROUND_SUFFIX,\
    solving_error_code, extracting_error_code
from cmd_line import process_args
from problem import ProblemException
from parser import ParsingException, Parser, Grounder
from preprocessors import PreprocessingException, SASPlusPreprocessor,\
    CachetPreprocessor, PlangraphPreprocessor
    
from cnf_encodings import EncodingException, EncodingWrapper
from solvers import SolvingException, SolverWrapper

class PlanningException(CodeException):
    def __init__(self, message, code):
        self.message = message
        self.code = code

def write_error_sln(output_file_name, exp_name, code):
    """ Write the given experiment name and error code to the error file
        
        (str, str, int) -> None
    """
    try:
        with file(output_file_name, 'w') as sln_file:
            print >> sln_file, exp_name
            print >> sln_file, 'NSAT'
            print >> sln_file, code
    except IOError:
        print "Error opening sln file:", out_name



def main():
    start_time = time.time()
    
    #Process the possible encodings, and solvers so we can provide useful
    #cmd line advice
    try:
        encoding_wrapper = EncodingWrapper()
        encoding_wrapper.read_encoding_list()
        
        solver_wrapper = SolverWrapper()
        solver_wrapper.read_solver_list()
    
    except (EncodingException, SolvingException) as e:
        print e.message
        sys.exit(1)
    
    #Process the cmd line args
    args = process_args(\
        encoding_wrapper.valid_encodings, encoding_wrapper.default_encoding,
        solver_wrapper.valid_solvers, solver_wrapper.default_solver)
    
    arg_processing_time = time.time()
    
    #Report on system start up
    if not args.quiet:
        print "Encodings registered:    ", len(encoding_wrapper.valid_encodings)
        print "Solvers registered:      ", len(solver_wrapper.valid_solvers)
        print "Cmd line arg processing time:", (arg_processing_time - start_time)
    
    #Ensure that the tmp_dir exists
    try:
        os.makedirs(args.tmp_path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            print "Error: could not create temporary directory:", args.tmp_path
            sys.exit(1)
    
    #Parse the input PDDL
    try:
        parser = Parser(args.domain_file_name, args.problem_file_name, args.quiet)
        if not args.quiet: print "Parsing the PDDL domain"
        parser.parse_domain()
        if not args.quiet: print "Parsing the PDDL problem"
        parser.parse_problem()
        
        if not args.quiet: print "Simplifying the problem representation"
        problem = parser.problem
        problem.simplify()
        problem.assign_cond_codes()

        end_parsing_time = time.time()
        if not args.quiet: print "Parsing time:", (end_parsing_time - arg_processing_time)

        if not args.quiet: print "Grounding the problem"
        grounder = Grounder(problem, os.path.join(args.tmp_path, args.exp_name + PRE_SUFFIX),
            os.path.join(args.tmp_path, args.exp_name + GROUND_SUFFIX), args.quiet)
        grounder.ground()
        
        end_grounding_time = time.time()
        if not args.quiet: print "Grounding time:", (end_grounding_time - end_parsing_time)
        
        if not args.quiet: print "Simplifying the ground encoding..."
        problem.compute_static_preds()
        problem.link_groundings()
        problem.make_flat_preconditions()
        problem.make_flat_effects()
        problem.get_encode_conds()
        problem.make_cond_and_cond_eff_lists()
        problem.link_conditions_to_actions()
        problem.make_strips_conditions()
        
        problem.prune_useless_strips_actions()

        problem.compute_conflict_mutex()
        
        end_linking_time = time.time()
        if not args.quiet: print "Simplify time:", (end_linking_time - end_grounding_time)
        
        object_invariants = []
        if args.plangraph_invariants:
            if not args.quiet: print "Generating Plangraph invariants..."
            plangraph_preprocessor = PlangraphPreprocessor(problem, args.quiet, args.plangraph_timeless_only)
            object_invariants = plangraph_preprocessor.run()
            if object_invariants == False:
                raise PreprocessingError()
            
        end_plangraph_time = time.time()
        if args.plangraph_invariants and not args.quiet:
            print "Plangraph invariants time:", (end_plangraph_time - end_linking_time)
        
        if args.sas_invariants:
            if not args.quiet: print "Generating SAS+ invariants..."
            sas_invariants = SASPlusPreprocessor(problem, args.quiet)
            sas_invariants.run()
        
        end_sas_time = time.time()    
        if args.sas_invariants and not args.quiet:
            print "SAS+ invariants time:", (end_sas_time - end_plangraph_time)
        
        if not args.quiet:
            print "Generating base encoding:", args.encoding, "..."

        #Make doing this conditional on actually requiring it for preprocessing
        #Or an encoding
        problem.precisely_split()

        encoding_wrapper.instantiate_encoding(args.encoding, args.encoding_args,
            problem, args.quiet)
        encoding = encoding_wrapper.encoding
        encoding.horizon = args.horizon
        encoding.metalayer_copy_layer = args.copy_layer
        encoding.cg_cut_above = args.cut_above
        encoding.encode_base()
        
        #Consider moving this stuff after the cachet invariants code
        encoding.add_object_invariants(object_invariants)
        
        end_encoding_base_time = time.time()
        if not args.quiet: print "Encoding generation time:",\
            (end_encoding_base_time - end_sas_time)

        if args.cachet_invariants:
            cachet_invariants = CachetPreprocessor(problem, encoding.clauses,
                encoding.variables, encoding.vars_per_layer, args.tmp_path,
                args.exp_name, args.cachet_unary_only, args.quiet)
            cachet_invariants.run()
            encoding.add_invariants(cachet_invariants.invariants)
        
        end_cachet_time = time.time()
        
        if args.cachet_invariants and not args.quiet:
            print "Cachet invariants time:", (end_cachet_time - end_encoding_base_time)

        
    except (ParsingException, PreprocessingException, ProblemException) as e:
        print e
        if args.soln_file != None:
            write_error_sln(args.soln_file, args.exp_name, e.code)
        sys.exit(1)
    
    if not args.quiet: print "Planning..."
    try:
        #We have something called query strategy in cmd line args
        
        cnf_file_name = os.path.join(args.tmp_path,
            args.exp_name + "_" + str(args.horizon) + ".cnf")

        variable_order_file_name = os.path.join(args.tmp_path,
            args.exp_name + "_" + str(args.horizon) + ".ordering")

        if not args.quiet: print "Writing CNF file..."
        encoding.compute_cnf_codes(args.horizon)
        encoding.write_cnf(cnf_file_name)
        encoding.write_ordering(variable_order_file_name)
        end_writing_cnf_time = time.time()
        if not args.quiet:
            print "Writing time:", (end_writing_cnf_time - end_cachet_time)
            
            print "Clause statistics:"
            for ctype, count in encoding.get_clause_types().iteritems():
                print ctype, ":", count

        if args.debug_cnf:
            if not args.quiet: print "Writing debug CNF..."
            encoding.write_debug_cnf(cnf_file_name + "_dbg", args.horizon)
        end_writing_dbg_cnf_time = time.time()
        if not args.quiet and args.debug_cnf:
            print "Writing time:", (end_writing_dbg_cnf_time - end_writing_cnf_time)
        
        if args.var_file:
            if not args.quiet: print "Writing var file..."
            var_file_name = os.path.join(args.tmp_path,
                args.exp_name + "_" + str(args.horizon) + ".var")
            encoding.write_variable_file(var_file_name)
        
        end_writing_all_cnf_time = time.time()
        if not args.quiet and args.var_file:
            print "Writing time:", (end_writing_all_cnf_time - end_writing_dbg_cnf_time)
        
        try:
            if not args.quiet: print "Solving..."

            solver_wrapper.instantiate_solver(args.solver, cnf_file_name,\
                args.tmp_path, args.exp_name, args.time_out, args.solver_args)
            
            (sln_res, sln_time, true_vars) = solver_wrapper.solver.solve()
            
            if not args.quiet:
                print "SAT" if sln_res else "UNSAT"
                print "Solution time:", sln_time
            
        except SolvingException as e:
            raise PlanningException(e.message, solving_error_code)
        
        if sln_res:
            encoding.set_true_variables(true_vars)
            encoding.build_plan()

            #encoding.print_true_variables()
            
            if not args.quiet:
                print "Plan:"
                for step, s_actions in enumerate(encoding.plan):
                    #mlayer = encoding.plan_metalayer[step]
                    
                    for (action, a_args) in s_actions:
                        a_str = action.name
                        if a_args:
                            a_str += " " + " ".join(a_args)
                        #print str(mlayer) + " - " + str(step) + ": " + a_str
                        print str(step) + ": " + a_str
            
            
            
            if not args.quiet:
                print "Simulating plan for validation."
            
            sim_res, plan_cost = problem.simulate_plan(encoding.plan)
            
            if sim_res: print "Plan valid. Cost:", plan_cost
            else: print "Error: INVALID PLAN!"
            
        end_time = time.time()
        if not args.quiet:
            print "Total time:", end_time - start_time

    except PlanningException as e:
       print "Planning Error: ", e.message
       sys.exit(1)

    sys.exit(0)

if __name__ == '__main__':
    main()

