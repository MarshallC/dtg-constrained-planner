CXX = 'g++'
CXXFLAGS = ['-std=c++0x', '-O3', '-Wall']
CPPPATH = []
CPPDEFINES = {'NDEBUG': 1}
LIBS = []
LIBPATH = []
LINKFLAGS = ['-std=c++0x', '-O3']
AR = 'ar'
ARFLAGS = ['rc']
RANLIB = 'ranlib'
BISON = 'bison'
RE2C = 're2c'
WITH_PYTHON = None
WITH_LUA = None
WITH_TBB = None
WITH_CPPUNIT = None
