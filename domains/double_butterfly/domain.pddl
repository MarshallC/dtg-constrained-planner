;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Simple Example
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain path-domain)
  (:predicates
   (custom_goal)
   (at_D)
   (at_A)           
   (at_B)
   (at_C)
   (at_G)           
   (at_E)
   (at_F)
   (visited_C)
   (visited_B)
   (visited_F)
   (visited_E)
   )
  
  (:action head-D-A
           :parameters ()
           :precondition (at_D)
           :effect
           (and (not (at_D))
                (at_A)))
  
  (:action head-A-B
           :parameters ()
           :precondition (at_A)
           :effect
           (and (not (at_A))
                (at_B)
                (visited_B)))
  
  (:action head-A-C
           :parameters ()
           :precondition (at_A)
           :effect
           (and (not (at_A))
                (at_C)
                (visited_C)))
  
  (:action head-C-D
           :parameters ()
           :precondition (at_C)
           :effect
           (and (not (at_C))
                (at_D)))
  
  (:action head-B-D
           :parameters ()
           :precondition (at_B)
           :effect
           (and (not (at_B))
                (at_D)))
  
  (:action head-D-G
           :parameters ()
           :precondition (at_D)
           :effect
           (and (not (at_D))
                (at_G)))
  
  (:action head-G-E
           :parameters ()
           :precondition (at_G)
           :effect
           (and (not (at_G))
                (at_E)
                (visited_E)))
  
  (:action head-G-F
           :parameters ()
           :precondition (at_G)
           :effect
           (and (not (at_G))
                (at_F)
                (visited_F)))
  
  (:action head-F-D
           :parameters ()
           :precondition (at_F)
           :effect
           (and (not (at_F))
                (at_D)))  
  
  (:action head-E-D
           :parameters ()
           :precondition (at_E)
           :effect
           (and (not (at_E))
                (at_D))))

)
