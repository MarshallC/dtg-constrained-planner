;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Simple Example
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain path-domain)
  (:requirements :strips)
  (:predicates (at_N)
	       (at_S)
	       (at_E)
	       (at_W)
	       (visited_W)
	       (visited_E)
	       )

  (:action head-S-N
	     :parameters ()
	     :precondition (at_S)
	     :effect
	     (and (not (at_S))
		  (at_N)))

  (:action head-N-E
	     :parameters ()
	     :precondition (at_N)
	     :effect
	     (and (not (at_N))
		  (at_E)
                  (visited_E)))

  (:action head-N-W
	     :parameters ()
	     :precondition (at_N)
	     :effect
	     (and (not (at_N))
		  (at_W)
                  (visited_W)))

  (:action head-W-S
	     :parameters ()
	     :precondition (at_W)
	     :effect
	     (and (not (at_W))
		  (at_S)))

  (:action head-E-S
	     :parameters ()
	     :precondition (at_E)
	     :effect
	     (and (not (at_E))
		  (at_S)))
  )
