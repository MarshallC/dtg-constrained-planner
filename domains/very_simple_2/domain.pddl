;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Very Simple Example
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain very_simple)
  (:requirements :strips)
  (:predicates (PRESSED ?x) (UNPRESSED ?x))
	       
  (:action press
	     :parameters (?x)
	     :precondition (and (UNPRESSED ?x))
	     :effect (and (PRESSED ?x) (not (UNPRESSED ?x)))))