;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Simple Example
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain path-domain)
  ;(:requffffirements :strfffffips)
  (:predicates
   (custom_goal)
   (at_S)
   (at_N1)	       
   (at_E1)
   (at_W1)
   (at_N2)	       
   (at_E2)
   (at_W2)
   (visited_W1)
   (visited_E1)
   (visited_W2)
   (visited_E2)
   )
  
  (:action head-S-N1
	   :parameters ()
	   :precondition (at_S)
	   :effect
	   (and (not (at_S))
		(at_N1)))
  
  (:action head-N1-E1
	   :parameters ()
	   :precondition (at_N1)
	   :effect
	   (and (not (at_N1))
		(at_E1)
		(visited_E1)))
  
  (:action head-N1-W1
	   :parameters ()
	   :precondition (at_N1)
	   :effect
	   (and (not (at_N1))
		(at_W1)
		(visited_W1)))
  
  (:action head-W1-S
	   :parameters ()
	   :precondition (at_W1)
	   :effect
	   (and (not (at_W1))
		(at_S)))
  
  (:action head-E1-S
	   :parameters ()
	   :precondition (at_E1)
	   :effect
	   (and (not (at_E1))
		(at_S)))
  
  (:action head-S-N2
	   :parameters ()
	   :precondition (at_S)
	   :effect
	   (and (not (at_S))
		(at_N2)))
  
  (:action head-N2-E2
	   :parameters ()
	   :precondition (at_N2)
	   :effect
	   (and (not (at_N2))
		(at_E2)
		(visited_E2)))
  
  (:action head-N2-W2
	   :parameters ()
	   :precondition (at_N2)
	   :effect
	   (and (not (at_N2))
		(at_W2)
		(visited_W2)))
  
  (:action head-W2-S
	   :parameters ()
	   :precondition (at_W2)
	   :effect
	   (and (not (at_W2))
		(at_S)))  
  
  (:action head-E2-S
	   :parameters ()
	   :precondition (at_E2)
	   :effect
	   (and (not (at_E2))
		(at_S))))

  (:action consolidate1
	   :parameters ()
	   :precondition (and (visited_W1) (visited_E1))
	   :effect
	   (and (custom_goal)))

  (:action consolidate2
	   :parameters ()
	   :precondition (and (visited_W2) (visited_E2))
 	   :effect
	   (and (custom_goal)))
  )
