(define (domain domain-1-2_1-0)
  (:predicates
  (at_Base)
  (at_End)
  (at_In_Node_Branch-0_Step-0)
  (visited_In_Node_Branch-0_Step-0)
  )

  (:action head-Base-End
           :parameters ()
           :precondition (at_Base)
           :effect
           (and (not (at_Base))
                (at_End)))

  (:action head-End-In_Node_Branch-0_Step-0
           :parameters ()
           :precondition (at_End)
           :effect
           (and (not (at_End))
                (at_In_Node_Branch-0_Step-0)
                (visited_In_Node_Branch-0_Step-0)))

  (:action head-In_Node_Branch-0_Step-0-Base
           :parameters ()
           :precondition (at_In_Node_Branch-0_Step-0)
           :effect
           (and (not (at_In_Node_Branch-0_Step-0))
                (at_Base)))
)
