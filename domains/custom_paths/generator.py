in_counts = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
out_counts = [1,2,3,4,5]
in_branch_steps = 1 # has to be at least 1 to have things to reach  (in is like the out paths)
out_branch_steps = 0

class Node():
    def __init__(self, name):
        self.name = name
        self.children = set()
        self.visit_necessary = None # Needs to be set later

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, comparison):
        return self.name == comparison.name

    def __lt__(self, comparison):
        return sorted([self.name, comparison.name])[0] == self.name
    
    def __str__(self):
        return self.name

    def __dump__(self):
        return self.name

def get_action(action_pair):
    a = action_pair[0]
    b = action_pair[1]
        
    action_text = ""
    action_text += "  (:action head-" + a.name + "-" + b.name + "\n"
    action_text += "           :parameters ()\n"
    action_text += "           :precondition (at_" + a.name + ")\n"
    action_text += "           :effect\n"
    action_text += "           (and (not (at_" + a.name + "))\n"
    action_text += "                (at_" + b.name + ")"

    assert isinstance(b.visit_necessary, bool)
    
    if b.visit_necessary:
        action_text += "\n                (visited_" + b.name + ")"
    action_text += "))\n"

    return action_text

files = []
for in_count in in_counts:
    for out_count in out_counts:
        # A problem instance
        problem_name = "problem-" + str(in_count) + '-' + str(out_count) + '_' + str(in_branch_steps) + '-' + str(out_branch_steps)
        domain_name = "domain-" + str(in_count) + '-' + str(out_count) + '_' + str(in_branch_steps) + '-' + str(out_branch_steps)

        files.append((domain_name + ".pddl", problem_name + ".pddl"))
        
        with open(domain_name + ".pddl", "w") as domain_file:
            with open(problem_name + ".pddl", "w") as problem_file:
                # Create all nodes and link them
                all_nodes = set()
                
                # set root ('S') and end ('N') (may be same if branch length == 0:
                base = Node("Base")
                base.visit_necessary = False
                if in_branch_steps == -1:
                    end = base
                else:
                    end = Node("End")
                    end.visit_necessary = False
                    
                all_nodes.add(base)
                all_nodes.add(end)
                    
                for out_branch_no in range(out_count):
                    parent_node = base
                    for out_branch_step in range(out_branch_steps+1):
                        if out_branch_step == out_branch_steps: # Final, link to "End"
                            child_node = end
                        else:
                            child_node = Node("Out_Node_Branch-"+str(out_branch_no) + "_Step-" + str(out_branch_step))
                            child_node.visit_necessary = False
                        parent_node.children.add(child_node)
                        all_nodes.add(child_node)
                        parent_node = child_node

                for in_branch_no in range(in_count):
                    parent_node = end
                    for in_branch_step in range(in_branch_steps+1):
                        if in_branch_step == in_branch_steps: # Final, link to "End"
                            child_node = base
                        else:
                            child_node = Node("In_Node_Branch-"+str(in_branch_no) + "_Step-" + str(in_branch_step))
                            child_node.visit_necessary = True
                        parent_node.children.add(child_node)
                        all_nodes.add(child_node)
                        parent_node = child_node

                all_nodes_sorted = sorted(all_nodes)
                all_necessary_nodes_sorted = sorted([x for x in all_nodes if x.visit_necessary])
                # Find action crossings       
                action_pairs = []
                for node in all_nodes_sorted:
                    action_pairs.extend([(node, node_child) for node_child in node.children])

                # Write precicates
                domain_file.write("(define (domain " + domain_name + ")\n")
                domain_file.write("  (:predicates\n")
                for node in all_nodes_sorted:
                    domain_file.write("  (at_" + node.name +  ")\n")
                for node in [x for x in all_nodes_sorted if x.visit_necessary]:
                    domain_file.write("  (visited_" + node.name +  ")\n")
                domain_file.write("  )\n")

                # Write actions
                for action_pair in action_pairs:
                    domain_file.write("\n")
                    domain_file.write(get_action(action_pair))

                domain_file.write(")\n")

                # Write problem file
                problem_file.write("(define (problem " + problem_name + ")\n")
                problem_file.write("(:domain " + domain_name + ")\n")
                problem_file.write("(:objects)\n")
                problem_file.write("(:INIT (at_" + base.name + "))\n")
                problem_file.write("(:goal (and")
                for necessary_node in all_necessary_nodes_sorted:
                    problem_file.write(" (visited_" + necessary_node.name + ")")
                problem_file.write("))\n")
                problem_file.write(")\n")

for domain, problem in files:
    print("python3 ./src/wrapper.py --domain ./domains/custom_paths/" + domain + " --problem ./marshall_tests/custom_paths/" + problem + " --solver lingelingDrupTrim --encoding limited --DTG_increment_parallel True --DTG_simulate_only_fastest_path False --DTG_increment_horizon_0 False > tmp_files/wrapper_logs/" + problem + ".all.limited.log  2>  tmp_files/wrapper_logs/" + problem + ".all.limited.e")
    
    print("python3 ./src/wrapper.py --domain ./domains/custom_paths/" + domain + " --problem ./marshall_tests/custom_paths/" + problem + " --solver lingelingDrupTrim --encoding limited --DTG_increment_parallel True --DTG_simulate_only_fastest_path True --DTG_increment_horizon_0 False > tmp_files/wrapper_logs/" + problem + ".some.limited.log  2>  tmp_files/wrapper_logs/" + problem + ".some.limited.e")
    
    print("python3 ./src/wrapper.py --domain ./domains/custom_paths/" + domain + " --problem ./marshall_tests/custom_paths/" + problem + " --solver lingelingDrupTrim --encoding flat --DTG_increment_parallel True --DTG_simulate_only_fastest_path False --DTG_increment_horizon_0 False > tmp_files/wrapper_logs/" + problem + ".flat.log  2>  tmp_files/wrapper_logs/" + problem + ".flat.e")
