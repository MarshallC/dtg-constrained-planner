(define (domain path-domain)
  (:requirements :strips)
  (:predicates (pressed_1)
;	       (pressed_2)
	       (custom_goal)
	       (TRUE_CONSTANT)
	       )

  (:action press-1
	     :parameters ()
	     :precondition (TRUE_CONSTANT)
	     :effect
	     (and (pressed_1)))

;  (:action press-2
;	     :parameters ()
;	     :precondition (TRUE_CONSTANT)
;	     :effect
;	     (and (pressed_2)))
    
  (:action consolidate_goal
	     :parameters ()
	     :precondition (and (pressed_1)); (pressed_2))
	     :effect
	     (and (custom_goal)))
)
